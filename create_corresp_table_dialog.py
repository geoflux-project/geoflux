        #!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from qgis.gui import QgsMapToolEmitPoint
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface



# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from create_corresp_table_dialog import Ui_Dialog

class create_corresp_table_dialog(QDialog): 
    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self) 
        self.caller = caller
        self.iface = self.caller.iface
        self.plugin_dir = self.caller.plugin_dir
        
        self.ui.comboBox_zonage.setModel(self.caller.modelZonage)
        self.ui.comboBox_work_schema.setModel(self.caller.modelSchemas)
        
        self.ODModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_od.setModel(self.ODModel)
        
        self.tableModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_corresp_table.setModel(self.tableModel)
        self.pointsEnqModel = QtSql.QSqlQueryModel()
        self.ui.tableView_points_enq.setModel(self.pointsEnqModel)
        
        self.clickTool = QgsMapToolEmitPoint(self.iface.mapCanvas()) # Outil permettant l'émission d'un QgsPoint à chaque clic sur le canevas 
        
        # Connexion des signaux et des slots
        self._connectSlots()

    def _connectSlots(self):
        self.clickTool.canvasClicked.connect(self._slotClickPoint)
        self.ui.comboBox_zonage.currentIndexChanged.connect(self._slot_od_changed)
        self.ui.comboBox_work_schema.currentIndexChanged.connect(self._slot_comboBox_work_schema_current_index_changed)
        self.ui.comboBox_corresp_table.currentIndexChanged.connect(self._slot_comboBox_corresp_table_current_index_changed)
        self.ui.comboBox_od.currentIndexChanged.connect(self._slot_comboBox_od_current_index_changed)
        self.ui.comboBox_point_enq.currentIndexChanged.connect(self._slot_comboBox_point_enq_current_index_changed)
        self.ui.pushButton_create_corresp_table.clicked.connect(self._slot_pushButton_create_corresp_table_clicked)
        self.ui.pushButton_delete.clicked.connect(self._slot_pushButton_delete_clicked)
        self.ui.pushButton_add.clicked.connect(self._slot_pushButton_add_clicked)
        self.ui.pushButton_choose_point_enq.clicked.connect(self._slot_pushButton_choose_point_enq_clicked)
        self.ui.radioButton_fictif.toggled.connect(self._slot_type_point_enq_changed)
        self.ui.radioButton_terrain.toggled.connect(self._slot_type_point_enq_changed)
        self.ui.radioButton_see_all.clicked.connect(self._slot_od_changed)
        self.ui.radioButton_see_represented.clicked.connect(self._slot_od_changed)
        self.ui.radioButton_see_missing.clicked.connect(self._slot_od_changed)
        self.ui.buttonBox.button(QDialogButtonBox.Close).clicked.connect(self._slot_close)
        self.ui.tableView_points_enq.selectionModel().selectionChanged.connect(self._slot_points_enq_selection_changed)
    
    def _slot_comboBox_work_schema_current_index_changed(self, indexChosenLine):
        s = "SELECT table_name FROM information_schema.tables WHERE table_schema = '"+self.ui.comboBox_work_schema.currentText()+"' ORDER BY 1"
        self.tableModel.setQuery(s, self.caller.db)
        
    def _slot_pushButton_create_corresp_table_clicked(self):
        s = "SELECT table_name FROM information_schema.tables WHERE table_schema = '"+self.ui.comboBox_work_schema.currentText()+"' AND table_name = '"+self.ui.comboBox_corresp_table.currentText()+"'"
        q = QtSql.QSqlQuery(self.caller.db)
        q.exec_(unicode(s))
        create = False
        if q.size()>0:
            self.hide()
            box = QMessageBox()
            box.setModal(True)
            box.setText(u"Une table portant ce nom existe déjà dans le schéma. Si vous continuez, elle sera écrasée.")
            box.setInformativeText("Confirmez-vous la création de table ?")
            box.setStandardButtons(QMessageBox.Cancel | QMessageBox.Ok)
            box.setDefaultButton(QMessageBox.Ok)
            box.exec_()            
            if (box.clickedButton() == box.defaultButton()):
                create = True
                self.show()
            else:
                self.show()
        else:
            create = True
        if (create):
            table_name = self.ui.comboBox_corresp_table.currentText()
            s = "DROP TABLE IF EXISTS "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+"; \
                 CREATE TABLE "+self.ui.comboBox_work_schema.currentText()+"."+table_name+" ( \
                        id serial PRIMARY KEY,\
                        code_zone_orig character varying,\
                        code_zone_dest character varying,\
                        coef double precision,\
                        id_point_enq_terrain integer,\
                        id_point_enq_fictif integer\
                 );"
            q = QtSql.QSqlQuery(self.caller.db)
            q.exec_(unicode(s))
            
            box = QMessageBox()
            box.setModal(True)
            box.setText(u"La table "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+" a été créée.")
            self._slot_comboBox_work_schema_current_index_changed(self.ui.comboBox_work_schema.currentIndex())
            self.ui.comboBox_corresp_table.setCurrentIndex(self.tableModel.match(self.tableModel.index(0,0),0,table_name, 1)[0].row())
    
    def _slot_comboBox_corresp_table_current_index_changed(self, indexChosenLine):
        s = "SELECT column_name FROM information_schema.columns WHERE table_name = '"+self.ui.comboBox_corresp_table.currentText()+"' AND table_schema = '"+self.ui.comboBox_work_schema.currentText()+"' AND column_name = ANY(ARRAY['code_zone_orig', 'code_zone_dest', 'coef', 'id_point_enq_fictif', 'id_point_enq_terrain']) ORDER BY 1"
        q = QtSql.QSqlQuery(self.caller.db)
        q.exec_(unicode(s))
        if (q.size()<5): 
            self.ui.groupBox.setEnabled(False)
        else:
            self.ui.groupBox.setEnabled(True)
        self._slot_od_changed()
        self._slot_type_point_enq_changed()
        self.ui.comboBox_od.setCurrentIndex(0)
        
        layers = QgsProject.instance().mapLayersByName("Correspondances")
        for layer in layers:
            QgsProject.instance().removeMapLayer(layer.id())
        
        self.corresp_table_layer = add_layer(self.caller.uri, self.iface, u"Correspondances", self.ui.comboBox_work_schema.currentText(), self.ui.comboBox_corresp_table.currentText(), None, "", "id", self.caller.create_corresp_table_node, 3, False, True)
        self.corresp_table_layer.loadNamedStyle(self.caller.styles_dir + '/corresp_table.qml')
        field_to_value_relation(self.corresp_table_layer, "id_point_enq_terrain", self.caller.point_enq_terrain_layer, "id", "id")
        field_to_value_relation(self.corresp_table_layer, "id_point_enq_fictif", self.caller.point_enq_fictif_layer, "id", "id")
         
    def _slot_od_changed(self):
        if (self.ui.radioButton_see_all.isChecked()):
            s = "SELECT zone_orig.code_zone || ' - ' || zone_dest.code_zone as od, zone_orig.code_zone as code_zone_orig, zone_dest.code_zone as code_zone_dest \
                 FROM "+self.caller.matrix_schema+".zone zone_orig CROSS JOIN "+self.caller.matrix_schema+".zone zone_dest \
                 WHERE zone_orig.id_zonage = "+str(self.caller.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))+" \
                  AND zone_dest.id_zonage = "+str(self.caller.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))+" \
                  AND zone_orig.code_zone != zone_dest.code_zone \
                 ORDER BY 2, 3"
        elif (self.ui.radioButton_see_represented.isChecked()):    
            s = "SELECT DISTINCT code_zone_orig || ' - ' || code_zone_dest as od, code_zone_orig, code_zone_dest \
                 FROM "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+" \
                 ORDER BY 2, 3"
        elif (self.ui.radioButton_see_missing.isChecked()):
            s = "(\
                     SELECT zone_orig.code_zone || ' - ' || zone_dest.code_zone as od, zone_orig.code_zone as code_zone_orig, zone_dest.code_zone as code_zone_dest \
                     FROM "+self.caller.matrix_schema+".zone zone_orig CROSS JOIN "+self.caller.matrix_schema+".zone zone_dest \
                     WHERE zone_orig.id_zonage = "+str(self.caller.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))+" \
                     AND zone_dest.id_zonage = "+str(self.caller.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))+" \
                     AND zone_orig.code_zone != zone_dest.code_zone \
                 ) \
                 EXCEPT \
                 ( \
                     SELECT DISTINCT code_zone_orig || ' - ' || code_zone_dest as od, code_zone_orig, code_zone_dest \
                     FROM "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+" \
                 )\
                 ORDER BY 2, 3" 
        self.ODModel.setQuery(s, self.caller.db)
    
    def _slot_comboBox_od_current_index_changed(self, indexChosenLine):
        self.caller.point_enq_terrain_layer.removeSelection()
        self.caller.point_enq_fictif_layer.removeSelection()
        if (indexChosenLine>=0):
            s = "SELECT coef, id_point_enq_terrain, id_point_enq_fictif FROM "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+" WHERE code_zone_orig = '"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_orig")+"' AND code_zone_dest = '"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_dest")+"'"
            self.pointsEnqModel.setQuery(s, self.caller.db)
            self.pointsEnqModel.setHeaderData(0, Qt.Horizontal, "Coef")
            self.pointsEnqModel.setHeaderData(1, Qt.Horizontal, "ID point terrain")
            self.pointsEnqModel.setHeaderData(2, Qt.Horizontal, "ID point fictif")
            self.ui.tableView_points_enq.verticalHeader().setVisible(False)
            self.ui.tableView_points_enq.verticalHeader().setSectionResizeMode(QHeaderView.Fixed)
            self.ui.tableView_points_enq.verticalHeader().setDefaultSectionSize(20)
            self.ui.tableView_points_enq.setColumnWidth(0,50)
            self.ui.tableView_points_enq.setColumnWidth(1,90)
            self.ui.tableView_points_enq.setColumnWidth(2,90)
            
            # Each update of the model must be accompanied by a new connexion of signal and slot on the listView selection
            self.ui.tableView_points_enq.selectionModel().selectionChanged.connect(self._slot_points_enq_selection_changed)
            self.ui.tableView_points_enq.selectAll()
            
            self.caller.zone_dest_layer.setSubsetString("id_zonage = "+str(self.caller.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))+" AND code_zone = '"+str(self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_dest"))+"'")
            self.caller.zone_orig_layer.setSubsetString("id_zonage = "+str(self.caller.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))+" AND code_zone = '"+str(self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_orig"))+"'")
        else:
            self.caller.zone_orig_layer.setSubsetString("1=2")
            self.caller.zone_dest_layer.setSubsetString("1=2") 
    
    def _slot_pushButton_delete_clicked(self):
        for item in self.ui.tableView_points_enq.selectionModel().selectedRows():
            s_fictif = " = "+str(self.pointsEnqModel.record(item.row()).value("id_point_enq_fictif")) 
            s_terrain = " = "+str(self.pointsEnqModel.record(item.row()).value("id_point_enq_terrain")) 
            if (str(self.pointsEnqModel.record(item.row()).value("id_point_enq_terrain"))  == "NULL"):
                s_terrain = " IS NULL"
            if (str(self.pointsEnqModel.record(item.row()).value("id_point_enq_fictif")) == "NULL"):
                s_fictif = " IS NULL"
            s = "DELETE FROM "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+" \
                 WHERE code_zone_orig = '"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_orig")+"' AND \
                       code_zone_dest = '"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_dest")+"' AND \
                       id_point_enq_terrain "+s_terrain+" AND \
                       id_point_enq_fictif "+s_fictif
            print(s)
            q = QtSql.QSqlQuery(self.caller.db)
            q.exec_(unicode(s))
        self._slot_comboBox_od_current_index_changed(self.ui.comboBox_od.currentIndex())
    
    def _slot_type_point_enq_changed(self):
        if (self.ui.radioButton_fictif.isChecked()):
            self.ui.comboBox_point_enq.setModel(self.caller.modelPointEnqFictif)
        else:
            self.ui.comboBox_point_enq.setModel(self.caller.modelPointEnqTerrain)
    
    def _slot_comboBox_point_enq_current_index_changed(self, indexChosenLine):
        if (indexChosenLine>0): 
            self.ui.pushButton_add.setEnabled(True)
        else:
            self.ui.pushButton_add.setEnabled(False)
    
    def _slot_points_enq_selection_changed(self):
        ids_fictifs_list = []
        ids_terrain_list = []
        
        for item in self.ui.tableView_points_enq.selectionModel().selectedRows():
            ids_fictifs_list.append(self.pointsEnqModel.record(item.row()).value("id_point_enq_fictif"))
        self.caller.point_enq_fictif_layer.selectByExpression("id IN ("+str(ids_fictifs_list)[1:len(str(ids_fictifs_list))-1]+")")
        for item in self.ui.tableView_points_enq.selectionModel().selectedRows():
            ids_terrain_list.append(self.pointsEnqModel.record(item.row()).value("id_point_enq_terrain"))
        self.caller.point_enq_terrain_layer.selectByExpression("id IN ("+str(ids_terrain_list)[1:len(str(ids_terrain_list))-1]+")")
    
    def _slot_pushButton_choose_point_enq_clicked(self):
        self.iface.mapCanvas().setMapTool(self.clickTool)
    
    def _slotClickPoint(self, point):
        if (self.ui.radioButton_fictif.isChecked()):
            s="SELECT id \
               FROM "+self.caller.survey_schema+".point_enq_fictif \
               ORDER BY st_distance(point_enq_fictif.geom, st_transform(st_setSRID(st_makepoint("+str(point.x())+", "+str(point.y())+"), "+str(self.iface.mapCanvas().mapSettings().destinationCrs().postgisSrid())+"), 2154)) \
               LIMIT 1"
            q=QtSql.QSqlQuery(unicode(s), self.caller.db)
            q.next()
            self.ui.comboBox_point_enq.setCurrentIndex(self.modelPointEnqFictif.match(self.modelPointEnqFictif.index(0,1),0,q.value(0), 1)[0].row())
               
        else:
            s="SELECT id \
               FROM "+self.caller.survey_schema+".point_enq_terrain \
               ORDER BY st_distance(point_enq_terrain.geom, st_transform(st_setSRID(st_makepoint("+str(point.x())+", "+str(point.y())+"), "+str(self.iface.mapCanvas().mapSettings().destinationCrs().postgisSrid())+"), 2154)) \
               LIMIT 1"
            q=QtSql.QSqlQuery(unicode(s), self.caller.db)
            q.next()
            self.ui.comboBox_point_enq.setCurrentIndex(self.caller.modelPointEnqTerrain.match(self.caller.modelPointEnqTerrain.index(0,1),0,q.value(0), 1)[0].row())
    
    def _slot_pushButton_add_clicked(self):
        if (self.ui.radioButton_terrain.isChecked()):
            if (self.ui.comboBox_point_enq.currentIndex()>0):
                s = "INSERT INTO "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+" (code_zone_orig, code_zone_dest, coef, id_point_enq_terrain) \
                     VALUES('"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_orig")+"', '"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_dest")+"', "+str(self.ui.doubleSpinBox_coef.value())+", "+str(self.caller.modelPointEnqTerrain.record(self.ui.comboBox_point_enq.currentIndex()).value("id"))+")"
        else:
            if (self.ui.comboBox_point_enq.currentIndex()>0):
                s = "INSERT INTO "+self.ui.comboBox_work_schema.currentText()+"."+self.ui.comboBox_corresp_table.currentText()+" (code_zone_orig, code_zone_dest, coef, id_point_enq_fictif) \
                     VALUES('"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_orig")+"', '"+self.ODModel.record(self.ui.comboBox_od.currentIndex()).value("code_zone_dest")+"', "+str(self.ui.doubleSpinBox_coef.value())+", "+str(self.caller.modelPointEnqFictif.record(self.ui.comboBox_point_enq.currentIndex()).value("id"))+")"
        q = QtSql.QSqlQuery(self.caller.db)
        q.exec_(unicode(s))
        self._slot_comboBox_od_current_index_changed(self.ui.comboBox_od.currentIndex())

    def _slot_close(self):
        self.hide()
        self.caller.point_enq_fictif_layer.removeSelection()
        self.caller.point_enq_terrain_layer.removeSelection()
        self.create_corresp_table_node = self.caller.node_group.findGroup("Création d'un fichier de correspondance OD - points d'enquête")
        self.caller.node_group.removeChildNode(self.create_corresp_table_node)
    
    
    
    
    
