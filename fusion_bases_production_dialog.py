#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import os
import sys
import subprocess

from PyQt5 import uic
from PyQt5 import QtWidgets
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtSql
from qgis.core import QgsVectorLayer, QgsDataSourceUri, QgsLayerTreeLayer, QgsProject
from .config import *

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtGui import QColor
from PyQt5.Qt import *
from qgis.core import *
from qgis.utils import iface

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from fusion_bases_production_dialog import Ui_Dialog

class fusion_bases_production_dialog(QDialog):
    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface
        self.plugin_dir = self.caller.plugin_dir
        
        self.ui.comboBox_schema_orig_1.setModel(self.caller.modelBasesEnquetes)
        self.ui.comboBox_schema_orig_2.setModel(self.caller.modelBasesEnquetes)
        
        # Connexion des signaux et des slots
        self._connectSlots()
        
    def _connectSlots(self):
        # Connexion signaux/slots
        self.ui.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self._slot_ok)
        self.ui.buttonBox.button(QDialogButtonBox.Cancel).clicked.connect(self._slot_cancel) 
    
    def _slot_ok(self): # Ouverture de Géoflux
        schema_orig_1 = 'p_enqod_route'+str(self.ui.comboBox_schema_orig_1.currentText())
        schema_orig_2 = 'p_enqod_route'+str(self.ui.comboBox_schema_orig_2.currentText())
        schema_dest = 'p_enqod_route'+str(self.ui.lineEdit_schema_dest_schema.text())
        
        fin = open(os.path.join(self.plugin_dir, 'sql', 'creer_base_production.sql'), 'r')
        fout = open(os.path.join(self.plugin_dir, 'sql', 'creer_base_production_temp.sql'), 'w')
        for line in fin:
            fout.write( line.replace('%schema_dest',schema_dest) )
        fin.close()
        fout.close()      
        
        nom_fichier = os.path.join(self.plugin_dir, 'sql', 'creer_base_production_temp.sql')
        cmd=[ PSQL_EXE, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "-d", self.caller.db.databaseName(), "-f", nom_fichier ]
        execute_external_cmd(cmd)
        
        fin = open(os.path.join(self.plugin_dir, 'sql', 'fusionner_bases_production.sql'), 'r')
        fout = open(os.path.join(self.plugin_dir, 'sql', 'fusionner_bases_production_temp.sql'), 'w')
        for line in fin:
            fout.write( line.replace('%schema_orig_1',schema_orig_1).replace('%schema_orig_2', schema_orig_2) )
        fin.close()
        fout.close()      
        
        nom_fichier = os.path.join(self.plugin_dir, 'sql', 'fusionner_bases_production_temp.sql')
        cmd=[ PSQL_EXE, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "-d", self.caller.db.databaseName(), "-f", nom_fichier ]
        execute_external_cmd(cmd)
        
        self.hide()
        
    def _slot_cancel(self):
        # En cliquant sur "Annuler", la fenêtre est fermée.
        self.hide()
        
        