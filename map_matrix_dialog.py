#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface
from processing.tools import postgis

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os
import processing

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from map_matrix_dialog import Ui_Dialog

class map_matrix_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface 
        self.plugin_dir = self.caller.plugin_dir
        self.styles_dir = self.caller.styles_dir
        
        self.modelZonage = QtSql.QSqlQueryModel()
        self.ui.comboBox_zonage.setModel(self.modelZonage)
        
        self.modelMacrozonage = QtSql.QSqlQueryModel()
        self.ui.comboBox_macrozonage.setModel(self.modelMacrozonage)
        
        self.modelMatrice = QtSql.QSqlQueryModel()
        self.ui.comboBox_matrice.setModel(self.modelMatrice) 
        
        # Connexion des signaux et des slots
        self._connectSlots()
    
    def _connectSlots(self):
        self.ui.comboBox_zonage.currentIndexChanged.connect(self._slot_comboBox_zonage_current_index_changed) 
        self.ui.comboBox_macrozonage.currentIndexChanged.connect(self._slot_comboBox_macrozonage_current_index_changed)
        self.ui.comboBox_matrice.currentIndexChanged.connect(self._slot_comboBox_matrice_current_index_changed)
        self.ui.pushButton_indic_zonage.clicked.connect(self._slot_pushButton_indic_zonage_clicked) 
        self.ui.pushButton_indic_macrozonage.clicked.connect(self._slot_pushButton_indic_macrozonage_clicked) 
        self.ui.buttonBox.button(QDialogButtonBox.Cancel).clicked.connect(self._slot_cancel) 
    
    def visumatrice(self, macrozonage):        
        database = self.caller.set_db_connection_dialog.ui.comboBox_connection.currentText()
        if (self.ui.radioButtonNbFlux.isChecked()):
            method = 0
            nb_flux = self.ui.spinBoxNbFlux.value()
            part_flux = 0
        else:
            method = 1
            part_flux = self.ui.doubleSpinBoxPartFlux.value()
            nb_flux = 0            
        
        if macrozonage == False:
            t = processing.run("Geoflux:7.1 Visualiser matrice", { 'DATABASE' : database, 
                                                                   'ID_MATRICE' : self.modelMatrice.record(self.ui.comboBox_matrice.currentIndex()).value("id"), 
                                                                   'LOGFILE' : 'TEMPORARY_OUTPUT', 
                                                                   'METHOD' : method, 
                                                                   'NB_FLUX' : nb_flux, 
                                                                   'PART_FLUX' : part_flux, 
                                                                   'SCHEMA_TRAVAIL' : self.caller.schema_travail
                                                                 }
                              )
        else:
            t = processing.run("Geoflux:7.1 Visualiser matrice", { 'DATABASE' : database, 
                                                                   'ID_MATRICE' : self.modelMatrice.record(self.ui.comboBox_matrice.currentIndex()).value("id"), 
                                                                   'ID_MACROZONAGE' : self.modelMacrozonage.record(self.ui.comboBox_macrozonage.currentIndex()).value("id"), 
                                                                   'LOGFILE' : 'TEMPORARY_OUTPUT', 
                                                                   'METHOD' : method, 
                                                                   'NB_FLUX' : nb_flux, 
                                                                   'PART_FLUX' : part_flux, 
                                                                   'SCHEMA_TRAVAIL' : self.caller.schema_travail
                                                                 }
                              )
        
        # Ligne conservée pour l'exemple de syntaxe
        # uri.setDataSource("", f"({s})", "geom", "", "id_zone") 
        
        QgsProject.instance().addMapLayer(t['OUTPUT5'], False)
        node_layer = QgsLayerTreeLayer(t['OUTPUT5'])
        node_layer.setExpanded(False)
        self.caller.node_od_matrix.insertChildNode(3, node_layer)
        field_to_value_relation(t['OUTPUT5'], "id_zone_orig", self.caller.zones_layer, "id", "code_zone") 
        field_to_value_relation(t['OUTPUT5'], "id_zone_dest", self.caller.zones_layer, "id", "code_zone") 
        
        QgsProject.instance().addMapLayer(t['OUTPUT4'], False)
        node_layer = QgsLayerTreeLayer(t['OUTPUT4'])
        node_layer.setExpanded(False)
        self.caller.node_od_matrix.insertChildNode(4, node_layer)
        field_to_value_relation(t['OUTPUT4'], "id_zone", self.caller.zones_layer, "id", "code_zone") 
        
        QgsProject.instance().addMapLayer(t['OUTPUT2'], False)
        node_layer = QgsLayerTreeLayer(t['OUTPUT2'])
        node_layer.setExpanded(False)
        self.caller.node_od_matrix.insertChildNode(5, node_layer)
        field_to_value_relation(t['OUTPUT2'], "id_zone", self.caller.zones_layer, "id", "code_zone") 
        
        QgsProject.instance().addMapLayer(t['OUTPUT3'], False)
        node_layer = QgsLayerTreeLayer(t['OUTPUT3'])
        node_layer.setExpanded(False)
        self.caller.node_od_matrix.insertChildNode(6, node_layer)
        field_to_value_relation(t['OUTPUT3'], "id_zone", self.caller.zones_layer, "id", "code_zone") 
        
        QgsProject.instance().addMapLayer(t['OUTPUT1'], False)
        node_layer = QgsLayerTreeLayer(t['OUTPUT1'])
        node_layer.setExpanded(False)
        self.caller.node_od_matrix.insertChildNode(7, node_layer)
        field_to_value_relation(t['OUTPUT1'], "id_zone", self.caller.zones_layer, "id", "code_zone") 
        
        self.caller.node_od_matrix.setItemVisibilityChecked(True)
    
    def _slot_pushButton_indic_zonage_clicked(self):
        self.visumatrice(False)
    
    def _slot_pushButton_indic_macrozonage_clicked(self):
        self.visumatrice(True)
    
    def apply_on_map_matrice(self):
        if (self.ui.comboBox_matrice.currentIndex()>0):
            s_matrice= "(id_matrice = "+str(self.modelMatrice.record(self.ui.comboBox_matrice.currentIndex()).value("id"))+")"
            self.od_matrix_values_layer.setSubsetString(s_matrice)  
        else:
            self.od_matrix_values_layer.setSubsetString("1=2")
    
    def _slot_comboBox_zonage_current_index_changed(self, indexChosenLine):
        # Update comboBox_macrozonage
        if (self.ui.comboBox_zonage.currentIndex()>0):
            s="SELECT 'Aucun zonage'::character varying as nom, null as id \
               UNION \
               ( \
                    SELECT DISTINCT macrozonage.nom, macrozonage.id \
                    FROM "+self.caller.schema_travail+".corresp_zones JOIN "+self.caller.schema_travail+".zone macrozone ON (corresp_zones.id_macrozone = macrozone.id) \
                                                               JOIN "+self.caller.schema_travail+".zone zone ON (corresp_zones.id_zone = zone.id) \
                                                               JOIN "+self.caller.schema_travail+".zonage macrozonage ON (macrozone.id_zonage = macrozonage.id) \
                    WHERE zone.id_zonage = "+str(self.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))+" \
               ) \
               ORDER BY 2 NULLS FIRST"
        else:
            s="SELECT 'Aucun zonage'::character varying as nom, null as id"
        
        self.modelMacrozonage.setQuery(s, self.caller.db)
        self.ui.comboBox_macrozonage.setCurrentIndex(0)
        
        # Update comboBox_matrice
        if (self.ui.comboBox_zonage.currentIndex() > 0): # Un zonage précis a été choisi
            filter = "matrice.id_zonage = "+str(self.modelZonage.record(self.ui.comboBox_zonage.currentIndex()).value("id"))
        else:
            filter = "(1=1)"
        
        if (self.ui.comboBox_zonage.currentIndex()>0):
            s="SELECT 'Aucune matrice'::character varying as nom, null as id \
               UNION \
               ( \
                    SELECT DISTINCT matrice.nom, matrice.id \
                    FROM "+self.caller.schema_travail+".matrice \
                    WHERE "+ filter +" \
               ) \
               ORDER BY 2 NULLS FIRST"
        else:
            s="SELECT 'Aucune matrice'::character varying as nom, null as id"
            
        self.modelMatrice.setQuery(s, self.caller.db)
        self.ui.comboBox_matrice.setCurrentIndex(0)
    
    def _slot_comboBox_macrozonage_current_index_changed(self, indexChosenLine):
        if (self.ui.comboBox_macrozonage.currentIndex()>0) and (self.ui.comboBox_matrice.currentIndex()>0):
            self.ui.pushButton_indic_macrozonage.setEnabled(True)
        else:
            self.ui.pushButton_indic_macrozonage.setEnabled(False)
    
    def _slot_comboBox_matrice_current_index_changed(self, indexChosenLine):
        self.apply_on_map_matrice()
        if (self.ui.comboBox_matrice.currentIndex()>0):
            self.ui.pushButton_indic_zonage.setEnabled(True)
        else:
            self.ui.pushButton_indic_zonage.setEnabled(False)
        
        if (self.ui.comboBox_macrozonage.currentIndex()>0) and (self.ui.comboBox_matrice.currentIndex()>0):
            self.ui.pushButton_indic_macrozonage.setEnabled(True)
        else:
            self.ui.pushButton_indic_macrozonage.setEnabled(False)
    
    def _slot_cancel(self):
        self.hide()
    
    