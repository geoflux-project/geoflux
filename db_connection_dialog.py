#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface
from processing.tools import postgis

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from db_connection_dialog import Ui_Dialog

class db_connection_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface 
        self.plugin_dir = self.caller.plugin_dir
        self.styles_dir = self.caller.styles_dir
        
        self.modelMatrixSchema = QtSql.QSqlQueryModel()
        self.modelSurveySchema = QtSql.QSqlQueryModel()
        self.modelRefSchema = QtSql.QSqlQueryModel()
        
        self.ui.comboBox_matrix_schema.setModel(self.modelMatrixSchema)
        self.ui.comboBox_survey_schema.setModel(self.modelSurveySchema)
                
        # Connexion des signaux et des slots
        self._connectSlots()
    
    def _connectSlots(self):
        self.ui.comboBox_connection.currentIndexChanged.connect(self._slot_comboBox_connection_current_index_changed)
        self.ui.buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self._slot_apply)
        self.ui.buttonBox.button(QDialogButtonBox.Cancel).clicked.connect(self._slot_cancel) 
    
    def _slot_comboBox_connection_current_index_changed(self, indexChosenLine):
        c = self.ui.comboBox_connection.currentText()
        if (c!=""):
            self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(True)
            
            self.caller.uri = postgis.uri_from_name(c)
            self.caller.db.setHostName(self.caller.uri.host())
            self.caller.db.setPort(int(self.caller.uri.port()))
            self.caller.db.setDatabaseName(self.caller.uri.database())
            self.caller.db.setUserName(self.caller.uri.username())
            self.caller.db.setPassword(self.caller.uri.password())
            self.caller.db_string = "dbname='"+self.caller.uri.database()+"' host="+self.caller.uri.host()+" port="+str(self.caller.uri.port())+" user='"+self.caller.uri.username()+"' password='"+self.caller.uri.password()+"'"
            ok=self.caller.db.open()
            
            if ok==False:
                self.iface.messageBar().pushMessage("Erreur","Connection failed: "+self.caller.db.lastError().text(),level=2)
                self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(False)
            else:
                self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(True)
                # Schéma des enquêtes OD routières
                s="SELECT schema_name FROM information_schema.schemata \
                    WHERE position('geoflux' in schema_name)>0 AND position('survey' in schema_name)>0 \
                    ORDER BY 1";
                self.modelSurveySchema.setQuery(s, self.caller.db)
                self.ui.comboBox_survey_schema.setCurrentIndex(0)
                
                # Schéma des matrices
                s="SELECT schema_name FROM information_schema.schemata \
                    WHERE position('geoflux' in schema_name)>0 AND position('matrix' in schema_name)>0 \
                    ORDER BY 1"
                self.modelMatrixSchema.setQuery(s, self.caller.db)
                self.ui.comboBox_matrix_schema.setCurrentIndex(0)
                
                # Schémas
                s = "SELECT schema_name FROM information_schema.schemata ORDER BY 1"
                self.caller.modelSchemas.setQuery(s, self.caller.db)
        else:
            self.iface.messageBar().pushMessage("Attention", "L'utilisation du plugin Géoflux nécessite de définir une connexion PostGIS.", level=1)
            self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(False)
        
    def _slot_apply(self):
        if (self.ui.groupBox_matrix.isChecked()):
            self.caller.matrix_schema = self.ui.comboBox_matrix_schema.currentText()
            self.caller.dockwidget.ui.tabWidget.setTabEnabled(2, True)
        else:
            self.caller.matrix_schema = ""
            self.caller.dockwidget.ui.tabWidget.setTabEnabled(2, False)
        
        if (self.ui.groupBox_survey.isChecked()):
            self.caller.survey_schema = self.ui.comboBox_survey_schema.currentText()
            self.caller.dockwidget.ui.tabWidget.setTabEnabled(0, True)
            self.caller.dockwidget.ui.tabWidget.setTabEnabled(1, True)
        else:
            self.caller.survey_schema = ""
            self.caller.dockwidget.ui.tabWidget.setTabEnabled(0, False)
            self.caller.dockwidget.ui.tabWidget.setTabEnabled(1, False)
        
        if ((self.caller.matrix_schema == "") and (self.caller.survey_schema == "")):
            self.iface.messageBar().pushMessage("Erreur",u"Renseignez les schémas de données",level=2)
        else:
            # Chargement du fond de plan OpenStreetMap (présent dans un projet)
            project = QgsProject.instance()
            project.read(os.path.join(self.plugin_dir, 'geoflux.qgz'))
            
            # Chargement des groupes et des couches
            self.caller.create_layers_groups()             
            self.caller.load_layers()
            
            # Fermeture de la fenêtre de connexion
            self.hide()
        
            # Affectation de valeurs aux "QueryModels"
            self.caller.init_models()
            
            # Zoom to a layer
            if (self.caller.survey_schema != ""):
                list = [self.caller.point_enq_terrain_layer] 
            else:
                list = []
            self.caller.zoomToLayersList(list, True)
        
    def _slot_cancel(self):
        self.hide()




