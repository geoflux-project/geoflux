# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\bousq\Documents\Code\geoflux\forms\db_connection_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(519, 275)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/geoflux/icon_geoflux.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(350, 240, 156, 23))
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply|QtWidgets.QDialogButtonBox.Cancel)
        self.buttonBox.setObjectName("buttonBox")
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 501, 31))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.comboBox_connection = QtWidgets.QComboBox(self.layoutWidget)
        self.comboBox_connection.setEditable(False)
        self.comboBox_connection.setObjectName("comboBox_connection")
        self.gridLayout.addWidget(self.comboBox_connection, 0, 1, 1, 1)
        self.groupBox_survey = QtWidgets.QGroupBox(Dialog)
        self.groupBox_survey.setGeometry(QtCore.QRect(10, 60, 501, 71))
        self.groupBox_survey.setCheckable(True)
        self.groupBox_survey.setObjectName("groupBox_survey")
        self.layoutWidget1 = QtWidgets.QWidget(self.groupBox_survey)
        self.layoutWidget1.setGeometry(QtCore.QRect(10, 30, 481, 31))
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.layoutWidget1)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_survey = QtWidgets.QLabel(self.layoutWidget1)
        self.label_survey.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_survey.setObjectName("label_survey")
        self.gridLayout_2.addWidget(self.label_survey, 0, 0, 1, 1)
        self.comboBox_survey_schema = QtWidgets.QComboBox(self.layoutWidget1)
        self.comboBox_survey_schema.setObjectName("comboBox_survey_schema")
        self.gridLayout_2.addWidget(self.comboBox_survey_schema, 0, 1, 1, 1)
        self.groupBox_matrix = QtWidgets.QGroupBox(Dialog)
        self.groupBox_matrix.setGeometry(QtCore.QRect(10, 160, 501, 71))
        self.groupBox_matrix.setCheckable(True)
        self.groupBox_matrix.setObjectName("groupBox_matrix")
        self.layoutWidget2 = QtWidgets.QWidget(self.groupBox_matrix)
        self.layoutWidget2.setGeometry(QtCore.QRect(10, 31, 481, 31))
        self.layoutWidget2.setObjectName("layoutWidget2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.layoutWidget2)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_matrix = QtWidgets.QLabel(self.layoutWidget2)
        self.label_matrix.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_matrix.setObjectName("label_matrix")
        self.horizontalLayout.addWidget(self.label_matrix)
        self.comboBox_matrix_schema = QtWidgets.QComboBox(self.layoutWidget2)
        self.comboBox_matrix_schema.setEnabled(True)
        self.comboBox_matrix_schema.setObjectName("comboBox_matrix_schema")
        self.horizontalLayout.addWidget(self.comboBox_matrix_schema)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Geoflux "))
        self.label.setText(_translate("Dialog", "Connexion"))
        self.groupBox_survey.setTitle(_translate("Dialog", "Charger les enquêtes OD routières"))
        self.label_survey.setText(_translate("Dialog", "Schéma des données d\'enquête"))
        self.groupBox_matrix.setTitle(_translate("Dialog", "Charger les matrices"))
        self.label_matrix.setText(_translate("Dialog", "Schéma des zonages utilisateurs et des matrices"))

import resources_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

