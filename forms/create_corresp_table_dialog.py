# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\bousq\Documents\Code\geoflux\forms\create_corresp_table_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(614, 444)
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 6, 591, 91))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.comboBox_zonage = QtWidgets.QComboBox(self.layoutWidget)
        self.comboBox_zonage.setEnabled(False)
        self.comboBox_zonage.setObjectName("comboBox_zonage")
        self.gridLayout.addWidget(self.comboBox_zonage, 0, 1, 1, 2)
        self.comboBox_work_schema = QtWidgets.QComboBox(self.layoutWidget)
        self.comboBox_work_schema.setObjectName("comboBox_work_schema")
        self.gridLayout.addWidget(self.comboBox_work_schema, 1, 1, 1, 2)
        self.comboBox_corresp_table = QtWidgets.QComboBox(self.layoutWidget)
        self.comboBox_corresp_table.setEditable(True)
        self.comboBox_corresp_table.setObjectName("comboBox_corresp_table")
        self.gridLayout.addWidget(self.comboBox_corresp_table, 2, 1, 1, 2)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.pushButton_create_corresp_table = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButton_create_corresp_table.setObjectName("pushButton_create_corresp_table")
        self.gridLayout.addWidget(self.pushButton_create_corresp_table, 2, 3, 1, 1)
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 120, 591, 281))
        self.groupBox.setObjectName("groupBox")
        self.layoutWidget1 = QtWidgets.QWidget(self.groupBox)
        self.layoutWidget1.setGeometry(QtCore.QRect(10, 70, 571, 191))
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.layoutWidget1)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.pushButton_choose_point_enq = QtWidgets.QPushButton(self.layoutWidget1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_choose_point_enq.sizePolicy().hasHeightForWidth())
        self.pushButton_choose_point_enq.setSizePolicy(sizePolicy)
        self.pushButton_choose_point_enq.setMaximumSize(QtCore.QSize(20, 16777215))
        self.pushButton_choose_point_enq.setObjectName("pushButton_choose_point_enq")
        self.gridLayout_2.addWidget(self.pushButton_choose_point_enq, 3, 5, 1, 1)
        self.radioButton_fictif = QtWidgets.QRadioButton(self.layoutWidget1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.radioButton_fictif.sizePolicy().hasHeightForWidth())
        self.radioButton_fictif.setSizePolicy(sizePolicy)
        self.radioButton_fictif.setObjectName("radioButton_fictif")
        self.gridLayout_2.addWidget(self.radioButton_fictif, 2, 1, 1, 3)
        self.pushButton_delete = QtWidgets.QPushButton(self.layoutWidget1)
        self.pushButton_delete.setObjectName("pushButton_delete")
        self.gridLayout_2.addWidget(self.pushButton_delete, 1, 6, 1, 1)
        self.pushButton_add = QtWidgets.QPushButton(self.layoutWidget1)
        self.pushButton_add.setObjectName("pushButton_add")
        self.gridLayout_2.addWidget(self.pushButton_add, 2, 6, 2, 1)
        self.label_5 = QtWidgets.QLabel(self.layoutWidget1)
        self.label_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 2, 0, 2, 1)
        self.radioButton_terrain = QtWidgets.QRadioButton(self.layoutWidget1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.radioButton_terrain.sizePolicy().hasHeightForWidth())
        self.radioButton_terrain.setSizePolicy(sizePolicy)
        self.radioButton_terrain.setChecked(True)
        self.radioButton_terrain.setObjectName("radioButton_terrain")
        self.gridLayout_2.addWidget(self.radioButton_terrain, 2, 4, 1, 2)
        self.comboBox_point_enq = QtWidgets.QComboBox(self.layoutWidget1)
        self.comboBox_point_enq.setObjectName("comboBox_point_enq")
        self.gridLayout_2.addWidget(self.comboBox_point_enq, 3, 2, 1, 3)
        self.label1 = QtWidgets.QLabel(self.layoutWidget1)
        self.label1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label1.setObjectName("label1")
        self.gridLayout_2.addWidget(self.label1, 0, 0, 1, 1)
        self.comboBox_od = QtWidgets.QComboBox(self.layoutWidget1)
        self.comboBox_od.setObjectName("comboBox_od")
        self.gridLayout_2.addWidget(self.comboBox_od, 0, 1, 1, 5)
        self.doubleSpinBox_coef = QtWidgets.QDoubleSpinBox(self.layoutWidget1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.doubleSpinBox_coef.sizePolicy().hasHeightForWidth())
        self.doubleSpinBox_coef.setSizePolicy(sizePolicy)
        self.doubleSpinBox_coef.setMaximumSize(QtCore.QSize(50, 16777215))
        self.doubleSpinBox_coef.setMaximum(1.0)
        self.doubleSpinBox_coef.setSingleStep(0.01)
        self.doubleSpinBox_coef.setProperty("value", 1.0)
        self.doubleSpinBox_coef.setObjectName("doubleSpinBox_coef")
        self.gridLayout_2.addWidget(self.doubleSpinBox_coef, 3, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.layoutWidget1)
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 1, 0, 1, 1)
        self.tableView_points_enq = QtWidgets.QTableView(self.layoutWidget1)
        self.tableView_points_enq.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.tableView_points_enq.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableView_points_enq.setObjectName("tableView_points_enq")
        self.tableView_points_enq.horizontalHeader().setSortIndicatorShown(True)
        self.tableView_points_enq.verticalHeader().setVisible(False)
        self.gridLayout_2.addWidget(self.tableView_points_enq, 1, 1, 1, 5)
        self.layoutWidget2 = QtWidgets.QWidget(self.groupBox)
        self.layoutWidget2.setGeometry(QtCore.QRect(10, 30, 571, 31))
        self.layoutWidget2.setObjectName("layoutWidget2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.layoutWidget2)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.radioButton_see_all = QtWidgets.QRadioButton(self.layoutWidget2)
        self.radioButton_see_all.setChecked(True)
        self.radioButton_see_all.setObjectName("radioButton_see_all")
        self.gridLayout_3.addWidget(self.radioButton_see_all, 0, 0, 1, 1)
        self.radioButton_see_missing = QtWidgets.QRadioButton(self.layoutWidget2)
        self.radioButton_see_missing.setObjectName("radioButton_see_missing")
        self.gridLayout_3.addWidget(self.radioButton_see_missing, 0, 2, 1, 1)
        self.radioButton_see_represented = QtWidgets.QRadioButton(self.layoutWidget2)
        self.radioButton_see_represented.setObjectName("radioButton_see_represented")
        self.gridLayout_3.addWidget(self.radioButton_see_represented, 0, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(430, 410, 156, 23))
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Créer une table de correspondance OD - points d\'enquête"))
        self.label_2.setText(_translate("Dialog", "Zonage cible"))
        self.label_3.setText(_translate("Dialog", "Table de correspondances"))
        self.label.setText(_translate("Dialog", "Schéma de travail"))
        self.pushButton_create_corresp_table.setText(_translate("Dialog", "Créer"))
        self.groupBox.setTitle(_translate("Dialog", "Mise à jour des correspondances"))
        self.pushButton_choose_point_enq.setText(_translate("Dialog", "+"))
        self.radioButton_fictif.setText(_translate("Dialog", "Fictif"))
        self.pushButton_delete.setText(_translate("Dialog", "Supprimer"))
        self.pushButton_add.setText(_translate("Dialog", "Ajouter"))
        self.label_5.setText(_translate("Dialog", "Point d\'enquête à ajouter à l\'OD"))
        self.radioButton_terrain.setText(_translate("Dialog", "Terrain"))
        self.label1.setText(_translate("Dialog", "OD"))
        self.label_4.setText(_translate("Dialog", "Points d\'enquête associés à l\'OD"))
        self.radioButton_see_all.setText(_translate("Dialog", "Voir toutes les OD"))
        self.radioButton_see_missing.setText(_translate("Dialog", "Voir les OD absentes"))
        self.radioButton_see_represented.setText(_translate("Dialog", "Voir les OD présentes"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

