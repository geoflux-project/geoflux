#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import os
import sys
import subprocess

from PyQt5 import uic
from PyQt5 import QtWidgets
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtSql
from qgis.core import QgsVectorLayer, QgsDataSourceUri, QgsLayerTreeLayer, QgsProject

from .config import *

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtGui import QColor
from PyQt5.Qt import *
from qgis.core import *
from qgis.utils import iface

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from import_fichier_dialog import Ui_Dialog

class import_fichier_dialog(QDialog):
    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface
        self.plugin_dir = self.caller.plugin_dir
        
        # Connexion des signaux et des slots
        self._connectSlots()
        
    def _connectSlots(self):
        # Connexion signaux/slots
        self.ui.pushButton_parcourir.clicked.connect(self._slot_parcourir)
        self.ui.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self._slot_ok)
        self.ui.buttonBox.button(QDialogButtonBox.Cancel).clicked.connect(self._slot_cancel) 
    
    def _slot_parcourir(self):
        self.cheminComplet = QFileDialog.getOpenFileName(caption = "Choisir un fichier .csv, séparateur ';'", directory=self.caller.last_dir, filter = "(*.csv)")
        self.caller.last_dir = os.path.dirname(self.cheminComplet[0])
        self.ui.lineEdit_chemin.setText(self.cheminComplet[0])
    
    def _slot_ok(self): 
        q=QtSql.QSqlQuery(self.caller.db)
        str1=''
        str2=''
        nom_table_import = "_import_"+self.caller.import_type+"_"+self.caller.user
        if self.caller.import_type == 'question':
            str1="nom character varying,libelle character varying,type character varying,type_veh integer"
            str2="nom, libelle, type, type_veh"
            s3="INSERT INTO "+self.caller.base_enquetes+".question(nom,libelle,type,type_veh) "+\
                   "SELECT DISTINCT import.nom, import.libelle, import.type, import.type_veh "+\
                   "FROM "+self.caller.schema_travail+"."+nom_table_import+" AS import "+\
                   "LEFT JOIN "+self.caller.base_enquetes+".question ON (import.nom = question.nom) "+\
                   "WHERE question.id IS NULL;"
            s4="DELETE FROM "+self.caller.schema_travail+"."+nom_table_import+" "+\
               "WHERE (nom, libelle, type, type_veh) IN "+\
               "(SELECT nom, libelle, type, type_veh FROM "+self.caller.base_enquetes+".question);" 
        elif self.caller.import_type == 'codif':
            str1="nom character varying, code_modalite character varying, libelle_modalite character varying"
            str2="nom, code_modalite, libelle_modalite"
            s3="INSERT INTO "+self.caller.base_enquetes+".codif(nom) "+\
                   "SELECT DISTINCT import.nom "+\
                   "FROM "+self.caller.schema_travail+"."+nom_table_import+" AS import "+\
                   "LEFT JOIN "+self.caller.base_enquetes+".codif ON (codif.nom = import.nom) "+\
                   "WHERE codif.id IS NULL;"+\
               "INSERT INTO "+self.caller.base_enquetes+".modalite(id_codif, code_modalite, libelle) "+\
                   "SELECT codif.id, import.code_modalite, import.libelle_modalite "+\
                   "FROM "+self.caller.schema_travail+"."+nom_table_import+" AS import "+\
                   "LEFT JOIN "+self.caller.base_enquetes+".codif ON (import.nom = codif.nom);"
            s4="DELETE FROM "+self.caller.schema_travail+"."+nom_table_import+" "+\
               "WHERE (nom, code_modalite, libelle_modalite) IN "+\
               "(SELECT codif.nom, modalite.code_modalite, modalite.libelle FROM "+self.caller.base_enquetes+".modalite JOIN "+self.caller.base_enquetes+".codif ON (codif.id = modalite.id_codif));"
        elif self.caller.import_type == 'questionnaire_vl':
            str1="nom character varying, ordre integer, nom_question character varying, nom_codif character varying, type_veh integer"
            str2="nom, ordre, nom_question, nom_codif"
            s3="INSERT INTO "+self.caller.base_enquetes+".questionnaire(nom, type_veh) "+\
                   "SELECT DISTINCT import.nom, 1 "+\
                   "FROM "+self.caller.schema_travail+"."+nom_table_import+" AS import "+\
                   "LEFT JOIN "+self.caller.base_enquetes+".questionnaire ON (questionnaire.nom = import.nom) "+\
                   "WHERE questionnaire.id IS NULL;"+\
               "INSERT INTO "+self.caller.base_enquetes+".composition_questionnaire(id_questionnaire, ordre, id_question, id_codif) "+\
                   "SELECT questionnaire.id, import.code_modalite, import.libelle "+\
                   "FROM "+self.caller.schema_travail+"."+nom_table_import+" AS import "+\
                   "LEFT JOIN "+self.caller.base_enquetes+".questionnaire ON (import.nom = questionnaire.nom) "+\
                   "WHERE import.id IS NULL;" 
            s4="DELETE FROM "+self.caller.schema_travail+"."+nom_table_import+" "+\
               "WHERE (nom, ordre, nom_question, nom_codif) IN "+\
               "(SELECT questionnaire.nom, composition_questionnaire.ordre, question.nom, codif.nom "+\
               "FROM "+self.caller.base_enquetes+".composition_questionnaire "+\
               "JOIN "+self.caller.base_enquetes+".questionnaire ON (questionnaire.id = composition_questionnaire.id_questionnaire) "+\
               "JOIN "+self.caller.base_enquetes+".question ON (question.id = composition_questionnaire.id_question) "+\
               "JOIN "+self.caller.base_enquetes+".codif ON (codif.id = composition_questionnaire.id_codif));"
        elif self.caller.import_type == 'questionnaire_pl':
            str1="nom character varying, ordre integer, nom_question character varying, nom_codif character varying, type_veh integer"
            str2="nom, ordre, nom_question, nom_codif"
            s3="INSERT INTO "+self.caller.base_enquetes+".questionnaire(nom, type_veh) "+\
                   "SELECT DISTINCT import.nom, 2 "+\
                   "FROM "+self.caller.schema_travail+"."+nom_table_import+" AS import "+\
                   "LEFT JOIN "+self.caller.base_enquetes+".questionnaire ON (questionnaire.nom = import.nom) "+\
                   "WHERE questionnaire.id IS NULL;"+\
               "INSERT INTO "+self.caller.base_enquetes+".composition_questionnaire(id_questionnaire, ordre, id_question, id_codif) "+\
                   "SELECT questionnaire.id, import.code_modalite, import.libelle "+\
                   "FROM "+self.caller.schema_travail+"."+nom_table_import+" AS import "+\
                   "LEFT JOIN "+self.caller.base_enquetes+".questionnaire ON (import.nom = questionnaire.nom) "+\
                   "WHERE import.id IS NULL;" 
            s4="DELETE FROM "+self.caller.schema_travail+"."+nom_table_import+" "+\
               "WHERE (nom, ordre, nom_question, nom_codif) IN "+\
               "(SELECT questionnaire.nom, composition_questionnaire.ordre, question.nom, codif.nom "+\
               "FROM "+self.caller.base_enquetes+".composition_questionnaire "+\
               "JOIN "+self.caller.base_enquetes+".questionnaire ON (questionnaire.id = composition_questionnaire.id_questionnaire) "+\
               "JOIN "+self.caller.base_enquetes+".question ON (question.id = composition_questionnaire.id_question) "+\
               "JOIN "+self.caller.base_enquetes+".codif ON (codif.id = composition_questionnaire.id_codif));"
        s1="DROP TABLE IF EXISTS "+self.caller.schema_travail+"."+nom_table_import+"; "+\
           "CREATE TABLE "+self.caller.schema_travail+"."+nom_table_import+"("+str1+");"
        s2="\\copy "+self.caller.schema_travail+"."+nom_table_import+"("+str2+")"\
           "FROM "+self.ui.lineEdit_chemin.text()+" CSV HEADER DELIMITER ';' ENCODING 'UTF-8'"
        
        # On crée une table d'import pour accueillir les données
        print(s1)
        q.exec_(s1)        
        # On copie dedans les données du fichier CSV (nécessite psql pour exécuter la commande \COPY)
        cmd=[ PSQL_EXE, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "-d", self.caller.db.databaseName(), "-c", s2 ]
        execute_external_cmd( cmd )        
        # On insère les question qui ne sont pas déjà présentes dans la base
        cmd=[ PSQL_EXE, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "-d", self.caller.db.databaseName(), "-c", s3 ]
        execute_external_cmd( cmd )
        # On supprime question insérées pour ne garder que les lignes qui ont posé problème
        cmd=[ PSQL_EXE, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "-d", self.caller.db.databaseName(), "-c", s4 ]
        execute_external_cmd( cmd )
        # On ajoute au projet QGIS la couche des lignes qui ont posé problème
        root = QgsProject.instance().layerTreeRoot()
        node_group = root.findGroup("Imports")
        root.removeChildNode(node_group)
        uri=QgsDataSourceUri()
        uri.setConnection(self.caller.db.hostName(), str(self.caller.db.port()), self.caller.db.databaseName(), self.caller.db.userName(), self.caller.db.password())
        
        lyr = QgsProject.instance().mapLayersByName("Erreurs "+self.caller.import_type)
        if lyr == []:
            uri.setDataSource(self.caller.schema_travail,nom_table_import, None) 
            layer = QgsVectorLayer(uri.uri(), u"Erreurs "+self.caller.import_type, "postgres")
            if (layer.isValid()):
                QgsProject.instance().addMapLayer(layer, False)
                node_layer = QgsLayerTreeLayer(layer)
                node_group.insertChildNode(0, node_layer)
            else:
                print("Couche "+self.caller.schema_travail+"."+nom_table_import+" non trouvée")        
        self.hide()
            
    def _slot_cancel(self):
        # En cliquant sur "Annuler", la fenêtre est fermée.
        self.hide()
        
        