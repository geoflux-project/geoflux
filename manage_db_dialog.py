#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

import sys
import string
import os

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from manage_db_dialog import Ui_Dialog


class manage_db_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.db = self.caller.db
        self.iface = self.caller.iface
        
        self.plugin_dir = self.caller.plugin_dir
        self.icon_dir = self.caller.icon_dir
        self.temp_db = QtSql.QSqlDatabase.addDatabase("QPSQL", connectionName="temp_db")
        
        self.ui.pushButtonLoad.setIcon(QIcon(self.icon_dir + "/icon_open.png"))
        
        self.ui.comboBoxDB.setModel(self.caller.modelDB)
        
        # Connexion des signaux et des slots
        self._connectSlots()
    
    def _connectSlots(self):
        self.ui.pushButtonLoad.clicked.connect(self._slotPushButtonLoadClicked)
        self.ui.buttonBox.rejected.connect(self._slotClose)
    
    def updateDBConnection(self):
        self.caller.db.setHostName(self.caller.db.hostName())
        self.caller.db.setUserName( self.caller.db.userName() )
        self.caller.db.setPort(int(self.caller.db.port()))
        self.caller.db.setPassword(self.caller.db.password())
        self.caller.db.setDatabaseName( str(self.ui.comboBoxDB.currentText()) )
        r=self.caller.db.open()
        
        box = QMessageBox()
        box.setModal(True)
        if r is False:
            box.setText(u"La connexion à la base a échoué : "+self.caller.db.lastError().text()+".")
            box.exec_()
    
    def _slotComboBoxDBIndexChanged(self):
        self.updateDBConnection()
            
    def _slotPushButtonLoadClicked(self):
        self.msq.setText(self.ui.comboBoxDB.currentText())
        self.updateDBConnection()
        
        if self.caller.db.open():
            
            
            # Update the map window
            self.caller.loadLayers()
            
            #self.ui.pushButtonLoad.setEnabled(True)
            
        else:
            box = QMessageBox()
            box.setModal(True)
            box.setText(u"La connexion à la base a échoué.")
            box.exec_()
        self.hide()
       
    def _slotClose(self):
        self.hide()

    
        