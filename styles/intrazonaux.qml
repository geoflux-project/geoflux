<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="0" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" simplifyDrawingHints="0" simplifyLocal="1" readOnly="0" version="3.10.6-A Coruña" styleCategories="AllStyleCategories" simplifyDrawingTol="1" simplifyAlgorithm="0" simplifyMaxScale="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" forceraster="0" symbollevels="0">
    <symbols>
      <symbol type="marker" alpha="1" clip_to_extent="1" name="0" force_rhr="0">
        <layer locked="0" pass="0" enabled="1" class="SimpleMarker">
          <prop v="0" k="angle"/>
          <prop v="0,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="141,141,141,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="1" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="size">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="&quot;size_flux&quot; *10" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id_zone</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeType="MM" backgroundAlpha="255" width="15" maxScaleDenominator="1e+08" barWidth="5" scaleDependency="Area" backgroundColor="#ffffff" sizeScale="3x:0,0,0,0,0,0" enabled="0" lineSizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" height="15" labelPlacementMethod="XHeight" scaleBasedVisibility="0" opacity="1" rotationOffset="270" lineSizeType="MM" penColor="#000000" minimumSize="0" penAlpha="255" diagramOrientation="Up" penWidth="0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="0" dist="0" priority="0" obstacle="0" zIndex="0" linePlacementFlags="18" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Zones_ea50638c_0553_4c75_8c22_1e79e0ae2c12" name="Layer"/>
            <Option type="QString" value="Zones" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(id_zonage = 6)" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="code_zone" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="emis">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="attir">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="intra">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="emis_attir">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size_emiss">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size_attra">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size_total">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size_intra">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="Code zone" field="id_zone"/>
    <alias index="1" name="Emissions" field="emis"/>
    <alias index="2" name="Attractions" field="attir"/>
    <alias index="3" name="Intrazonaux" field="intra"/>
    <alias index="4" name="Emissions + attractions" field="emis_attir"/>
    <alias index="5" name="" field="size_emiss"/>
    <alias index="6" name="" field="size_attra"/>
    <alias index="7" name="" field="size_total"/>
    <alias index="8" name="" field="size_intra"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id_zone"/>
    <default applyOnUpdate="0" expression="" field="emis"/>
    <default applyOnUpdate="0" expression="" field="attir"/>
    <default applyOnUpdate="0" expression="" field="intra"/>
    <default applyOnUpdate="0" expression="" field="emis_attir"/>
    <default applyOnUpdate="0" expression="" field="size_emiss"/>
    <default applyOnUpdate="0" expression="" field="size_attra"/>
    <default applyOnUpdate="0" expression="" field="size_total"/>
    <default applyOnUpdate="0" expression="" field="size_intra"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id_zone" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="emis" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="attir" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="intra" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="emis_attir" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="size_emiss" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="size_attra" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="size_total" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="size_intra" constraints="3" notnull_strength="1" unique_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_zone" exp="" desc=""/>
    <constraint field="emis" exp="" desc=""/>
    <constraint field="attir" exp="" desc=""/>
    <constraint field="intra" exp="" desc=""/>
    <constraint field="emis_attir" exp="" desc=""/>
    <constraint field="size_emiss" exp="" desc=""/>
    <constraint field="size_attra" exp="" desc=""/>
    <constraint field="size_total" exp="" desc=""/>
    <constraint field="size_intra" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column type="field" width="100" name="id_zone" hidden="0"/>
      <column type="field" width="128" name="emis" hidden="0"/>
      <column type="field" width="126" name="attir" hidden="0"/>
      <column type="field" width="133" name="emis_attir" hidden="0"/>
      <column type="field" width="127" name="intra" hidden="0"/>
      <column type="field" width="-1" name="size_emiss" hidden="1"/>
      <column type="field" width="-1" name="size_attra" hidden="1"/>
      <column type="field" width="-1" name="size_total" hidden="1"/>
      <column type="field" width="-1" name="size_intra" hidden="1"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="attir" editable="1"/>
    <field name="attires" editable="1"/>
    <field name="attra" editable="1"/>
    <field name="emis" editable="1"/>
    <field name="emis_attir" editable="1"/>
    <field name="emiss" editable="1"/>
    <field name="flux" editable="1"/>
    <field name="id_zone" editable="1"/>
    <field name="intra" editable="1"/>
    <field name="size_attra" editable="1"/>
    <field name="size_emiss" editable="1"/>
    <field name="size_flux" editable="1"/>
    <field name="size_intra" editable="1"/>
    <field name="size_total" editable="1"/>
    <field name="total" editable="1"/>
    <field name="zone" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="attir"/>
    <field labelOnTop="0" name="attires"/>
    <field labelOnTop="0" name="attra"/>
    <field labelOnTop="0" name="emis"/>
    <field labelOnTop="0" name="emis_attir"/>
    <field labelOnTop="0" name="emiss"/>
    <field labelOnTop="0" name="flux"/>
    <field labelOnTop="0" name="id_zone"/>
    <field labelOnTop="0" name="intra"/>
    <field labelOnTop="0" name="size_attra"/>
    <field labelOnTop="0" name="size_emiss"/>
    <field labelOnTop="0" name="size_flux"/>
    <field labelOnTop="0" name="size_intra"/>
    <field labelOnTop="0" name="size_total"/>
    <field labelOnTop="0" name="total"/>
    <field labelOnTop="0" name="zone"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
