<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" maxScale="0" styleCategories="AllStyleCategories" readOnly="0" version="3.10.6-A Coruña">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>"nom_court"</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="siren_siret">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_court">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_long">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="commanditaire">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option name="CheckedState" value="" type="QString"/>
            <Option name="UncheckedState" value="" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prestataire">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option name="CheckedState" value="" type="QString"/>
            <Option name="UncheckedState" value="" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire_route">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option name="CheckedState" value="" type="QString"/>
            <Option name="UncheckedState" value="" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire_tc">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option name="CheckedState" value="" type="QString"/>
            <Option name="UncheckedState" value="" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="SIREN ou SIRET" field="siren_siret" index="0"/>
    <alias name="Nom court (unique)" field="nom_court" index="1"/>
    <alias name="Nom long" field="nom_long" index="2"/>
    <alias name="Commanditaire d'enquêtes ?" field="commanditaire" index="3"/>
    <alias name="Prestataire d'enquêtes ?" field="prestataire" index="4"/>
    <alias name="Gestionnaire route ?" field="gestionnaire_route" index="5"/>
    <alias name="Gestionnaire TC ?" field="gestionnaire_tc" index="6"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="siren_siret" applyOnUpdate="0"/>
    <default expression="" field="nom_court" applyOnUpdate="0"/>
    <default expression="" field="nom_long" applyOnUpdate="0"/>
    <default expression="" field="commanditaire" applyOnUpdate="0"/>
    <default expression="" field="prestataire" applyOnUpdate="0"/>
    <default expression="" field="gestionnaire_route" applyOnUpdate="0"/>
    <default expression="" field="gestionnaire_tc" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" field="siren_siret" notnull_strength="1" exp_strength="0" unique_strength="1"/>
    <constraint constraints="0" field="nom_court" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="nom_long" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="commanditaire" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="prestataire" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="gestionnaire_route" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="gestionnaire_tc" notnull_strength="0" exp_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="siren_siret" desc="" exp=""/>
    <constraint field="nom_court" desc="" exp=""/>
    <constraint field="nom_long" desc="" exp=""/>
    <constraint field="commanditaire" desc="" exp=""/>
    <constraint field="prestataire" desc="" exp=""/>
    <constraint field="gestionnaire_route" desc="" exp=""/>
    <constraint field="gestionnaire_tc" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column name="siren_siret" type="field" width="-1" hidden="0"/>
      <column name="nom_court" type="field" width="-1" hidden="0"/>
      <column name="nom_long" type="field" width="-1" hidden="0"/>
      <column name="commanditaire" type="field" width="-1" hidden="0"/>
      <column name="prestataire" type="field" width="-1" hidden="0"/>
      <column name="gestionnaire_route" type="field" width="-1" hidden="0"/>
      <column name="gestionnaire_tc" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="commanditaire" editable="1"/>
    <field name="gestionnaire_route" editable="1"/>
    <field name="gestionnaire_tc" editable="1"/>
    <field name="nom_court" editable="1"/>
    <field name="nom_long" editable="1"/>
    <field name="prestataire" editable="1"/>
    <field name="siren_siret" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="commanditaire" labelOnTop="0"/>
    <field name="gestionnaire_route" labelOnTop="0"/>
    <field name="gestionnaire_tc" labelOnTop="0"/>
    <field name="nom_court" labelOnTop="0"/>
    <field name="nom_long" labelOnTop="0"/>
    <field name="prestataire" labelOnTop="0"/>
    <field name="siren_siret" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>nom_court</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
