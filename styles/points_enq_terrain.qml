<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" simplifyAlgorithm="0" simplifyMaxScale="1" simplifyDrawingTol="1" simplifyDrawingHints="0" labelsEnabled="1" readOnly="0" styleCategories="AllStyleCategories" simplifyLocal="1" maxScale="0" minScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" symbollevels="0" forceraster="0">
    <symbols>
      <symbol alpha="1" type="marker" force_rhr="0" name="0" clip_to_extent="1">
        <layer class="SimpleMarker" pass="0" locked="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="0,0,0,0" k="color"/>
          <prop v="2" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="arrow" k="name"/>
          <prop v="5,5" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="2" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="angle"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer class="SvgMarker" pass="0" locked="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gU3ZnIFZlY3RvciBJY29ucyA6IGh0dHA6Ly93d3cub25saW5ld2ViZm9udHMuY29tL2ljb24gLS0+Cgo8c3ZnCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAxMDAwIDEwMDAiCiAgIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEwMDAgMTAwMCIKICAgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIKICAgaWQ9InN2ZzQwIgogICBzb2RpcG9kaTpkb2NuYW1lPSIzNjAzNTk5ZWUzMWZjZTEyYmUwZmFmMDk1ODI2MzM1Ni5zdmciCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNSAoMjA2MGVjMWY5ZiwgMjAyMC0wNC0wOCkiPjxkZWZzCiAgIGlkPSJkZWZzNDQiIC8+PHNvZGlwb2RpOm5hbWVkdmlldwogICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICBib3JkZXJvcGFjaXR5PSIxIgogICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICBncmlkdG9sZXJhbmNlPSIxMCIKICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxOTIwIgogICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDE3IgogICBpZD0ibmFtZWR2aWV3NDIiCiAgIHNob3dncmlkPSJmYWxzZSIKICAgaW5rc2NhcGU6em9vbT0iMC4yMzYiCiAgIGlua3NjYXBlOmN4PSI1MDAiCiAgIGlua3NjYXBlOmN5PSI1MDAiCiAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmc0MCIgLz4KPG1ldGFkYXRhCiAgIGlkPSJtZXRhZGF0YTIiPiBTdmcgVmVjdG9yIEljb25zIDogaHR0cDovL3d3dy5vbmxpbmV3ZWJmb250cy5jb20vaWNvbiA8cmRmOlJERj48Y2M6V29yawogICAgIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZQogICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+CjxnCiAgIGlkPSJnMzgiCiAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiPjxnCiAgICAgaWQ9Imc2IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiPjxwYXRoCiAgICAgICBkPSJNNTAwLDYwNi4xYzEyMiwwLDIyNC4xLTQxLjUsMjMwLjMtOTUuNmMtMTguMS01MC45LTM3LjktMTA2LjQtNTcuNC0xNjAuOWMtMTMuNSwzOC45LTg4LDY2LjMtMTcyLjksNjYuM2MtODQuOSwwLTE1OS40LTI3LjQtMTcyLjktNjYuM2MtMTkuNCw1NC41LTM5LjIsMTEwLTU3LjMsMTYwLjlDMjc1LjksNTY0LjYsMzc4LDYwNi4xLDUwMCw2MDYuMXogTTUwMCwyNjMuOGM1Ny4zLDAsMTEwLjYtMTcuOCwxMjYuMi00NS4zYy0yMS41LTYwLjQtNDAtMTEyLjEtNTEuNi0xNDQuOGMtNy44LTIxLjgtNDIuOS0zMy4xLTc0LjYtMzMuMWMtMzEuNywwLTY2LjgsMTEuMy03NC42LDMzLjFjLTExLjYsMzIuNy0zMC4xLDg0LjUtNTEuNiwxNDQuOEMzODkuNCwyNDYuMSw0NDIuNywyNjMuOCw1MDAsMjYzLjh6IE05NDguMiw2NTYuNWwtMTkxLjctNzcuMmwyMi4xLDYxLjZjLTEuMSw2NS4zLTEyNy44LDExNy4zLTI3OC42LDExNy4zYy0xNTAuNywwLTI3Ny41LTUyLTI3OC42LTExNy4zbDIyLjEtNjEuNkw1MS44LDY1Ni41Yy01My43LDIxLjctNTYsNjEuOC01LDg5LjFsMzYwLjUsMTkzLjNjNTEsMjcuMywxMzQuNCwyNy4zLDE4NS40LDBsMzYwLjUtMTkzLjNDMTAwNC4yLDcxOC4zLDEwMDEuOSw2NzguMiw5NDguMiw2NTYuNXoiCiAgICAgICBpZD0icGF0aDQiCiAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjwvZz48ZwogICAgIGlkPSJnOCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcxMCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcxMiIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcxNCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcxNiIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcxOCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcyMCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcyMiIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcyNCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcyNiIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImcyOCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImczMCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImczMiIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImczNCIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjxnCiAgICAgaWQ9ImczNiIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIiAvPjwvZz4KPC9zdmc+" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="5" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontCapitals="0" fontItalic="0" fontWordSpacing="0" previewBkgrdColor="255,255,255,255" fontWeight="50" textOrientation="horizontal" blendMode="0" fontUnderline="0" fontSize="6" fontKerning="1" fontLetterSpacing="0" textOpacity="1" fontStrikeout="0" multilineHeight="1" fontFamily="MS Shell Dlg 2" useSubstitutions="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" namedStyle="Normal" isExpression="1" textColor="0,0,0,255" fontSizeUnit="Point" fieldName=" year(  &quot;date&quot;  )">
        <text-buffer bufferNoFill="1" bufferColor="255,255,255,255" bufferSizeUnits="MM" bufferJoinStyle="128" bufferBlendMode="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferSize="1" bufferDraw="0"/>
        <background shapeOffsetY="0" shapeBorderWidth="0" shapeDraw="0" shapeRotation="0" shapeFillColor="255,255,255,255" shapeSizeX="0" shapeOffsetUnit="MM" shapeSizeUnit="MM" shapeRadiiUnit="MM" shapeBorderColor="128,128,128,255" shapeJoinStyle="64" shapeType="0" shapeRadiiY="0" shapeBorderWidthUnit="MM" shapeOffsetX="0" shapeRadiiX="0" shapeSVGFile="" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeOpacity="1" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeSizeY="0" shapeBlendMode="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0">
          <symbol alpha="1" type="marker" force_rhr="0" name="markerSymbol" clip_to_extent="1">
            <layer class="SimpleMarker" pass="0" locked="0" enabled="1">
              <prop v="0" k="angle"/>
              <prop v="225,89,137,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="circle" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetDist="1" shadowOpacity="0.7" shadowColor="0,0,0,255" shadowRadiusUnit="MM" shadowDraw="0" shadowScale="100" shadowRadiusAlphaOnly="0" shadowBlendMode="6" shadowUnder="0" shadowRadius="1.5" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format plussign="0" addDirectionSymbol="0" multilineAlign="3" leftDirectionSymbol="&lt;" decimals="3" wrapChar="" reverseDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" autoWrapLength="0" placeDirectionSymbol="0" formatNumbers="0"/>
      <placement quadOffset="7" placementFlags="10" xOffset="0" geometryGeneratorEnabled="0" rotationAngle="0" priority="5" repeatDistance="0" geometryGeneratorType="PointGeometry" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" yOffset="3" offsetUnits="MM" offsetType="0" overrunDistanceUnit="MM" centroidWhole="0" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" placement="1" fitInPolygonOnly="0" maxCurvedCharAngleIn="25" centroidInside="0" preserveRotation="1" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" distUnits="MM" layerType="PointGeometry" geometryGenerator="" dist="0" overrunDistance="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleOut="-25"/>
      <rendering fontMaxPixelSize="10000" obstacle="1" mergeLines="0" labelPerPart="0" zIndex="0" drawLabels="1" obstacleType="0" limitNumLabels="0" minFeatureSize="0" scaleMin="0" fontLimitPixelSize="0" scaleVisibility="1" scaleMax="50000" obstacleFactor="1" upsidedownLabels="0" fontMinPixelSize="3" maxNumLabels="2000" displayAll="0"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" name="name" value=""/>
          <Option name="properties"/>
          <Option type="QString" name="type" value="collection"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
          <Option type="bool" name="drawToAllParts" value="false"/>
          <Option type="QString" name="enabled" value="0"/>
          <Option type="QString" name="lineSymbol" value="&lt;symbol alpha=&quot;1&quot; type=&quot;line&quot; force_rhr=&quot;0&quot; name=&quot;symbol&quot; clip_to_extent=&quot;1&quot;>&lt;layer class=&quot;SimpleLine&quot; pass=&quot;0&quot; locked=&quot;0&quot; enabled=&quot;1&quot;>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;60,60,60,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
          <Option type="double" name="minLength" value="0"/>
          <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="minLengthUnit" value="MM"/>
          <Option type="double" name="offsetFromAnchor" value="0"/>
          <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
          <Option type="double" name="offsetFromLabel" value="0"/>
          <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions" value="id"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory rotationOffset="270" minimumSize="0" penColor="#000000" maxScaleDenominator="1e+08" sizeScale="3x:0,0,0,0,0,0" height="15" scaleBasedVisibility="0" backgroundAlpha="255" width="15" sizeType="MM" scaleDependency="Area" backgroundColor="#ffffff" penWidth="0" enabled="0" barWidth="5" opacity="1" minScaleDenominator="0" lineSizeType="MM" diagramOrientation="Up" penAlpha="255" labelPlacementMethod="XHeight" lineSizeScale="3x:0,0,0,0,0,0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" obstacle="0" dist="0" showAll="1" placement="0" zIndex="0" priority="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_poste_ini">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="num_point_ini">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="0"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="angle">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="double" name="Max" value="360"/>
            <Option type="double" name="Min" value="0"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="double" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lib_sens">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date_enq">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="yyyy-MM-dd"/>
            <Option type="QString" name="field_format" value="yyyy-MM-dd"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="route">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_gest">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pr">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="-2147483648"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="abs">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="-2147483648"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_pos">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value=""/>
            <Option type="QString" name="UncheckedState" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_loc">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="protocole">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Distribution enveloppes T" value="1"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Enquête par Inernet des usagers passés au télépéage" value="2"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Feux temporaires" value="3"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Feux permanents" value="4"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Arrêt sur le côté" value="5"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Arrêt pleine voie" value="6"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Enquête embarquée (bateau, train)" value="7"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Déviation du flux par une aire de repos" value="8"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="support">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Papier" value="1"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Electronique" value="2"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enq_vl">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value=""/>
            <Option type="QString" name="UncheckedState" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enq_pl">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value=""/>
            <Option type="QString" name="UncheckedState" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_command">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="siren_siret"/>
            <Option type="QString" name="Layer" value="Entreprises_ou_établissements_edba3fe0_0e47_4ad9_9b21_feae234ecf76"/>
            <Option type="QString" name="LayerName" value="Entreprises ou établissements"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="nom_court"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_amo">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="siren_siret"/>
            <Option type="QString" name="Layer" value="Entreprises_ou_établissements_edba3fe0_0e47_4ad9_9b21_feae234ecf76"/>
            <Option type="QString" name="LayerName" value="Entreprises ou établissements"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="nom_court"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_prest">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="siren_siret"/>
            <Option type="QString" name="Layer" value="Entreprises_ou_établissements_edba3fe0_0e47_4ad9_9b21_feae234ecf76"/>
            <Option type="QString" name="LayerName" value="Entreprises ou établissements"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="nom_court"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_campagne_enq">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Campagnes_enquêtes_67102278_896a_406f_8792_faa0a6c68ded"/>
            <Option type="QString" name="LayerName" value="Campagnes enquêtes"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="nom"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nb_enq">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="30"/>
            <Option type="int" name="Min" value="0"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nb_pers">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="-2147483648"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fiable">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value=""/>
            <Option type="QString" name="UncheckedState" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_point_cpt">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Points_de_comptage_c47c65bf_6a0b_4520_9b43_d3cff8819fc0"/>
            <Option type="QString" name="LayerName" value="Points de comptage"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="id"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pj">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Gestionnaires_des_données_dcfb2a40_710d_463b_9dea_2caa9682039c"/>
            <Option type="QString" name="LayerName" value="Gestionnaires des données"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="nom"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prive">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value=""/>
            <Option type="QString" name="UncheckedState" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name="ID"/>
    <alias field="code_poste_ini" index="1" name="Code poste"/>
    <alias field="num_point_ini" index="2" name="Num point"/>
    <alias field="angle" index="3" name="Angle sens enquêté"/>
    <alias field="lib_sens" index="4" name="Libellé sens enquêté"/>
    <alias field="date_enq" index="5" name="Date"/>
    <alias field="route" index="6" name="Route"/>
    <alias field="id_gest" index="7" name="Gestionnaire route"/>
    <alias field="pr" index="8" name="PR"/>
    <alias field="abs" index="9" name="Abs"/>
    <alias field="prec_pos" index="10" name="Position précise ?"/>
    <alias field="prec_loc" index="11" name="Précision localisation"/>
    <alias field="protocole" index="12" name="Protocole"/>
    <alias field="support" index="13" name="Support collecte"/>
    <alias field="enq_vl" index="14" name="VL ?"/>
    <alias field="enq_pl" index="15" name="PL ?"/>
    <alias field="id_command" index="16" name="Commanditaire"/>
    <alias field="id_amo" index="17" name="AMO"/>
    <alias field="id_prest" index="18" name="Prestataire"/>
    <alias field="id_campagne_enq" index="19" name="Campagne enquêtes"/>
    <alias field="comment" index="20" name="Commentaires"/>
    <alias field="nb_enq" index="21" name="Nb enquêteurs"/>
    <alias field="nb_pers" index="22" name="Nb personnes"/>
    <alias field="fiable" index="23" name="Fiable ?"/>
    <alias field="id_point_cpt" index="24" name="ID point comptage auto"/>
    <alias field="pj" index="25" name="Pièces jointes"/>
    <alias field="gestionnaire" index="26" name="Gestionnaire donnée"/>
    <alias field="prive" index="27" name="Ligne privée ?"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="code_poste_ini" applyOnUpdate="0"/>
    <default expression="1" field="num_point_ini" applyOnUpdate="0"/>
    <default expression="" field="angle" applyOnUpdate="0"/>
    <default expression="" field="lib_sens" applyOnUpdate="0"/>
    <default expression="" field="date_enq" applyOnUpdate="0"/>
    <default expression="" field="route" applyOnUpdate="0"/>
    <default expression="" field="id_gest" applyOnUpdate="0"/>
    <default expression="" field="pr" applyOnUpdate="0"/>
    <default expression="" field="abs" applyOnUpdate="0"/>
    <default expression="" field="prec_pos" applyOnUpdate="0"/>
    <default expression="" field="prec_loc" applyOnUpdate="0"/>
    <default expression="" field="protocole" applyOnUpdate="0"/>
    <default expression="" field="support" applyOnUpdate="0"/>
    <default expression="" field="enq_vl" applyOnUpdate="0"/>
    <default expression="" field="enq_pl" applyOnUpdate="0"/>
    <default expression="" field="id_command" applyOnUpdate="0"/>
    <default expression="" field="id_amo" applyOnUpdate="0"/>
    <default expression="" field="id_prest" applyOnUpdate="0"/>
    <default expression="" field="id_campagne_enq" applyOnUpdate="0"/>
    <default expression="" field="comment" applyOnUpdate="0"/>
    <default expression="" field="nb_enq" applyOnUpdate="0"/>
    <default expression="" field="nb_pers" applyOnUpdate="0"/>
    <default expression="" field="fiable" applyOnUpdate="0"/>
    <default expression="" field="id_point_cpt" applyOnUpdate="0"/>
    <default expression="" field="pj" applyOnUpdate="0"/>
    <default expression="" field="gestionnaire" applyOnUpdate="0"/>
    <default expression="False" field="prive" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="id" unique_strength="1" exp_strength="0" notnull_strength="1" constraints="3"/>
    <constraint field="code_poste_ini" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="num_point_ini" unique_strength="0" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint field="angle" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="lib_sens" unique_strength="0" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint field="date_enq" unique_strength="0" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint field="route" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="id_gest" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="pr" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="abs" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="prec_pos" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="prec_loc" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="protocole" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="support" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="enq_vl" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="enq_pl" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="id_command" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="id_amo" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="id_prest" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="id_campagne_enq" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="comment" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="nb_enq" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="nb_pers" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="fiable" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="id_point_cpt" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="pj" unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint field="gestionnaire" unique_strength="0" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint field="prive" unique_strength="0" exp_strength="0" notnull_strength="1" constraints="1"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" desc="" exp=""/>
    <constraint field="code_poste_ini" desc="" exp=""/>
    <constraint field="num_point_ini" desc="" exp=""/>
    <constraint field="angle" desc="" exp=""/>
    <constraint field="lib_sens" desc="" exp=""/>
    <constraint field="date_enq" desc="" exp=""/>
    <constraint field="route" desc="" exp=""/>
    <constraint field="id_gest" desc="" exp=""/>
    <constraint field="pr" desc="" exp=""/>
    <constraint field="abs" desc="" exp=""/>
    <constraint field="prec_pos" desc="" exp=""/>
    <constraint field="prec_loc" desc="" exp=""/>
    <constraint field="protocole" desc="" exp=""/>
    <constraint field="support" desc="" exp=""/>
    <constraint field="enq_vl" desc="" exp=""/>
    <constraint field="enq_pl" desc="" exp=""/>
    <constraint field="id_command" desc="" exp=""/>
    <constraint field="id_amo" desc="" exp=""/>
    <constraint field="id_prest" desc="" exp=""/>
    <constraint field="id_campagne_enq" desc="" exp=""/>
    <constraint field="comment" desc="" exp=""/>
    <constraint field="nb_enq" desc="" exp=""/>
    <constraint field="nb_pers" desc="" exp=""/>
    <constraint field="fiable" desc="" exp=""/>
    <constraint field="id_point_cpt" desc="" exp=""/>
    <constraint field="pj" desc="" exp=""/>
    <constraint field="gestionnaire" desc="" exp=""/>
    <constraint field="prive" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="&quot;date&quot;">
    <columns>
      <column type="field" hidden="0" name="id" width="62"/>
      <column type="field" hidden="0" name="lib_sens" width="207"/>
      <column type="field" hidden="0" name="angle" width="118"/>
      <column type="field" hidden="0" name="pr" width="50"/>
      <column type="field" hidden="0" name="abs" width="62"/>
      <column type="field" hidden="0" name="prec_pos" width="100"/>
      <column type="field" hidden="0" name="prec_loc" width="524"/>
      <column type="field" hidden="0" name="id_campagne_enq" width="191"/>
      <column type="field" hidden="0" name="comment" width="186"/>
      <column type="field" hidden="0" name="nb_enq" width="94"/>
      <column type="field" hidden="0" name="fiable" width="-1"/>
      <column type="field" hidden="0" name="prive" width="100"/>
      <column type="field" hidden="0" name="gestionnaire" width="165"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="pj" width="-1"/>
      <column type="field" hidden="0" name="support" width="-1"/>
      <column type="field" hidden="0" name="code_poste_ini" width="-1"/>
      <column type="field" hidden="0" name="num_point_ini" width="-1"/>
      <column type="field" hidden="0" name="route" width="-1"/>
      <column type="field" hidden="0" name="id_point_cpt" width="-1"/>
      <column type="field" hidden="0" name="date_enq" width="-1"/>
      <column type="field" hidden="0" name="id_gest" width="-1"/>
      <column type="field" hidden="0" name="protocole" width="-1"/>
      <column type="field" hidden="0" name="enq_vl" width="-1"/>
      <column type="field" hidden="0" name="enq_pl" width="-1"/>
      <column type="field" hidden="0" name="id_command" width="-1"/>
      <column type="field" hidden="0" name="id_amo" width="-1"/>
      <column type="field" hidden="0" name="id_prest" width="-1"/>
      <column type="field" hidden="0" name="nb_pers" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorContainer showLabel="1" visibilityExpressionEnabled="0" visibilityExpression="" columnCount="1" name="Point d'enquête" groupBox="0">
      <attributeEditorField showLabel="1" index="0" name="id"/>
      <attributeEditorField showLabel="1" index="19" name="id_campagne_enq"/>
      <attributeEditorField showLabel="1" index="1" name="code_poste_ini"/>
      <attributeEditorField showLabel="1" index="2" name="num_point_ini"/>
      <attributeEditorField showLabel="1" index="3" name="angle"/>
      <attributeEditorField showLabel="1" index="4" name="lib_sens"/>
      <attributeEditorField showLabel="1" index="5" name="date_enq"/>
      <attributeEditorField showLabel="1" index="6" name="route"/>
      <attributeEditorField showLabel="1" index="7" name="id_gest"/>
      <attributeEditorField showLabel="1" index="8" name="pr"/>
      <attributeEditorField showLabel="1" index="9" name="abs"/>
      <attributeEditorField showLabel="1" index="10" name="prec_pos"/>
      <attributeEditorField showLabel="1" index="11" name="prec_loc"/>
      <attributeEditorField showLabel="1" index="12" name="protocole"/>
      <attributeEditorField showLabel="1" index="13" name="support"/>
      <attributeEditorField showLabel="1" index="14" name="enq_vl"/>
      <attributeEditorField showLabel="1" index="15" name="enq_pl"/>
      <attributeEditorField showLabel="1" index="16" name="id_command"/>
      <attributeEditorField showLabel="1" index="18" name="id_prest"/>
      <attributeEditorField showLabel="1" index="17" name="id_amo"/>
      <attributeEditorField showLabel="1" index="20" name="comment"/>
      <attributeEditorField showLabel="1" index="21" name="nb_enq"/>
      <attributeEditorField showLabel="1" index="22" name="nb_pers"/>
      <attributeEditorField showLabel="1" index="23" name="fiable"/>
      <attributeEditorField showLabel="1" index="24" name="id_point_cpt"/>
      <attributeEditorField showLabel="1" index="-1" name="millesime"/>
      <attributeEditorField showLabel="1" index="26" name="gestionnaire"/>
      <attributeEditorField showLabel="1" index="25" name="pj"/>
      <attributeEditorField showLabel="1" index="27" name="prive"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" visibilityExpressionEnabled="0" visibilityExpression="" columnCount="1" name="Questionnaire" groupBox="0">
      <attributeEditorRelation showLabel="1" relation="questionnaire_point_enq_terrain" name="questionnaire_point_enq_terrain" showUnlinkButton="1" showLinkButton="1"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="abs"/>
    <field editable="1" name="angle"/>
    <field editable="1" name="angle2"/>
    <field editable="0" name="campagne_pj"/>
    <field editable="1" name="code_poste"/>
    <field editable="1" name="code_poste_ini"/>
    <field editable="1" name="comment"/>
    <field editable="1" name="date"/>
    <field editable="1" name="date_enq"/>
    <field editable="1" name="enq_pl"/>
    <field editable="1" name="enq_vl"/>
    <field editable="1" name="exploit_standard"/>
    <field editable="1" name="fiable"/>
    <field editable="1" name="fichier"/>
    <field editable="1" name="geom"/>
    <field editable="1" name="gestionnaire"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_amo"/>
    <field editable="1" name="id_campagne_enq"/>
    <field editable="1" name="id_command"/>
    <field editable="1" name="id_commanditaire"/>
    <field editable="1" name="id_cpt_detail_horaire"/>
    <field editable="1" name="id_gest"/>
    <field editable="1" name="id_gest_cerema"/>
    <field editable="1" name="id_gestionnaire_route"/>
    <field editable="1" name="id_point_cpt"/>
    <field editable="1" name="id_poste"/>
    <field editable="1" name="id_poste_enq"/>
    <field editable="1" name="id_prest"/>
    <field editable="1" name="id_prest_amo"/>
    <field editable="1" name="id_prest_terrain"/>
    <field editable="1" name="id_questionnaire_pl"/>
    <field editable="1" name="id_questionnaire_vl"/>
    <field editable="1" name="id_type_enq"/>
    <field editable="1" name="lib_sens"/>
    <field editable="1" name="millesime"/>
    <field editable="1" name="nb_chef_poste"/>
    <field editable="1" name="nb_enq"/>
    <field editable="1" name="nb_manip_feux"/>
    <field editable="1" name="nb_pers"/>
    <field editable="1" name="nb_rab"/>
    <field editable="1" name="nb_rec"/>
    <field editable="1" name="nom_ini"/>
    <field editable="1" name="nom_route"/>
    <field editable="1" name="num_point"/>
    <field editable="1" name="num_point_ini"/>
    <field editable="1" name="photo1"/>
    <field editable="1" name="photo2"/>
    <field editable="1" name="photo3"/>
    <field editable="1" name="pj"/>
    <field editable="1" name="pj1"/>
    <field editable="1" name="pj2"/>
    <field editable="1" name="pl"/>
    <field editable="1" name="pr"/>
    <field editable="1" name="prec_loc"/>
    <field editable="1" name="prec_pos"/>
    <field editable="1" name="prive"/>
    <field editable="1" name="protocole"/>
    <field editable="1" name="rapport_terrain"/>
    <field editable="1" name="route"/>
    <field editable="1" name="row_owner"/>
    <field editable="1" name="sens_fichier"/>
    <field editable="1" name="support"/>
    <field editable="1" name="type_enq"/>
    <field editable="1" name="vl"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="abs"/>
    <field labelOnTop="0" name="angle"/>
    <field labelOnTop="0" name="angle2"/>
    <field labelOnTop="0" name="campagne_pj"/>
    <field labelOnTop="0" name="code_poste"/>
    <field labelOnTop="0" name="code_poste_ini"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="date"/>
    <field labelOnTop="0" name="date_enq"/>
    <field labelOnTop="0" name="enq_pl"/>
    <field labelOnTop="0" name="enq_vl"/>
    <field labelOnTop="0" name="exploit_standard"/>
    <field labelOnTop="0" name="fiable"/>
    <field labelOnTop="0" name="fichier"/>
    <field labelOnTop="0" name="geom"/>
    <field labelOnTop="0" name="gestionnaire"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_amo"/>
    <field labelOnTop="0" name="id_campagne_enq"/>
    <field labelOnTop="0" name="id_command"/>
    <field labelOnTop="0" name="id_commanditaire"/>
    <field labelOnTop="0" name="id_cpt_detail_horaire"/>
    <field labelOnTop="0" name="id_gest"/>
    <field labelOnTop="0" name="id_gest_cerema"/>
    <field labelOnTop="0" name="id_gestionnaire_route"/>
    <field labelOnTop="0" name="id_point_cpt"/>
    <field labelOnTop="0" name="id_poste"/>
    <field labelOnTop="0" name="id_poste_enq"/>
    <field labelOnTop="0" name="id_prest"/>
    <field labelOnTop="0" name="id_prest_amo"/>
    <field labelOnTop="0" name="id_prest_terrain"/>
    <field labelOnTop="0" name="id_questionnaire_pl"/>
    <field labelOnTop="0" name="id_questionnaire_vl"/>
    <field labelOnTop="0" name="id_type_enq"/>
    <field labelOnTop="0" name="lib_sens"/>
    <field labelOnTop="0" name="millesime"/>
    <field labelOnTop="0" name="nb_chef_poste"/>
    <field labelOnTop="0" name="nb_enq"/>
    <field labelOnTop="0" name="nb_manip_feux"/>
    <field labelOnTop="0" name="nb_pers"/>
    <field labelOnTop="0" name="nb_rab"/>
    <field labelOnTop="0" name="nb_rec"/>
    <field labelOnTop="0" name="nom_ini"/>
    <field labelOnTop="0" name="nom_route"/>
    <field labelOnTop="0" name="num_point"/>
    <field labelOnTop="0" name="num_point_ini"/>
    <field labelOnTop="0" name="photo1"/>
    <field labelOnTop="0" name="photo2"/>
    <field labelOnTop="0" name="photo3"/>
    <field labelOnTop="0" name="pj"/>
    <field labelOnTop="0" name="pj1"/>
    <field labelOnTop="0" name="pj2"/>
    <field labelOnTop="0" name="pl"/>
    <field labelOnTop="0" name="pr"/>
    <field labelOnTop="0" name="prec_loc"/>
    <field labelOnTop="0" name="prec_pos"/>
    <field labelOnTop="0" name="prive"/>
    <field labelOnTop="0" name="protocole"/>
    <field labelOnTop="0" name="rapport_terrain"/>
    <field labelOnTop="0" name="route"/>
    <field labelOnTop="0" name="row_owner"/>
    <field labelOnTop="0" name="sens_fichier"/>
    <field labelOnTop="0" name="support"/>
    <field labelOnTop="0" name="type_enq"/>
    <field labelOnTop="0" name="vl"/>
  </labelOnTop>
  <widgets>
    <widget name="questionnaire_point_enq_terrain">
      <config type="Map">
        <Option type="QString" name="nm-rel" value=""/>
      </config>
    </widget>
  </widgets>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
