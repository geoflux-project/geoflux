<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="0" simplifyDrawingHints="0" simplifyMaxScale="1" simplifyDrawingTol="1" readOnly="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="1" maxScale="0" minScale="500001" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" type="marker" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="196,60,57,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="1"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" backgroundColor="#ffffff" sizeType="MM" penColor="#000000" enabled="0" barWidth="5" penAlpha="255" scaleDependency="Area" rotationOffset="270" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" scaleBasedVisibility="0" width="15" lineSizeType="MM" maxScaleDenominator="1e+08" diagramOrientation="Up" labelPlacementMethod="XHeight" opacity="1" minScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="0" zIndex="0" linePlacementFlags="18" showAll="1" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_chef_lieu">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_commune">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="statut">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insee_com">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insee_arr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="insee_reg">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_epci">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nom_com">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nom_chf">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dep">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom_carto">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id_chef_lieu" name="" index="0"/>
    <alias field="id_commune" name="" index="1"/>
    <alias field="statut" name="" index="2"/>
    <alias field="insee_com" name="" index="3"/>
    <alias field="insee_arr" name="" index="4"/>
    <alias field="insee_reg" name="" index="5"/>
    <alias field="code_epci" name="" index="6"/>
    <alias field="type" name="" index="7"/>
    <alias field="nom_com" name="" index="8"/>
    <alias field="nom_chf" name="" index="9"/>
    <alias field="dep" name="" index="10"/>
    <alias field="geom" name="" index="11"/>
    <alias field="geom_carto" name="" index="12"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id_chef_lieu"/>
    <default applyOnUpdate="0" expression="" field="id_commune"/>
    <default applyOnUpdate="0" expression="" field="statut"/>
    <default applyOnUpdate="0" expression="" field="insee_com"/>
    <default applyOnUpdate="0" expression="" field="insee_arr"/>
    <default applyOnUpdate="0" expression="" field="insee_reg"/>
    <default applyOnUpdate="0" expression="" field="code_epci"/>
    <default applyOnUpdate="0" expression="" field="type"/>
    <default applyOnUpdate="0" expression="" field="nom_com"/>
    <default applyOnUpdate="0" expression="" field="nom_chf"/>
    <default applyOnUpdate="0" expression="" field="dep"/>
    <default applyOnUpdate="0" expression="" field="geom"/>
    <default applyOnUpdate="0" expression="" field="geom_carto"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id_chef_lieu" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_commune" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="statut" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="insee_com" unique_strength="1" constraints="3" notnull_strength="1"/>
    <constraint exp_strength="0" field="insee_arr" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="insee_reg" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="code_epci" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="type" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nom_com" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nom_chf" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="dep" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="geom" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="geom_carto" unique_strength="0" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id_chef_lieu" desc=""/>
    <constraint exp="" field="id_commune" desc=""/>
    <constraint exp="" field="statut" desc=""/>
    <constraint exp="" field="insee_com" desc=""/>
    <constraint exp="" field="insee_arr" desc=""/>
    <constraint exp="" field="insee_reg" desc=""/>
    <constraint exp="" field="code_epci" desc=""/>
    <constraint exp="" field="type" desc=""/>
    <constraint exp="" field="nom_com" desc=""/>
    <constraint exp="" field="nom_chf" desc=""/>
    <constraint exp="" field="dep" desc=""/>
    <constraint exp="" field="geom" desc=""/>
    <constraint exp="" field="geom_carto" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="-1" hidden="0" type="field" name="id_chef_lieu"/>
      <column width="-1" hidden="0" type="field" name="id_commune"/>
      <column width="-1" hidden="0" type="field" name="statut"/>
      <column width="-1" hidden="0" type="field" name="insee_com"/>
      <column width="-1" hidden="0" type="field" name="insee_arr"/>
      <column width="-1" hidden="0" type="field" name="insee_reg"/>
      <column width="-1" hidden="0" type="field" name="code_epci"/>
      <column width="-1" hidden="0" type="field" name="type"/>
      <column width="-1" hidden="0" type="field" name="nom_com"/>
      <column width="-1" hidden="0" type="field" name="nom_chf"/>
      <column width="-1" hidden="0" type="field" name="dep"/>
      <column width="-1" hidden="0" type="field" name="geom"/>
      <column width="-1" hidden="0" type="field" name="geom_carto"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="code_epci" editable="1"/>
    <field name="dep" editable="1"/>
    <field name="geom" editable="1"/>
    <field name="geom_carto" editable="1"/>
    <field name="id_chef_lieu" editable="1"/>
    <field name="id_commune" editable="1"/>
    <field name="insee_arr" editable="1"/>
    <field name="insee_com" editable="1"/>
    <field name="insee_reg" editable="1"/>
    <field name="nom_chf" editable="1"/>
    <field name="nom_com" editable="1"/>
    <field name="statut" editable="1"/>
    <field name="type" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="code_epci"/>
    <field labelOnTop="0" name="dep"/>
    <field labelOnTop="0" name="geom"/>
    <field labelOnTop="0" name="geom_carto"/>
    <field labelOnTop="0" name="id_chef_lieu"/>
    <field labelOnTop="0" name="id_commune"/>
    <field labelOnTop="0" name="insee_arr"/>
    <field labelOnTop="0" name="insee_com"/>
    <field labelOnTop="0" name="insee_reg"/>
    <field labelOnTop="0" name="nom_chf"/>
    <field labelOnTop="0" name="nom_com"/>
    <field labelOnTop="0" name="statut"/>
    <field labelOnTop="0" name="type"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_chef_lieu</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
