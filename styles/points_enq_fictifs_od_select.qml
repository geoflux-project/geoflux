<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="1" simplifyDrawingHints="0" simplifyMaxScale="1" simplifyDrawingTol="1" readOnly="0" simplifyLocal="0" hasScaleBasedVisibilityFlag="0" maxScale="100000" minScale="1e+08" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" type="marker" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="137,102,60,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="arrow"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="178,223,138,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.8"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="area"/>
          <prop k="size" v="7"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option value="true" type="bool" name="active"/>
                  <Option value="angle" type="QString" name="field"/>
                  <Option value="2" type="int" name="type"/>
                </Option>
              </Option>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style multilineHeight="1" fontWordSpacing="0" blendMode="0" previewBkgrdColor="255,255,255,255" fontWeight="50" namedStyle="Normal" fontSize="8.25" fontUnderline="0" textOrientation="horizontal" fieldName="nom_route_nom_route_court" fontKerning="1" textOpacity="1" fontStrikeout="0" isExpression="1" useSubstitutions="0" fontFamily="MS Shell Dlg 2" fontLetterSpacing="0" fontSizeUnit="Point" fontCapitals="0" textColor="210,143,34,255" fontItalic="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0">
        <text-buffer bufferDraw="0" bufferNoFill="0" bufferColor="255,255,255,255" bufferJoinStyle="64" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM" bufferBlendMode="0" bufferSize="1"/>
        <background shapeSVGFile="" shapeRotation="0" shapeRotationType="0" shapeType="0" shapeSizeY="0" shapeOffsetUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOffsetY="0" shapeSizeX="0" shapeBlendMode="0" shapeSizeUnit="MM" shapeJoinStyle="64" shapeRadiiX="0" shapeRadiiY="0" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderColor="128,128,128,255" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeRadiiUnit="MM" shapeOffsetX="0" shapeBorderWidth="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1">
          <symbol force_rhr="0" type="marker" clip_to_extent="1" name="markerSymbol" alpha="1">
            <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
              <prop k="angle" v="0"/>
              <prop k="color" v="133,182,111,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowUnder="0" shadowColor="0,0,0,255" shadowOffsetDist="1" shadowScale="100" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.7" shadowDraw="0" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowBlendMode="6" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowOffsetAngle="135" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" wrapChar="" reverseDirectionSymbol="0" plussign="0" useMaxLineLengthForAutoWrap="1" autoWrapLength="0" placeDirectionSymbol="0" multilineAlign="0" formatNumbers="0" decimals="3" addDirectionSymbol="0" rightDirectionSymbol=">"/>
      <placement geometryGeneratorType="PointGeometry" dist="2.1" geometryGenerator="" xOffset="0" repeatDistanceUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" yOffset="0" centroidWhole="0" offsetType="0" placementFlags="10" distMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" maxCurvedCharAngleIn="20" offsetUnits="MapUnit" preserveRotation="1" quadOffset="4" priority="5" overrunDistanceUnit="MM" placement="0" maxCurvedCharAngleOut="-20" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" layerType="PointGeometry" centroidInside="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" geometryGeneratorEnabled="0" rotationAngle="0" distUnits="MM"/>
      <rendering minFeatureSize="0" scaleMax="10000000" maxNumLabels="2000" fontMinPixelSize="3" fontMaxPixelSize="10000" labelPerPart="0" displayAll="0" drawLabels="1" obstacleFactor="1" zIndex="0" obstacle="1" upsidedownLabels="0" obstacleType="0" mergeLines="0" fontLimitPixelSize="0" limitNumLabels="0" scaleVisibility="0" scaleMin="1"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
          <Option type="Map" name="ddProperties">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
          <Option value="false" type="bool" name="drawToAllParts"/>
          <Option value="0" type="QString" name="enabled"/>
          <Option value="&lt;symbol force_rhr=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; enabled=&quot;1&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
          <Option value="0" type="double" name="minLength"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
          <Option value="MM" type="QString" name="minLengthUnit"/>
          <Option value="0" type="double" name="offsetFromAnchor"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
          <Option value="0" type="double" name="offsetFromLabel"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property value="COALESCE( &quot;id&quot;, '&lt;NULL>' ) ||' - Point n°' || &quot;num_point&quot;  || ' - Sens : ' || COALESCE(&quot;lib_sens&quot;,'&lt;NULL>') || ' - Localisation : ' || COALESCE(&quot;prec_loc&quot;, '&lt;NULL>')  || ' - ' ||   COALESCE(&quot;nom_route&quot; , '&lt;NULL>')" key="dualview/previewExpressions"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" backgroundColor="#ffffff" sizeType="MM" penColor="#000000" enabled="0" barWidth="5" penAlpha="255" scaleDependency="Area" rotationOffset="270" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" scaleBasedVisibility="0" width="15" lineSizeType="MM" maxScaleDenominator="1e+08" diagramOrientation="Up" labelPlacementMethod="XHeight" opacity="1" minScaleDenominator="100000" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="0" zIndex="0" linePlacementFlags="2" showAll="1" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_ini">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="num_point_ini">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="angle">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="360" type="double" name="Max"/>
            <Option value="-360" type="double" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="0.1" type="double" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lib_sens">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_route">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_loc">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_commanditaire">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_cpt_detail_horaire">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="prive">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" type="QString" name="CheckedState"/>
            <Option value="" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="Identifiant" index="0"/>
    <alias field="nom_ini" name="" index="1"/>
    <alias field="num_point_ini" name="" index="2"/>
    <alias field="angle" name="Angle du sens sur la carte" index="3"/>
    <alias field="lib_sens" name="Libellé du sens reconstitué" index="4"/>
    <alias field="nom_route" name="Nom de la route" index="5"/>
    <alias field="prec_loc" name="Précision de localisation" index="6"/>
    <alias field="id_commanditaire" name="" index="7"/>
    <alias field="id_cpt_detail_horaire" name="Identifiant du comptage horaire" index="8"/>
    <alias field="comment" name="Commentaires" index="9"/>
    <alias field="gestionnaire" name="" index="10"/>
    <alias field="prive" name="Ligne privée" index="11"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="nom_ini"/>
    <default applyOnUpdate="0" expression="" field="num_point_ini"/>
    <default applyOnUpdate="0" expression="" field="angle"/>
    <default applyOnUpdate="0" expression="" field="lib_sens"/>
    <default applyOnUpdate="0" expression="" field="nom_route"/>
    <default applyOnUpdate="0" expression="" field="prec_loc"/>
    <default applyOnUpdate="0" expression="" field="id_commanditaire"/>
    <default applyOnUpdate="0" expression="" field="id_cpt_detail_horaire"/>
    <default applyOnUpdate="0" expression="" field="comment"/>
    <default applyOnUpdate="0" expression="" field="gestionnaire"/>
    <default applyOnUpdate="0" expression="" field="prive"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id" unique_strength="1" constraints="3" notnull_strength="1"/>
    <constraint exp_strength="0" field="nom_ini" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="num_point_ini" unique_strength="0" constraints="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="angle" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="lib_sens" unique_strength="0" constraints="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="nom_route" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="prec_loc" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_commanditaire" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_cpt_detail_horaire" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="comment" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="gestionnaire" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="prive" unique_strength="0" constraints="1" notnull_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="nom_ini" desc=""/>
    <constraint exp="" field="num_point_ini" desc=""/>
    <constraint exp="" field="angle" desc=""/>
    <constraint exp="" field="lib_sens" desc=""/>
    <constraint exp="" field="nom_route" desc=""/>
    <constraint exp="" field="prec_loc" desc=""/>
    <constraint exp="" field="id_commanditaire" desc=""/>
    <constraint exp="" field="id_cpt_detail_horaire" desc=""/>
    <constraint exp="" field="comment" desc=""/>
    <constraint exp="" field="gestionnaire" desc=""/>
    <constraint exp="" field="prive" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
    <actionsetting icon="" isEnabledOnlyWhenEditable="0" id="{11a98ca0-60b1-49fd-ad12-56a90a66dc62}" action="echo &quot;[% &quot;MY_FIELD&quot; %]&quot;" capture="1" type="0" name="Affiche la valeur de l'attribut" notificationMessage="" shortTitle="">
      <actionScope id="Field"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting icon="" isEnabledOnlyWhenEditable="0" id="{43dab254-5b88-4d4b-a328-bb53a6ead77b}" action="ogr2ogr -f &quot;ESRI Shapefile&quot; &quot;[% &quot;OUTPUT_PATH&quot; %]&quot; &quot;[% &quot;INPUT_FILE&quot; %]&quot;" capture="1" type="0" name="Lancer une application" notificationMessage="" shortTitle="">
      <actionScope id="Field"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting icon="" isEnabledOnlyWhenEditable="0" id="{e81e6b56-b172-41d9-b353-9e2966486f59}" action="QtGui.QMessageBox.information(None, &quot;Feature id&quot;, &quot;feature id is [% $id %]&quot;)" capture="0" type="1" name="Récupère l'id de l'entité" notificationMessage="" shortTitle="">
      <actionScope id="Field"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting icon="" isEnabledOnlyWhenEditable="0" id="{2876553b-b43f-4b37-acd5-02363d2ebd03}" action="QtGui.QMessageBox.information(None, &quot;Current field's value&quot;, &quot;[% @current_field %]&quot;)" capture="0" type="1" name="Valeur du champ sélectionné (Outil d'identification des entités) " notificationMessage="" shortTitle="">
      <actionScope id="Field"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting icon="" isEnabledOnlyWhenEditable="0" id="{79f774dc-986e-4fb2-b6d7-67a12d3bfed5}" action="QtGui.QMessageBox.information(None, &quot;Clicked coords&quot;, &quot;layer: [% @layer_id %]\ncoords: ([% @click_x %],[% @click_y %])&quot;)" capture="0" type="1" name="Coordonées du clic (outil d'exécution d'action sur entité) " notificationMessage="" shortTitle="">
      <actionScope id="Field"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting icon="" isEnabledOnlyWhenEditable="0" id="{c6babc04-c4c0-4a95-9d1f-71d94e63856f}" action="[% &quot;PATH&quot; %]" capture="0" type="5" name="Ouvrir fichier" notificationMessage="" shortTitle="">
      <actionScope id="Field"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting icon="" isEnabledOnlyWhenEditable="0" id="{ac90e54b-6c26-4124-b700-6de4d6e47c94}" action="http://www.google.com/search?q=[% &quot;ATTRIBUTE&quot; %]" capture="0" type="5" name="Recherche web basée sur la valeur de l'attribut" notificationMessage="" shortTitle="">
      <actionScope id="Field"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id&quot;" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="93" hidden="0" type="field" name="id"/>
      <column width="35" hidden="0" type="field" name="angle"/>
      <column width="431" hidden="0" type="field" name="prec_loc"/>
      <column width="-1" hidden="1" type="actions"/>
      <column width="411" hidden="0" type="field" name="lib_sens"/>
      <column width="134" hidden="0" type="field" name="id_cpt_detail_horaire"/>
      <column width="-1" hidden="0" type="field" name="nom_route"/>
      <column width="-1" hidden="0" type="field" name="comment"/>
      <column width="-1" hidden="0" type="field" name="prive"/>
      <column width="-1" hidden="0" type="field" name="nom_ini"/>
      <column width="-1" hidden="0" type="field" name="num_point_ini"/>
      <column width="-1" hidden="0" type="field" name="id_commanditaire"/>
      <column width="-1" hidden="0" type="field" name="gestionnaire"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C16LM0066-BDD_transport_mutualisee/Documents techniques/Migration_geoflux</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorContainer showLabel="1" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" name="Identité de l'enquête" visibilityExpression="">
      <attributeEditorField showLabel="1" name="id" index="0"/>
      <attributeEditorField showLabel="1" name="id_poste_enq" index="-1"/>
      <attributeEditorField showLabel="1" name="num_point" index="-1"/>
      <attributeEditorField showLabel="1" name="comment" index="9"/>
      <attributeEditorField showLabel="1" name="row_owner" index="-1"/>
      <attributeEditorField showLabel="1" name="prive" index="11"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" name="Localisation géographique" visibilityExpression="">
      <attributeEditorField showLabel="1" name="nom_route" index="5"/>
      <attributeEditorField showLabel="1" name="prec_loc" index="6"/>
      <attributeEditorField showLabel="1" name="lib_sens" index="4"/>
      <attributeEditorField showLabel="1" name="angle" index="3"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" name="Comptages associés" visibilityExpression="">
      <attributeEditorField showLabel="1" name="id_cpt_detail_horaire" index="8"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="angle" editable="1"/>
    <field name="comment" editable="1"/>
    <field name="gestionnaire" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_commanditaire" editable="1"/>
    <field name="id_cpt_detail_horaire" editable="1"/>
    <field name="id_poste" editable="1"/>
    <field name="id_poste_enq" editable="1"/>
    <field name="id_producteur" editable="1"/>
    <field name="lib_sens" editable="1"/>
    <field name="nom_ini" editable="1"/>
    <field name="nom_route" editable="1"/>
    <field name="num_point" editable="1"/>
    <field name="num_point_ini" editable="1"/>
    <field name="prec_loc" editable="1"/>
    <field name="prive" editable="1"/>
    <field name="row_owner" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="angle"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="gestionnaire"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_commanditaire"/>
    <field labelOnTop="0" name="id_cpt_detail_horaire"/>
    <field labelOnTop="0" name="id_poste"/>
    <field labelOnTop="0" name="id_poste_enq"/>
    <field labelOnTop="0" name="id_producteur"/>
    <field labelOnTop="0" name="lib_sens"/>
    <field labelOnTop="0" name="nom_ini"/>
    <field labelOnTop="0" name="nom_route"/>
    <field labelOnTop="0" name="num_point"/>
    <field labelOnTop="0" name="num_point_ini"/>
    <field labelOnTop="0" name="prec_loc"/>
    <field labelOnTop="0" name="prive"/>
    <field labelOnTop="0" name="row_owner"/>
  </labelOnTop>
  <widgets>
    <widget name="nom_route_nom_route">
      <config/>
    </widget>
  </widgets>
  <previewExpression>COALESCE( "id", '&lt;NULL>' ) ||' - Point n°' || "num_point"  || ' - Sens : ' || COALESCE("lib_sens",'&lt;NULL>') || ' - Localisation : ' || COALESCE("prec_loc", '&lt;NULL>')  || ' - ' ||   COALESCE("nom_route" , '&lt;NULL>')</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
