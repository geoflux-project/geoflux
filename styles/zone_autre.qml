<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="1" simplifyDrawingHints="1" simplifyMaxScale="1" simplifyDrawingTol="1" readOnly="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" type="fill" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="191,134,99,0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="227,26,28,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.46"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style multilineHeight="1" fontWordSpacing="0" blendMode="0" previewBkgrdColor="255,255,255,255" fontWeight="50" namedStyle="Normal" fontSize="8.25" fontUnderline="0" textOrientation="horizontal" fieldName="admin" fontKerning="1" textOpacity="1" fontStrikeout="0" isExpression="1" useSubstitutions="0" fontFamily="MS Shell Dlg 2" fontLetterSpacing="0" fontSizeUnit="Point" fontCapitals="0" textColor="227,26,28,255" fontItalic="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0">
        <text-buffer bufferDraw="0" bufferNoFill="0" bufferColor="255,255,255,255" bufferJoinStyle="128" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM" bufferBlendMode="0" bufferSize="1"/>
        <background shapeSVGFile="" shapeRotation="0" shapeRotationType="0" shapeType="0" shapeSizeY="0" shapeOffsetUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOffsetY="0" shapeSizeX="0" shapeBlendMode="0" shapeSizeUnit="MM" shapeJoinStyle="64" shapeRadiiX="0" shapeRadiiY="0" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderColor="128,128,128,255" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeRadiiUnit="MM" shapeOffsetX="0" shapeBorderWidth="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1">
          <symbol force_rhr="0" type="marker" clip_to_extent="1" name="markerSymbol" alpha="1">
            <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
              <prop k="angle" v="0"/>
              <prop k="color" v="190,207,80,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowUnder="0" shadowColor="0,0,0,255" shadowOffsetDist="1" shadowScale="100" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.7" shadowDraw="0" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowBlendMode="6" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowOffsetAngle="135" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" wrapChar="" reverseDirectionSymbol="0" plussign="0" useMaxLineLengthForAutoWrap="1" autoWrapLength="0" placeDirectionSymbol="0" multilineAlign="4294967295" formatNumbers="0" decimals="3" addDirectionSymbol="0" rightDirectionSymbol=">"/>
      <placement geometryGeneratorType="PointGeometry" dist="0" geometryGenerator="" xOffset="0" repeatDistanceUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" yOffset="0" centroidWhole="0" offsetType="0" placementFlags="10" distMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" maxCurvedCharAngleIn="25" offsetUnits="MapUnit" preserveRotation="1" quadOffset="4" priority="5" overrunDistanceUnit="MM" placement="1" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" layerType="PolygonGeometry" centroidInside="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" geometryGeneratorEnabled="0" rotationAngle="0" distUnits="MM"/>
      <rendering minFeatureSize="0" scaleMax="10000000" maxNumLabels="2000" fontMinPixelSize="3" fontMaxPixelSize="10000" labelPerPart="0" displayAll="0" drawLabels="1" obstacleFactor="1" zIndex="0" obstacle="1" upsidedownLabels="0" obstacleType="0" mergeLines="0" fontLimitPixelSize="0" limitNumLabels="0" scaleVisibility="0" scaleMin="1"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
          <Option type="Map" name="ddProperties">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
          <Option value="false" type="bool" name="drawToAllParts"/>
          <Option value="0" type="QString" name="enabled"/>
          <Option value="&lt;symbol force_rhr=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; enabled=&quot;1&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
          <Option value="0" type="double" name="minLength"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
          <Option value="MM" type="QString" name="minLengthUnit"/>
          <Option value="0" type="double" name="offsetFromAnchor"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
          <Option value="0" type="double" name="offsetFromLabel"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" backgroundColor="#ffffff" sizeType="MM" penColor="#000000" enabled="0" barWidth="5" penAlpha="255" scaleDependency="Area" rotationOffset="270" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" scaleBasedVisibility="0" width="15" lineSizeType="MM" maxScaleDenominator="1e+08" diagramOrientation="Up" labelPlacementMethod="XHeight" opacity="1" minScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="0" zIndex="0" linePlacementFlags="2" showAll="1" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id" unique_strength="1" constraints="3" notnull_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="-1" hidden="1" type="actions"/>
      <column width="-1" hidden="0" type="field" name="id"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="abbrev" editable="1"/>
    <field name="abbrev_len" editable="1"/>
    <field name="adm0_a3" editable="1"/>
    <field name="adm0_a3_is" editable="1"/>
    <field name="adm0_a3_un" editable="1"/>
    <field name="adm0_a3_us" editable="1"/>
    <field name="adm0_a3_wb" editable="1"/>
    <field name="adm0_dif" editable="1"/>
    <field name="admin" editable="1"/>
    <field name="brk_a3" editable="1"/>
    <field name="brk_diff" editable="1"/>
    <field name="brk_group" editable="1"/>
    <field name="brk_name" editable="1"/>
    <field name="cntr_code" editable="1"/>
    <field name="continent" editable="1"/>
    <field name="economy" editable="1"/>
    <field name="featurecla" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="fips_10_" editable="1"/>
    <field name="formal_en" editable="1"/>
    <field name="formal_fr" editable="1"/>
    <field name="gdp_md_est" editable="1"/>
    <field name="gdp_year" editable="1"/>
    <field name="geou_dif" editable="1"/>
    <field name="geounit" editable="1"/>
    <field name="gu_a3" editable="1"/>
    <field name="homepart" editable="1"/>
    <field name="id" editable="1"/>
    <field name="income_grp" editable="1"/>
    <field name="iso_a2" editable="1"/>
    <field name="iso_a3" editable="1"/>
    <field name="iso_n3" editable="1"/>
    <field name="labelrank" editable="1"/>
    <field name="lastcensus" editable="1"/>
    <field name="level" editable="1"/>
    <field name="levl_code" editable="1"/>
    <field name="long_len" editable="1"/>
    <field name="mapcolor13" editable="1"/>
    <field name="mapcolor7" editable="1"/>
    <field name="mapcolor8" editable="1"/>
    <field name="mapcolor9" editable="1"/>
    <field name="name" editable="1"/>
    <field name="name_alt" editable="1"/>
    <field name="name_len" editable="1"/>
    <field name="name_long" editable="1"/>
    <field name="name_sort" editable="1"/>
    <field name="note_adm0" editable="1"/>
    <field name="note_brk" editable="1"/>
    <field name="nuts_id" editable="1"/>
    <field name="nuts_name" editable="1"/>
    <field name="pop_est" editable="1"/>
    <field name="pop_year" editable="1"/>
    <field name="postal" editable="1"/>
    <field name="region_un" editable="1"/>
    <field name="region_wb" editable="1"/>
    <field name="scalerank" editable="1"/>
    <field name="sov_a3" editable="1"/>
    <field name="sovereignt" editable="1"/>
    <field name="su_a3" editable="1"/>
    <field name="su_dif" editable="1"/>
    <field name="subregion" editable="1"/>
    <field name="subunit" editable="1"/>
    <field name="tiny" editable="1"/>
    <field name="type" editable="1"/>
    <field name="un_a3" editable="1"/>
    <field name="wb_a2" editable="1"/>
    <field name="wb_a3" editable="1"/>
    <field name="wikipedia" editable="1"/>
    <field name="woe_id" editable="1"/>
    <field name="woe_id_eh" editable="1"/>
    <field name="woe_note" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="abbrev"/>
    <field labelOnTop="0" name="abbrev_len"/>
    <field labelOnTop="0" name="adm0_a3"/>
    <field labelOnTop="0" name="adm0_a3_is"/>
    <field labelOnTop="0" name="adm0_a3_un"/>
    <field labelOnTop="0" name="adm0_a3_us"/>
    <field labelOnTop="0" name="adm0_a3_wb"/>
    <field labelOnTop="0" name="adm0_dif"/>
    <field labelOnTop="0" name="admin"/>
    <field labelOnTop="0" name="brk_a3"/>
    <field labelOnTop="0" name="brk_diff"/>
    <field labelOnTop="0" name="brk_group"/>
    <field labelOnTop="0" name="brk_name"/>
    <field labelOnTop="0" name="cntr_code"/>
    <field labelOnTop="0" name="continent"/>
    <field labelOnTop="0" name="economy"/>
    <field labelOnTop="0" name="featurecla"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="fips_10_"/>
    <field labelOnTop="0" name="formal_en"/>
    <field labelOnTop="0" name="formal_fr"/>
    <field labelOnTop="0" name="gdp_md_est"/>
    <field labelOnTop="0" name="gdp_year"/>
    <field labelOnTop="0" name="geou_dif"/>
    <field labelOnTop="0" name="geounit"/>
    <field labelOnTop="0" name="gu_a3"/>
    <field labelOnTop="0" name="homepart"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="income_grp"/>
    <field labelOnTop="0" name="iso_a2"/>
    <field labelOnTop="0" name="iso_a3"/>
    <field labelOnTop="0" name="iso_n3"/>
    <field labelOnTop="0" name="labelrank"/>
    <field labelOnTop="0" name="lastcensus"/>
    <field labelOnTop="0" name="level"/>
    <field labelOnTop="0" name="levl_code"/>
    <field labelOnTop="0" name="long_len"/>
    <field labelOnTop="0" name="mapcolor13"/>
    <field labelOnTop="0" name="mapcolor7"/>
    <field labelOnTop="0" name="mapcolor8"/>
    <field labelOnTop="0" name="mapcolor9"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="name_alt"/>
    <field labelOnTop="0" name="name_len"/>
    <field labelOnTop="0" name="name_long"/>
    <field labelOnTop="0" name="name_sort"/>
    <field labelOnTop="0" name="note_adm0"/>
    <field labelOnTop="0" name="note_brk"/>
    <field labelOnTop="0" name="nuts_id"/>
    <field labelOnTop="0" name="nuts_name"/>
    <field labelOnTop="0" name="pop_est"/>
    <field labelOnTop="0" name="pop_year"/>
    <field labelOnTop="0" name="postal"/>
    <field labelOnTop="0" name="region_un"/>
    <field labelOnTop="0" name="region_wb"/>
    <field labelOnTop="0" name="scalerank"/>
    <field labelOnTop="0" name="sov_a3"/>
    <field labelOnTop="0" name="sovereignt"/>
    <field labelOnTop="0" name="su_a3"/>
    <field labelOnTop="0" name="su_dif"/>
    <field labelOnTop="0" name="subregion"/>
    <field labelOnTop="0" name="subunit"/>
    <field labelOnTop="0" name="tiny"/>
    <field labelOnTop="0" name="type"/>
    <field labelOnTop="0" name="un_a3"/>
    <field labelOnTop="0" name="wb_a2"/>
    <field labelOnTop="0" name="wb_a3"/>
    <field labelOnTop="0" name="wikipedia"/>
    <field labelOnTop="0" name="woe_id"/>
    <field labelOnTop="0" name="woe_id_eh"/>
    <field labelOnTop="0" name="woe_note"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>nuts_name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
