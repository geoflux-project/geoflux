<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" maxScale="0" readOnly="0" styleCategories="AllStyleCategories" labelsEnabled="0" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="0" simplifyAlgorithm="0" minScale="1e+08" simplifyMaxScale="1" simplifyDrawingTol="1" simplifyLocal="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" type="singleSymbol" forceraster="0" enableorderby="0">
    <symbols>
      <symbol alpha="1" type="marker" clip_to_extent="1" name="0" force_rhr="0">
        <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
          <prop v="0" k="angle"/>
          <prop v="0,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="141,141,141,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="1" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="size">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="10* &quot;size_total&quot;" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="id_zone" key="dualview/previewExpressions"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory rotationOffset="270" backgroundAlpha="255" height="15" enabled="0" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" minScaleDenominator="0" diagramOrientation="Up" barWidth="5" penAlpha="255" scaleBasedVisibility="0" maxScaleDenominator="1e+08" lineSizeType="MM" sizeType="MM" labelPlacementMethod="XHeight" width="15" scaleDependency="Area" penWidth="0" penColor="#000000" sizeScale="3x:0,0,0,0,0,0" opacity="1" backgroundColor="#ffffff">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" obstacle="0" placement="0" priority="0" dist="0" showAll="1" linePlacementFlags="18">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emiss">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attra">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="total">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="size_emiss">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="size_attra">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="size_total">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id_zone"/>
    <alias index="1" name="" field="emiss"/>
    <alias index="2" name="" field="attra"/>
    <alias index="3" name="" field="total"/>
    <alias index="4" name="" field="size_emiss"/>
    <alias index="5" name="" field="size_attra"/>
    <alias index="6" name="" field="size_total"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id_zone"/>
    <default expression="" applyOnUpdate="0" field="emiss"/>
    <default expression="" applyOnUpdate="0" field="attra"/>
    <default expression="" applyOnUpdate="0" field="total"/>
    <default expression="" applyOnUpdate="0" field="size_emiss"/>
    <default expression="" applyOnUpdate="0" field="size_attra"/>
    <default expression="" applyOnUpdate="0" field="size_total"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="id_zone" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="emiss" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="attra" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="total" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="size_emiss" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="size_attra" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="size_total" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id_zone"/>
    <constraint desc="" exp="" field="emiss"/>
    <constraint desc="" exp="" field="attra"/>
    <constraint desc="" exp="" field="total"/>
    <constraint desc="" exp="" field="size_emiss"/>
    <constraint desc="" exp="" field="size_attra"/>
    <constraint desc="" exp="" field="size_total"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" hidden="0" name="id_zone" width="-1"/>
      <column type="field" hidden="0" name="total" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="emiss" width="-1"/>
      <column type="field" hidden="0" name="attra" width="-1"/>
      <column type="field" hidden="0" name="size_emiss" width="-1"/>
      <column type="field" hidden="0" name="size_attra" width="-1"/>
      <column type="field" hidden="0" name="size_total" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="attires"/>
    <field editable="1" name="attra"/>
    <field editable="1" name="emis"/>
    <field editable="1" name="emiss"/>
    <field editable="1" name="id_zone"/>
    <field editable="1" name="size_attra"/>
    <field editable="1" name="size_emiss"/>
    <field editable="1" name="size_total"/>
    <field editable="1" name="total"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="attires"/>
    <field labelOnTop="0" name="attra"/>
    <field labelOnTop="0" name="emis"/>
    <field labelOnTop="0" name="emiss"/>
    <field labelOnTop="0" name="id_zone"/>
    <field labelOnTop="0" name="size_attra"/>
    <field labelOnTop="0" name="size_emiss"/>
    <field labelOnTop="0" name="size_total"/>
    <field labelOnTop="0" name="total"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
