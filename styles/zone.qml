<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" styleCategories="AllStyleCategories" maxScale="0" simplifyDrawingHints="1" simplifyMaxScale="1" version="3.10.6-A Coruña" readOnly="0" minScale="1e+08" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="1" simplifyLocal="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" symbollevels="0" forceraster="0">
    <symbols>
      <symbol type="fill" name="0" clip_to_extent="1" alpha="1" force_rhr="0">
        <layer locked="0" pass="0" class="SimpleFill" enabled="1">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="206,206,206,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontSizeUnit="Point" fontUnderline="0" textColor="0,0,0,255" fontSize="6" previewBkgrdColor="255,255,255,255" fontStrikeout="0" multilineHeight="1" fontLetterSpacing="0" namedStyle="Normal" fieldName="  concat( to_string(code_zone), ' - ', nom)" textOpacity="1" fontFamily="MS Shell Dlg 2" fontWordSpacing="0" fontKerning="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontWeight="50" fontItalic="0" fontCapitals="0" textOrientation="horizontal" blendMode="0" useSubstitutions="0" isExpression="1">
        <text-buffer bufferOpacity="1" bufferNoFill="1" bufferColor="255,255,255,255" bufferDraw="0" bufferBlendMode="0" bufferJoinStyle="128" bufferSize="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM"/>
        <background shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="MM" shapeSizeX="0" shapeSizeY="0" shapeOpacity="1" shapeBorderWidthUnit="MM" shapeOffsetY="0" shapeBorderColor="128,128,128,255" shapeBorderWidth="0" shapeSVGFile="" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeJoinStyle="64" shapeFillColor="255,255,255,255" shapeDraw="0" shapeBlendMode="0" shapeRadiiX="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeOffsetX="0" shapeSizeUnit="MM" shapeRadiiUnit="MM" shapeRotation="0" shapeRadiiY="0" shapeType="0">
          <symbol type="marker" name="markerSymbol" clip_to_extent="1" alpha="1" force_rhr="0">
            <layer locked="0" pass="0" class="SimpleMarker" enabled="1">
              <prop k="angle" v="0"/>
              <prop k="color" v="229,182,54,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowRadiusAlphaOnly="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetGlobal="1" shadowOffsetAngle="135" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowDraw="0" shadowRadius="1.5" shadowUnder="0" shadowRadiusUnit="MM" shadowScale="100" shadowOffsetDist="1" shadowOpacity="0.7" shadowColor="0,0,0,255"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format wrapChar="" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" multilineAlign="4294967295" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" reverseDirectionSymbol="0" decimals="3" useMaxLineLengthForAutoWrap="1" autoWrapLength="0" formatNumbers="0"/>
      <placement layerType="PolygonGeometry" yOffset="0" rotationAngle="0" maxCurvedCharAngleOut="-25" preserveRotation="1" centroidWhole="0" repeatDistance="0" geometryGeneratorType="PointGeometry" quadOffset="4" geometryGeneratorEnabled="0" fitInPolygonOnly="0" offsetType="0" centroidInside="0" dist="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" placementFlags="10" geometryGenerator="" xOffset="0" distUnits="MM" maxCurvedCharAngleIn="25" placement="0" distMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" offsetUnits="MM" priority="5" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM"/>
      <rendering fontMaxPixelSize="10000" scaleMin="0" limitNumLabels="0" obstacleFactor="1" fontLimitPixelSize="0" obstacleType="0" fontMinPixelSize="3" upsidedownLabels="0" obstacle="1" scaleVisibility="0" displayAll="0" scaleMax="50000" drawLabels="1" maxNumLabels="2000" zIndex="0" labelPerPart="0" mergeLines="0" minFeatureSize="0"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" name="name" value=""/>
          <Option name="properties"/>
          <Option type="QString" name="type" value="collection"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
          <Option type="bool" name="drawToAllParts" value="false"/>
          <Option type="QString" name="enabled" value="0"/>
          <Option type="QString" name="lineSymbol" value="&lt;symbol type=&quot;line&quot; name=&quot;symbol&quot; clip_to_extent=&quot;1&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot;>&lt;layer locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
          <Option type="double" name="minLength" value="0"/>
          <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="minLengthUnit" value="MM"/>
          <Option type="double" name="offsetFromAnchor" value="0"/>
          <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
          <Option type="double" name="offsetFromLabel" value="0"/>
          <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>id</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
      <value>COALESCE( "code_zone", '&lt;NULL>' )</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory opacity="1" penColor="#000000" enabled="0" rotationOffset="270" diagramOrientation="Up" maxScaleDenominator="1e+08" lineSizeType="MM" sizeType="MM" sizeScale="3x:0,0,0,0,0,0" lineSizeScale="3x:0,0,0,0,0,0" backgroundAlpha="255" minScaleDenominator="0" labelPlacementMethod="XHeight" penWidth="0" minimumSize="0" scaleBasedVisibility="0" scaleDependency="Area" barWidth="5" backgroundColor="#ffffff" height="15" width="15" penAlpha="255">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" showAll="1" dist="0" linePlacementFlags="18" placement="1" zIndex="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option type="double" name="allowedGapsBuffer" value="0"/>
        <Option type="bool" name="allowedGapsEnabled" value="false"/>
        <Option type="QString" name="allowedGapsLayer" value=""/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zonage">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Zonages_e18c9ab7_5307_44ad_82ed_b6f0f9df9198"/>
            <Option type="QString" name="LayerName" value="Zonages"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="nom"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_zone">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="0"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_geo">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Pays NUTS0" value="10"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Zone NUTS1" value="21"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Zone NUTS2" value="22"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Zone NUTS3" value="23"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Commune" value="30"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Infracommunal" value="40"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zones_adj">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom_adj">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id"/>
    <alias name="" index="1" field="id_zonage"/>
    <alias name="Code de la zone" index="2" field="code_zone"/>
    <alias name="Nom de la zone" index="3" field="nom"/>
    <alias name="Précision géo" index="4" field="prec_geo"/>
    <alias name="ID zones adjacentes" index="5" field="id_zones_adj"/>
    <alias name="" index="6" field="geom_adj"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="id_zonage"/>
    <default applyOnUpdate="0" expression="" field="code_zone"/>
    <default applyOnUpdate="0" expression="" field="nom"/>
    <default applyOnUpdate="0" expression="" field="prec_geo"/>
    <default applyOnUpdate="0" expression="" field="id_zones_adj"/>
    <default applyOnUpdate="0" expression="" field="geom_adj"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="1" constraints="3" notnull_strength="1" field="id"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="id_zonage"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="code_zone"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="nom"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="prec_geo"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="id_zones_adj"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="geom_adj"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="id_zonage"/>
    <constraint desc="" exp="" field="code_zone"/>
    <constraint desc="" exp="" field="nom"/>
    <constraint desc="" exp="" field="prec_geo"/>
    <constraint desc="" exp="" field="id_zones_adj"/>
    <constraint desc="" exp="" field="geom_adj"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="&quot;code_zone&quot;" sortOrder="0">
    <columns>
      <column type="field" name="id" hidden="1" width="-1"/>
      <column type="field" name="id_zonage" hidden="1" width="-1"/>
      <column type="field" name="code_zone" hidden="0" width="-1"/>
      <column type="field" name="nom" hidden="0" width="208"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" name="prec_geo" hidden="0" width="368"/>
      <column type="field" name="id_zones_adj" hidden="0" width="-1"/>
      <column type="field" name="geom_adj" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="code_zone" showLabel="1" index="2"/>
    <attributeEditorField name="nom" showLabel="1" index="3"/>
    <attributeEditorField name="prec_geo" showLabel="1" index="4"/>
    <attributeEditorField name="id_zones_adj" showLabel="1" index="5"/>
  </attributeEditorForm>
  <editable>
    <field name="code_zone" editable="1"/>
    <field name="geom_adj" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_zonage" editable="1"/>
    <field name="id_zones_adj" editable="1"/>
    <field name="nom" editable="1"/>
    <field name="prec_geo" editable="1"/>
    <field name="precision_geoloc" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="code_zone" labelOnTop="0"/>
    <field name="geom_adj" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="id_zonage" labelOnTop="0"/>
    <field name="id_zones_adj" labelOnTop="0"/>
    <field name="nom" labelOnTop="0"/>
    <field name="prec_geo" labelOnTop="0"/>
    <field name="precision_geoloc" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE( "code_zone", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
