<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" hasScaleBasedVisibilityFlag="0" maxScale="100000" styleCategories="AllStyleCategories" minScale="1e+08" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE(nom, '&lt;NULL>')</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="libelle">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="standard">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="type_rep">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="1" name="Codification (hors zonage)"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="2" name="Valeur numérique"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="3" name="Zonage codifié"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="4" name="Texte libre"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name="ID"/>
    <alias index="1" field="nom" name="Nom de la question (unique)"/>
    <alias index="2" field="libelle" name="Libellé question"/>
    <alias index="3" field="standard" name="Question standard ?"/>
    <alias index="4" field="type_rep" name="Type de réponse"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="nom" applyOnUpdate="0"/>
    <default expression="" field="libelle" applyOnUpdate="0"/>
    <default expression="" field="standard" applyOnUpdate="0"/>
    <default expression="" field="type_rep" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" unique_strength="1" field="id" constraints="3" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" field="nom" constraints="3" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" field="libelle" constraints="1" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" field="standard" constraints="1" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" field="type_rep" constraints="1" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="nom" desc=""/>
    <constraint exp="" field="libelle" desc=""/>
    <constraint exp="" field="standard" desc=""/>
    <constraint exp="" field="type_rep" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="1" sortExpression="&quot;standard&quot;">
    <columns>
      <column hidden="0" type="field" width="100" name="id"/>
      <column hidden="0" type="field" width="201" name="nom"/>
      <column hidden="0" type="field" width="758" name="libelle"/>
      <column hidden="1" type="actions" width="-1"/>
      <column hidden="0" type="field" width="173" name="standard"/>
      <column hidden="0" type="field" width="198" name="type_rep"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C16LM0066-BDD_transport_mutualisee/Documents techniques/Migration_geoflux</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField index="0" showLabel="1" name="id"/>
    <attributeEditorField index="1" showLabel="1" name="nom"/>
    <attributeEditorField index="2" showLabel="1" name="libelle"/>
    <attributeEditorField index="3" showLabel="1" name="standard"/>
    <attributeEditorField index="4" showLabel="1" name="type_rep"/>
    <attributeEditorRelation showUnlinkButton="1" relation="codif_id_question_fkey" showLabel="1" showLinkButton="1" name="codif_id_question_fkey"/>
    <attributeEditorRelation showUnlinkButton="1" relation="" showLabel="1" showLinkButton="1" name=""/>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="id"/>
    <field editable="1" name="identifiant_questionnaire"/>
    <field editable="1" name="libelle"/>
    <field editable="1" name="nom"/>
    <field editable="1" name="nom_court"/>
    <field editable="1" name="standard"/>
    <field editable="1" name="traitee"/>
    <field editable="1" name="type"/>
    <field editable="1" name="type_rep"/>
    <field editable="1" name="type_veh"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="identifiant_questionnaire"/>
    <field labelOnTop="0" name="libelle"/>
    <field labelOnTop="0" name="nom"/>
    <field labelOnTop="0" name="nom_court"/>
    <field labelOnTop="0" name="standard"/>
    <field labelOnTop="0" name="traitee"/>
    <field labelOnTop="0" name="type"/>
    <field labelOnTop="0" name="type_rep"/>
    <field labelOnTop="0" name="type_veh"/>
  </labelOnTop>
  <widgets>
    <widget name="Codifications_e2b27b4b_aee8_4985_af8d_714b1a2c4e62_id_question_Questions_a1a9251f_d004_4d0c_b741_54aa07ad87c9_id">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
    <widget name="Composition_questionnaires_23edf2bc_95c8_4fc0_9bda_40691cd00a9f_id_question_Questions_5a0357c6_5cb9_4edb_bb7d_b7b6eac5b54b_id">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
    <widget name="codif_id_question_fkey">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
    <widget name="composition_questionnaire_question">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
    <widget name="question_codif">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
    <widget name="question_q_id_question_question20_id">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
  </widgets>
  <previewExpression>COALESCE(nom, '&lt;NULL>')</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
