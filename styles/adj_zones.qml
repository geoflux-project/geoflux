<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" styleCategories="AllStyleCategories" maxScale="0" simplifyDrawingHints="1" simplifyMaxScale="1" version="3.10.6-A Coruña" readOnly="0" minScale="1e+08" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" simplifyLocal="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" symbollevels="0" forceraster="0">
    <symbols>
      <symbol type="fill" name="0" clip_to_extent="1" alpha="0.5" force_rhr="0">
        <layer locked="0" pass="0" class="SimpleFill" enabled="1">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.46"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>nom</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory opacity="1" penColor="#000000" enabled="0" rotationOffset="270" diagramOrientation="Up" maxScaleDenominator="1e+08" lineSizeType="MM" sizeType="MM" sizeScale="3x:0,0,0,0,0,0" lineSizeScale="3x:0,0,0,0,0,0" backgroundAlpha="255" minScaleDenominator="0" labelPlacementMethod="XHeight" penWidth="0" minimumSize="0" scaleBasedVisibility="0" scaleDependency="Area" barWidth="5" backgroundColor="#ffffff" height="15" width="15" penAlpha="255">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" showAll="1" dist="0" linePlacementFlags="18" placement="1" zIndex="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option type="double" name="allowedGapsBuffer" value="0"/>
        <Option type="bool" name="allowedGapsEnabled" value="false"/>
        <Option type="QString" name="allowedGapsLayer" value=""/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zonage">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Zonages_e18c9ab7_5307_44ad_82ed_b6f0f9df9198"/>
            <Option type="QString" name="LayerName" value="Zonages"/>
            <Option type="QString" name="LayerProviderName" value="postgres"/>
            <Option type="QString" name="LayerSource" value="dbname='base' host=localhost port=5434 user='postgres' key='id' estimatedmetadata=true checkPrimaryKeyUnicity='1' table=&quot;o_geoflux_matrix&quot;.&quot;zonage&quot; sql="/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="nom"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_zone">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_geo">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Pays NUTS0" value="10"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Zone NUTS1" value="21"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Zone NUTS2" value="22"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Zone NUTS3" value="23"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Commune" value="30"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Infracommunal" value="40"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zones_adj">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="ID" index="0" field="id"/>
    <alias name="ID zonage" index="1" field="id_zonage"/>
    <alias name="Code zone" index="2" field="code_zone"/>
    <alias name="Nom zone" index="3" field="nom"/>
    <alias name="Précision géo" index="4" field="prec_geo"/>
    <alias name="ID zones adjacentes" index="5" field="id_zones_adj"/>
    <alias name="" index="6" field="geom"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="id_zonage"/>
    <default applyOnUpdate="0" expression="" field="code_zone"/>
    <default applyOnUpdate="0" expression="" field="nom"/>
    <default applyOnUpdate="0" expression="" field="prec_geo"/>
    <default applyOnUpdate="0" expression="" field="id_zones_adj"/>
    <default applyOnUpdate="0" expression="" field="geom"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="1" constraints="3" notnull_strength="1" field="id"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="id_zonage"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="code_zone"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="nom"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="prec_geo"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="id_zones_adj"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="geom"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="id_zonage"/>
    <constraint desc="" exp="" field="code_zone"/>
    <constraint desc="" exp="" field="nom"/>
    <constraint desc="" exp="" field="prec_geo"/>
    <constraint desc="" exp="" field="id_zones_adj"/>
    <constraint desc="" exp="" field="geom"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column type="field" name="id" hidden="1" width="-1"/>
      <column type="field" name="id_zonage" hidden="1" width="-1"/>
      <column type="field" name="code_zone" hidden="0" width="-1"/>
      <column type="field" name="nom" hidden="0" width="-1"/>
      <column type="field" name="prec_geo" hidden="0" width="-1"/>
      <column type="field" name="id_zones_adj" hidden="0" width="-1"/>
      <column type="field" name="geom" hidden="1" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="code_zone" editable="1"/>
    <field name="geom" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_zonage" editable="1"/>
    <field name="id_zones_adj" editable="1"/>
    <field name="nom" editable="1"/>
    <field name="prec_geo" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="code_zone" labelOnTop="0"/>
    <field name="geom" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="id_zonage" labelOnTop="0"/>
    <field name="id_zones_adj" labelOnTop="0"/>
    <field name="nom" labelOnTop="0"/>
    <field name="prec_geo" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>nom</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
