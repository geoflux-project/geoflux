<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="1" simplifyDrawingHints="0" simplifyMaxScale="1" simplifyDrawingTol="1" readOnly="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" type="marker" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SvgMarker" locked="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="59,52,124,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="transport/amenity=airport.svg"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="4"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style multilineHeight="1" fontWordSpacing="0" blendMode="0" previewBkgrdColor="255,255,255,255" fontWeight="50" namedStyle="Normal" fontSize="6.25" fontUnderline="0" textOrientation="horizontal" fieldName="airp_name_" fontKerning="1" textOpacity="1" fontStrikeout="0" isExpression="0" useSubstitutions="0" fontFamily="MS Shell Dlg 2" fontLetterSpacing="0" fontSizeUnit="Point" fontCapitals="0" textColor="59,52,124,255" fontItalic="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0">
        <text-buffer bufferDraw="0" bufferNoFill="0" bufferColor="255,255,255,255" bufferJoinStyle="128" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM" bufferBlendMode="0" bufferSize="1"/>
        <background shapeSVGFile="" shapeRotation="0" shapeRotationType="0" shapeType="0" shapeSizeY="0" shapeOffsetUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOffsetY="0" shapeSizeX="0" shapeBlendMode="0" shapeSizeUnit="MM" shapeJoinStyle="64" shapeRadiiX="0" shapeRadiiY="0" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderColor="128,128,128,255" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeRadiiUnit="MM" shapeOffsetX="0" shapeBorderWidth="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1">
          <symbol force_rhr="0" type="marker" clip_to_extent="1" name="markerSymbol" alpha="1">
            <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
              <prop k="angle" v="0"/>
              <prop k="color" v="190,207,80,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowUnder="0" shadowColor="0,0,0,255" shadowOffsetDist="1" shadowScale="100" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.7" shadowDraw="0" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowBlendMode="6" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowOffsetAngle="135" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" wrapChar="" reverseDirectionSymbol="0" plussign="0" useMaxLineLengthForAutoWrap="1" autoWrapLength="0" placeDirectionSymbol="0" multilineAlign="3" formatNumbers="0" decimals="3" addDirectionSymbol="0" rightDirectionSymbol=">"/>
      <placement geometryGeneratorType="PointGeometry" dist="3" geometryGenerator="" xOffset="0" repeatDistanceUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" yOffset="0" centroidWhole="0" offsetType="0" placementFlags="10" distMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" maxCurvedCharAngleIn="25" offsetUnits="MapUnit" preserveRotation="1" quadOffset="4" priority="5" overrunDistanceUnit="MM" placement="6" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" layerType="PointGeometry" centroidInside="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" geometryGeneratorEnabled="0" rotationAngle="0" distUnits="MM"/>
      <rendering minFeatureSize="0" scaleMax="10000000" maxNumLabels="2000" fontMinPixelSize="3" fontMaxPixelSize="10000" labelPerPart="0" displayAll="0" drawLabels="1" obstacleFactor="1" zIndex="0" obstacle="1" upsidedownLabels="0" obstacleType="0" mergeLines="0" fontLimitPixelSize="0" limitNumLabels="0" scaleVisibility="0" scaleMin="1"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
          <Option type="Map" name="ddProperties">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
          <Option value="false" type="bool" name="drawToAllParts"/>
          <Option value="0" type="QString" name="enabled"/>
          <Option value="&lt;symbol force_rhr=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; enabled=&quot;1&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
          <Option value="0" type="double" name="minLength"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
          <Option value="MM" type="QString" name="minLengthUnit"/>
          <Option value="0" type="double" name="offsetFromAnchor"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
          <Option value="0" type="double" name="offsetFromLabel"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" backgroundColor="#ffffff" sizeType="MM" penColor="#000000" enabled="0" barWidth="5" penAlpha="255" scaleDependency="Area" rotationOffset="270" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" scaleBasedVisibility="0" width="15" lineSizeType="MM" maxScaleDenominator="1e+08" diagramOrientation="Up" labelPlacementMethod="XHeight" opacity="1" minScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="0" zIndex="0" linePlacementFlags="2" showAll="1" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="objectid">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_icao_">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_hier">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_name_">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cntr_code">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_icrg_">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="iata_code">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_pass">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="publ_flag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="ifr">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="vfr">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_ownr">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_land">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_traf">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_intr">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_city">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="alt">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lat_eur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lon_eur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_use">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_group">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_getis">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="airp_lcov_">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="objectid" name="" index="0"/>
    <alias field="airp_icao_" name="" index="1"/>
    <alias field="airp_hier" name="" index="2"/>
    <alias field="airp_name_" name="" index="3"/>
    <alias field="cntr_code" name="" index="4"/>
    <alias field="airp_icrg_" name="" index="5"/>
    <alias field="iata_code" name="" index="6"/>
    <alias field="airp_pass" name="" index="7"/>
    <alias field="publ_flag" name="" index="8"/>
    <alias field="ifr" name="" index="9"/>
    <alias field="vfr" name="" index="10"/>
    <alias field="airp_ownr" name="" index="11"/>
    <alias field="airp_land" name="" index="12"/>
    <alias field="airp_traf" name="" index="13"/>
    <alias field="airp_intr" name="" index="14"/>
    <alias field="airp_city" name="" index="15"/>
    <alias field="alt" name="" index="16"/>
    <alias field="lat_eur" name="" index="17"/>
    <alias field="lon_eur" name="" index="18"/>
    <alias field="airp_use" name="" index="19"/>
    <alias field="airp_group" name="" index="20"/>
    <alias field="airp_getis" name="" index="21"/>
    <alias field="airp_id" name="" index="22"/>
    <alias field="airp_lcov_" name="" index="23"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="objectid"/>
    <default applyOnUpdate="0" expression="" field="airp_icao_"/>
    <default applyOnUpdate="0" expression="" field="airp_hier"/>
    <default applyOnUpdate="0" expression="" field="airp_name_"/>
    <default applyOnUpdate="0" expression="" field="cntr_code"/>
    <default applyOnUpdate="0" expression="" field="airp_icrg_"/>
    <default applyOnUpdate="0" expression="" field="iata_code"/>
    <default applyOnUpdate="0" expression="" field="airp_pass"/>
    <default applyOnUpdate="0" expression="" field="publ_flag"/>
    <default applyOnUpdate="0" expression="" field="ifr"/>
    <default applyOnUpdate="0" expression="" field="vfr"/>
    <default applyOnUpdate="0" expression="" field="airp_ownr"/>
    <default applyOnUpdate="0" expression="" field="airp_land"/>
    <default applyOnUpdate="0" expression="" field="airp_traf"/>
    <default applyOnUpdate="0" expression="" field="airp_intr"/>
    <default applyOnUpdate="0" expression="" field="airp_city"/>
    <default applyOnUpdate="0" expression="" field="alt"/>
    <default applyOnUpdate="0" expression="" field="lat_eur"/>
    <default applyOnUpdate="0" expression="" field="lon_eur"/>
    <default applyOnUpdate="0" expression="" field="airp_use"/>
    <default applyOnUpdate="0" expression="" field="airp_group"/>
    <default applyOnUpdate="0" expression="" field="airp_getis"/>
    <default applyOnUpdate="0" expression="" field="airp_id"/>
    <default applyOnUpdate="0" expression="" field="airp_lcov_"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="objectid" unique_strength="1" constraints="3" notnull_strength="1"/>
    <constraint exp_strength="0" field="airp_icao_" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_hier" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_name_" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="cntr_code" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_icrg_" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="iata_code" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_pass" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="publ_flag" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="ifr" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="vfr" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_ownr" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_land" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_traf" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_intr" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_city" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="alt" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="lat_eur" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="lon_eur" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_use" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_group" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_getis" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_id" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="airp_lcov_" unique_strength="0" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="objectid" desc=""/>
    <constraint exp="" field="airp_icao_" desc=""/>
    <constraint exp="" field="airp_hier" desc=""/>
    <constraint exp="" field="airp_name_" desc=""/>
    <constraint exp="" field="cntr_code" desc=""/>
    <constraint exp="" field="airp_icrg_" desc=""/>
    <constraint exp="" field="iata_code" desc=""/>
    <constraint exp="" field="airp_pass" desc=""/>
    <constraint exp="" field="publ_flag" desc=""/>
    <constraint exp="" field="ifr" desc=""/>
    <constraint exp="" field="vfr" desc=""/>
    <constraint exp="" field="airp_ownr" desc=""/>
    <constraint exp="" field="airp_land" desc=""/>
    <constraint exp="" field="airp_traf" desc=""/>
    <constraint exp="" field="airp_intr" desc=""/>
    <constraint exp="" field="airp_city" desc=""/>
    <constraint exp="" field="alt" desc=""/>
    <constraint exp="" field="lat_eur" desc=""/>
    <constraint exp="" field="lon_eur" desc=""/>
    <constraint exp="" field="airp_use" desc=""/>
    <constraint exp="" field="airp_group" desc=""/>
    <constraint exp="" field="airp_getis" desc=""/>
    <constraint exp="" field="airp_id" desc=""/>
    <constraint exp="" field="airp_lcov_" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="-1" hidden="0" type="field" name="objectid"/>
      <column width="-1" hidden="0" type="field" name="airp_icao_"/>
      <column width="-1" hidden="0" type="field" name="airp_hier"/>
      <column width="-1" hidden="0" type="field" name="airp_name_"/>
      <column width="-1" hidden="0" type="field" name="cntr_code"/>
      <column width="-1" hidden="0" type="field" name="airp_icrg_"/>
      <column width="-1" hidden="0" type="field" name="iata_code"/>
      <column width="-1" hidden="0" type="field" name="airp_pass"/>
      <column width="-1" hidden="0" type="field" name="publ_flag"/>
      <column width="-1" hidden="0" type="field" name="ifr"/>
      <column width="-1" hidden="0" type="field" name="vfr"/>
      <column width="-1" hidden="0" type="field" name="airp_ownr"/>
      <column width="-1" hidden="0" type="field" name="airp_land"/>
      <column width="-1" hidden="0" type="field" name="airp_traf"/>
      <column width="-1" hidden="0" type="field" name="airp_intr"/>
      <column width="-1" hidden="0" type="field" name="airp_city"/>
      <column width="-1" hidden="0" type="field" name="alt"/>
      <column width="-1" hidden="0" type="field" name="lat_eur"/>
      <column width="-1" hidden="0" type="field" name="lon_eur"/>
      <column width="-1" hidden="0" type="field" name="airp_use"/>
      <column width="-1" hidden="0" type="field" name="airp_group"/>
      <column width="-1" hidden="0" type="field" name="airp_getis"/>
      <column width="-1" hidden="0" type="field" name="airp_id"/>
      <column width="-1" hidden="0" type="field" name="airp_lcov_"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="airp_city" editable="1"/>
    <field name="airp_getis" editable="1"/>
    <field name="airp_group" editable="1"/>
    <field name="airp_hier" editable="1"/>
    <field name="airp_icao_" editable="1"/>
    <field name="airp_icrg_" editable="1"/>
    <field name="airp_id" editable="1"/>
    <field name="airp_intr" editable="1"/>
    <field name="airp_land" editable="1"/>
    <field name="airp_lcov_" editable="1"/>
    <field name="airp_name_" editable="1"/>
    <field name="airp_ownr" editable="1"/>
    <field name="airp_pass" editable="1"/>
    <field name="airp_traf" editable="1"/>
    <field name="airp_use" editable="1"/>
    <field name="alt" editable="1"/>
    <field name="cntr_code" editable="1"/>
    <field name="iata_code" editable="1"/>
    <field name="ifr" editable="1"/>
    <field name="lat_eur" editable="1"/>
    <field name="lon_eur" editable="1"/>
    <field name="objectid" editable="1"/>
    <field name="publ_flag" editable="1"/>
    <field name="vfr" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="airp_city"/>
    <field labelOnTop="0" name="airp_getis"/>
    <field labelOnTop="0" name="airp_group"/>
    <field labelOnTop="0" name="airp_hier"/>
    <field labelOnTop="0" name="airp_icao_"/>
    <field labelOnTop="0" name="airp_icrg_"/>
    <field labelOnTop="0" name="airp_id"/>
    <field labelOnTop="0" name="airp_intr"/>
    <field labelOnTop="0" name="airp_land"/>
    <field labelOnTop="0" name="airp_lcov_"/>
    <field labelOnTop="0" name="airp_name_"/>
    <field labelOnTop="0" name="airp_ownr"/>
    <field labelOnTop="0" name="airp_pass"/>
    <field labelOnTop="0" name="airp_traf"/>
    <field labelOnTop="0" name="airp_use"/>
    <field labelOnTop="0" name="alt"/>
    <field labelOnTop="0" name="cntr_code"/>
    <field labelOnTop="0" name="iata_code"/>
    <field labelOnTop="0" name="ifr"/>
    <field labelOnTop="0" name="lat_eur"/>
    <field labelOnTop="0" name="lon_eur"/>
    <field labelOnTop="0" name="objectid"/>
    <field labelOnTop="0" name="publ_flag"/>
    <field labelOnTop="0" name="vfr"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE( "airp_name_", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
