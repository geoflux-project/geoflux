<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" maxScale="0" styleCategories="AllStyleCategories" readOnly="0" version="3.10.6-A Coruña">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>'Point ' || to_string("id_point_enq_terrain") || ' - ' || to_string( "ordre" ) ||  ' - Question ' || to_string("id_question" )</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_point_enq_terrain">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option name="AllowAddFeatures" value="false" type="bool"/>
            <Option name="AllowNULL" value="false" type="bool"/>
            <Option name="MapIdentification" value="false" type="bool"/>
            <Option name="OrderByValue" value="false" type="bool"/>
            <Option name="ReadOnly" value="false" type="bool"/>
            <Option name="Relation" value="questionnaire_point_enq_terrain" type="QString"/>
            <Option name="ShowForm" value="false" type="bool"/>
            <Option name="ShowOpenFormButton" value="true" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="ordre">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" value="true" type="bool"/>
            <Option name="Max" value="2147483647" type="int"/>
            <Option name="Min" value="-2147483648" type="int"/>
            <Option name="Precision" value="0" type="int"/>
            <Option name="Step" value="1" type="int"/>
            <Option name="Style" value="SpinBox" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_question">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" value="false" type="bool"/>
            <Option name="AllowNull" value="false" type="bool"/>
            <Option name="FilterExpression" value="" type="QString"/>
            <Option name="Key" value="id" type="QString"/>
            <Option name="Layer" value="Questions_43a8b24d_8b59_453d_8790_2105809e3cde" type="QString"/>
            <Option name="LayerName" value="Questions" type="QString"/>
            <Option name="LayerProviderName" value="postgres" type="QString"/>
            <Option name="LayerSource" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' estimatedmetadata=true checkPrimaryKeyUnicity='1' table=&quot;o_geoflux_survey2&quot;.&quot;question&quot; sql=" type="QString"/>
            <Option name="NofColumns" value="1" type="int"/>
            <Option name="OrderByValue" value="false" type="bool"/>
            <Option name="UseCompleter" value="false" type="bool"/>
            <Option name="Value" value="nom" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="quest_vl">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option name="CheckedState" value="" type="QString"/>
            <Option name="UncheckedState" value="" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="quest_pl">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option name="CheckedState" value="" type="QString"/>
            <Option name="UncheckedState" value="" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="ID point d'enquête terrain" field="id_point_enq_terrain" index="0"/>
    <alias name="Ordre" field="ordre" index="1"/>
    <alias name="Question" field="id_question" index="2"/>
    <alias name="VL ?" field="quest_vl" index="3"/>
    <alias name="PL ?" field="quest_pl" index="4"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id_point_enq_terrain" applyOnUpdate="0"/>
    <default expression="" field="ordre" applyOnUpdate="0"/>
    <default expression="" field="id_question" applyOnUpdate="0"/>
    <default expression="" field="quest_vl" applyOnUpdate="0"/>
    <default expression="" field="quest_pl" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="1" field="id_point_enq_terrain" notnull_strength="1" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="ordre" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="1" field="id_question" notnull_strength="1" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="quest_vl" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="quest_pl" notnull_strength="0" exp_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_point_enq_terrain" desc="" exp=""/>
    <constraint field="ordre" desc="" exp=""/>
    <constraint field="id_question" desc="" exp=""/>
    <constraint field="quest_vl" desc="" exp=""/>
    <constraint field="quest_pl" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="&quot;ordre&quot;" sortOrder="1">
    <columns>
      <column name="id_point_enq_terrain" type="field" width="-1" hidden="0"/>
      <column name="ordre" type="field" width="-1" hidden="0"/>
      <column name="id_question" type="field" width="-1" hidden="0"/>
      <column name="quest_vl" type="field" width="-1" hidden="0"/>
      <column name="quest_pl" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="id_point_enq_terrain" editable="1"/>
    <field name="id_question" editable="1"/>
    <field name="ordre" editable="1"/>
    <field name="quest_pl" editable="1"/>
    <field name="quest_vl" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="id_point_enq_terrain" labelOnTop="0"/>
    <field name="id_question" labelOnTop="0"/>
    <field name="ordre" labelOnTop="0"/>
    <field name="quest_pl" labelOnTop="0"/>
    <field name="quest_vl" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>'Point ' || to_string("id_point_enq_terrain") || ' - ' || to_string( "ordre" ) ||  ' - Question ' || to_string("id_question" )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
