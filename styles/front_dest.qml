<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyAlgorithm="0" simplifyDrawingHints="0" simplifyDrawingTol="1" maxScale="0" hasScaleBasedVisibilityFlag="0" minScale="1e+08" styleCategories="AllStyleCategories" labelsEnabled="0" readOnly="0" simplifyLocal="1" simplifyMaxScale="1" version="3.10.6-A Coruña">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 enableorderby="0" type="singleSymbol" symbollevels="0" forceraster="0">
    <symbols>
      <symbol force_rhr="0" alpha="1" clip_to_extent="1" type="marker" name="0">
        <layer class="SvgMarker" locked="0" enabled="1" pass="0">
          <prop v="0" k="angle"/>
          <prop v="227,26,28,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="gpsicons/flag.svg" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="255,255,255,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="8" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions" value="code_pays"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penColor="#000000" backgroundAlpha="255" opacity="1" height="15" diagramOrientation="Up" maxScaleDenominator="1e+08" scaleDependency="Area" barWidth="5" enabled="0" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" width="15" sizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" backgroundColor="#ffffff" penWidth="0" rotationOffset="270" labelPlacementMethod="XHeight" penAlpha="255" minScaleDenominator="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" dist="0" placement="0" linePlacementFlags="18" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="code_pays">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_zone">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_com">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_pole">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_voie">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_adr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_port">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_front">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="libelle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type_lieu">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom_polygon">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="code_pays" index="0" name=""/>
    <alias field="code_zone" index="1" name=""/>
    <alias field="code_com" index="2" name=""/>
    <alias field="code_pole" index="3" name=""/>
    <alias field="code_voie" index="4" name=""/>
    <alias field="code_adr" index="5" name=""/>
    <alias field="code_port" index="6" name=""/>
    <alias field="code_front" index="7" name=""/>
    <alias field="libelle" index="8" name=""/>
    <alias field="type_lieu" index="9" name=""/>
    <alias field="geom_polygon" index="10" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="code_pays" applyOnUpdate="0" expression=""/>
    <default field="code_zone" applyOnUpdate="0" expression=""/>
    <default field="code_com" applyOnUpdate="0" expression=""/>
    <default field="code_pole" applyOnUpdate="0" expression=""/>
    <default field="code_voie" applyOnUpdate="0" expression=""/>
    <default field="code_adr" applyOnUpdate="0" expression=""/>
    <default field="code_port" applyOnUpdate="0" expression=""/>
    <default field="code_front" applyOnUpdate="0" expression=""/>
    <default field="libelle" applyOnUpdate="0" expression=""/>
    <default field="type_lieu" applyOnUpdate="0" expression=""/>
    <default field="geom_polygon" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="code_pays" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="code_zone" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="code_com" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="code_pole" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="code_voie" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="code_adr" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="code_port" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="code_front" exp_strength="0" notnull_strength="1" constraints="1"/>
    <constraint unique_strength="0" field="libelle" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="type_lieu" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="geom_polygon" exp_strength="0" notnull_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="code_pays" desc="" exp=""/>
    <constraint field="code_zone" desc="" exp=""/>
    <constraint field="code_com" desc="" exp=""/>
    <constraint field="code_pole" desc="" exp=""/>
    <constraint field="code_voie" desc="" exp=""/>
    <constraint field="code_adr" desc="" exp=""/>
    <constraint field="code_port" desc="" exp=""/>
    <constraint field="code_front" desc="" exp=""/>
    <constraint field="libelle" desc="" exp=""/>
    <constraint field="type_lieu" desc="" exp=""/>
    <constraint field="geom_polygon" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" hidden="0" type="field" name="code_pays"/>
      <column width="-1" hidden="1" type="field" name="code_zone"/>
      <column width="-1" hidden="1" type="field" name="code_com"/>
      <column width="-1" hidden="1" type="field" name="code_pole"/>
      <column width="-1" hidden="1" type="field" name="code_voie"/>
      <column width="-1" hidden="1" type="field" name="code_adr"/>
      <column width="-1" hidden="1" type="field" name="code_port"/>
      <column width="-1" hidden="0" type="field" name="code_front"/>
      <column width="-1" hidden="0" type="field" name="libelle"/>
      <column width="-1" hidden="0" type="field" name="type_lieu"/>
      <column width="-1" hidden="1" type="field" name="geom_polygon"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="code_adr" editable="1"/>
    <field name="code_com" editable="1"/>
    <field name="code_front" editable="1"/>
    <field name="code_pays" editable="1"/>
    <field name="code_pole" editable="1"/>
    <field name="code_port" editable="1"/>
    <field name="code_voie" editable="1"/>
    <field name="code_zone" editable="1"/>
    <field name="geom_polygon" editable="1"/>
    <field name="libelle" editable="1"/>
    <field name="type_lieu" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="code_adr"/>
    <field labelOnTop="0" name="code_com"/>
    <field labelOnTop="0" name="code_front"/>
    <field labelOnTop="0" name="code_pays"/>
    <field labelOnTop="0" name="code_pole"/>
    <field labelOnTop="0" name="code_port"/>
    <field labelOnTop="0" name="code_voie"/>
    <field labelOnTop="0" name="code_zone"/>
    <field labelOnTop="0" name="geom_polygon"/>
    <field labelOnTop="0" name="libelle"/>
    <field labelOnTop="0" name="type_lieu"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>code_pays</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
