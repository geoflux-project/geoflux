<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" maxScale="0" minScale="1e+08" hasScaleBasedVisibilityFlag="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_zone_orig">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_zone_dest">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="coef">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_point_enq_terrain">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Points_enquêtes_terrain_f5a7ab03_57fa_4cab_bf2d_82aaa2841d6d" name="Layer" type="QString"/>
            <Option value="Points enquêtes terrain" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='base' host=localhost port=5434 user='postgres' key='id' estimatedmetadata=true checkPrimaryKeyUnicity='1' table=&quot;o_geoflux_survey&quot;.&quot;point_enq_terrain&quot; (geom) sql=id = ANY(ARRAY[1]::integer[])" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="id" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_point_enq_fictif">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Points_enquêtes_fictifs_b210000c_b141_4ea2_b1f6_915168214223" name="Layer" type="QString"/>
            <Option value="Points enquêtes fictifs" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='base' host=localhost port=5434 user='postgres' key='id' estimatedmetadata=true checkPrimaryKeyUnicity='1' table=&quot;o_geoflux_survey&quot;.&quot;point_enq_fictif&quot; (geom) sql=id = ANY(ARRAY[]::integer[])" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="id" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="ID" field="id"/>
    <alias index="1" name="Code zone O" field="code_zone_orig"/>
    <alias index="2" name="Code zone D" field="code_zone_dest"/>
    <alias index="3" name="Coef" field="coef"/>
    <alias index="4" name="ID point enq terrain" field="id_point_enq_terrain"/>
    <alias index="5" name="ID point enq fictif" field="id_point_enq_fictif"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="code_zone_orig"/>
    <default applyOnUpdate="0" expression="" field="code_zone_dest"/>
    <default applyOnUpdate="0" expression="" field="coef"/>
    <default applyOnUpdate="0" expression="" field="id_point_enq_terrain"/>
    <default applyOnUpdate="0" expression="" field="id_point_enq_fictif"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" constraints="3" exp_strength="0" field="id" unique_strength="1"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" field="code_zone_orig" unique_strength="0"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" field="code_zone_dest" unique_strength="0"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" field="coef" unique_strength="0"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" field="id_point_enq_terrain" unique_strength="0"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" field="id_point_enq_fictif" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="code_zone_orig"/>
    <constraint desc="" exp="" field="code_zone_dest"/>
    <constraint desc="" exp="" field="coef"/>
    <constraint desc="" exp="" field="id_point_enq_terrain"/>
    <constraint desc="" exp="" field="id_point_enq_fictif"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="51" hidden="1" name="id" type="field"/>
      <column width="97" hidden="0" name="code_zone_orig" type="field"/>
      <column width="91" hidden="0" name="code_zone_dest" type="field"/>
      <column width="-1" hidden="0" name="coef" type="field"/>
      <column width="139" hidden="0" name="id_point_enq_terrain" type="field"/>
      <column width="131" hidden="0" name="id_point_enq_fictif" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="code_zone_dest"/>
    <field editable="1" name="code_zone_orig"/>
    <field editable="1" name="coef"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_point_enq_fictif"/>
    <field editable="1" name="id_point_enq_terrain"/>
  </editable>
  <labelOnTop>
    <field name="code_zone_dest" labelOnTop="0"/>
    <field name="code_zone_orig" labelOnTop="0"/>
    <field name="coef" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="id_point_enq_fictif" labelOnTop="0"/>
    <field name="id_point_enq_terrain" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
