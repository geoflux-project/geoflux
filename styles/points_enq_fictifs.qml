<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" labelsEnabled="0" simplifyDrawingTol="1" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1" minScale="1e+08" simplifyDrawingHints="0" simplifyAlgorithm="0" simplifyLocal="1" readOnly="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" enableorderby="0" forceraster="0" type="singleSymbol">
    <symbols>
      <symbol name="0" type="marker" alpha="1" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleMarker" locked="0" pass="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="0,0,0,0"/>
          <prop k="horizontal_anchor_point" v="2"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="arrow"/>
          <prop k="offset" v="5,5"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="3"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="2"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="angle" name="field" type="QString"/>
                  <Option value="2" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer enabled="1" class="SvgMarker" locked="0" pass="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="255,0,0,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmcyIgogICB3aWR0aD0iMjY2LjY2NjY2IgogICBoZWlnaHQ9IjI2Ni42NjY2NiIKICAgdmlld0JveD0iMCAwIDI2Ni42NjY2NiAyNjYuNjY2NjYiCiAgIHNvZGlwb2RpOmRvY25hbWU9Imljb25fZmljdGlmLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi41ICgyMDYwZWMxZjlmLCAyMDIwLTA0LTA4KSI+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhOCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGUgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczYiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxOTIwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMTciCiAgICAgaWQ9Im5hbWVkdmlldzQiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjEuMjUxNTc5MSIKICAgICBpbmtzY2FwZTpjeD0iLTQ1LjI0Njc5NCIKICAgICBpbmtzY2FwZTpjeT0iMjM2LjkwNzc3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmcyIiAvPgogIDxlbGxpcHNlCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6OC40ODcwMDA0NztzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgaWQ9InBhdGg4MzEiCiAgICAgY3g9IjExOC4zMzQwNSIKICAgICBjeT0iMTQyLjMyNDU4IgogICAgIHJ4PSIzNC45NjQ5NTQiCiAgICAgcnk9IjM4LjEzOTI4NiIgLz4KICA8ZwogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgaWQ9ImczOCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjA1Mjk3MjYyLDAsMCwwLjA3MDAyNDE0LDIwOS44NTQ0NywxMDYuNjU1OCkiPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzYiPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIgogICAgICAgICBpZD0icGF0aDQiCiAgICAgICAgIGQ9Im0gNTAwLDYwNi4xIGMgMTIyLDAgMjI0LjEsLTQxLjUgMjMwLjMsLTk1LjYgLTE4LjEsLTUwLjkgLTM3LjksLTEwNi40IC01Ny40LC0xNjAuOSAtMTMuNSwzOC45IC04OCw2Ni4zIC0xNzIuOSw2Ni4zIC04NC45LDAgLTE1OS40LC0yNy40IC0xNzIuOSwtNjYuMyAtMTkuNCw1NC41IC0zOS4yLDExMCAtNTcuMywxNjAuOSA2LjEsNTQuMSAxMDguMiw5NS42IDIzMC4yLDk1LjYgeiBtIDAsLTM0Mi4zIGMgNTcuMywwIDExMC42LC0xNy44IDEyNi4yLC00NS4zIEMgNjA0LjcsMTU4LjEgNTg2LjIsMTA2LjQgNTc0LjYsNzMuNyA1NjYuOCw1MS45IDUzMS43LDQwLjYgNTAwLDQwLjYgYyAtMzEuNywwIC02Ni44LDExLjMgLTc0LjYsMzMuMSAtMTEuNiwzMi43IC0zMC4xLDg0LjUgLTUxLjYsMTQ0LjggMTUuNiwyNy42IDY4LjksNDUuMyAxMjYuMiw0NS4zIHogbSA0NDguMiwzOTIuNyAtMTkxLjcsLTc3LjIgMjIuMSw2MS42IGMgLTEuMSw2NS4zIC0xMjcuOCwxMTcuMyAtMjc4LjYsMTE3LjMgLTE1MC43LDAgLTI3Ny41LC01MiAtMjc4LjYsLTExNy4zIGwgMjIuMSwtNjEuNiAtMTkxLjcsNzcuMiBjIC01My43LDIxLjcgLTU2LDYxLjggLTUsODkuMSBsIDM2MC41LDE5My4zIGMgNTEsMjcuMyAxMzQuNCwyNy4zIDE4NS40LDAgTCA5NTMuMiw3NDUuNiBjIDUxLC0yNy4zIDQ4LjcsLTY3LjQgLTUsLTg5LjEgeiIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvZz4KICAgIDxnCiAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIgogICAgICAgaWQ9Imc4IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzEwIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzEyIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzE0IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzE2IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzE4IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzIwIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzIyIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzI0IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzI2IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzI4IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzMwIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzMyIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzM0IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzM2IiAvPgogIDwvZz4KICA8ZwogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgaWQ9ImczOC0zIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuMDUyOTcyNjIsMCwwLDAuMDcwMDI0MTQsMTcuODIyMzYxLDQuNTgzMzQyMykiPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzYtOCI+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICAgIGlkPSJwYXRoNC0yIgogICAgICAgICBkPSJtIDUwMCw2MDYuMSBjIDEyMiwwIDIyNC4xLC00MS41IDIzMC4zLC05NS42IC0xOC4xLC01MC45IC0zNy45LC0xMDYuNCAtNTcuNCwtMTYwLjkgLTEzLjUsMzguOSAtODgsNjYuMyAtMTcyLjksNjYuMyAtODQuOSwwIC0xNTkuNCwtMjcuNCAtMTcyLjksLTY2LjMgLTE5LjQsNTQuNSAtMzkuMiwxMTAgLTU3LjMsMTYwLjkgNi4xLDU0LjEgMTA4LjIsOTUuNiAyMzAuMiw5NS42IHogbSAwLC0zNDIuMyBjIDU3LjMsMCAxMTAuNiwtMTcuOCAxMjYuMiwtNDUuMyBDIDYwNC43LDE1OC4xIDU4Ni4yLDEwNi40IDU3NC42LDczLjcgNTY2LjgsNTEuOSA1MzEuNyw0MC42IDUwMCw0MC42IGMgLTMxLjcsMCAtNjYuOCwxMS4zIC03NC42LDMzLjEgLTExLjYsMzIuNyAtMzAuMSw4NC41IC01MS42LDE0NC44IDE1LjYsMjcuNiA2OC45LDQ1LjMgMTI2LjIsNDUuMyB6IG0gNDQ4LjIsMzkyLjcgLTE5MS43LC03Ny4yIDIyLjEsNjEuNiBjIC0xLjEsNjUuMyAtMTI3LjgsMTE3LjMgLTI3OC42LDExNy4zIC0xNTAuNywwIC0yNzcuNSwtNTIgLTI3OC42LC0xMTcuMyBsIDIyLjEsLTYxLjYgLTE5MS43LDc3LjIgYyAtNTMuNywyMS43IC01Niw2MS44IC01LDg5LjEgbCAzNjAuNSwxOTMuMyBjIDUxLDI3LjMgMTM0LjQsMjcuMyAxODUuNCwwIEwgOTUzLjIsNzQ1LjYgYyA1MSwtMjcuMyA0OC43LC02Ny40IC01LC04OS4xIHoiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L2c+CiAgICA8ZwogICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICAgIGlkPSJnOC0zIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzEwLTkiIC8+CiAgICA8ZwogICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICAgIGlkPSJnMTItMCIgLz4KICAgIDxnCiAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIgogICAgICAgaWQ9ImcxNC0xIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzE2LTEiIC8+CiAgICA8ZwogICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICAgIGlkPSJnMTgtMiIgLz4KICAgIDxnCiAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIgogICAgICAgaWQ9ImcyMC0zIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzIyLTEiIC8+CiAgICA8ZwogICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICAgIGlkPSJnMjQtOSIgLz4KICAgIDxnCiAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIgogICAgICAgaWQ9ImcyNi05IiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzI4LTEiIC8+CiAgICA8ZwogICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICAgIGlkPSJnMzAtMSIgLz4KICAgIDxnCiAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIgogICAgICAgaWQ9ImczMi0xIiAvPgogICAgPGcKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgICBpZD0iZzM0LTAiIC8+CiAgICA8ZwogICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICAgIGlkPSJnMzYtMiIgLz4KICA8L2c+CiAgPGcKICAgICBpZD0iZzYtMSIKICAgICBzdHlsZT0iZmlsbDojMDAwMDAwIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuMDUyOTcyNjIsMCwwLDAuMDcwMDI0MTQsNC40MzUzNTg2LDE4MS40Nzc4MSkiPgogICAgPHBhdGgKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBkPSJtIDUwMCw2MDYuMSBjIDEyMiwwIDIyNC4xLC00MS41IDIzMC4zLC05NS42IC0xOC4xLC01MC45IC0zNy45LC0xMDYuNCAtNTcuNCwtMTYwLjkgLTEzLjUsMzguOSAtODgsNjYuMyAtMTcyLjksNjYuMyAtODQuOSwwIC0xNTkuNCwtMjcuNCAtMTcyLjksLTY2LjMgLTE5LjQsNTQuNSAtMzkuMiwxMTAgLTU3LjMsMTYwLjkgNi4xLDU0LjEgMTA4LjIsOTUuNiAyMzAuMiw5NS42IHogbSAwLC0zNDIuMyBjIDU3LjMsMCAxMTAuNiwtMTcuOCAxMjYuMiwtNDUuMyBDIDYwNC43LDE1OC4xIDU4Ni4yLDEwNi40IDU3NC42LDczLjcgNTY2LjgsNTEuOSA1MzEuNyw0MC42IDUwMCw0MC42IGMgLTMxLjcsMCAtNjYuOCwxMS4zIC03NC42LDMzLjEgLTExLjYsMzIuNyAtMzAuMSw4NC41IC01MS42LDE0NC44IDE1LjYsMjcuNiA2OC45LDQ1LjMgMTI2LjIsNDUuMyB6IG0gNDQ4LjIsMzkyLjcgLTE5MS43LC03Ny4yIDIyLjEsNjEuNiBjIC0xLjEsNjUuMyAtMTI3LjgsMTE3LjMgLTI3OC42LDExNy4zIC0xNTAuNywwIC0yNzcuNSwtNTIgLTI3OC42LC0xMTcuMyBsIDIyLjEsLTYxLjYgLTE5MS43LDc3LjIgYyAtNTMuNywyMS43IC01Niw2MS44IC01LDg5LjEgbCAzNjAuNSwxOTMuMyBjIDUxLDI3LjMgMTM0LjQsMjcuMyAxODUuNCwwIEwgOTUzLjIsNzQ1LjYgYyA1MSwtMjcuMyA0OC43LC02Ny40IC01LC04OS4xIHoiCiAgICAgICBpZD0icGF0aDQtMyIKICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiIC8+CiAgPC9nPgogIDxnCiAgICAgaWQ9Imc4LTciCiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjA1Mjk3MjYyLDAsMCwwLjA3MDAyNDE0LDQuNDM1MzU4NiwxODIuMjc2OCkiIC8+CiAgPGcKICAgICBpZD0iZzEwLTciCiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjA1Mjk3MjYyLDAsMCwwLjA3MDAyNDE0LDQuNDM1MzU4NiwxODIuMjc2OCkiIC8+CiAgPGcKICAgICBpZD0iZzEyLTIiCiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjA1Mjk3MjYyLDAsMCwwLjA3MDAyNDE0LDQuNDM1MzU4NiwxODIuMjc2OCkiIC8+CiAgPGcKICAgICBpZD0iZzE0LTYiCiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjA1Mjk3MjYyLDAsMCwwLjA3MDAyNDE0LDQuNDM1MzU4NiwxODIuMjc2OCkiIC8+CiAgPGcKICAgICBpZD0iZzE2LTYiCiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjA1Mjk3MjYyLDAsMCwwLjA3MDAyNDE0LDQuNDM1MzU4NiwxODIuMjc2OCkiIC8+CiAgPGcKICAgICBpZD0iZzE4LTI4IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImcyMC05IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImcyMi05IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImcyNC03IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImcyNi04IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImcyOC00IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImczMC0zIgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImczMi01IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImczNC01IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxnCiAgICAgaWQ9ImczNi00IgogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4wNTI5NzI2MiwwLDAsMC4wNzAwMjQxNCw0LjQzNTM1ODYsMTgyLjI3NjgpIiAvPgogIDxyZWN0CiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6NS4xMTAzMDc2OTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6MjAuNDQxMjMzNjUsIDUuMTEwMzA4NDE7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIgogICAgIGlkPSJyZWN0MTAwOCIKICAgICB3aWR0aD0iNTkuMDE5MDYyIgogICAgIGhlaWdodD0iOC4zMDQyMTM1IgogICAgIHg9IjkyLjM3NTk1NCIKICAgICB5PSItMTMuNDU3MjQzIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNjA0NTIyNDYsMC43OTY1ODgxLC0wLjY2MzQ0ODA0LDAuNzQ4MjIyMzYsMCwwKSIgLz4KICA8cmVjdAogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjcuNjczNTk1NDM7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5OjMwLjY5NDM4MjIsIDcuNjczNTk1NTU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIgogICAgIGlkPSJyZWN0MTAxMCIKICAgICB3aWR0aD0iNy42NzI2MTY1IgogICAgIGhlaWdodD0iNjMuMDEyODI5IgogICAgIHg9IjE3NC4zNjg5MyIKICAgICB5PSI1Ni45MTQwNzQiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC43MjcwMjQ4MywwLjY4NjYxMTE3LC0wLjY0ODI5MTk3LDAuNzYxMzkxODMsMCwwKSIgLz4KICA8cmVjdAogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjcuNjY4NTEwOTE7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5OjMwLjY3NDA0NDkyLCA3LjY2ODUxMTIzO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBpZD0icmVjdDEwMTIiCiAgICAgd2lkdGg9IjU4LjIxNTU2MSIKICAgICBoZWlnaHQ9IjguODQxNjQxNCIKICAgICB4PSIxNTAuODAxNDgiCiAgICAgeT0iMTM4LjM3OTE0IiAvPgo8L3N2Zz4K"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="8"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>nom_ini</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" opacity="1" labelPlacementMethod="XHeight" width="15" enabled="0" scaleBasedVisibility="0" barWidth="5" backgroundColor="#ffffff" scaleDependency="Area" sizeScale="3x:0,0,0,0,0,0" diagramOrientation="Up" maxScaleDenominator="1e+08" height="15" rotationOffset="270" backgroundAlpha="255" sizeType="MM" lineSizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" penAlpha="255" minimumSize="0" penColor="#000000" minScaleDenominator="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="0" showAll="1" obstacle="0" linePlacementFlags="18" priority="0" dist="0" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_poste_ini">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="num_point_ini">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_etude">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Etudes_237c30ee_9527_4971_8774_4bafbc1cd77e" name="Layer" type="QString"/>
            <Option value="Etudes" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='base' host=localhost port=5434 user='postgres' key='id' estimatedmetadata=true checkPrimaryKeyUnicity='1' table=&quot;o_geoflux_survey&quot;.&quot;etude&quot; sql=id = ANY(ARRAY[1]::integer[])" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="nom" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="angle">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lib_sens">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="route">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_loc">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_command">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="siren_siret" name="Key" type="QString"/>
            <Option value="Entreprises_ou_établissements_69d31d26_f369_4eab_8fb6_93d9a65fac65" name="Layer" type="QString"/>
            <Option value="Entreprises ou établissements" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="nom_court" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_point_cpt">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Points_de_comptage_e9a57104_361b_4321_9657_d6560fcb457d" name="Layer" type="QString"/>
            <Option value="Points de comptage" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="id" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="annee">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2030" name="Max" type="int"/>
            <Option value="1980" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enq_vl">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enq_pl">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Gestionnaires_des_données_78a88376_b753_4fd9_b30b_a44cf72fa343" name="Layer" type="QString"/>
            <Option value="Gestionnaires des données" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="nom" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prive">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="ID" field="id" index="0"/>
    <alias name="Code poste" field="code_poste_ini" index="1"/>
    <alias name="Num point" field="num_point_ini" index="2"/>
    <alias name="Etude" field="id_etude" index="3"/>
    <alias name="Angle" field="angle" index="4"/>
    <alias name="Libellé sens" field="lib_sens" index="5"/>
    <alias name="Route" field="route" index="6"/>
    <alias name="Localisation" field="prec_loc" index="7"/>
    <alias name="Commanditaire" field="id_command" index="8"/>
    <alias name="ID point comptage auto" field="id_point_cpt" index="9"/>
    <alias name="Commentaires" field="comment" index="10"/>
    <alias name="Année" field="annee" index="11"/>
    <alias name="VL enquêtés ?" field="enq_vl" index="12"/>
    <alias name="PL enquêtés ?" field="enq_pl" index="13"/>
    <alias name="Gestionnaire donnée" field="gestionnaire" index="14"/>
    <alias name="Ligne privée ?" field="prive" index="15"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id"/>
    <default expression="" applyOnUpdate="0" field="code_poste_ini"/>
    <default expression="" applyOnUpdate="0" field="num_point_ini"/>
    <default expression="" applyOnUpdate="0" field="id_etude"/>
    <default expression="" applyOnUpdate="0" field="angle"/>
    <default expression="" applyOnUpdate="0" field="lib_sens"/>
    <default expression="" applyOnUpdate="0" field="route"/>
    <default expression="" applyOnUpdate="0" field="prec_loc"/>
    <default expression="" applyOnUpdate="0" field="id_command"/>
    <default expression="" applyOnUpdate="0" field="id_point_cpt"/>
    <default expression="" applyOnUpdate="0" field="comment"/>
    <default expression="" applyOnUpdate="0" field="annee"/>
    <default expression="" applyOnUpdate="0" field="enq_vl"/>
    <default expression="" applyOnUpdate="0" field="enq_pl"/>
    <default expression="" applyOnUpdate="0" field="gestionnaire"/>
    <default expression="" applyOnUpdate="0" field="prive"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="id" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="code_poste_ini" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" constraints="1" field="num_point_ini" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="id_etude" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="angle" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" constraints="1" field="lib_sens" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="route" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="prec_loc" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="id_command" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="id_point_cpt" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="comment" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="annee" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="enq_vl" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="enq_pl" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="gestionnaire" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" constraints="1" field="prive" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="code_poste_ini" exp=""/>
    <constraint desc="" field="num_point_ini" exp=""/>
    <constraint desc="" field="id_etude" exp=""/>
    <constraint desc="" field="angle" exp=""/>
    <constraint desc="" field="lib_sens" exp=""/>
    <constraint desc="" field="route" exp=""/>
    <constraint desc="" field="prec_loc" exp=""/>
    <constraint desc="" field="id_command" exp=""/>
    <constraint desc="" field="id_point_cpt" exp=""/>
    <constraint desc="" field="comment" exp=""/>
    <constraint desc="" field="annee" exp=""/>
    <constraint desc="" field="enq_vl" exp=""/>
    <constraint desc="" field="enq_pl" exp=""/>
    <constraint desc="" field="gestionnaire" exp=""/>
    <constraint desc="" field="prive" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column name="id" hidden="0" type="field" width="-1"/>
      <column name="code_poste_ini" hidden="0" type="field" width="-1"/>
      <column name="num_point_ini" hidden="0" type="field" width="-1"/>
      <column name="id_etude" hidden="0" type="field" width="134"/>
      <column name="angle" hidden="0" type="field" width="-1"/>
      <column name="lib_sens" hidden="0" type="field" width="-1"/>
      <column name="prec_loc" hidden="0" type="field" width="-1"/>
      <column name="comment" hidden="0" type="field" width="-1"/>
      <column name="gestionnaire" hidden="0" type="field" width="-1"/>
      <column name="prive" hidden="0" type="field" width="-1"/>
      <column name="annee" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column name="route" hidden="0" type="field" width="-1"/>
      <column name="id_command" hidden="0" type="field" width="100"/>
      <column name="id_point_cpt" hidden="0" type="field" width="142"/>
      <column name="enq_vl" hidden="0" type="field" width="-1"/>
      <column name="enq_pl" hidden="0" type="field" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="angle"/>
    <field editable="1" name="annee"/>
    <field editable="1" name="code_poste_ini"/>
    <field editable="1" name="comment"/>
    <field editable="1" name="enq_pl"/>
    <field editable="1" name="enq_vl"/>
    <field editable="1" name="gestionnaire"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_command"/>
    <field editable="1" name="id_commanditaire"/>
    <field editable="1" name="id_cpt_detail_horaire"/>
    <field editable="1" name="id_etude"/>
    <field editable="1" name="id_point_cpt"/>
    <field editable="1" name="id_questionnaire_pl"/>
    <field editable="1" name="id_questionnaire_vl"/>
    <field editable="1" name="lib_sens"/>
    <field editable="1" name="nom_ini"/>
    <field editable="1" name="nom_route"/>
    <field editable="1" name="num_point_ini"/>
    <field editable="1" name="prec_loc"/>
    <field editable="1" name="prive"/>
    <field editable="1" name="quest_pl"/>
    <field editable="1" name="quest_vl"/>
    <field editable="1" name="route"/>
  </editable>
  <labelOnTop>
    <field name="angle" labelOnTop="0"/>
    <field name="annee" labelOnTop="0"/>
    <field name="code_poste_ini" labelOnTop="0"/>
    <field name="comment" labelOnTop="0"/>
    <field name="enq_pl" labelOnTop="0"/>
    <field name="enq_vl" labelOnTop="0"/>
    <field name="gestionnaire" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="id_command" labelOnTop="0"/>
    <field name="id_commanditaire" labelOnTop="0"/>
    <field name="id_cpt_detail_horaire" labelOnTop="0"/>
    <field name="id_etude" labelOnTop="0"/>
    <field name="id_point_cpt" labelOnTop="0"/>
    <field name="id_questionnaire_pl" labelOnTop="0"/>
    <field name="id_questionnaire_vl" labelOnTop="0"/>
    <field name="lib_sens" labelOnTop="0"/>
    <field name="nom_ini" labelOnTop="0"/>
    <field name="nom_route" labelOnTop="0"/>
    <field name="num_point_ini" labelOnTop="0"/>
    <field name="prec_loc" labelOnTop="0"/>
    <field name="prive" labelOnTop="0"/>
    <field name="quest_pl" labelOnTop="0"/>
    <field name="quest_vl" labelOnTop="0"/>
    <field name="route" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>nom_ini</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
