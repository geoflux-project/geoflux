<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" minScale="1e+08" readOnly="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>nom</value>
      <value>"nom"</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Gestionnaires_des_données_78a88376_b753_4fd9_b30b_a44cf72fa343" name="Layer" type="QString"/>
            <Option value="Gestionnaires des données" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='base' host=localhost port=5434 user='postgres' key='id' estimatedmetadata=true checkPrimaryKeyUnicity='1' table=&quot;o_geoflux_survey&quot;.&quot;gestionnaire_donnee&quot; (geom) sql=" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="nom" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prive">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pj">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="ID" field="id" index="0"/>
    <alias name="Nom" field="nom" index="1"/>
    <alias name="Commentaire" field="comment" index="2"/>
    <alias name="Gestionnaire donnée" field="gestionnaire" index="3"/>
    <alias name="Ligne privée ?" field="prive" index="4"/>
    <alias name="PJ" field="pj" index="5"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id"/>
    <default expression="" applyOnUpdate="0" field="nom"/>
    <default expression="" applyOnUpdate="0" field="comment"/>
    <default expression="" applyOnUpdate="0" field="gestionnaire"/>
    <default expression="" applyOnUpdate="0" field="prive"/>
    <default expression="" applyOnUpdate="0" field="pj"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="id" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="1" constraints="3" field="nom" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="comment" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" constraints="1" field="gestionnaire" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" constraints="1" field="prive" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" constraints="0" field="pj" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="nom" exp=""/>
    <constraint desc="" field="comment" exp=""/>
    <constraint desc="" field="gestionnaire" exp=""/>
    <constraint desc="" field="prive" exp=""/>
    <constraint desc="" field="pj" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column name="id" hidden="0" type="field" width="-1"/>
      <column name="nom" hidden="0" type="field" width="-1"/>
      <column name="comment" hidden="0" type="field" width="-1"/>
      <column name="gestionnaire" hidden="0" type="field" width="-1"/>
      <column name="prive" hidden="0" type="field" width="-1"/>
      <column name="pj" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="comment"/>
    <field editable="1" name="gestionnaire"/>
    <field editable="1" name="id"/>
    <field editable="1" name="nom"/>
    <field editable="1" name="pj"/>
    <field editable="1" name="prive"/>
  </editable>
  <labelOnTop>
    <field name="comment" labelOnTop="0"/>
    <field name="gestionnaire" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="nom" labelOnTop="0"/>
    <field name="pj" labelOnTop="0"/>
    <field name="prive" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>nom</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
