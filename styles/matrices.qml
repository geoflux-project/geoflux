<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" maxScale="0" version="3.10.6-A Coruña" styleCategories="AllStyleCategories" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zonage">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="Zonages_73381e20_e368_44f4_bcb6_e684c1f7ec24" type="QString" name="Layer"/>
            <Option value="Zonages" type="QString" name="LayerName"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="scenario">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="periode">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="modes">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="motifs">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="types_flux">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name="ID"/>
    <alias index="1" field="nom" name="Nom"/>
    <alias index="2" field="id_zonage" name="Zonage"/>
    <alias index="3" field="scenario" name="Scénario"/>
    <alias index="4" field="periode" name="Période"/>
    <alias index="5" field="modes" name="Modes"/>
    <alias index="6" field="motifs" name="Motifs"/>
    <alias index="7" field="types_flux" name="Types flux"/>
    <alias index="8" field="comment" name="Commentaires"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="nom" applyOnUpdate="0"/>
    <default expression="" field="id_zonage" applyOnUpdate="0"/>
    <default expression="" field="scenario" applyOnUpdate="0"/>
    <default expression="" field="periode" applyOnUpdate="0"/>
    <default expression="" field="modes" applyOnUpdate="0"/>
    <default expression="" field="motifs" applyOnUpdate="0"/>
    <default expression="" field="types_flux" applyOnUpdate="0"/>
    <default expression="" field="comment" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" unique_strength="1" field="id" notnull_strength="1" exp_strength="0"/>
    <constraint constraints="2" unique_strength="1" field="nom" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="id_zonage" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="scenario" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="periode" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="modes" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="motifs" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="types_flux" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="comment" notnull_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="nom"/>
    <constraint exp="" desc="" field="id_zonage"/>
    <constraint exp="" desc="" field="scenario"/>
    <constraint exp="" desc="" field="periode"/>
    <constraint exp="" desc="" field="modes"/>
    <constraint exp="" desc="" field="motifs"/>
    <constraint exp="" desc="" field="types_flux"/>
    <constraint exp="" desc="" field="comment"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" type="field" hidden="0" name="id"/>
      <column width="93" type="field" hidden="0" name="nom"/>
      <column width="169" type="field" hidden="0" name="id_zonage"/>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" hidden="0" name="scenario"/>
      <column width="-1" type="field" hidden="0" name="periode"/>
      <column width="126" type="field" hidden="0" name="modes"/>
      <column width="228" type="field" hidden="0" name="motifs"/>
      <column width="-1" type="field" hidden="0" name="types_flux"/>
      <column width="485" type="field" hidden="0" name="comment"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="comment"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_zonage"/>
    <field editable="1" name="modes"/>
    <field editable="1" name="motifs"/>
    <field editable="1" name="nom"/>
    <field editable="1" name="periode"/>
    <field editable="1" name="scenario"/>
    <field editable="1" name="types_flux"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_zonage"/>
    <field labelOnTop="0" name="modes"/>
    <field labelOnTop="0" name="motifs"/>
    <field labelOnTop="0" name="nom"/>
    <field labelOnTop="0" name="periode"/>
    <field labelOnTop="0" name="scenario"/>
    <field labelOnTop="0" name="types_flux"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
