<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" simplifyLocal="1" simplifyAlgorithm="0" styleCategories="AllStyleCategories" minScale="1e+08" labelsEnabled="0" simplifyDrawingTol="1" simplifyMaxScale="1" readOnly="0" hasScaleBasedVisibilityFlag="0" maxScale="0" simplifyDrawingHints="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" symbollevels="0" type="singleSymbol" enableorderby="0">
    <symbols>
      <symbol clip_to_extent="1" type="marker" name="0" force_rhr="0" alpha="1">
        <layer enabled="1" pass="0" locked="0" class="SimpleMarker">
          <prop k="angle" v="0"/>
          <prop k="color" v="219,30,42,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="128,17,25,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="1"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="size">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="&quot;size&quot; * 10"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id_zone</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundColor="#ffffff" scaleBasedVisibility="0" penWidth="0" minimumSize="0" sizeScale="3x:0,0,0,0,0,0" width="15" height="15" enabled="0" diagramOrientation="Up" lineSizeType="MM" rotationOffset="270" opacity="1" sizeType="MM" maxScaleDenominator="1e+08" penColor="#000000" scaleDependency="Area" barWidth="5" backgroundAlpha="255" minScaleDenominator="0" lineSizeScale="3x:0,0,0,0,0,0" penAlpha="255" labelPlacementMethod="XHeight">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="0" linePlacementFlags="18" priority="0" zIndex="0" obstacle="0" showAll="1" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Zones_2991bd5d_212a_4173_94ef_561ac862aa5c"/>
            <Option type="QString" name="LayerName" value="Zones"/>
            <Option type="QString" name="LayerProviderName" value="postgres"/>
            <Option type="QString" name="LayerSource" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(id_zonage = 6)"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="code_zone"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="valeur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="echant">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id_zone" name="Code zone"/>
    <alias index="1" field="valeur" name="Valeur"/>
    <alias index="2" field="echant" name="Echant"/>
    <alias index="3" field="size" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id_zone" applyOnUpdate="0" expression=""/>
    <default field="valeur" applyOnUpdate="0" expression=""/>
    <default field="echant" applyOnUpdate="0" expression=""/>
    <default field="size" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="3" exp_strength="0" field="id_zone" unique_strength="1" notnull_strength="1"/>
    <constraint constraints="3" exp_strength="0" field="valeur" unique_strength="1" notnull_strength="1"/>
    <constraint constraints="3" exp_strength="0" field="echant" unique_strength="1" notnull_strength="1"/>
    <constraint constraints="3" exp_strength="0" field="size" unique_strength="1" notnull_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_zone" desc="" exp=""/>
    <constraint field="valeur" desc="" exp=""/>
    <constraint field="echant" desc="" exp=""/>
    <constraint field="size" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="&quot;id_zone&quot;">
    <columns>
      <column width="-1" type="field" name="id_zone" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" name="valeur" hidden="0"/>
      <column width="-1" type="field" name="echant" hidden="0"/>
      <column width="-1" type="field" name="size" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="attires"/>
    <field editable="1" name="attra"/>
    <field editable="1" name="echant"/>
    <field editable="1" name="emis"/>
    <field editable="1" name="emiss"/>
    <field editable="1" name="id_zone"/>
    <field editable="1" name="size"/>
    <field editable="1" name="size_attra"/>
    <field editable="1" name="size_emiss"/>
    <field editable="1" name="size_total"/>
    <field editable="1" name="total"/>
    <field editable="1" name="valeur"/>
  </editable>
  <labelOnTop>
    <field name="attires" labelOnTop="0"/>
    <field name="attra" labelOnTop="0"/>
    <field name="echant" labelOnTop="0"/>
    <field name="emis" labelOnTop="0"/>
    <field name="emiss" labelOnTop="0"/>
    <field name="id_zone" labelOnTop="0"/>
    <field name="size" labelOnTop="0"/>
    <field name="size_attra" labelOnTop="0"/>
    <field name="size_emiss" labelOnTop="0"/>
    <field name="size_total" labelOnTop="0"/>
    <field name="total" labelOnTop="0"/>
    <field name="valeur" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
