<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" maxScale="0" minScale="1e+08" version="3.10.6-A Coruña" hasScaleBasedVisibilityFlag="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id_matrice</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_orig">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Zone_s__origine_cf4fb9dc_5e7a_418e_82c7_365a09fe6f17" name="Layer" type="QString"/>
            <Option value="Zone(s) origine" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="code_zone" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_dest">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Zone_s__destination_8fb0c7a9_af4e_40ab_95ed_0e20aff04397" name="Layer" type="QString"/>
            <Option value="Zone(s) destination" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="code_zone" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="valeur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id"/>
    <alias index="1" name="Code zone orig" field="id_zone_orig"/>
    <alias index="2" name="Code zone dest" field="id_zone_dest"/>
    <alias index="3" name="Valeur" field="valeur"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="id_zone_orig" expression="" applyOnUpdate="0"/>
    <default field="id_zone_dest" expression="" applyOnUpdate="0"/>
    <default field="valeur" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" notnull_strength="1" unique_strength="1" exp_strength="0" field="id"/>
    <constraint constraints="1" notnull_strength="1" unique_strength="0" exp_strength="0" field="id_zone_orig"/>
    <constraint constraints="1" notnull_strength="1" unique_strength="0" exp_strength="0" field="id_zone_dest"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" exp_strength="0" field="valeur"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="id_zone_orig"/>
    <constraint exp="" desc="" field="id_zone_dest"/>
    <constraint exp="" desc="" field="valeur"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="1" sortExpression="&quot;valeur&quot;">
    <columns>
      <column hidden="1" width="-1" name="id" type="field"/>
      <column hidden="0" width="-1" name="id_zone_orig" type="field"/>
      <column hidden="0" width="-1" name="id_zone_dest" type="field"/>
      <column hidden="0" width="-1" name="valeur" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="borne_max_ic_valeur_redressee"/>
    <field editable="1" name="borne_max_ic_valeur_reponderee"/>
    <field editable="1" name="borne_min_ic_valeur_redressee"/>
    <field editable="1" name="borne_min_ic_valeur_reponderee"/>
    <field editable="1" name="echant"/>
    <field editable="1" name="echantillon"/>
    <field editable="1" name="formule_point_enq_fictif"/>
    <field editable="1" name="formule_point_enq_terrain"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_matrice"/>
    <field editable="1" name="id_zone_dest"/>
    <field editable="1" name="id_zone_orig"/>
    <field editable="1" name="valeur"/>
    <field editable="1" name="valeur_redressee"/>
    <field editable="1" name="valeur_reponderee"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="borne_max_ic_valeur_redressee"/>
    <field labelOnTop="0" name="borne_max_ic_valeur_reponderee"/>
    <field labelOnTop="0" name="borne_min_ic_valeur_redressee"/>
    <field labelOnTop="0" name="borne_min_ic_valeur_reponderee"/>
    <field labelOnTop="0" name="echant"/>
    <field labelOnTop="0" name="echantillon"/>
    <field labelOnTop="0" name="formule_point_enq_fictif"/>
    <field labelOnTop="0" name="formule_point_enq_terrain"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_matrice"/>
    <field labelOnTop="0" name="id_zone_dest"/>
    <field labelOnTop="0" name="id_zone_orig"/>
    <field labelOnTop="0" name="valeur"/>
    <field labelOnTop="0" name="valeur_redressee"/>
    <field labelOnTop="0" name="valeur_reponderee"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_matrice</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
