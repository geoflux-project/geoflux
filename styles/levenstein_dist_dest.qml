<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyMaxScale="1" simplifyDrawingHints="1" simplifyLocal="1" readOnly="0" styleCategories="AllStyleCategories" minScale="1e+08" maxScale="0" simplifyDrawingTol="1" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" version="3.10.6-A Coruña" labelsEnabled="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 attr="d_nlod" graduatedMethod="GraduatedColor" symbollevels="0" forceraster="0" type="graduatedSymbol" enableorderby="0">
    <ranges>
      <range upper="0.100000000000000" symbol="0" lower="0.000000000000000" label="0 - 0,1 " render="true"/>
      <range upper="0.200000000000000" symbol="1" lower="0.100000000000000" label="0,1 - 0,2 " render="true"/>
      <range upper="0.300000000000000" symbol="2" lower="0.200000000000000" label="0,2 - 0,3 " render="true"/>
      <range upper="0.400000000000000" symbol="3" lower="0.300000000000000" label="0,3 - 0,4 " render="true"/>
      <range upper="0.500000000000000" symbol="4" lower="0.400000000000000" label="0,4 - 0,5 " render="true"/>
      <range upper="0.600000000000000" symbol="5" lower="0.500000000000000" label="0,5 - 0,6 " render="true"/>
      <range upper="0.700000000000000" symbol="6" lower="0.600000000000000" label="0,6 - 0,7 " render="true"/>
      <range upper="0.800000000000000" symbol="7" lower="0.700000000000000" label="0,7 - 0,8 " render="true"/>
      <range upper="0.900000000000000" symbol="8" lower="0.800000000000000" label="0,8 - 0,9 " render="true"/>
      <range upper="1.000000000000000" symbol="9" lower="0.900000000000000" label="0,9 - 1 " render="true"/>
    </ranges>
    <symbols>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="0">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,245,240,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="1">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,227,215,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="2">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="253,198,175,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="3">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="252,164,135,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="4">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="252,129,97,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="5">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="248,93,66,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="6">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="235,54,42,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="7">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="204,24,29,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="8">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="169,15,21,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="9">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="103,0,13,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="0">
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="160,44,44,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="141,141,141,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp type="gradient" name="[source]">
      <prop k="color1" v="255,245,240,255"/>
      <prop k="color2" v="103,0,13,255"/>
      <prop k="discrete" v="0"/>
      <prop k="rampType" v="gradient"/>
      <prop k="stops" v="0.13;254,224,210,255:0.26;252,187,161,255:0.39;252,146,114,255:0.52;251,106,74,255:0.65;239,59,44,255:0.78;203,24,29,255:0.9;165,15,21,255"/>
    </colorramp>
    <classificationMethod id="EqualInterval">
      <symmetricMode astride="0" symmetrypoint="0" enabled="0"/>
      <labelFormat labelprecision="4" trimtrailingzeroes="1" format="%1 - %2 "/>
      <extraInformation/>
    </classificationMethod>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundColor="#ffffff" labelPlacementMethod="XHeight" lineSizeScale="3x:0,0,0,0,0,0" opacity="1" rotationOffset="270" height="15" width="15" scaleDependency="Area" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" barWidth="5" penAlpha="255" penColor="#000000" minScaleDenominator="0" sizeType="MM" diagramOrientation="Up" maxScaleDenominator="1e+08" scaleBasedVisibility="0" penWidth="0" backgroundAlpha="255" enabled="0" minimumSize="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" zIndex="0" placement="1" linePlacementFlags="18" dist="0" showAll="1" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_comparaison">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_zone">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adjacency_correlation">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adjacency_correlation">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adjacency_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adjacency_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="id_comparaison" index="1" name=""/>
    <alias field="id_zone" index="2" name=""/>
    <alias field="o_adjacency_correlation" index="3" name=""/>
    <alias field="d_adjacency_correlation" index="4" name=""/>
    <alias field="o_adjacency_mssim" index="5" name=""/>
    <alias field="d_adjacency_mssim" index="6" name=""/>
    <alias field="o_nlod" index="7" name=""/>
    <alias field="d_nlod" index="8" name=""/>
    <alias field="o_struct_nlod" index="9" name=""/>
    <alias field="d_struct_nlod" index="10" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="id_comparaison" expression=""/>
    <default applyOnUpdate="0" field="id_zone" expression=""/>
    <default applyOnUpdate="0" field="o_adjacency_correlation" expression=""/>
    <default applyOnUpdate="0" field="d_adjacency_correlation" expression=""/>
    <default applyOnUpdate="0" field="o_adjacency_mssim" expression=""/>
    <default applyOnUpdate="0" field="d_adjacency_mssim" expression=""/>
    <default applyOnUpdate="0" field="o_nlod" expression=""/>
    <default applyOnUpdate="0" field="d_nlod" expression=""/>
    <default applyOnUpdate="0" field="o_struct_nlod" expression=""/>
    <default applyOnUpdate="0" field="d_struct_nlod" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" field="id" constraints="3" notnull_strength="1" exp_strength="0"/>
    <constraint unique_strength="0" field="id_comparaison" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="id_zone" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="o_adjacency_correlation" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="d_adjacency_correlation" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="o_adjacency_mssim" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="d_adjacency_mssim" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="o_nlod" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="d_nlod" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="o_struct_nlod" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="d_struct_nlod" constraints="0" notnull_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="id_comparaison" exp="" desc=""/>
    <constraint field="id_zone" exp="" desc=""/>
    <constraint field="o_adjacency_correlation" exp="" desc=""/>
    <constraint field="d_adjacency_correlation" exp="" desc=""/>
    <constraint field="o_adjacency_mssim" exp="" desc=""/>
    <constraint field="d_adjacency_mssim" exp="" desc=""/>
    <constraint field="o_nlod" exp="" desc=""/>
    <constraint field="d_nlod" exp="" desc=""/>
    <constraint field="o_struct_nlod" exp="" desc=""/>
    <constraint field="d_struct_nlod" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column hidden="0" type="field" width="-1" name="id_comparaison"/>
      <column hidden="0" type="field" width="-1" name="id_zone"/>
      <column hidden="1" type="actions" width="-1"/>
      <column hidden="0" type="field" width="-1" name="id"/>
      <column hidden="0" type="field" width="-1" name="o_adjacency_correlation"/>
      <column hidden="0" type="field" width="-1" name="d_adjacency_correlation"/>
      <column hidden="0" type="field" width="-1" name="o_adjacency_mssim"/>
      <column hidden="0" type="field" width="-1" name="d_adjacency_mssim"/>
      <column hidden="0" type="field" width="-1" name="o_nlod"/>
      <column hidden="0" type="field" width="-1" name="d_nlod"/>
      <column hidden="0" type="field" width="-1" name="o_struct_nlod"/>
      <column hidden="0" type="field" width="-1" name="d_struct_nlod"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="attractions_nlod" editable="1"/>
    <field name="attractions_struct_nlod" editable="1"/>
    <field name="d_adjacency_correlation" editable="1"/>
    <field name="d_adjacency_mssim" editable="1"/>
    <field name="d_nlod" editable="1"/>
    <field name="d_struct_nlod" editable="1"/>
    <field name="emissions_nlod" editable="1"/>
    <field name="emissions_struct_nlod" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_comparaison" editable="1"/>
    <field name="id_zone" editable="1"/>
    <field name="o_adjacency_correlation" editable="1"/>
    <field name="o_adjacency_mssim" editable="1"/>
    <field name="o_nlod" editable="1"/>
    <field name="o_struct_nlod" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="attractions_nlod"/>
    <field labelOnTop="0" name="attractions_struct_nlod"/>
    <field labelOnTop="0" name="d_adjacency_correlation"/>
    <field labelOnTop="0" name="d_adjacency_mssim"/>
    <field labelOnTop="0" name="d_nlod"/>
    <field labelOnTop="0" name="d_struct_nlod"/>
    <field labelOnTop="0" name="emissions_nlod"/>
    <field labelOnTop="0" name="emissions_struct_nlod"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_comparaison"/>
    <field labelOnTop="0" name="id_zone"/>
    <field labelOnTop="0" name="o_adjacency_correlation"/>
    <field labelOnTop="0" name="o_adjacency_mssim"/>
    <field labelOnTop="0" name="o_nlod"/>
    <field labelOnTop="0" name="o_struct_nlod"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_comparaison</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
