<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" simplifyMaxScale="1" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" version="3.10.6-A Coruña" simplifyDrawingTol="1" maxScale="0" simplifyLocal="1" minScale="1e+08" simplifyDrawingHints="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" symbollevels="0" forceraster="0">
    <symbols>
      <symbol type="marker" alpha="0.5" name="0" clip_to_extent="1" force_rhr="0">
        <layer class="SimpleMarker" pass="0" locked="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="51,160,44,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="255,255,255,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="5" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions" value="id"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory backgroundAlpha="255" backgroundColor="#ffffff" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" penColor="#000000" minScaleDenominator="0" scaleBasedVisibility="0" height="15" maxScaleDenominator="1e+08" labelPlacementMethod="XHeight" opacity="1" sizeScale="3x:0,0,0,0,0,0" sizeType="MM" scaleDependency="Area" width="15" rotationOffset="270" penWidth="0" lineSizeType="MM" barWidth="5" enabled="0" diagramOrientation="Up" penAlpha="255">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" obstacle="0" zIndex="0" showAll="1" dist="0" placement="0" priority="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_pays">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_zone">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_com">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_pole">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type_pole">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_voie">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="code_adr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="libelle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type_lieu">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom_polygon">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name=""/>
    <alias index="1" field="code_pays" name=""/>
    <alias index="2" field="code_zone" name=""/>
    <alias index="3" field="code_com" name=""/>
    <alias index="4" field="code_pole" name=""/>
    <alias index="5" field="type_pole" name=""/>
    <alias index="6" field="code_voie" name=""/>
    <alias index="7" field="code_adr" name=""/>
    <alias index="8" field="libelle" name=""/>
    <alias index="9" field="type_lieu" name=""/>
    <alias index="10" field="geom_polygon" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="code_pays" applyOnUpdate="0"/>
    <default expression="" field="code_zone" applyOnUpdate="0"/>
    <default expression="" field="code_com" applyOnUpdate="0"/>
    <default expression="" field="code_pole" applyOnUpdate="0"/>
    <default expression="" field="type_pole" applyOnUpdate="0"/>
    <default expression="" field="code_voie" applyOnUpdate="0"/>
    <default expression="" field="code_adr" applyOnUpdate="0"/>
    <default expression="" field="libelle" applyOnUpdate="0"/>
    <default expression="" field="type_lieu" applyOnUpdate="0"/>
    <default expression="" field="geom_polygon" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" constraints="3" exp_strength="0" field="id" notnull_strength="1"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="code_pays" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="code_zone" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="code_com" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="code_pole" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="type_pole" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="code_voie" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="code_adr" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="libelle" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="type_lieu" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="geom_polygon" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="code_pays"/>
    <constraint desc="" exp="" field="code_zone"/>
    <constraint desc="" exp="" field="code_com"/>
    <constraint desc="" exp="" field="code_pole"/>
    <constraint desc="" exp="" field="type_pole"/>
    <constraint desc="" exp="" field="code_voie"/>
    <constraint desc="" exp="" field="code_adr"/>
    <constraint desc="" exp="" field="libelle"/>
    <constraint desc="" exp="" field="type_lieu"/>
    <constraint desc="" exp="" field="geom_polygon"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column type="field" width="-1" hidden="0" name="id"/>
      <column type="field" width="-1" hidden="0" name="code_pays"/>
      <column type="field" width="-1" hidden="0" name="code_zone"/>
      <column type="field" width="-1" hidden="0" name="code_com"/>
      <column type="field" width="-1" hidden="0" name="code_pole"/>
      <column type="field" width="-1" hidden="0" name="code_voie"/>
      <column type="field" width="-1" hidden="0" name="code_adr"/>
      <column type="field" width="-1" hidden="0" name="libelle"/>
      <column type="field" width="-1" hidden="0" name="type_lieu"/>
      <column type="field" width="-1" hidden="1" name="geom_polygon"/>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" width="-1" hidden="0" name="type_pole"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="code_adr"/>
    <field editable="1" name="code_com"/>
    <field editable="1" name="code_pays"/>
    <field editable="1" name="code_pole"/>
    <field editable="1" name="code_voie"/>
    <field editable="1" name="code_zone"/>
    <field editable="1" name="geom_polygon"/>
    <field editable="1" name="id"/>
    <field editable="1" name="libelle"/>
    <field editable="1" name="type_lieu"/>
    <field editable="1" name="type_pole"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="code_adr"/>
    <field labelOnTop="0" name="code_com"/>
    <field labelOnTop="0" name="code_pays"/>
    <field labelOnTop="0" name="code_pole"/>
    <field labelOnTop="0" name="code_voie"/>
    <field labelOnTop="0" name="code_zone"/>
    <field labelOnTop="0" name="geom_polygon"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="libelle"/>
    <field labelOnTop="0" name="type_lieu"/>
    <field labelOnTop="0" name="type_pole"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
