<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="0" simplifyDrawingHints="0" simplifyMaxScale="1" simplifyDrawingTol="1" readOnly="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="0" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" type="marker" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="255,127,0,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="arrow"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="227,26,28,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.8"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="7"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option value="true" type="bool" name="active"/>
                  <Option value="angle" type="QString" name="field"/>
                  <Option value="2" type="int" name="type"/>
                </Option>
              </Option>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="id" key="dualview/previewExpressions"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" backgroundColor="#ffffff" sizeType="MM" penColor="#000000" enabled="0" barWidth="5" penAlpha="255" scaleDependency="Area" rotationOffset="270" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" scaleBasedVisibility="0" width="15" lineSizeType="MM" maxScaleDenominator="0" diagramOrientation="Up" labelPlacementMethod="XHeight" opacity="1" minScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="0" zIndex="0" linePlacementFlags="18" showAll="1" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_ini">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="num_point_ini">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="0" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="angle">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="360" type="double" name="Max"/>
            <Option value="0" type="double" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="double" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lib_sens">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="yyyy-MM-dd" type="QString" name="display_format"/>
            <Option value="yyyy-MM-dd" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_route">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_gestionnaire_route">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="siren_siret" type="QString" name="Key"/>
            <Option value="entreprise_etablissement20181003162411190" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom_court" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pr">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="abs">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_pos">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" type="QString" name="CheckedState"/>
            <Option value="" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_loc">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="type_enq">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="10" type="QString" name="Feux temporaires, support papier"/>
              </Option>
              <Option type="Map">
                <Option value="11" type="QString" name="Feux temporaires, support électronique"/>
              </Option>
              <Option type="Map">
                <Option value="12" type="QString" name="Feux permanents, support papier"/>
              </Option>
              <Option type="Map">
                <Option value="13" type="QString" name="Feux permanents, support électronique"/>
              </Option>
              <Option type="Map">
                <Option value="14" type="QString" name="Arrêt sur le côté, support papier"/>
              </Option>
              <Option type="Map">
                <Option value="15" type="QString" name="Arrêt sur le côté, support électronique"/>
              </Option>
              <Option type="Map">
                <Option value="16" type="QString" name="Arrêt pleine voie, support papier"/>
              </Option>
              <Option type="Map">
                <Option value="17" type="QString" name="Arrêt pleine voie, support électronique"/>
              </Option>
              <Option type="Map">
                <Option value="18" type="QString" name="Enquête embarquée (bateau, train), support papier"/>
              </Option>
              <Option type="Map">
                <Option value="19" type="QString" name="Enquête embarquée - bateau, train, support électronique"/>
              </Option>
              <Option type="Map">
                <Option value="2" type="QString" name="Feux permanents, support inconnu"/>
              </Option>
              <Option type="Map">
                <Option value="3" type="QString" name="Arrêt sur le côté, support inconnu"/>
              </Option>
              <Option type="Map">
                <Option value="4" type="QString" name="Déviation du flux par une aire, support inconnu"/>
              </Option>
              <Option type="Map">
                <Option value="5" type="QString" name="Arrêt pleine voie, support inconnu"/>
              </Option>
              <Option type="Map">
                <Option value="6" type="QString" name="Distribution enveloppes T"/>
              </Option>
              <Option type="Map">
                <Option value="7" type="QString" name="Enquête par Internet des usagers passés au télépéage"/>
              </Option>
              <Option type="Map">
                <Option value="8" type="QString" name="Retraitement recensement INSEE"/>
              </Option>
              <Option type="Map">
                <Option value="9" type="QString" name="Enquête embarquée (bateau, train), support inconnu"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_questionnaire_vl">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="questionnaire20180914161438984" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_questionnaire_pl">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="questionnaire20180914161438984" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_commanditaire">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="siren_siret" type="QString" name="Key"/>
            <Option value="entreprise_etablissement20181003162411190" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom_court" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_prest_amo">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="siren_siret" type="QString" name="Key"/>
            <Option value="entreprise_etablissement20181003162411190" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom_court" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_prest_terrain">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="siren_siret" type="QString" name="Key"/>
            <Option value="entreprise_etablissement20181003162411190" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom_court" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_campagne_enq">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="campagne_enq20170217132323497" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nb_enq">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="30" type="int" name="Max"/>
            <Option value="0" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nb_rec">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="30" type="int" name="Max"/>
            <Option value="0" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nb_chef_poste">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="30" type="int" name="Max"/>
            <Option value="0" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nb_rab">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="30" type="int" name="Max"/>
            <Option value="0" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nb_manip_feux">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="30" type="int" name="Max"/>
            <Option value="0" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fiable">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" type="QString" name="CheckedState"/>
            <Option value="" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_cpt_detail_horaire">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="0" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="photo1">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="1" type="int" name="DocumentViewer"/>
            <Option value="0" type="int" name="DocumentViewerHeight"/>
            <Option value="0" type="int" name="DocumentViewerWidth"/>
            <Option value="true" type="bool" name="FileWidget"/>
            <Option value="true" type="bool" name="FileWidgetButton"/>
            <Option value="" type="QString" name="FileWidgetFilter"/>
            <Option type="Map" name="PropertyCollection">
              <Option value="" type="QString" name="name"/>
              <Option type="invalid" name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
            <Option value="0" type="int" name="RelativeStorage"/>
            <Option value="0" type="int" name="StorageMode"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="photo2">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="1" type="int" name="DocumentViewer"/>
            <Option value="0" type="int" name="DocumentViewerHeight"/>
            <Option value="0" type="int" name="DocumentViewerWidth"/>
            <Option value="true" type="bool" name="FileWidget"/>
            <Option value="true" type="bool" name="FileWidgetButton"/>
            <Option value="" type="QString" name="FileWidgetFilter"/>
            <Option type="Map" name="PropertyCollection">
              <Option value="" type="QString" name="name"/>
              <Option type="invalid" name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
            <Option value="0" type="int" name="RelativeStorage"/>
            <Option value="0" type="int" name="StorageMode"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="photo3">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="1" type="int" name="DocumentViewer"/>
            <Option value="0" type="int" name="DocumentViewerHeight"/>
            <Option value="0" type="int" name="DocumentViewerWidth"/>
            <Option value="true" type="bool" name="FileWidget"/>
            <Option value="true" type="bool" name="FileWidgetButton"/>
            <Option value="" type="QString" name="FileWidgetFilter"/>
            <Option type="Map" name="PropertyCollection">
              <Option value="" type="QString" name="name"/>
              <Option type="invalid" name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
            <Option value="0" type="int" name="RelativeStorage"/>
            <Option value="0" type="int" name="StorageMode"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="exploit_standard">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="0" type="int" name="DocumentViewer"/>
            <Option value="0" type="int" name="DocumentViewerHeight"/>
            <Option value="0" type="int" name="DocumentViewerWidth"/>
            <Option value="true" type="bool" name="FileWidget"/>
            <Option value="true" type="bool" name="FileWidgetButton"/>
            <Option value="" type="QString" name="FileWidgetFilter"/>
            <Option type="Map" name="PropertyCollection">
              <Option value="" type="QString" name="name"/>
              <Option type="invalid" name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
            <Option value="0" type="int" name="RelativeStorage"/>
            <Option value="0" type="int" name="StorageMode"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="rapport_terrain">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="0" type="int" name="DocumentViewer"/>
            <Option value="0" type="int" name="DocumentViewerHeight"/>
            <Option value="0" type="int" name="DocumentViewerWidth"/>
            <Option value="true" type="bool" name="FileWidget"/>
            <Option value="true" type="bool" name="FileWidgetButton"/>
            <Option value="" type="QString" name="FileWidgetFilter"/>
            <Option type="Map" name="PropertyCollection">
              <Option value="" type="QString" name="name"/>
              <Option type="invalid" name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
            <Option value="0" type="int" name="RelativeStorage"/>
            <Option value="0" type="int" name="StorageMode"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="gestionnaire_cerema20181003162502152" type="QString" name="Layer"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prive">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" type="QString" name="CheckedState"/>
            <Option value="" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="Identifiant" index="0"/>
    <alias field="nom_ini" name="Nom dans le référentiel d'origine" index="1"/>
    <alias field="num_point_ini" name="Numéro dans le référentiel original" index="2"/>
    <alias field="angle" name="Angle sens enquêté" index="3"/>
    <alias field="lib_sens" name="Libellé sens enquêté" index="4"/>
    <alias field="date" name="Date de réalisation" index="5"/>
    <alias field="nom_route" name="Nom de la route" index="6"/>
    <alias field="id_gestionnaire_route" name="Gestionnaire de la route" index="7"/>
    <alias field="pr" name="PR" index="8"/>
    <alias field="abs" name="Abscisse" index="9"/>
    <alias field="prec_pos" name="Position précise ?" index="10"/>
    <alias field="prec_loc" name="Précision de localisation" index="11"/>
    <alias field="type_enq" name="Type de protocole d'enquête" index="12"/>
    <alias field="id_questionnaire_vl" name="Questionnaire VL" index="13"/>
    <alias field="id_questionnaire_pl" name="Questionnaire PL" index="14"/>
    <alias field="id_commanditaire" name="Commanditaire enquête" index="15"/>
    <alias field="id_prest_amo" name="Prestataire AMO" index="16"/>
    <alias field="id_prest_terrain" name="Prestataire terrain" index="17"/>
    <alias field="id_campagne_enq" name="Campagne d'enquête" index="18"/>
    <alias field="comment" name="Commentaires" index="19"/>
    <alias field="nb_enq" name="Nombre d'enquêteurs" index="20"/>
    <alias field="nb_rec" name="Nombre de recenseurs" index="21"/>
    <alias field="nb_chef_poste" name="Nombre de chefs de poste" index="22"/>
    <alias field="nb_rab" name="Nombre de rabatteurs" index="23"/>
    <alias field="nb_manip_feux" name="Nombre de manip feux" index="24"/>
    <alias field="fiable" name="Fiable ?" index="25"/>
    <alias field="id_cpt_detail_horaire" name="Identifiant du comptage" index="26"/>
    <alias field="photo1" name="Photo 1" index="27"/>
    <alias field="photo2" name="Photo 2" index="28"/>
    <alias field="photo3" name="Photo 3" index="29"/>
    <alias field="exploit_standard" name="Exploitation standard" index="30"/>
    <alias field="rapport_terrain" name="Rapport de terrain" index="31"/>
    <alias field="gestionnaire" name="Gestionnaire de la donnée" index="32"/>
    <alias field="prive" name="Ligne privée ?" index="33"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="nom_ini"/>
    <default applyOnUpdate="0" expression="" field="num_point_ini"/>
    <default applyOnUpdate="0" expression="" field="angle"/>
    <default applyOnUpdate="0" expression="" field="lib_sens"/>
    <default applyOnUpdate="0" expression="" field="date"/>
    <default applyOnUpdate="0" expression="" field="nom_route"/>
    <default applyOnUpdate="0" expression="" field="id_gestionnaire_route"/>
    <default applyOnUpdate="0" expression="" field="pr"/>
    <default applyOnUpdate="0" expression="" field="abs"/>
    <default applyOnUpdate="0" expression="" field="prec_pos"/>
    <default applyOnUpdate="0" expression="" field="prec_loc"/>
    <default applyOnUpdate="0" expression="" field="type_enq"/>
    <default applyOnUpdate="0" expression="" field="id_questionnaire_vl"/>
    <default applyOnUpdate="0" expression="" field="id_questionnaire_pl"/>
    <default applyOnUpdate="0" expression="" field="id_commanditaire"/>
    <default applyOnUpdate="0" expression="" field="id_prest_amo"/>
    <default applyOnUpdate="0" expression="" field="id_prest_terrain"/>
    <default applyOnUpdate="0" expression="" field="id_campagne_enq"/>
    <default applyOnUpdate="0" expression="" field="comment"/>
    <default applyOnUpdate="0" expression="" field="nb_enq"/>
    <default applyOnUpdate="0" expression="" field="nb_rec"/>
    <default applyOnUpdate="0" expression="" field="nb_chef_poste"/>
    <default applyOnUpdate="0" expression="" field="nb_rab"/>
    <default applyOnUpdate="0" expression="" field="nb_manip_feux"/>
    <default applyOnUpdate="0" expression="" field="fiable"/>
    <default applyOnUpdate="0" expression="" field="id_cpt_detail_horaire"/>
    <default applyOnUpdate="0" expression="" field="photo1"/>
    <default applyOnUpdate="0" expression="" field="photo2"/>
    <default applyOnUpdate="0" expression="" field="photo3"/>
    <default applyOnUpdate="0" expression="" field="exploit_standard"/>
    <default applyOnUpdate="0" expression="" field="rapport_terrain"/>
    <default applyOnUpdate="0" expression="" field="gestionnaire"/>
    <default applyOnUpdate="0" expression="False" field="prive"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id" unique_strength="1" constraints="3" notnull_strength="1"/>
    <constraint exp_strength="0" field="nom_ini" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="num_point_ini" unique_strength="0" constraints="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="angle" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="lib_sens" unique_strength="0" constraints="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="date" unique_strength="0" constraints="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="nom_route" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_gestionnaire_route" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="pr" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="abs" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="prec_pos" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="prec_loc" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="type_enq" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_questionnaire_vl" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_questionnaire_pl" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_commanditaire" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_prest_amo" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_prest_terrain" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_campagne_enq" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="comment" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nb_enq" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nb_rec" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nb_chef_poste" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nb_rab" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nb_manip_feux" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="fiable" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="id_cpt_detail_horaire" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="photo1" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="photo2" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="photo3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="exploit_standard" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="rapport_terrain" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="gestionnaire" unique_strength="0" constraints="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="prive" unique_strength="0" constraints="1" notnull_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="nom_ini" desc=""/>
    <constraint exp="" field="num_point_ini" desc=""/>
    <constraint exp="" field="angle" desc=""/>
    <constraint exp="" field="lib_sens" desc=""/>
    <constraint exp="" field="date" desc=""/>
    <constraint exp="" field="nom_route" desc=""/>
    <constraint exp="" field="id_gestionnaire_route" desc=""/>
    <constraint exp="" field="pr" desc=""/>
    <constraint exp="" field="abs" desc=""/>
    <constraint exp="" field="prec_pos" desc=""/>
    <constraint exp="" field="prec_loc" desc=""/>
    <constraint exp="" field="type_enq" desc=""/>
    <constraint exp="" field="id_questionnaire_vl" desc=""/>
    <constraint exp="" field="id_questionnaire_pl" desc=""/>
    <constraint exp="" field="id_commanditaire" desc=""/>
    <constraint exp="" field="id_prest_amo" desc=""/>
    <constraint exp="" field="id_prest_terrain" desc=""/>
    <constraint exp="" field="id_campagne_enq" desc=""/>
    <constraint exp="" field="comment" desc=""/>
    <constraint exp="" field="nb_enq" desc=""/>
    <constraint exp="" field="nb_rec" desc=""/>
    <constraint exp="" field="nb_chef_poste" desc=""/>
    <constraint exp="" field="nb_rab" desc=""/>
    <constraint exp="" field="nb_manip_feux" desc=""/>
    <constraint exp="" field="fiable" desc=""/>
    <constraint exp="" field="id_cpt_detail_horaire" desc=""/>
    <constraint exp="" field="photo1" desc=""/>
    <constraint exp="" field="photo2" desc=""/>
    <constraint exp="" field="photo3" desc=""/>
    <constraint exp="" field="exploit_standard" desc=""/>
    <constraint exp="" field="rapport_terrain" desc=""/>
    <constraint exp="" field="gestionnaire" desc=""/>
    <constraint exp="" field="prive" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="-1" hidden="0" type="field" name="id"/>
      <column width="-1" hidden="0" type="field" name="nom_ini"/>
      <column width="-1" hidden="0" type="field" name="num_point_ini"/>
      <column width="-1" hidden="0" type="field" name="angle"/>
      <column width="-1" hidden="0" type="field" name="lib_sens"/>
      <column width="-1" hidden="0" type="field" name="date"/>
      <column width="-1" hidden="0" type="field" name="nom_route"/>
      <column width="-1" hidden="0" type="field" name="id_gestionnaire_route"/>
      <column width="-1" hidden="0" type="field" name="pr"/>
      <column width="-1" hidden="0" type="field" name="abs"/>
      <column width="-1" hidden="0" type="field" name="prec_pos"/>
      <column width="-1" hidden="0" type="field" name="prec_loc"/>
      <column width="-1" hidden="0" type="field" name="type_enq"/>
      <column width="-1" hidden="0" type="field" name="id_questionnaire_vl"/>
      <column width="-1" hidden="0" type="field" name="id_questionnaire_pl"/>
      <column width="-1" hidden="0" type="field" name="id_commanditaire"/>
      <column width="-1" hidden="0" type="field" name="id_prest_amo"/>
      <column width="-1" hidden="0" type="field" name="id_prest_terrain"/>
      <column width="-1" hidden="0" type="field" name="id_campagne_enq"/>
      <column width="-1" hidden="0" type="field" name="comment"/>
      <column width="-1" hidden="0" type="field" name="nb_enq"/>
      <column width="-1" hidden="0" type="field" name="nb_rec"/>
      <column width="-1" hidden="0" type="field" name="nb_chef_poste"/>
      <column width="-1" hidden="0" type="field" name="nb_rab"/>
      <column width="-1" hidden="0" type="field" name="nb_manip_feux"/>
      <column width="-1" hidden="0" type="field" name="fiable"/>
      <column width="-1" hidden="0" type="field" name="id_cpt_detail_horaire"/>
      <column width="-1" hidden="0" type="field" name="photo1"/>
      <column width="-1" hidden="0" type="field" name="photo2"/>
      <column width="-1" hidden="0" type="field" name="photo3"/>
      <column width="-1" hidden="0" type="field" name="exploit_standard"/>
      <column width="-1" hidden="0" type="field" name="rapport_terrain"/>
      <column width="-1" hidden="0" type="field" name="gestionnaire"/>
      <column width="-1" hidden="0" type="field" name="prive"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorContainer showLabel="1" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" name="Identité de l'enquête" visibilityExpression="">
      <attributeEditorField showLabel="0" name="id" index="0"/>
      <attributeEditorField showLabel="1" name="nom_ini" index="1"/>
      <attributeEditorField showLabel="1" name="num_point_ini" index="2"/>
      <attributeEditorField showLabel="1" name="id_campagne_enq" index="18"/>
      <attributeEditorField showLabel="1" name="id_questionnaire_vl" index="13"/>
      <attributeEditorField showLabel="1" name="id_questionnaire_pl" index="14"/>
      <attributeEditorField showLabel="1" name="type_enq" index="12"/>
      <attributeEditorField showLabel="1" name="comment" index="19"/>
      <attributeEditorField showLabel="1" name="fiable" index="25"/>
      <attributeEditorField showLabel="1" name="date" index="5"/>
      <attributeEditorField showLabel="1" name="gestionnaire" index="32"/>
      <attributeEditorField showLabel="1" name="prive" index="33"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" name="Localisation géographique" visibilityExpression="">
      <attributeEditorField showLabel="1" name="nom_route" index="6"/>
      <attributeEditorField showLabel="1" name="lib_sens" index="4"/>
      <attributeEditorField showLabel="1" name="prec_pos" index="10"/>
      <attributeEditorField showLabel="1" name="prec_loc" index="11"/>
      <attributeEditorField showLabel="1" name="pr" index="8"/>
      <attributeEditorField showLabel="1" name="abs" index="9"/>
      <attributeEditorField showLabel="1" name="angle" index="3"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" name="Intervenants" visibilityExpression="">
      <attributeEditorField showLabel="1" name="id_gestionnaire_route" index="7"/>
      <attributeEditorField showLabel="1" name="id_commanditaire" index="15"/>
      <attributeEditorField showLabel="1" name="id_prest_amo" index="16"/>
      <attributeEditorField showLabel="1" name="id_prest_terrain" index="17"/>
      <attributeEditorField showLabel="1" name="nb_enq" index="20"/>
      <attributeEditorField showLabel="1" name="nb_rec" index="21"/>
      <attributeEditorField showLabel="1" name="nb_manip_feux" index="24"/>
      <attributeEditorField showLabel="1" name="nb_rab" index="23"/>
      <attributeEditorField showLabel="1" name="nb_chef_poste" index="22"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" name="Pièces jointes" visibilityExpression="">
      <attributeEditorField showLabel="1" name="exploit_standard" index="30"/>
      <attributeEditorField showLabel="1" name="rapport_terrain" index="31"/>
      <attributeEditorField showLabel="1" name="photo1" index="27"/>
      <attributeEditorField showLabel="1" name="photo2" index="28"/>
      <attributeEditorField showLabel="1" name="photo3" index="29"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="abs" editable="1"/>
    <field name="angle" editable="1"/>
    <field name="angle2" editable="1"/>
    <field name="comment" editable="1"/>
    <field name="date" editable="1"/>
    <field name="exploit_standard" editable="1"/>
    <field name="fiable" editable="1"/>
    <field name="geom" editable="1"/>
    <field name="gestionnaire" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_campagne_enq" editable="1"/>
    <field name="id_commanditaire" editable="1"/>
    <field name="id_cpt_detail_horaire" editable="1"/>
    <field name="id_gest_cerema" editable="1"/>
    <field name="id_gestionnaire_route" editable="1"/>
    <field name="id_poste" editable="1"/>
    <field name="id_poste_enq" editable="1"/>
    <field name="id_prest_amo" editable="1"/>
    <field name="id_prest_terrain" editable="1"/>
    <field name="id_questionnaire_pl" editable="1"/>
    <field name="id_questionnaire_vl" editable="1"/>
    <field name="id_type_enq" editable="1"/>
    <field name="lib_sens" editable="1"/>
    <field name="nb_chef_poste" editable="1"/>
    <field name="nb_enq" editable="1"/>
    <field name="nb_manip_feux" editable="1"/>
    <field name="nb_rab" editable="1"/>
    <field name="nb_rec" editable="1"/>
    <field name="nom_ini" editable="1"/>
    <field name="nom_route" editable="1"/>
    <field name="num_point" editable="1"/>
    <field name="num_point_ini" editable="1"/>
    <field name="photo1" editable="1"/>
    <field name="photo2" editable="1"/>
    <field name="photo3" editable="1"/>
    <field name="pl" editable="1"/>
    <field name="pr" editable="1"/>
    <field name="prec_loc" editable="1"/>
    <field name="prec_pos" editable="1"/>
    <field name="prive" editable="1"/>
    <field name="rapport_terrain" editable="1"/>
    <field name="row_owner" editable="1"/>
    <field name="type_enq" editable="1"/>
    <field name="vl" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="abs"/>
    <field labelOnTop="0" name="angle"/>
    <field labelOnTop="0" name="angle2"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="date"/>
    <field labelOnTop="0" name="exploit_standard"/>
    <field labelOnTop="0" name="fiable"/>
    <field labelOnTop="0" name="geom"/>
    <field labelOnTop="0" name="gestionnaire"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_campagne_enq"/>
    <field labelOnTop="0" name="id_commanditaire"/>
    <field labelOnTop="0" name="id_cpt_detail_horaire"/>
    <field labelOnTop="0" name="id_gest_cerema"/>
    <field labelOnTop="0" name="id_gestionnaire_route"/>
    <field labelOnTop="0" name="id_poste"/>
    <field labelOnTop="0" name="id_poste_enq"/>
    <field labelOnTop="0" name="id_prest_amo"/>
    <field labelOnTop="0" name="id_prest_terrain"/>
    <field labelOnTop="0" name="id_questionnaire_pl"/>
    <field labelOnTop="0" name="id_questionnaire_vl"/>
    <field labelOnTop="0" name="id_type_enq"/>
    <field labelOnTop="0" name="lib_sens"/>
    <field labelOnTop="0" name="nb_chef_poste"/>
    <field labelOnTop="0" name="nb_enq"/>
    <field labelOnTop="0" name="nb_manip_feux"/>
    <field labelOnTop="0" name="nb_rab"/>
    <field labelOnTop="0" name="nb_rec"/>
    <field labelOnTop="0" name="nom_ini"/>
    <field labelOnTop="0" name="nom_route"/>
    <field labelOnTop="0" name="num_point"/>
    <field labelOnTop="0" name="num_point_ini"/>
    <field labelOnTop="0" name="photo1"/>
    <field labelOnTop="0" name="photo2"/>
    <field labelOnTop="0" name="photo3"/>
    <field labelOnTop="0" name="pl"/>
    <field labelOnTop="0" name="pr"/>
    <field labelOnTop="0" name="prec_loc"/>
    <field labelOnTop="0" name="prec_pos"/>
    <field labelOnTop="0" name="prive"/>
    <field labelOnTop="0" name="rapport_terrain"/>
    <field labelOnTop="0" name="row_owner"/>
    <field labelOnTop="0" name="type_enq"/>
    <field labelOnTop="0" name="vl"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
