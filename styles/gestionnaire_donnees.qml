<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="1" simplifyMaxScale="1" minScale="1e+08" simplifyDrawingTol="1" maxScale="0" labelsEnabled="1" simplifyAlgorithm="0" styleCategories="AllStyleCategories" readOnly="0" version="3.10.6-A Coruña" simplifyLocal="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" enableorderby="0" type="singleSymbol" forceraster="0">
    <symbols>
      <symbol name="0" force_rhr="0" alpha="0.254902" type="fill" clip_to_extent="1">
        <layer pass="0" locked="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="42,34,201,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontStrikeout="0" fontKerning="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fieldName="dter" fontLetterSpacing="0" fontWeight="50" namedStyle="Normal" fontItalic="0" fontUnderline="0" previewBkgrdColor="255,255,255,255" useSubstitutions="0" fontSize="10" textOrientation="horizontal" textColor="0,0,0,255" fontCapitals="0" isExpression="1" multilineHeight="1" blendMode="0" fontWordSpacing="0" fontSizeUnit="Point" fontFamily="MS Shell Dlg 2" textOpacity="1">
        <text-buffer bufferBlendMode="0" bufferColor="255,255,255,255" bufferDraw="0" bufferOpacity="1" bufferJoinStyle="128" bufferNoFill="1" bufferSizeUnits="MM" bufferSize="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
        <background shapeRotationType="0" shapeDraw="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeJoinStyle="64" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255" shapeOffsetX="0" shapeType="0" shapeOffsetY="0" shapeRadiiY="0" shapeSizeY="0" shapeSizeType="0" shapeRotation="0" shapeOffsetUnit="MM" shapeBorderColor="128,128,128,255" shapeBorderWidth="0" shapeRadiiX="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeRadiiUnit="MM" shapeBorderWidthUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeOpacity="1" shapeSVGFile="" shapeSizeUnit="MM">
          <symbol name="markerSymbol" force_rhr="0" alpha="1" type="marker" clip_to_extent="1">
            <layer pass="0" locked="0" enabled="1" class="SimpleMarker">
              <prop k="angle" v="0"/>
              <prop k="color" v="164,113,88,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString"/>
                  <Option name="properties"/>
                  <Option name="type" value="collection" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowUnder="0" shadowOffsetAngle="135" shadowOffsetUnit="MM" shadowOpacity="0.7" shadowScale="100" shadowRadius="1.5" shadowRadiusUnit="MM" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowOffsetGlobal="1" shadowColor="0,0,0,255" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0"/>
        <dd_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" useMaxLineLengthForAutoWrap="1" multilineAlign="4294967295" reverseDirectionSymbol="0" addDirectionSymbol="0" autoWrapLength="0" formatNumbers="0" plussign="0" placeDirectionSymbol="0" wrapChar="" decimals="3" rightDirectionSymbol=">"/>
      <placement fitInPolygonOnly="0" xOffset="0" yOffset="0" centroidInside="0" centroidWhole="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" geometryGenerator="" geometryGeneratorEnabled="0" placement="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" overrunDistance="0" distUnits="MM" geometryGeneratorType="PointGeometry" maxCurvedCharAngleOut="-25" maxCurvedCharAngleIn="25" dist="0" overrunDistanceUnit="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" rotationAngle="0" offsetUnits="MM" preserveRotation="1" placementFlags="10" layerType="PolygonGeometry" priority="5" quadOffset="4" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" offsetType="0" distMapUnitScale="3x:0,0,0,0,0,0"/>
      <rendering obstacle="1" minFeatureSize="0" scaleMax="0" zIndex="0" upsidedownLabels="0" scaleMin="0" scaleVisibility="0" maxNumLabels="2000" fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" obstacleType="0" mergeLines="0" fontMinPixelSize="3" limitNumLabels="0" labelPerPart="0" obstacleFactor="1" displayAll="0"/>
      <dd_properties>
        <Option type="Map">
          <Option name="name" value="" type="QString"/>
          <Option name="properties"/>
          <Option name="type" value="collection" type="QString"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option name="anchorPoint" value="pole_of_inaccessibility" type="QString"/>
          <Option name="ddProperties" type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
          <Option name="drawToAllParts" value="false" type="bool"/>
          <Option name="enabled" value="0" type="QString"/>
          <Option name="lineSymbol" value="&lt;symbol name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot;>&lt;layer pass=&quot;0&quot; locked=&quot;0&quot; enabled=&quot;1&quot; class=&quot;SimpleLine&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString"/>
          <Option name="minLength" value="0" type="double"/>
          <Option name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="minLengthUnit" value="MM" type="QString"/>
          <Option name="offsetFromAnchor" value="0" type="double"/>
          <Option name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="offsetFromAnchorUnit" value="MM" type="QString"/>
          <Option name="offsetFromLabel" value="0" type="double"/>
          <Option name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="offsetFromLabelUnit" value="MM" type="QString"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE( "id", '&lt;NULL>' )</value>
      <value>COALESCE( "id", '&lt;NULL>' )</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory sizeScale="3x:0,0,0,0,0,0" lineSizeScale="3x:0,0,0,0,0,0" penColor="#000000" scaleDependency="Area" penWidth="0" barWidth="5" scaleBasedVisibility="0" backgroundAlpha="255" maxScaleDenominator="1e+08" diagramOrientation="Up" opacity="1" backgroundColor="#ffffff" minimumSize="0" enabled="0" width="15" minScaleDenominator="0" rotationOffset="270" sizeType="MM" penAlpha="255" labelPlacementMethod="XHeight" lineSizeType="MM" height="15">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" placement="0" linePlacementFlags="2" priority="0" obstacle="0" zIndex="0" dist="0">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option name="allowedGapsBuffer" value="0" type="double"/>
        <Option name="allowedGapsEnabled" value="false" type="bool"/>
        <Option name="allowedGapsLayer" value="" type="QString"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="users_group">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="ID" field="id" index="0"/>
    <alias name="Nom (unique)" field="nom" index="1"/>
    <alias name="Groupe utilisateurs (BDD)" field="users_group" index="2"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="nom" applyOnUpdate="0"/>
    <default expression="" field="users_group" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" field="id" notnull_strength="1" exp_strength="0" unique_strength="1"/>
    <constraint constraints="0" field="nom" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="users_group" notnull_strength="0" exp_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" desc="" exp=""/>
    <constraint field="nom" desc="" exp=""/>
    <constraint field="users_group" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column name="id" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column name="nom" type="field" width="-1" hidden="0"/>
      <column name="users_group" type="field" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="dter" editable="1"/>
    <field name="id" editable="1"/>
    <field name="nom" editable="1"/>
    <field name="nom_court" editable="1"/>
    <field name="nom_long" editable="1"/>
    <field name="users_group" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="dter" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="nom" labelOnTop="0"/>
    <field name="nom_court" labelOnTop="0"/>
    <field name="nom_long" labelOnTop="0"/>
    <field name="users_group" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE( "id", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
