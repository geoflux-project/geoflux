<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="0" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" simplifyDrawingHints="1" simplifyLocal="1" readOnly="0" version="3.10.6-A Coruña" styleCategories="AllStyleCategories" simplifyDrawingTol="1" simplifyAlgorithm="0" simplifyMaxScale="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" forceraster="0" symbollevels="0">
    <symbols>
      <symbol type="line" alpha="1" clip_to_extent="1" name="0" force_rhr="0">
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <prop v="Line" k="SymbolType"/>
          <prop v="$geometry" k="geometryModifier"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="line" alpha="1" clip_to_extent="1" name="@0@0" force_rhr="0">
            <layer locked="0" pass="0" enabled="1" class="ArrowLine">
              <prop v="1" k="arrow_start_width"/>
              <prop v="MM" k="arrow_start_width_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="arrow_start_width_unit_scale"/>
              <prop v="1" k="arrow_type"/>
              <prop v="1" k="arrow_width"/>
              <prop v="MM" k="arrow_width_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="arrow_width_unit_scale"/>
              <prop v="3" k="head_length"/>
              <prop v="MM" k="head_length_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="head_length_unit_scale"/>
              <prop v="2.9" k="head_thickness"/>
              <prop v="MM" k="head_thickness_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="head_thickness_unit_scale"/>
              <prop v="0" k="head_type"/>
              <prop v="1" k="is_curved"/>
              <prop v="1" k="is_repeated"/>
              <prop v="1" k="offset"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_unit_scale"/>
              <prop v="0" k="ring_filter"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="arrowStartWidth">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="size_flux" name="field"/>
                      <Option type="int" value="2" name="type"/>
                    </Option>
                    <Option type="Map" name="arrowWidth">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="size_flux" name="field"/>
                      <Option type="int" value="2" name="type"/>
                    </Option>
                  </Option>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="fill" alpha="1" clip_to_extent="1" name="@@0@0@0" force_rhr="0">
                <layer locked="0" pass="0" enabled="1" class="SimpleFill">
                  <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
                  <prop v="0,0,255,255" k="color"/>
                  <prop v="bevel" k="joinstyle"/>
                  <prop v="0,0" k="offset"/>
                  <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
                  <prop v="MM" k="offset_unit"/>
                  <prop v="35,35,35,255" k="outline_color"/>
                  <prop v="solid" k="outline_style"/>
                  <prop v="0.26" k="outline_width"/>
                  <prop v="MM" k="outline_width_unit"/>
                  <prop v="solid" k="style"/>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeType="MM" backgroundAlpha="255" width="15" maxScaleDenominator="1e+08" barWidth="5" scaleDependency="Area" backgroundColor="#ffffff" sizeScale="3x:0,0,0,0,0,0" enabled="0" lineSizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" height="15" labelPlacementMethod="XHeight" scaleBasedVisibility="0" opacity="1" rotationOffset="270" lineSizeType="MM" penColor="#000000" minimumSize="0" penAlpha="255" diagramOrientation="Up" penWidth="0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="2" dist="0" priority="0" obstacle="0" zIndex="0" linePlacementFlags="18" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_o">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Zones_ea50638c_0553_4c75_8c22_1e79e0ae2c12" name="Layer"/>
            <Option type="QString" value="Zones" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(id_zonage = 6)" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="code_zone" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_d">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Zones_ea50638c_0553_4c75_8c22_1e79e0ae2c12" name="Layer"/>
            <Option type="QString" value="Zones" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(id_zonage = 6)" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="code_zone" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="zone_o">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="zone_d">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="valeur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id"/>
    <alias index="1" name="Code zone orig" field="id_zone_o"/>
    <alias index="2" name="Code zone dest" field="id_zone_d"/>
    <alias index="3" name="" field="zone_o"/>
    <alias index="4" name="" field="zone_d"/>
    <alias index="5" name="Valeur" field="valeur"/>
    <alias index="6" name="" field="size"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="id_zone_o"/>
    <default applyOnUpdate="0" expression="" field="id_zone_d"/>
    <default applyOnUpdate="0" expression="" field="zone_o"/>
    <default applyOnUpdate="0" expression="" field="zone_d"/>
    <default applyOnUpdate="0" expression="" field="valeur"/>
    <default applyOnUpdate="0" expression="" field="size"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="id_zone_o" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="id_zone_d" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint exp_strength="0" field="zone_o" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="zone_d" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="valeur" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="size" constraints="3" notnull_strength="1" unique_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="id_zone_o" exp="" desc=""/>
    <constraint field="id_zone_d" exp="" desc=""/>
    <constraint field="zone_o" exp="" desc=""/>
    <constraint field="zone_d" exp="" desc=""/>
    <constraint field="valeur" exp="" desc=""/>
    <constraint field="size" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="1" sortExpression="&quot;valeur&quot;">
    <columns>
      <column type="field" width="-1" name="id" hidden="1"/>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" width="-1" name="id_zone_o" hidden="0"/>
      <column type="field" width="-1" name="id_zone_d" hidden="0"/>
      <column type="field" width="-1" name="zone_o" hidden="1"/>
      <column type="field" width="-1" name="zone_d" hidden="1"/>
      <column type="field" width="-1" name="valeur" hidden="0"/>
      <column type="field" width="-1" name="size" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="code_zone_dest" editable="1"/>
    <field name="code_zone_orig" editable="1"/>
    <field name="flux" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_zone_d" editable="1"/>
    <field name="id_zone_o" editable="1"/>
    <field name="size" editable="1"/>
    <field name="size_flux" editable="1"/>
    <field name="size_valeur" editable="1"/>
    <field name="valeur" editable="1"/>
    <field name="weight" editable="1"/>
    <field name="zone_d" editable="1"/>
    <field name="zone_o" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="code_zone_dest"/>
    <field labelOnTop="0" name="code_zone_orig"/>
    <field labelOnTop="0" name="flux"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_zone_d"/>
    <field labelOnTop="0" name="id_zone_o"/>
    <field labelOnTop="0" name="size"/>
    <field labelOnTop="0" name="size_flux"/>
    <field labelOnTop="0" name="size_valeur"/>
    <field labelOnTop="0" name="valeur"/>
    <field labelOnTop="0" name="weight"/>
    <field labelOnTop="0" name="zone_d"/>
    <field labelOnTop="0" name="zone_o"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
