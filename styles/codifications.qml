<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" hasScaleBasedVisibilityFlag="0" maxScale="0" styleCategories="AllStyleCategories" minScale="1e+08" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>code_modalite || ' - '  ||  libelle</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_question">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Questions_a1a9251f_d004_4d0c_b741_54aa07ad87c9" name="Layer"/>
            <Option type="QString" value="Questions" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' estimatedmetadata=true checkPrimaryKeyUnicity='1' table=&quot;o_geoflux_survey&quot;.&quot;question&quot; sql=" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="nom" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_modalite">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="libelle">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name="ID"/>
    <alias index="1" field="id_question" name="Question"/>
    <alias index="2" field="code_modalite" name="Modalité"/>
    <alias index="3" field="libelle" name="Libellé"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="id_question" applyOnUpdate="0"/>
    <default expression="" field="code_modalite" applyOnUpdate="0"/>
    <default expression="" field="libelle" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" unique_strength="1" field="id" constraints="3" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" field="id_question" constraints="1" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" field="code_modalite" constraints="0" exp_strength="0"/>
    <constraint notnull_strength="1" unique_strength="0" field="libelle" constraints="1" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="id_question" desc=""/>
    <constraint exp="" field="code_modalite" desc=""/>
    <constraint exp="" field="libelle" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="CASE &#xd;&#xa;WHEN length(id)=1 THEN concat( '000', id ) &#xd;&#xa;WHEN length(id)=2 THEN concat('00', id)&#xd;&#xa;WHEN length(id)=3 THEN concat('0', id)&#xd;&#xa;END || ' - '  ||  &quot;nom&quot; ">
    <columns>
      <column hidden="1" type="field" width="-1" name="id"/>
      <column hidden="0" type="field" width="137" name="id_question"/>
      <column hidden="0" type="field" width="100" name="code_modalite"/>
      <column hidden="0" type="field" width="592" name="libelle"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C16LM0066-BDD_transport_mutualisee/Documents techniques/Migration_geoflux</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField index="0" showLabel="1" name="id"/>
    <attributeEditorField index="1" showLabel="1" name="id_question"/>
    <attributeEditorField index="2" showLabel="1" name="code_modalite"/>
    <attributeEditorField index="3" showLabel="1" name="libelle"/>
    <attributeEditorRelation showUnlinkButton="1" relation="" showLabel="1" showLinkButton="1" name=""/>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="code_modalite"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_question"/>
    <field editable="1" name="libelle"/>
    <field editable="1" name="nom"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="code_modalite"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_question"/>
    <field labelOnTop="0" name="libelle"/>
    <field labelOnTop="0" name="nom"/>
  </labelOnTop>
  <widgets>
    <widget name="codif_modalites">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
    <widget name="modalite20_id_codif_codif20170_id">
      <config type="Map">
        <Option type="QString" value="" name="nm-rel"/>
      </config>
    </widget>
  </widgets>
  <previewExpression>code_modalite || ' - '  ||  libelle</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
