<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" minScale="1e+08" version="3.10.6-A Coruña" hasScaleBasedVisibilityFlag="0" maxScale="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id_matrice</value>
      <value>"id_matrice"</value>
      <value>id_matrice</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_matrice">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Liste_matrices_OD_1d3ef426_4939_43a2_941d_15fffeb919cc" name="Layer" type="QString"/>
            <Option value="Liste matrices OD" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;matrice&quot; sql= id_zonage = 6" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="nom" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_orig">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Zones_0085f1cd_0d95_4838_9cb2_d1cc6426d5c6" name="Layer" type="QString"/>
            <Option value="Zones" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(id_zonage = 6) AND id IN (                                     SELECT zone.id                                     FROM w_geoflux_mmr_coherence.corresp_zones JOIN w_geoflux_mmr_coherence.zone macrozone ON (corresp_zones.id_macrozone = macrozone.id)                                                                                JOIN w_geoflux_mmr_coherence.zone zone ON (corresp_zones.id_zone = zone.id)                                                                                JOIN w_geoflux_mmr_coherence.zonage ON (macrozone.id_zonage = zonage.id)                                     WHERE zone.id_zonage = 6 AND macrozone.id_zonage = 1                               )" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="code_zone" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_dest">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Zones_0085f1cd_0d95_4838_9cb2_d1cc6426d5c6" name="Layer" type="QString"/>
            <Option value="Zones" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(id_zonage = 6) AND id IN (                                     SELECT zone.id                                     FROM w_geoflux_mmr_coherence.corresp_zones JOIN w_geoflux_mmr_coherence.zone macrozone ON (corresp_zones.id_macrozone = macrozone.id)                                                                                JOIN w_geoflux_mmr_coherence.zone zone ON (corresp_zones.id_zone = zone.id)                                                                                JOIN w_geoflux_mmr_coherence.zonage ON (macrozone.id_zonage = zonage.id)                                     WHERE zone.id_zonage = 6 AND macrozone.id_zonage = 1                               )" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="code_zone" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="echant">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="valeur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="Matrice" index="0" field="id_matrice"/>
    <alias name="Code zone orig" index="1" field="id_zone_orig"/>
    <alias name="Code zone dest" index="2" field="id_zone_dest"/>
    <alias name="Echant" index="3" field="echant"/>
    <alias name="Valeur" index="4" field="valeur"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id_matrice"/>
    <default applyOnUpdate="0" expression="" field="id_zone_orig"/>
    <default applyOnUpdate="0" expression="" field="id_zone_dest"/>
    <default applyOnUpdate="0" expression="" field="echant"/>
    <default applyOnUpdate="0" expression="" field="valeur"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="1" field="id_matrice" constraints="1"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="1" field="id_zone_orig" constraints="1"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="1" field="id_zone_dest" constraints="1"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" field="echant" constraints="0"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" field="valeur" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id_matrice"/>
    <constraint desc="" exp="" field="id_zone_orig"/>
    <constraint desc="" exp="" field="id_zone_dest"/>
    <constraint desc="" exp="" field="echant"/>
    <constraint desc="" exp="" field="valeur"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id_zone_dest&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column name="id_matrice" hidden="0" type="field" width="281"/>
      <column name="id_zone_orig" hidden="0" type="field" width="-1"/>
      <column name="id_zone_dest" hidden="0" type="field" width="-1"/>
      <column name="valeur" hidden="0" type="field" width="-1"/>
      <column name="echant" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="borne_max_ic_valeur_redressee"/>
    <field editable="1" name="borne_max_ic_valeur_reponderee"/>
    <field editable="1" name="borne_min_ic_valeur_redressee"/>
    <field editable="1" name="borne_min_ic_valeur_reponderee"/>
    <field editable="1" name="echant"/>
    <field editable="1" name="echantillon"/>
    <field editable="1" name="formule_point_enq_fictif"/>
    <field editable="1" name="formule_point_enq_terrain"/>
    <field editable="1" name="id_matrice"/>
    <field editable="1" name="id_zone_dest"/>
    <field editable="1" name="id_zone_orig"/>
    <field editable="1" name="valeur"/>
    <field editable="1" name="valeur_redressee"/>
    <field editable="1" name="valeur_reponderee"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="borne_max_ic_valeur_redressee"/>
    <field labelOnTop="0" name="borne_max_ic_valeur_reponderee"/>
    <field labelOnTop="0" name="borne_min_ic_valeur_redressee"/>
    <field labelOnTop="0" name="borne_min_ic_valeur_reponderee"/>
    <field labelOnTop="0" name="echant"/>
    <field labelOnTop="0" name="echantillon"/>
    <field labelOnTop="0" name="formule_point_enq_fictif"/>
    <field labelOnTop="0" name="formule_point_enq_terrain"/>
    <field labelOnTop="0" name="id_matrice"/>
    <field labelOnTop="0" name="id_zone_dest"/>
    <field labelOnTop="0" name="id_zone_orig"/>
    <field labelOnTop="0" name="valeur"/>
    <field labelOnTop="0" name="valeur_redressee"/>
    <field labelOnTop="0" name="valeur_reponderee"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_matrice</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
