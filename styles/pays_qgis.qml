<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="1" simplifyDrawingHints="1" simplifyMaxScale="1" simplifyDrawingTol="1" readOnly="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" type="fill" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="191,134,99,0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="227,26,28,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.46"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style multilineHeight="1" fontWordSpacing="0" blendMode="0" previewBkgrdColor="255,255,255,255" fontWeight="50" namedStyle="Normal" fontSize="8.25" fontUnderline="0" textOrientation="horizontal" fieldName="admin" fontKerning="1" textOpacity="1" fontStrikeout="0" isExpression="0" useSubstitutions="0" fontFamily="MS Shell Dlg 2" fontLetterSpacing="0" fontSizeUnit="Point" fontCapitals="0" textColor="227,26,28,255" fontItalic="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0">
        <text-buffer bufferDraw="0" bufferNoFill="0" bufferColor="255,255,255,255" bufferJoinStyle="128" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM" bufferBlendMode="0" bufferSize="1"/>
        <background shapeSVGFile="" shapeRotation="0" shapeRotationType="0" shapeType="0" shapeSizeY="0" shapeOffsetUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOffsetY="0" shapeSizeX="0" shapeBlendMode="0" shapeSizeUnit="MM" shapeJoinStyle="64" shapeRadiiX="0" shapeRadiiY="0" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderColor="128,128,128,255" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeRadiiUnit="MM" shapeOffsetX="0" shapeBorderWidth="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1">
          <symbol force_rhr="0" type="marker" clip_to_extent="1" name="markerSymbol" alpha="1">
            <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
              <prop k="angle" v="0"/>
              <prop k="color" v="183,72,75,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowUnder="0" shadowColor="0,0,0,255" shadowOffsetDist="1" shadowScale="100" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.7" shadowDraw="0" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowBlendMode="6" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowOffsetAngle="135" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" wrapChar="" reverseDirectionSymbol="0" plussign="0" useMaxLineLengthForAutoWrap="1" autoWrapLength="0" placeDirectionSymbol="0" multilineAlign="4294967295" formatNumbers="0" decimals="3" addDirectionSymbol="0" rightDirectionSymbol=">"/>
      <placement geometryGeneratorType="PointGeometry" dist="0" geometryGenerator="" xOffset="0" repeatDistanceUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" yOffset="0" centroidWhole="0" offsetType="0" placementFlags="10" distMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" maxCurvedCharAngleIn="25" offsetUnits="MapUnit" preserveRotation="1" quadOffset="4" priority="5" overrunDistanceUnit="MM" placement="1" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" layerType="PolygonGeometry" centroidInside="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" geometryGeneratorEnabled="0" rotationAngle="0" distUnits="MM"/>
      <rendering minFeatureSize="0" scaleMax="10000000" maxNumLabels="2000" fontMinPixelSize="3" fontMaxPixelSize="10000" labelPerPart="0" displayAll="0" drawLabels="1" obstacleFactor="1" zIndex="0" obstacle="1" upsidedownLabels="0" obstacleType="0" mergeLines="0" fontLimitPixelSize="0" limitNumLabels="0" scaleVisibility="0" scaleMin="1"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
          <Option type="Map" name="ddProperties">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
          <Option value="false" type="bool" name="drawToAllParts"/>
          <Option value="0" type="QString" name="enabled"/>
          <Option value="&lt;symbol force_rhr=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; enabled=&quot;1&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
          <Option value="0" type="double" name="minLength"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
          <Option value="MM" type="QString" name="minLengthUnit"/>
          <Option value="0" type="double" name="offsetFromAnchor"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
          <Option value="0" type="double" name="offsetFromLabel"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" backgroundColor="#ffffff" sizeType="MM" penColor="#000000" enabled="0" barWidth="5" penAlpha="255" scaleDependency="Area" rotationOffset="270" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" scaleBasedVisibility="0" width="15" lineSizeType="MM" maxScaleDenominator="1e+08" diagramOrientation="Up" labelPlacementMethod="XHeight" opacity="1" minScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="0" zIndex="0" linePlacementFlags="2" showAll="1" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abbrev">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abbrev_len">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adm0_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adm0_a3_is">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adm0_a3_un">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adm0_a3_us">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adm0_a3_wb">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adm0_dif">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="admin">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brk_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brk_diff">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brk_group">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brk_name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="continent">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="economy">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fips_10_">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="formal_en">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="formal_fr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gdp_md_est">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gdp_year">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geounit">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geou_dif">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gu_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="homepart">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="income_grp">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="iso_a2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="iso_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="iso_n3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="labelrank">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lastcensus">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="level">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="long_len">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapcolor13">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapcolor7">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapcolor8">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapcolor9">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name_alt">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name_len">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name_long">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name_sort">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="note_adm0">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="note_brk">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pop_est">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pop_year">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="postal">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="region_un">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="region_wb">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sovereignt">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sov_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="subregion">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="subunit">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="su_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="su_dif">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tiny">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="un_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wb_a2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wb_a3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wikipedia">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="woe_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="woe_id_eh">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="woe_note">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featurecla">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="scalerank">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
    <alias field="abbrev" name="" index="1"/>
    <alias field="abbrev_len" name="" index="2"/>
    <alias field="adm0_a3" name="" index="3"/>
    <alias field="adm0_a3_is" name="" index="4"/>
    <alias field="adm0_a3_un" name="" index="5"/>
    <alias field="adm0_a3_us" name="" index="6"/>
    <alias field="adm0_a3_wb" name="" index="7"/>
    <alias field="adm0_dif" name="" index="8"/>
    <alias field="admin" name="" index="9"/>
    <alias field="brk_a3" name="" index="10"/>
    <alias field="brk_diff" name="" index="11"/>
    <alias field="brk_group" name="" index="12"/>
    <alias field="brk_name" name="" index="13"/>
    <alias field="continent" name="" index="14"/>
    <alias field="economy" name="" index="15"/>
    <alias field="fips_10_" name="" index="16"/>
    <alias field="formal_en" name="" index="17"/>
    <alias field="formal_fr" name="" index="18"/>
    <alias field="gdp_md_est" name="" index="19"/>
    <alias field="gdp_year" name="" index="20"/>
    <alias field="geounit" name="" index="21"/>
    <alias field="geou_dif" name="" index="22"/>
    <alias field="gu_a3" name="" index="23"/>
    <alias field="homepart" name="" index="24"/>
    <alias field="income_grp" name="" index="25"/>
    <alias field="iso_a2" name="" index="26"/>
    <alias field="iso_a3" name="" index="27"/>
    <alias field="iso_n3" name="" index="28"/>
    <alias field="labelrank" name="" index="29"/>
    <alias field="lastcensus" name="" index="30"/>
    <alias field="level" name="" index="31"/>
    <alias field="long_len" name="" index="32"/>
    <alias field="mapcolor13" name="" index="33"/>
    <alias field="mapcolor7" name="" index="34"/>
    <alias field="mapcolor8" name="" index="35"/>
    <alias field="mapcolor9" name="" index="36"/>
    <alias field="name" name="" index="37"/>
    <alias field="name_alt" name="" index="38"/>
    <alias field="name_len" name="" index="39"/>
    <alias field="name_long" name="" index="40"/>
    <alias field="name_sort" name="" index="41"/>
    <alias field="note_adm0" name="" index="42"/>
    <alias field="note_brk" name="" index="43"/>
    <alias field="pop_est" name="" index="44"/>
    <alias field="pop_year" name="" index="45"/>
    <alias field="postal" name="" index="46"/>
    <alias field="region_un" name="" index="47"/>
    <alias field="region_wb" name="" index="48"/>
    <alias field="sovereignt" name="" index="49"/>
    <alias field="sov_a3" name="" index="50"/>
    <alias field="subregion" name="" index="51"/>
    <alias field="subunit" name="" index="52"/>
    <alias field="su_a3" name="" index="53"/>
    <alias field="su_dif" name="" index="54"/>
    <alias field="tiny" name="" index="55"/>
    <alias field="type" name="" index="56"/>
    <alias field="un_a3" name="" index="57"/>
    <alias field="wb_a2" name="" index="58"/>
    <alias field="wb_a3" name="" index="59"/>
    <alias field="wikipedia" name="" index="60"/>
    <alias field="woe_id" name="" index="61"/>
    <alias field="woe_id_eh" name="" index="62"/>
    <alias field="woe_note" name="" index="63"/>
    <alias field="featurecla" name="" index="64"/>
    <alias field="scalerank" name="" index="65"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id"/>
    <default applyOnUpdate="0" expression="" field="abbrev"/>
    <default applyOnUpdate="0" expression="" field="abbrev_len"/>
    <default applyOnUpdate="0" expression="" field="adm0_a3"/>
    <default applyOnUpdate="0" expression="" field="adm0_a3_is"/>
    <default applyOnUpdate="0" expression="" field="adm0_a3_un"/>
    <default applyOnUpdate="0" expression="" field="adm0_a3_us"/>
    <default applyOnUpdate="0" expression="" field="adm0_a3_wb"/>
    <default applyOnUpdate="0" expression="" field="adm0_dif"/>
    <default applyOnUpdate="0" expression="" field="admin"/>
    <default applyOnUpdate="0" expression="" field="brk_a3"/>
    <default applyOnUpdate="0" expression="" field="brk_diff"/>
    <default applyOnUpdate="0" expression="" field="brk_group"/>
    <default applyOnUpdate="0" expression="" field="brk_name"/>
    <default applyOnUpdate="0" expression="" field="continent"/>
    <default applyOnUpdate="0" expression="" field="economy"/>
    <default applyOnUpdate="0" expression="" field="fips_10_"/>
    <default applyOnUpdate="0" expression="" field="formal_en"/>
    <default applyOnUpdate="0" expression="" field="formal_fr"/>
    <default applyOnUpdate="0" expression="" field="gdp_md_est"/>
    <default applyOnUpdate="0" expression="" field="gdp_year"/>
    <default applyOnUpdate="0" expression="" field="geounit"/>
    <default applyOnUpdate="0" expression="" field="geou_dif"/>
    <default applyOnUpdate="0" expression="" field="gu_a3"/>
    <default applyOnUpdate="0" expression="" field="homepart"/>
    <default applyOnUpdate="0" expression="" field="income_grp"/>
    <default applyOnUpdate="0" expression="" field="iso_a2"/>
    <default applyOnUpdate="0" expression="" field="iso_a3"/>
    <default applyOnUpdate="0" expression="" field="iso_n3"/>
    <default applyOnUpdate="0" expression="" field="labelrank"/>
    <default applyOnUpdate="0" expression="" field="lastcensus"/>
    <default applyOnUpdate="0" expression="" field="level"/>
    <default applyOnUpdate="0" expression="" field="long_len"/>
    <default applyOnUpdate="0" expression="" field="mapcolor13"/>
    <default applyOnUpdate="0" expression="" field="mapcolor7"/>
    <default applyOnUpdate="0" expression="" field="mapcolor8"/>
    <default applyOnUpdate="0" expression="" field="mapcolor9"/>
    <default applyOnUpdate="0" expression="" field="name"/>
    <default applyOnUpdate="0" expression="" field="name_alt"/>
    <default applyOnUpdate="0" expression="" field="name_len"/>
    <default applyOnUpdate="0" expression="" field="name_long"/>
    <default applyOnUpdate="0" expression="" field="name_sort"/>
    <default applyOnUpdate="0" expression="" field="note_adm0"/>
    <default applyOnUpdate="0" expression="" field="note_brk"/>
    <default applyOnUpdate="0" expression="" field="pop_est"/>
    <default applyOnUpdate="0" expression="" field="pop_year"/>
    <default applyOnUpdate="0" expression="" field="postal"/>
    <default applyOnUpdate="0" expression="" field="region_un"/>
    <default applyOnUpdate="0" expression="" field="region_wb"/>
    <default applyOnUpdate="0" expression="" field="sovereignt"/>
    <default applyOnUpdate="0" expression="" field="sov_a3"/>
    <default applyOnUpdate="0" expression="" field="subregion"/>
    <default applyOnUpdate="0" expression="" field="subunit"/>
    <default applyOnUpdate="0" expression="" field="su_a3"/>
    <default applyOnUpdate="0" expression="" field="su_dif"/>
    <default applyOnUpdate="0" expression="" field="tiny"/>
    <default applyOnUpdate="0" expression="" field="type"/>
    <default applyOnUpdate="0" expression="" field="un_a3"/>
    <default applyOnUpdate="0" expression="" field="wb_a2"/>
    <default applyOnUpdate="0" expression="" field="wb_a3"/>
    <default applyOnUpdate="0" expression="" field="wikipedia"/>
    <default applyOnUpdate="0" expression="" field="woe_id"/>
    <default applyOnUpdate="0" expression="" field="woe_id_eh"/>
    <default applyOnUpdate="0" expression="" field="woe_note"/>
    <default applyOnUpdate="0" expression="" field="featurecla"/>
    <default applyOnUpdate="0" expression="" field="scalerank"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id" unique_strength="1" constraints="3" notnull_strength="1"/>
    <constraint exp_strength="0" field="abbrev" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="abbrev_len" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="adm0_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="adm0_a3_is" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="adm0_a3_un" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="adm0_a3_us" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="adm0_a3_wb" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="adm0_dif" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="admin" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="brk_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="brk_diff" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="brk_group" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="brk_name" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="continent" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="economy" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="fips_10_" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="formal_en" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="formal_fr" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="gdp_md_est" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="gdp_year" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="geounit" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="geou_dif" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="gu_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="homepart" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="income_grp" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="iso_a2" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="iso_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="iso_n3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="labelrank" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="lastcensus" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="level" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="long_len" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="mapcolor13" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="mapcolor7" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="mapcolor8" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="mapcolor9" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="name" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="name_alt" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="name_len" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="name_long" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="name_sort" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="note_adm0" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="note_brk" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="pop_est" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="pop_year" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="postal" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="region_un" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="region_wb" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="sovereignt" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="sov_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="subregion" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="subunit" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="su_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="su_dif" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="tiny" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="type" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="un_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="wb_a2" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="wb_a3" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="wikipedia" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="woe_id" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="woe_id_eh" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="woe_note" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="featurecla" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="scalerank" unique_strength="0" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="abbrev" desc=""/>
    <constraint exp="" field="abbrev_len" desc=""/>
    <constraint exp="" field="adm0_a3" desc=""/>
    <constraint exp="" field="adm0_a3_is" desc=""/>
    <constraint exp="" field="adm0_a3_un" desc=""/>
    <constraint exp="" field="adm0_a3_us" desc=""/>
    <constraint exp="" field="adm0_a3_wb" desc=""/>
    <constraint exp="" field="adm0_dif" desc=""/>
    <constraint exp="" field="admin" desc=""/>
    <constraint exp="" field="brk_a3" desc=""/>
    <constraint exp="" field="brk_diff" desc=""/>
    <constraint exp="" field="brk_group" desc=""/>
    <constraint exp="" field="brk_name" desc=""/>
    <constraint exp="" field="continent" desc=""/>
    <constraint exp="" field="economy" desc=""/>
    <constraint exp="" field="fips_10_" desc=""/>
    <constraint exp="" field="formal_en" desc=""/>
    <constraint exp="" field="formal_fr" desc=""/>
    <constraint exp="" field="gdp_md_est" desc=""/>
    <constraint exp="" field="gdp_year" desc=""/>
    <constraint exp="" field="geounit" desc=""/>
    <constraint exp="" field="geou_dif" desc=""/>
    <constraint exp="" field="gu_a3" desc=""/>
    <constraint exp="" field="homepart" desc=""/>
    <constraint exp="" field="income_grp" desc=""/>
    <constraint exp="" field="iso_a2" desc=""/>
    <constraint exp="" field="iso_a3" desc=""/>
    <constraint exp="" field="iso_n3" desc=""/>
    <constraint exp="" field="labelrank" desc=""/>
    <constraint exp="" field="lastcensus" desc=""/>
    <constraint exp="" field="level" desc=""/>
    <constraint exp="" field="long_len" desc=""/>
    <constraint exp="" field="mapcolor13" desc=""/>
    <constraint exp="" field="mapcolor7" desc=""/>
    <constraint exp="" field="mapcolor8" desc=""/>
    <constraint exp="" field="mapcolor9" desc=""/>
    <constraint exp="" field="name" desc=""/>
    <constraint exp="" field="name_alt" desc=""/>
    <constraint exp="" field="name_len" desc=""/>
    <constraint exp="" field="name_long" desc=""/>
    <constraint exp="" field="name_sort" desc=""/>
    <constraint exp="" field="note_adm0" desc=""/>
    <constraint exp="" field="note_brk" desc=""/>
    <constraint exp="" field="pop_est" desc=""/>
    <constraint exp="" field="pop_year" desc=""/>
    <constraint exp="" field="postal" desc=""/>
    <constraint exp="" field="region_un" desc=""/>
    <constraint exp="" field="region_wb" desc=""/>
    <constraint exp="" field="sovereignt" desc=""/>
    <constraint exp="" field="sov_a3" desc=""/>
    <constraint exp="" field="subregion" desc=""/>
    <constraint exp="" field="subunit" desc=""/>
    <constraint exp="" field="su_a3" desc=""/>
    <constraint exp="" field="su_dif" desc=""/>
    <constraint exp="" field="tiny" desc=""/>
    <constraint exp="" field="type" desc=""/>
    <constraint exp="" field="un_a3" desc=""/>
    <constraint exp="" field="wb_a2" desc=""/>
    <constraint exp="" field="wb_a3" desc=""/>
    <constraint exp="" field="wikipedia" desc=""/>
    <constraint exp="" field="woe_id" desc=""/>
    <constraint exp="" field="woe_id_eh" desc=""/>
    <constraint exp="" field="woe_note" desc=""/>
    <constraint exp="" field="featurecla" desc=""/>
    <constraint exp="" field="scalerank" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="-1" hidden="1" type="actions"/>
      <column width="-1" hidden="0" type="field" name="id"/>
      <column width="-1" hidden="0" type="field" name="abbrev"/>
      <column width="-1" hidden="0" type="field" name="abbrev_len"/>
      <column width="-1" hidden="0" type="field" name="adm0_a3"/>
      <column width="-1" hidden="0" type="field" name="adm0_a3_is"/>
      <column width="-1" hidden="0" type="field" name="adm0_a3_un"/>
      <column width="-1" hidden="0" type="field" name="adm0_a3_us"/>
      <column width="-1" hidden="0" type="field" name="adm0_a3_wb"/>
      <column width="-1" hidden="0" type="field" name="adm0_dif"/>
      <column width="-1" hidden="0" type="field" name="admin"/>
      <column width="-1" hidden="0" type="field" name="brk_a3"/>
      <column width="-1" hidden="0" type="field" name="brk_diff"/>
      <column width="-1" hidden="0" type="field" name="brk_group"/>
      <column width="-1" hidden="0" type="field" name="brk_name"/>
      <column width="-1" hidden="0" type="field" name="continent"/>
      <column width="-1" hidden="0" type="field" name="economy"/>
      <column width="-1" hidden="0" type="field" name="fips_10_"/>
      <column width="-1" hidden="0" type="field" name="formal_en"/>
      <column width="-1" hidden="0" type="field" name="formal_fr"/>
      <column width="-1" hidden="0" type="field" name="gdp_md_est"/>
      <column width="-1" hidden="0" type="field" name="gdp_year"/>
      <column width="-1" hidden="0" type="field" name="geounit"/>
      <column width="-1" hidden="0" type="field" name="geou_dif"/>
      <column width="-1" hidden="0" type="field" name="gu_a3"/>
      <column width="-1" hidden="0" type="field" name="homepart"/>
      <column width="-1" hidden="0" type="field" name="income_grp"/>
      <column width="-1" hidden="0" type="field" name="iso_a2"/>
      <column width="-1" hidden="0" type="field" name="iso_a3"/>
      <column width="-1" hidden="0" type="field" name="iso_n3"/>
      <column width="-1" hidden="0" type="field" name="labelrank"/>
      <column width="-1" hidden="0" type="field" name="lastcensus"/>
      <column width="-1" hidden="0" type="field" name="level"/>
      <column width="-1" hidden="0" type="field" name="long_len"/>
      <column width="-1" hidden="0" type="field" name="mapcolor13"/>
      <column width="-1" hidden="0" type="field" name="mapcolor7"/>
      <column width="-1" hidden="0" type="field" name="mapcolor8"/>
      <column width="-1" hidden="0" type="field" name="mapcolor9"/>
      <column width="-1" hidden="0" type="field" name="name"/>
      <column width="-1" hidden="0" type="field" name="name_alt"/>
      <column width="-1" hidden="0" type="field" name="name_len"/>
      <column width="-1" hidden="0" type="field" name="name_long"/>
      <column width="-1" hidden="0" type="field" name="name_sort"/>
      <column width="-1" hidden="0" type="field" name="note_adm0"/>
      <column width="-1" hidden="0" type="field" name="note_brk"/>
      <column width="-1" hidden="0" type="field" name="pop_est"/>
      <column width="-1" hidden="0" type="field" name="pop_year"/>
      <column width="-1" hidden="0" type="field" name="postal"/>
      <column width="-1" hidden="0" type="field" name="region_un"/>
      <column width="-1" hidden="0" type="field" name="region_wb"/>
      <column width="-1" hidden="0" type="field" name="sovereignt"/>
      <column width="-1" hidden="0" type="field" name="sov_a3"/>
      <column width="-1" hidden="0" type="field" name="subregion"/>
      <column width="-1" hidden="0" type="field" name="subunit"/>
      <column width="-1" hidden="0" type="field" name="su_a3"/>
      <column width="-1" hidden="0" type="field" name="su_dif"/>
      <column width="-1" hidden="0" type="field" name="tiny"/>
      <column width="-1" hidden="0" type="field" name="type"/>
      <column width="-1" hidden="0" type="field" name="un_a3"/>
      <column width="-1" hidden="0" type="field" name="wb_a2"/>
      <column width="-1" hidden="0" type="field" name="wb_a3"/>
      <column width="-1" hidden="0" type="field" name="wikipedia"/>
      <column width="-1" hidden="0" type="field" name="woe_id"/>
      <column width="-1" hidden="0" type="field" name="woe_id_eh"/>
      <column width="-1" hidden="0" type="field" name="woe_note"/>
      <column width="-1" hidden="0" type="field" name="featurecla"/>
      <column width="-1" hidden="0" type="field" name="scalerank"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/Users/aurelie-p.bousquet/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/plugin_geoflux_route</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="abbrev" editable="1"/>
    <field name="abbrev_len" editable="1"/>
    <field name="adm0_a3" editable="1"/>
    <field name="adm0_a3_is" editable="1"/>
    <field name="adm0_a3_un" editable="1"/>
    <field name="adm0_a3_us" editable="1"/>
    <field name="adm0_a3_wb" editable="1"/>
    <field name="adm0_dif" editable="1"/>
    <field name="admin" editable="1"/>
    <field name="brk_a3" editable="1"/>
    <field name="brk_diff" editable="1"/>
    <field name="brk_group" editable="1"/>
    <field name="brk_name" editable="1"/>
    <field name="cntr_code" editable="1"/>
    <field name="continent" editable="1"/>
    <field name="economy" editable="1"/>
    <field name="featurecla" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="fips_10_" editable="1"/>
    <field name="formal_en" editable="1"/>
    <field name="formal_fr" editable="1"/>
    <field name="gdp_md_est" editable="1"/>
    <field name="gdp_year" editable="1"/>
    <field name="geou_dif" editable="1"/>
    <field name="geounit" editable="1"/>
    <field name="gu_a3" editable="1"/>
    <field name="homepart" editable="1"/>
    <field name="id" editable="1"/>
    <field name="income_grp" editable="1"/>
    <field name="iso_a2" editable="1"/>
    <field name="iso_a3" editable="1"/>
    <field name="iso_n3" editable="1"/>
    <field name="labelrank" editable="1"/>
    <field name="lastcensus" editable="1"/>
    <field name="level" editable="1"/>
    <field name="levl_code" editable="1"/>
    <field name="long_len" editable="1"/>
    <field name="mapcolor13" editable="1"/>
    <field name="mapcolor7" editable="1"/>
    <field name="mapcolor8" editable="1"/>
    <field name="mapcolor9" editable="1"/>
    <field name="name" editable="1"/>
    <field name="name_alt" editable="1"/>
    <field name="name_len" editable="1"/>
    <field name="name_long" editable="1"/>
    <field name="name_sort" editable="1"/>
    <field name="note_adm0" editable="1"/>
    <field name="note_brk" editable="1"/>
    <field name="nuts_id" editable="1"/>
    <field name="nuts_name" editable="1"/>
    <field name="pop_est" editable="1"/>
    <field name="pop_year" editable="1"/>
    <field name="postal" editable="1"/>
    <field name="region_un" editable="1"/>
    <field name="region_wb" editable="1"/>
    <field name="scalerank" editable="1"/>
    <field name="sov_a3" editable="1"/>
    <field name="sovereignt" editable="1"/>
    <field name="su_a3" editable="1"/>
    <field name="su_dif" editable="1"/>
    <field name="subregion" editable="1"/>
    <field name="subunit" editable="1"/>
    <field name="tiny" editable="1"/>
    <field name="type" editable="1"/>
    <field name="un_a3" editable="1"/>
    <field name="wb_a2" editable="1"/>
    <field name="wb_a3" editable="1"/>
    <field name="wikipedia" editable="1"/>
    <field name="woe_id" editable="1"/>
    <field name="woe_id_eh" editable="1"/>
    <field name="woe_note" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="abbrev"/>
    <field labelOnTop="0" name="abbrev_len"/>
    <field labelOnTop="0" name="adm0_a3"/>
    <field labelOnTop="0" name="adm0_a3_is"/>
    <field labelOnTop="0" name="adm0_a3_un"/>
    <field labelOnTop="0" name="adm0_a3_us"/>
    <field labelOnTop="0" name="adm0_a3_wb"/>
    <field labelOnTop="0" name="adm0_dif"/>
    <field labelOnTop="0" name="admin"/>
    <field labelOnTop="0" name="brk_a3"/>
    <field labelOnTop="0" name="brk_diff"/>
    <field labelOnTop="0" name="brk_group"/>
    <field labelOnTop="0" name="brk_name"/>
    <field labelOnTop="0" name="cntr_code"/>
    <field labelOnTop="0" name="continent"/>
    <field labelOnTop="0" name="economy"/>
    <field labelOnTop="0" name="featurecla"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="fips_10_"/>
    <field labelOnTop="0" name="formal_en"/>
    <field labelOnTop="0" name="formal_fr"/>
    <field labelOnTop="0" name="gdp_md_est"/>
    <field labelOnTop="0" name="gdp_year"/>
    <field labelOnTop="0" name="geou_dif"/>
    <field labelOnTop="0" name="geounit"/>
    <field labelOnTop="0" name="gu_a3"/>
    <field labelOnTop="0" name="homepart"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="income_grp"/>
    <field labelOnTop="0" name="iso_a2"/>
    <field labelOnTop="0" name="iso_a3"/>
    <field labelOnTop="0" name="iso_n3"/>
    <field labelOnTop="0" name="labelrank"/>
    <field labelOnTop="0" name="lastcensus"/>
    <field labelOnTop="0" name="level"/>
    <field labelOnTop="0" name="levl_code"/>
    <field labelOnTop="0" name="long_len"/>
    <field labelOnTop="0" name="mapcolor13"/>
    <field labelOnTop="0" name="mapcolor7"/>
    <field labelOnTop="0" name="mapcolor8"/>
    <field labelOnTop="0" name="mapcolor9"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="name_alt"/>
    <field labelOnTop="0" name="name_len"/>
    <field labelOnTop="0" name="name_long"/>
    <field labelOnTop="0" name="name_sort"/>
    <field labelOnTop="0" name="note_adm0"/>
    <field labelOnTop="0" name="note_brk"/>
    <field labelOnTop="0" name="nuts_id"/>
    <field labelOnTop="0" name="nuts_name"/>
    <field labelOnTop="0" name="pop_est"/>
    <field labelOnTop="0" name="pop_year"/>
    <field labelOnTop="0" name="postal"/>
    <field labelOnTop="0" name="region_un"/>
    <field labelOnTop="0" name="region_wb"/>
    <field labelOnTop="0" name="scalerank"/>
    <field labelOnTop="0" name="sov_a3"/>
    <field labelOnTop="0" name="sovereignt"/>
    <field labelOnTop="0" name="su_a3"/>
    <field labelOnTop="0" name="su_dif"/>
    <field labelOnTop="0" name="subregion"/>
    <field labelOnTop="0" name="subunit"/>
    <field labelOnTop="0" name="tiny"/>
    <field labelOnTop="0" name="type"/>
    <field labelOnTop="0" name="un_a3"/>
    <field labelOnTop="0" name="wb_a2"/>
    <field labelOnTop="0" name="wb_a3"/>
    <field labelOnTop="0" name="wikipedia"/>
    <field labelOnTop="0" name="woe_id"/>
    <field labelOnTop="0" name="woe_id_eh"/>
    <field labelOnTop="0" name="woe_note"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>nuts_name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
