<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" simplifyLocal="1" labelsEnabled="0" simplifyDrawingHints="0" readOnly="0" maxScale="0" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1" version="3.10.6-A Coruña" simplifyAlgorithm="0" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" symbollevels="0" type="singleSymbol" enableorderby="0">
    <symbols>
      <symbol clip_to_extent="1" type="marker" name="0" force_rhr="0" alpha="1">
        <layer pass="0" enabled="1" locked="0" class="SvgMarker">
          <prop v="0" k="angle"/>
          <prop v="190,178,151,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gU3ZnIFZlY3RvciBJY29ucyA6IGh0dHA6Ly93d3cub25saW5ld2ViZm9udHMuY29tL2ljb24gLS0+Cgo8c3ZnCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAxMDAwIDEwMDAiCiAgIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEwMDAgMTAwMCIKICAgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIKICAgaWQ9InN2ZzQwIgogICBzb2RpcG9kaTpkb2NuYW1lPSJpY29uX2NvdW50aW5nLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi41ICgyMDYwZWMxZjlmLCAyMDIwLTA0LTA4KSI+PGRlZnMKICAgaWQ9ImRlZnM0NCIgLz48c29kaXBvZGk6bmFtZWR2aWV3CiAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE5MjAiCiAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMTciCiAgIGlkPSJuYW1lZHZpZXc0MiIKICAgc2hvd2dyaWQ9ImZhbHNlIgogICBpbmtzY2FwZTp6b29tPSIwLjIzNiIKICAgaW5rc2NhcGU6Y3g9Ii0yNzMuMzA1MDgiCiAgIGlua3NjYXBlOmN5PSI1MDAiCiAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmc0MCIgLz4KPG1ldGFkYXRhCiAgIGlkPSJtZXRhZGF0YTIiPiBTdmcgVmVjdG9yIEljb25zIDogaHR0cDovL3d3dy5vbmxpbmV3ZWJmb250cy5jb20vaWNvbiA8cmRmOlJERj48Y2M6V29yawogICAgIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZQogICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+Cgo8cGF0aAogICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgZD0ibSAxODIuMjAzMzksNjkwLjY3Nzk2IDQ5NS43NjI3MSwtNTI1LjQyMzcyIDI5LjY2MTAyLDM4LjEzNTU5IC01MDAsNTI1LjQyMzcyIHoiCiAgIGlkPSJwYXRoNDUzMiIKICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz48cGF0aAogICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgZD0iTSAyNjIuNzAwNzQsNzc3LjUyNzEyIDc1OC40NjM0NiwyNTIuMTAzNCA3ODguMTI0NDgsMjkwLjIzODk5IDI4OC4xMjQ0Nyw4MTUuNjYyNzEgWiIKICAgaWQ9InBhdGg0NTMyLTUiCiAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+PC9zdmc+" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="10" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="id" key="dualview/previewExpressions"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory sizeType="MM" sizeScale="3x:0,0,0,0,0,0" enabled="0" minScaleDenominator="0" backgroundAlpha="255" penAlpha="255" opacity="1" diagramOrientation="Up" maxScaleDenominator="1e+08" penColor="#000000" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" width="15" labelPlacementMethod="XHeight" scaleBasedVisibility="0" rotationOffset="270" backgroundColor="#ffffff" barWidth="5" scaleDependency="Area" penWidth="0" height="15" lineSizeType="MM">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" color="#000000" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="0" showAll="1" dist="0" zIndex="0" linePlacementFlags="18" priority="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_gest">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="angle">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lib_sens">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sens_confondus">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" type="QString" name="CheckedState"/>
            <Option value="" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="route">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pr">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="abs">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_pos">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" type="QString" name="CheckedState"/>
            <Option value="" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prec_loc">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="materiel">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="1" type="QString" name="Boucle électromagnétique fixe"/>
              </Option>
              <Option type="Map">
                <Option value="2" type="QString" name="Tubes pneumatiques"/>
              </Option>
              <Option type="Map">
                <Option value="3" type="QString" name="Plaques"/>
              </Option>
              <Option type="Map">
                <Option value="4" type="QString" name="Radars"/>
              </Option>
              <Option type="Map">
                <Option value="5" type="QString" name="Caméras classiques"/>
              </Option>
              <Option type="Map">
                <Option value="6" type="QString" name="Caméras LAPI"/>
              </Option>
              <Option type="Map">
                <Option value="7" type="QString" name="Caméras sur drônes"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="def_pl">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pj">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gestionnaire">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="Gestionnaires_des_données_9577c660_5e23_41cd_96c7_f441bbc4455a" type="QString" name="Layer"/>
            <Option value="Gestionnaires des données" type="QString" name="LayerName"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="nom" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="prive">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name="ID"/>
    <alias field="id_gest" index="1" name="Gestionnaire de la route"/>
    <alias field="angle" index="2" name="Angle"/>
    <alias field="lib_sens" index="3" name="Libellé sens"/>
    <alias field="sens_confondus" index="4" name="2 sens ?"/>
    <alias field="route" index="5" name="Route"/>
    <alias field="pr" index="6" name="PR"/>
    <alias field="abs" index="7" name="Abs"/>
    <alias field="prec_pos" index="8" name="Position précise ?"/>
    <alias field="prec_loc" index="9" name="Localisation"/>
    <alias field="materiel" index="10" name="Matériel"/>
    <alias field="def_pl" index="11" name="Définition PL"/>
    <alias field="comment" index="12" name="Commentaires"/>
    <alias field="pj" index="13" name="PJ"/>
    <alias field="gestionnaire" index="14" name="Gestionnaire donnée"/>
    <alias field="prive" index="15" name="Ligne privée ?"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="id_gest" expression="" applyOnUpdate="0"/>
    <default field="angle" expression="" applyOnUpdate="0"/>
    <default field="lib_sens" expression="" applyOnUpdate="0"/>
    <default field="sens_confondus" expression="" applyOnUpdate="0"/>
    <default field="route" expression="" applyOnUpdate="0"/>
    <default field="pr" expression="" applyOnUpdate="0"/>
    <default field="abs" expression="" applyOnUpdate="0"/>
    <default field="prec_pos" expression="" applyOnUpdate="0"/>
    <default field="prec_loc" expression="" applyOnUpdate="0"/>
    <default field="materiel" expression="" applyOnUpdate="0"/>
    <default field="def_pl" expression="" applyOnUpdate="0"/>
    <default field="comment" expression="" applyOnUpdate="0"/>
    <default field="pj" expression="" applyOnUpdate="0"/>
    <default field="gestionnaire" expression="" applyOnUpdate="0"/>
    <default field="prive" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" field="id" exp_strength="0" constraints="3"/>
    <constraint unique_strength="0" notnull_strength="0" field="id_gest" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="angle" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="1" field="lib_sens" exp_strength="0" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="0" field="sens_confondus" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="route" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="pr" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="abs" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="prec_pos" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="prec_loc" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="materiel" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="def_pl" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="comment" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="pj" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="1" field="gestionnaire" exp_strength="0" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="1" field="prive" exp_strength="0" constraints="1"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="id_gest" desc=""/>
    <constraint exp="" field="angle" desc=""/>
    <constraint exp="" field="lib_sens" desc=""/>
    <constraint exp="" field="sens_confondus" desc=""/>
    <constraint exp="" field="route" desc=""/>
    <constraint exp="" field="pr" desc=""/>
    <constraint exp="" field="abs" desc=""/>
    <constraint exp="" field="prec_pos" desc=""/>
    <constraint exp="" field="prec_loc" desc=""/>
    <constraint exp="" field="materiel" desc=""/>
    <constraint exp="" field="def_pl" desc=""/>
    <constraint exp="" field="comment" desc=""/>
    <constraint exp="" field="pj" desc=""/>
    <constraint exp="" field="gestionnaire" desc=""/>
    <constraint exp="" field="prive" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" hidden="0" type="field" name="id"/>
      <column width="-1" hidden="0" type="field" name="id_gest"/>
      <column width="-1" hidden="0" type="field" name="angle"/>
      <column width="-1" hidden="0" type="field" name="lib_sens"/>
      <column width="-1" hidden="0" type="field" name="sens_confondus"/>
      <column width="-1" hidden="0" type="field" name="route"/>
      <column width="-1" hidden="0" type="field" name="pr"/>
      <column width="-1" hidden="0" type="field" name="abs"/>
      <column width="-1" hidden="0" type="field" name="prec_pos"/>
      <column width="-1" hidden="0" type="field" name="prec_loc"/>
      <column width="-1" hidden="0" type="field" name="materiel"/>
      <column width="-1" hidden="0" type="field" name="def_pl"/>
      <column width="-1" hidden="0" type="field" name="comment"/>
      <column width="-1" hidden="0" type="field" name="pj"/>
      <column width="-1" hidden="0" type="field" name="gestionnaire"/>
      <column width="-1" hidden="1" type="field" name="prive"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="abs" editable="1"/>
    <field name="angle" editable="1"/>
    <field name="comment" editable="1"/>
    <field name="def_pl" editable="1"/>
    <field name="gestionnaire" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_gest" editable="1"/>
    <field name="lib_sens" editable="1"/>
    <field name="materiel" editable="1"/>
    <field name="pj" editable="1"/>
    <field name="pr" editable="1"/>
    <field name="prec_loc" editable="1"/>
    <field name="prec_pos" editable="1"/>
    <field name="prive" editable="1"/>
    <field name="route" editable="1"/>
    <field name="sens_confondus" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="abs"/>
    <field labelOnTop="0" name="angle"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="def_pl"/>
    <field labelOnTop="0" name="gestionnaire"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_gest"/>
    <field labelOnTop="0" name="lib_sens"/>
    <field labelOnTop="0" name="materiel"/>
    <field labelOnTop="0" name="pj"/>
    <field labelOnTop="0" name="pr"/>
    <field labelOnTop="0" name="prec_loc"/>
    <field labelOnTop="0" name="prec_pos"/>
    <field labelOnTop="0" name="prive"/>
    <field labelOnTop="0" name="route"/>
    <field labelOnTop="0" name="sens_confondus"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
