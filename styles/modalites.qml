<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" readOnly="0" maxScale="100000" version="3.10.6-A Coruña" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE( &#xd;&#xd;&#xd;
	CASE &#xd;&#xd;&#xd;
	WHEN length(code_modalite)=1 THEN concat('00', code_modalite)&#xd;&#xd;&#xd;
	WHEN length(code_modalite)=2 THEN concat('0', code_modalite)&#xd;&#xd;&#xd;
	WHEN length(code_modalite)=3 THEN code_modalite&#xd;&#xd;&#xd;
	END&#xd;&#xd;&#xd;
	||' - '|| "libelle", '&lt;NULL>' )</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_codif">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Codifications_24b76dae_793d_41dc_929f_78b781e0ce20" name="Layer" type="QString"/>
            <Option value="Codifications" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet_hdf' key='id' checkPrimaryKeyUnicity='1' table=&quot;p_enqod_route&quot;.&quot;codif&quot; sql=" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="nom" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code_modalite">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="libelle">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id_codif" name="Codification" index="0"/>
    <alias field="code_modalite" name="Code de la modalité" index="1"/>
    <alias field="libelle" name="Libellé" index="2"/>
    <alias field="id" name="Identifiant unique" index="3"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id_codif" expression="" applyOnUpdate="0"/>
    <default field="code_modalite" expression="" applyOnUpdate="0"/>
    <default field="libelle" expression="" applyOnUpdate="0"/>
    <default field="id" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" constraints="1" field="id_codif" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="1" field="code_modalite" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="1" field="libelle" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="3" field="id" unique_strength="1" notnull_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_codif" desc="" exp=""/>
    <constraint field="code_modalite" desc="" exp=""/>
    <constraint field="libelle" desc="" exp=""/>
    <constraint field="id" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="COALESCE( &#xd;&#xa;&#x9;codification || ' - ' || &#xd;&#xa;&#x9;CASE &#xd;&#xa;&#x9;WHEN length(code_modalite)=1 THEN concat('00', code_modalite)&#xd;&#xa;&#x9;WHEN length(code_modalite)=2 THEN concat('0', code_modalite)&#xd;&#xa;&#x9;WHEN length(code_modalite)=3 THEN code_modalite&#xd;&#xa;&#x9;END, '&lt;NULL>' )">
    <columns>
      <column hidden="1" width="100" name="id" type="field"/>
      <column hidden="0" width="146" name="id_codif" type="field"/>
      <column hidden="0" width="120" name="code_modalite" type="field"/>
      <column hidden="0" width="373" name="libelle" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C16LM0066-BDD_transport_mutualisee/Documents techniques/Migration_geoflux</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="id_codif" index="0" showLabel="1"/>
    <attributeEditorField name="code_modalite" index="1" showLabel="1"/>
    <attributeEditorField name="libelle" index="2" showLabel="1"/>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="code_modalite"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_codif"/>
    <field editable="1" name="id_modalite"/>
    <field editable="1" name="libelle"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="code_modalite"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_codif"/>
    <field labelOnTop="0" name="id_modalite"/>
    <field labelOnTop="0" name="libelle"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE( &#xd;&#xd;&#xd;
	CASE &#xd;&#xd;&#xd;
	WHEN length(code_modalite)=1 THEN concat('00', code_modalite)&#xd;&#xd;&#xd;
	WHEN length(code_modalite)=2 THEN concat('0', code_modalite)&#xd;&#xd;&#xd;
	WHEN length(code_modalite)=3 THEN code_modalite&#xd;&#xd;&#xd;
	END&#xd;&#xd;&#xd;
	||' - '|| "libelle", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
