<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="0" simplifyDrawingHints="1" simplifyMaxScale="1" simplifyDrawingTol="1" readOnly="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" attr="class_adm" type="categorizedSymbol">
    <categories>
      <category label="Autoroute" value="Autoroute" symbol="0" render="true"/>
      <category label="Nationale" value="Nationale" symbol="1" render="true"/>
      <category label="Départementale" value="Départementale" symbol="2" render="true"/>
      <category label="Sans objet" value="Sans objet" symbol="3" render="true"/>
      <category label="" value="" symbol="4" render="true"/>
    </categories>
    <symbols>
      <symbol force_rhr="0" type="line" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,54,250,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.7"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" type="line" clip_to_extent="1" name="1" alpha="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="232,8,8,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.4"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" type="line" clip_to_extent="1" name="2" alpha="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="209,187,18,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.2"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" type="line" clip_to_extent="1" name="3" alpha="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="102,102,102,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.16"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" type="line" clip_to_extent="1" name="4" alpha="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="102,102,102,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.16"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol force_rhr="0" type="line" clip_to_extent="1" name="0" alpha="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="125,139,143,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp type="gradient" name="[source]">
      <prop k="color1" v="250,250,250,255"/>
      <prop k="color2" v="5,5,5,255"/>
      <prop k="discrete" v="0"/>
      <prop k="rampType" v="gradient"/>
    </colorramp>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" backgroundColor="#ffffff" sizeType="MM" penColor="#000000" enabled="0" barWidth="5" penAlpha="255" scaleDependency="Area" rotationOffset="270" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" scaleBasedVisibility="0" width="15" lineSizeType="MM" maxScaleDenominator="1e+08" diagramOrientation="Up" labelPlacementMethod="XHeight" opacity="1" minScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="2" zIndex="0" linePlacementFlags="18" showAll="1" priority="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_rte500">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="vocation">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nb_chausse">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nb_voies">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="etat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="acces">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="res_vert">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sens">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="res_europe">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="num_route">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="class_adm">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="longueur">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id_rte500" name="" index="0"/>
    <alias field="vocation" name="" index="1"/>
    <alias field="nb_chausse" name="" index="2"/>
    <alias field="nb_voies" name="" index="3"/>
    <alias field="etat" name="" index="4"/>
    <alias field="acces" name="" index="5"/>
    <alias field="res_vert" name="" index="6"/>
    <alias field="sens" name="" index="7"/>
    <alias field="res_europe" name="" index="8"/>
    <alias field="num_route" name="" index="9"/>
    <alias field="class_adm" name="" index="10"/>
    <alias field="longueur" name="" index="11"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id_rte500"/>
    <default applyOnUpdate="0" expression="" field="vocation"/>
    <default applyOnUpdate="0" expression="" field="nb_chausse"/>
    <default applyOnUpdate="0" expression="" field="nb_voies"/>
    <default applyOnUpdate="0" expression="" field="etat"/>
    <default applyOnUpdate="0" expression="" field="acces"/>
    <default applyOnUpdate="0" expression="" field="res_vert"/>
    <default applyOnUpdate="0" expression="" field="sens"/>
    <default applyOnUpdate="0" expression="" field="res_europe"/>
    <default applyOnUpdate="0" expression="" field="num_route"/>
    <default applyOnUpdate="0" expression="" field="class_adm"/>
    <default applyOnUpdate="0" expression="" field="longueur"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id_rte500" unique_strength="1" constraints="3" notnull_strength="1"/>
    <constraint exp_strength="0" field="vocation" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nb_chausse" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nb_voies" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="etat" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="acces" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="res_vert" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="sens" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="res_europe" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="num_route" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="class_adm" unique_strength="0" constraints="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="longueur" unique_strength="0" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id_rte500" desc=""/>
    <constraint exp="" field="vocation" desc=""/>
    <constraint exp="" field="nb_chausse" desc=""/>
    <constraint exp="" field="nb_voies" desc=""/>
    <constraint exp="" field="etat" desc=""/>
    <constraint exp="" field="acces" desc=""/>
    <constraint exp="" field="res_vert" desc=""/>
    <constraint exp="" field="sens" desc=""/>
    <constraint exp="" field="res_europe" desc=""/>
    <constraint exp="" field="num_route" desc=""/>
    <constraint exp="" field="class_adm" desc=""/>
    <constraint exp="" field="longueur" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="-1" hidden="0" type="field" name="id_rte500"/>
      <column width="-1" hidden="0" type="field" name="vocation"/>
      <column width="-1" hidden="0" type="field" name="nb_chausse"/>
      <column width="-1" hidden="0" type="field" name="nb_voies"/>
      <column width="-1" hidden="0" type="field" name="etat"/>
      <column width="-1" hidden="0" type="field" name="acces"/>
      <column width="-1" hidden="0" type="field" name="res_vert"/>
      <column width="-1" hidden="0" type="field" name="sens"/>
      <column width="-1" hidden="0" type="field" name="res_europe"/>
      <column width="-1" hidden="0" type="field" name="num_route"/>
      <column width="-1" hidden="0" type="field" name="class_adm"/>
      <column width="-1" hidden="0" type="field" name="longueur"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="acces" editable="1"/>
    <field name="class_adm" editable="1"/>
    <field name="etat" editable="1"/>
    <field name="id_rte500" editable="1"/>
    <field name="longueur" editable="1"/>
    <field name="nb_chausse" editable="1"/>
    <field name="nb_voies" editable="1"/>
    <field name="num_route" editable="1"/>
    <field name="res_europe" editable="1"/>
    <field name="res_vert" editable="1"/>
    <field name="sens" editable="1"/>
    <field name="vocation" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="acces"/>
    <field labelOnTop="0" name="class_adm"/>
    <field labelOnTop="0" name="etat"/>
    <field labelOnTop="0" name="id_rte500"/>
    <field labelOnTop="0" name="longueur"/>
    <field labelOnTop="0" name="nb_chausse"/>
    <field labelOnTop="0" name="nb_voies"/>
    <field labelOnTop="0" name="num_route"/>
    <field labelOnTop="0" name="res_europe"/>
    <field labelOnTop="0" name="res_vert"/>
    <field labelOnTop="0" name="sens"/>
    <field labelOnTop="0" name="vocation"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_rte500</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
