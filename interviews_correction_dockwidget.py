        #!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface
from processing.tools import postgis

import sys
import string
import os

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *
from .standard import *
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from interviews_correction_dockwidget import Ui_DockWidget

class interviews_correction_dockwidget(QDockWidget): 
    def __init__(self, caller, iface):
        QDockWidget.__init__(self)
        self.ui= Ui_DockWidget()
        self.ui.setupUi(self) 
        self.caller = caller
        self.iface = self.caller.iface
        self.plugin_dir = self.caller.plugin_dir
        
        self.schemasModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_schema_enq.setModel(self.schemasModel)
        self.ui.comboBox_schema_ref.setModel(self.schemasModel)
        
        self.geoTableModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_table_geo.setModel(self.geoTableModel)
        
        self.vehTypeModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_veh_type.setModel(self.vehTypeModel)
        
        self.countryModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_o_country.setModel(self.countryModel)
        self.ui.comboBox_d_country.setModel(self.countryModel)
        self.ui.comboBox_plate_country.setModel(self.countryModel)
        
        self.zoneModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_o_zone.setModel(self.zoneModel)
        self.ui.comboBox_d_zone.setModel(self.zoneModel)
        
        self.code_com_o = "N"
        self.code_com_d = "N"
        self.code_pole_o = "N"
        self.code_pole_d = "N"
        self.code_voie_o = "N"
        self.code_voie_d = "N"
        self.code_adr_o = "N"
        self.code_adr_d = "N"
        
        self.portModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_o_port.setModel(self.portModel)
        self.ui.comboBox_d_port.setModel(self.portModel)
        
        self.boundaryModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_o_boundary.setModel(self.boundaryModel)
        self.ui.comboBox_d_boundary.setModel(self.boundaryModel)
        
        self.purposeModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_o_purpose.setModel(self.purposeModel)
        self.ui.comboBox_d_purpose.setModel(self.purposeModel)
        
        self.interviewModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_interview.setModel(self.interviewModel)
        
        self.ODCodeModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_od_code.setModel(self.ODCodeModel)
        
        self.resGeoModel = QtSql.QSqlQueryModel()
        self.ui.tableView_geo.setModel(self.resGeoModel)
        self.ui.tableView_geo.verticalHeader().hide()
                
        self.resGoodsModel = QtSql.QSqlQueryModel()
        self.ui.tableView_goods.setModel(self.resGoodsModel)
        self.ui.tableView_goods.verticalHeader().hide()
        
        self.goodsModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_goods.setModel(self.goodsModel)        
        self.interviewModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_interview.setModel(self.interviewModel)
        
        self.dangerGoodModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_matiere.setModel(self.dangerGoodModel)
        
        self.plaquesOrangeModel = QtSql.QSqlQueryModel()
        self.ui.comboBox_plaques_orange.setModel(self.plaquesOrangeModel)
        
        self.resGeoModel = QtSql.QSqlQueryModel()
        self.ui.tableView_goods.setModel(self.resGeoModel)
        self.ui.tableView_goods.verticalHeader().hide()
        
        self.ui.textEdit_interviews_filter.setText("")
        
        # Connexion des signaux et des slots
        self._connectSlots()
        
        self.enq_table_od_control=False
        self.filter_ok=False
        self.enq_table_goods_control=False
        self.point_enq_table_ok=False
        self.ports_table_ok=False
        self.geo_table_ok=False
        self.goods_asso_ok=False
        self.codif_table_ok=False
        
        self._slot_comboBox_connection_current_index_changed(0)
    
    def _connectSlots(self):
        self.ui.tabWidget.currentChanged.connect(self._slot_tabWidget_current_index_changed)
        
        # 1st tab
        self.ui.comboBox_connection.currentIndexChanged.connect(self._slot_comboBox_connection_current_index_changed)
        self.ui.comboBox_schema_ref.currentIndexChanged.connect(self._slot_comboBox_schema_ref_current_index_changed)
        self.ui.pushButton_test_sql_filter.clicked.connect(self._slot_pushButton_test_sql_filter_clicked)
        self.ui.pushButton_launch.clicked.connect(self._slot_pushButton_launch_clicked)
        
        # 2nd tab
        self.ui.comboBox_o_country.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_d_country.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_o_zone.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_d_zone.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_o_port.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_d_port.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_o_boundary.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_d_boundary.currentIndexChanged.connect(self.geo_changed)
        self.ui.comboBox_od_code.currentIndexChanged.connect(self.sth_changed)
        self.ui.comboBox_plate_country.currentIndexChanged.connect(self.sth_changed)
        self.ui.pushButton_search_o_zone.clicked.connect(self._slot_pushButton_search_o_zone_clicked)
        self.ui.pushButton_search_d_zone.clicked.connect(self._slot_pushButton_search_d_zone_clicked)
        self.ui.pushButton_search_o_municipality.clicked.connect(self._slot_pushButton_search_o_municipality_clicked)
        self.ui.pushButton_search_d_municipality.clicked.connect(self._slot_pushButton_search_d_municipality_clicked)
        self.ui.pushButton_search_o_pole.clicked.connect(self._slot_pushButton_search_o_pole_clicked)
        self.ui.pushButton_search_d_pole.clicked.connect(self._slot_pushButton_search_d_pole_clicked)
        self.ui.pushButton_search_o_street.clicked.connect(self._slot_pushButton_search_o_street_clicked)
        self.ui.pushButton_search_d_street.clicked.connect(self._slot_pushButton_search_d_street_clicked)
        self.ui.pushButton_search_o_address.clicked.connect(self._slot_pushButton_search_o_address_clicked)
        self.ui.pushButton_search_d_address.clicked.connect(self._slot_pushButton_search_d_address_clicked)
        self.ui.pushButton_search_o_port.clicked.connect(self._slot_pushButton_search_o_port_clicked)
        self.ui.pushButton_search_d_port.clicked.connect(self._slot_pushButton_search_d_port_clicked)
        self.ui.pushButton_search_o_boundary.clicked.connect(self._slot_pushButton_search_o_boundary_clicked)
        self.ui.pushButton_search_d_boundary.clicked.connect(self._slot_pushButton_search_d_boundary_clicked)
        self.ui.pushButton_choose_o.clicked.connect(self._slot_pushButton_choose_o)
        self.ui.pushButton_choose_d.clicked.connect(self._slot_pushButton_choose_d)
        self.ui.pushButton_invert_od.clicked.connect(self._slot_pushButton_invert_od_clicked)
        self.ui.pushButton_invert_od_purposes.clicked.connect(self._slot_pushButton_invert_od_purposes_clicked)
        self.ui.pushButton_invert_od_ports.clicked.connect(self._slot_pushButton_invert_od_ports_clicked)
        self.ui.pushButton_invert_od_boundaries.clicked.connect(self._slot_pushButton_invert_od_boundaries_clicked)
        
        # 3rd tab
        self.ui.pushButton_search_goods.clicked.connect(self._slot_pushButton_search_goods_clicked)
        self.ui.comboBox_goods.currentIndexChanged.connect(self.sth_changed)
        self.ui.pushButton_choose_goods.clicked.connect(self._slot_pushButton_choose_goods_clicked)
        self.ui.pushButton_save_goods_asso.clicked.connect(self._slot_pushButton_save_goods_asso_clicked)
        self.ui.pushButton_delete_goods_asso.clicked.connect(self._slot_pushButton_delete_goods_asso_clicked)
        self.ui.checkBox_tonnage.toggled.connect(self._slot_checkBox_tonnage_toggled)
        self.ui.checkBox_tonnage_non_reponse.toggled.connect(self.sth_changed)
        self.ui.doubleSpinBox_tonnage.valueChanged.connect(self.sth_changed)
        self.ui.comboBox_plaques_orange.currentIndexChanged.connect(self.sth_changed)
        
        # Global
        self.ui.comboBox_interview.currentIndexChanged.connect(self._slot_comboBox_interview_current_index_changed)
        self.ui.comboBox_veh_type.currentIndexChanged.connect(self.sth_changed)
        self.ui.pushButton_prev.clicked.connect(self._slot_pushButton_prev_clicked)
        self.ui.pushButton_next.clicked.connect(self._slot_pushButton_next_clicked)
        self.ui.pushButton_save.clicked.connect(self._slot_pushButton_save_clicked)
        self.ui.pushButton_reset.clicked.connect(self._slot_pushButton_reset_clicked)
        self.ui.pushButton_open_form.clicked.connect(self._slot_pushButton_open_form_clicked)
        self.ui.buttonBox.button(QDialogButtonBox.Close).clicked.connect(self._slot_close)
    
    def sth_changed(self):
        self.ui.pushButton_save.setEnabled(True)
    
    def _slot_comboBox_connection_current_index_changed(self, indexChosenLine):
        c = self.ui.comboBox_connection.currentText()
        if (c!=""):
            self.ui.pushButton_launch.setEnabled(True)
            
            self.uri = postgis.uri_from_name(c)
            self.db = QtSql.QSqlDatabase.addDatabase("QPSQL", connectionName="db_connection_temp")
            self.db.setHostName(self.uri.host())
            self.db.setPort(int(self.uri.port()))
            self.db.setDatabaseName(self.uri.database())
            self.db.setUserName(self.uri.username())
            self.db.setPassword(self.uri.password())
            self.db_string = "dbname='"+self.uri.database()+"' host="+self.uri.host()+" port="+str(self.uri.port())+" user='"+self.uri.username()+"' password='"+self.uri.password()+"'"
            ok=self.db.open()
            
            if ok==False:
                self.iface.messageBar().pushMessage("Erreur","Connection failed: "+self.db.lastError().text(),level=2)
            else:
                s="select schema_name from information_schema.schemata order by 1"
                self.schemasModel.setQuery(s, self.db)
                
                self.ui.comboBox_schema_enq.setCurrentIndex(0)        
                if len(self.schemasModel.match(self.schemasModel.index(0,0), 0, "o_geoflux_ref", 1))>0:
                    self.ui.comboBox_schema_ref.setCurrentIndex(self.schemasModel.match(self.schemasModel.index(0,0), 0, "o_geoflux_ref", 1)[0].row())
                else:
                    self.ui.comboBox_schema_ref.setCurrentIndex(0)
                if len(self.geoTableModel.match(self.geoTableModel.index(0,0), 0, "n_geographie_2021", 1))>0:
                    self.ui.comboBox_table_geo.setCurrentIndex(self.geoTableModel.match(self.geoTableModel.index(0,0), 0, "n_geographie_2021", 1)[0].row())
                else:
                    self.ui.comboBox_table_geo.setCurrentIndex(0)
        else:
            self.iface.messageBar().pushMessage("Attention", "L'utilisation du plugin Géoflux nécessite de définir une connexion PostGIS.", level=1)
            self.ui.pushButton_launch.setEnabled(False)
            
    def _slot_comboBox_schema_ref_current_index_changed(self, indexChosenLine):
        s = "select table_name from information_schema.tables where table_schema = '"+self.ui.comboBox_schema_ref.currentText()+"' order by 1"
        self.schema_ref = self.ui.comboBox_schema_ref.currentText()
        self.geoTableModel.setQuery(s, self.db)
        self.ui.comboBox_table_geo.setCurrentIndex(0)        
    
    def _slot_pushButton_test_sql_filter_clicked(self):
        filter = self.ui.textEdit_interviews_filter.toPlainText()
        if filter == "":
            filter = "1=1"
        s = "SELECT count(*) FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" WHERE "+ filter
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next()
        box = QMessageBox()
        box.setModal(True)
        box.setStandardButtons(QMessageBox.Close)
        if q.value(0) != None:
            self.filter_ok=True
            box.setText(" La requête a retourné "+str(q.value(0))+" lignes.")
        else:
            self.filter_ok=False
            box.setText(" La requête "+s+" a échoué.")
        box.exec_()
    
    def _slot_tabWidget_current_index_changed(self, indexChosenTab):
        if indexChosenTab == 0:
            self.ui.comboBox_interview.setEnabled(False)
            self.ui.label_interview.setEnabled(False)
            self.ui.pushButton_reset.setEnabled(False)
            self.ui.pushButton_save.setEnabled(False)
            self.ui.pushButton_prev.setEnabled(False)
            self.ui.pushButton_next.setEnabled(False)
            self.ui.label_veh_type.setEnabled(False)
            self.ui.comboBox_veh_type.setEnabled(False)
            self.ui.label_date_time.setEnabled(False)
            self.ui.pushButton_open_form.setEnabled(False)
            self.ui.label_per_enq.setEnabled(False)
            self.ui.label_per_enq_value.setEnabled(False)
        else:
            self.ui.comboBox_interview.setEnabled(True)
            self.ui.label_interview.setEnabled(True)
            self.ui.pushButton_reset.setEnabled(True)
            self.ui.pushButton_save.setEnabled(True)
            self.ui.pushButton_prev.setEnabled(True)
            self.ui.pushButton_next.setEnabled(True)
            self.ui.label_veh_type.setEnabled(True)
            self.ui.comboBox_veh_type.setEnabled(True)
            self.ui.label_date_time.setEnabled(True)
            self.ui.pushButton_open_form.setEnabled(True)
            self.ui.label_per_enq.setEnabled(True)
            self.ui.label_per_enq_value.setEnabled(True)
    
    def _slot_pushButton_launch_clicked(self):
        self.table_geo = self.ui.comboBox_table_geo.currentText()
        msg= ""
        s = "SELECT array_agg(a.attname) as col_names \
             FROM pg_catalog.pg_attribute a LEFT JOIN pg_catalog.pg_attrdef adef ON a.attrelid=adef.adrelid AND a.attnum=adef.adnum \
             WHERE a.attrelid = (SELECT oid FROM pg_catalog.pg_class WHERE relname='"+self.table_geo+"' \
               AND relnamespace = (SELECT oid FROM pg_catalog.pg_namespace WHERE nspname = '"+self.schema_ref+"')) \
               AND a.attnum > 0 AND NOT a.attisdropped"
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next() 
        missing_mandatory_columns = list(set(GEO_MANDATORY_COL) - set(pg_array2py_str_list(q.value(0))))
        
        if (missing_mandatory_columns == []):
            self.geo_table_ok = True 
        else:
            self.geo_table_ok = False            
            msg = msg + u"Colonnes manquantes dans la table des géographies : "+str(missing_mandatory_columns)+"\n"
        
        s = "SELECT array_agg(a.attname) as col_names \
             FROM pg_catalog.pg_attribute a LEFT JOIN pg_catalog.pg_attrdef adef ON a.attrelid=adef.adrelid AND a.attnum=adef.adnum \
             WHERE a.attrelid = (SELECT oid FROM pg_catalog.pg_class WHERE relname='"+TEMP_TABLE_POINT_ENQ + SUFFIXE_IMPORT+"' \
               AND relnamespace = (SELECT oid FROM pg_catalog.pg_namespace WHERE nspname = '"+self.ui.comboBox_schema_enq.currentText()+"')) \
               AND a.attnum > 0 AND NOT a.attisdropped"
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next()
        
        existing_col_list = pg_array2py_str_list(q.value(0))
        mandatory_columns = ["code_poste", "num_point", "date_enq", "geom"]
        missing_mandatory_columns = list(set(mandatory_columns) - set(existing_col_list))
        
        if (missing_mandatory_columns == []):
            self.points_enq_table_ok = True 
        else:
            self.points_enq_table_ok = False
            msg = msg + u"Colonnes manquantes dans la table des points d'enquête : "+str(missing_mandatory_columns)+"\n"
        
        s = "SELECT array_agg(a.attname) as col_names \
             FROM pg_catalog.pg_attribute a LEFT JOIN pg_catalog.pg_attrdef adef ON a.attrelid=adef.adrelid AND a.attnum=adef.adnum \
             WHERE a.attrelid = (SELECT oid FROM pg_catalog.pg_class WHERE relname='n_codif_standard' \
               AND relnamespace = (SELECT oid FROM pg_catalog.pg_namespace WHERE nspname = '"+self.schema_ref+"')) \
               AND a.attnum > 0 AND NOT a.attisdropped"
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next()
        
        existing_col_list = pg_array2py_str_list(q.value(0))
        mandatory_columns = ["champ", "controle", "modalite", "libelle"]
        missing_mandatory_columns = list(set(mandatory_columns) - set(existing_col_list))
        
        if (missing_mandatory_columns == []):
            self.codif_table_ok = True 
        else:
            self.codif_table_ok = False
            msg = msg + u"Colonnes manquantes dans la table des codifications standard : "+str(missing_mandatory_columns)+"\n"
        
        s = "SELECT array_agg(a.attname) as col_names \
             FROM pg_catalog.pg_attribute a LEFT JOIN pg_catalog.pg_attrdef adef ON a.attrelid=adef.adrelid AND a.attnum=adef.adnum \
             WHERE a.attrelid = (SELECT oid FROM pg_catalog.pg_class WHERE relname='n_corresp_marchandises' \
               AND relnamespace = (SELECT oid FROM pg_catalog.pg_namespace WHERE nspname = '"+self.schema_ref+"')) \
               AND a.attnum > 0 AND NOT a.attisdropped"
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next()
        
        existing_col_list = pg_array2py_str_list(q.value(0))
        mandatory_columns = ["modalite", "libelle"]
        missing_mandatory_columns = list(set(mandatory_columns) - set(existing_col_list))
        
        if (missing_mandatory_columns == []):
            self.goods_asso_ok = True 
        else:
            self.goods_asso_ok = False
            msg = msg + u"Colonnes manquantes dans la table des associations de marchandises : "+str(missing_mandatory_columns)+"\n"
        
        s = "SELECT array_agg(a.attname) as col_names \
             FROM pg_catalog.pg_attribute a LEFT JOIN pg_catalog.pg_attrdef adef ON a.attrelid=adef.adrelid AND a.attnum=adef.adnum \
             WHERE a.attrelid = (SELECT oid FROM pg_catalog.pg_class WHERE relname='"+TEMP_TABLE_ITW + SUFFIXE_IMPORT+"' \
               AND relnamespace = (SELECT oid FROM pg_catalog.pg_namespace WHERE nspname = '"+self.ui.comboBox_schema_enq.currentText()+"')) \
               AND a.attnum > 0 AND NOT a.attisdropped"
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next()
        
        self.existing_col_list = pg_array2py_str_list(q.value(0))
        mandatory_goods_columns = ["id_itw", "code_poste", "num_point", "immat_pays", "type_veh", "marchandises_prec", "marchandises"]
        mandatory_od_columns = ["id_itw", "code_poste", "num_point", "immat_pays", "type_veh", "pays_dest_ini", "pays_dest", "zone_dest_ini", "zone_dest", "commune_dest_ini", "commune_dest_prec", "commune_dest", "pays_orig_ini", "pays_orig", "zone_orig_ini", "zone_orig", "commune_orig_ini", "commune_orig_prec", "commune_orig", "code_controle_od"]
        missing_mandatory_od_columns = list(set(mandatory_od_columns) - set(self.existing_col_list))  
        missing_mandatory_goods_columns = list(set(mandatory_goods_columns) - set(self.existing_col_list))
        
        if (missing_mandatory_od_columns == []):
            self.enq_table_od_control = True
            self.real_optional_od_columns = list(set(ITW_OD_CONTROL_OPT_COL) & set(self.existing_col_list))
        else:
            self.iface.messageBar().pushMessage("Attention",u"Colonnes manquantes pour le contrôle des OD dans la table des interviews : "+str(missing_mandatory_od_columns)+"\n")
            self.enq_table_od_control = False
            
        if (missing_mandatory_goods_columns == []):
            self.enq_table_goods_control = True            
            self.real_optional_goods_columns = list(set(ITW_GOODS_CONTROL_OPT_COL) & set(self.existing_col_list)) 
        else:
            self.iface.messageBar().pushMessage("Attention",u"Colonnes manquantes pour la codification des marchandises dans la table des interviews : "+str(missing_mandatory_goods_columns)+"\n")
            self.enq_table_goods_control = False
        
        if (missing_mandatory_goods_columns != []) and (missing_mandatory_od_columns != []):
            self.ui.textEdit_interviews_filter.setEnabled(False)
            self.ui.pushButton_test_sql_filter.setEnabled(False)
        else:
            self.ui.textEdit_interviews_filter.setEnabled(True)
            self.ui.pushButton_test_sql_filter.setEnabled(True)                
        
        filter = self.ui.textEdit_interviews_filter.toPlainText()
        if filter == "":
            filter = "1=1"
        s = "SELECT count(*) FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" WHERE "+ filter
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next()
        if q.value(0) != None:
            self.filter_ok=True
        else:
            self.filter_ok=False
            msg = msg + u"Le filtre des interviews a échoué."
        
        if (self.enq_table_goods_control and self.filter_ok and self.points_enq_table_ok and self.goods_asso_ok and self.codif_table_ok):
            self.goods_codif = True
            self.ui.tabWidget.setTabEnabled(2, True)
            
            s = "SELECT modalite || ' - ' || libelle, modalite, libelle \
                 FROM "+self.schema_ref+".n_codif_standard \
                 WHERE champ = 'marchandises' AND modalite != 'I' \
                 ORDER BY 1"
            self.goodsModel.setQuery(s, self.db)
            
            s = "SELECT modalite || ' - ' || libelle, modalite, libelle \
                 FROM "+self.schema_ref+".n_codif_standard \
                 WHERE champ = 'code_matiere' \
                 ORDER BY 1"
            self.dangerGoodModel.setQuery(s, self.db)
            
            s = "SELECT modalite || ' - ' || libelle, modalite, libelle \
                 FROM "+self.schema_ref+".n_codif_standard \
                 WHERE champ = 'plaques_orange' \
                 ORDER BY 1"
            self.plaquesOrangeModel.setQuery(s, self.db)
            
            self.ui.tabWidget.setCurrentIndex(2) 
        else: # Contrôle des marchandises impossible 
            self.goods_codif = False
            self.ui.tabWidget.setTabEnabled(2, False)
        
        if (self.enq_table_od_control and self.filter_ok and self.points_enq_table_ok and self.geo_table_ok and self.codif_table_ok):
            self.od_control = True
            self.ui.tabWidget.setTabEnabled(1, True)
            
            # Ajout des couches géographiques
            self.interviews_check_node = self.caller.node_group.findGroup("Contrôle manuel des interviews")
            self.caller.node_group.removeChildNode(self.interviews_check_node)
            self.interviews_check_node = self.caller.node_group.insertGroup(3, "Contrôle manuel des interviews")
            self.interviews_check_node.setExpanded(True)
            self.interviews_check_node.setItemVisibilityChecked(True)
            self.o_point_layer = add_layer(self.uri, self.iface, u"Point origine", self.schema_ref, self.table_geo, "geom_point", "1=2", "", self.interviews_check_node, 1, False, True)        
            self.o_point_layer.loadNamedStyle(self.caller.styles_dir + '/point_orig.qml')
            self.d_point_layer = add_layer(self.uri, self.iface, u"Point destination", self.schema_ref, self.table_geo, "geom_point", "1=2", "", self.interviews_check_node, 2, False, True)
            self.d_point_layer.loadNamedStyle(self.caller.styles_dir + '/point_dest.qml')
            self.point_enq_layer = add_layer(self.uri, self.iface, u"Points d'enquête", self.ui.comboBox_schema_enq.currentText(), TEMP_TABLE_POINT_ENQ + SUFFIXE_IMPORT, "geom", "1=2", "", self.interviews_check_node, 3, False, True)
            self.point_enq_layer.loadNamedStyle(self.caller.styles_dir + '/points_enq_terrain.qml')
            self.o_port_layer = add_layer(self.uri, self.iface, u"Port origine", self.schema_ref, self.table_geo, "geom_point", "1=2", "", self.interviews_check_node, 4, False, True)
            self.o_port_layer.loadNamedStyle(self.caller.styles_dir + '/port_orig.qml')
            self.d_port_layer = add_layer(self.uri, self.iface, u"Port destination", self.schema_ref, self.table_geo, "geom_point", "1=2", "", self.interviews_check_node, 5, False, True)
            self.d_port_layer.loadNamedStyle(self.caller.styles_dir + '/port_dest.qml')
            self.o_boundary_layer = add_layer(self.uri, self.iface, u"Frontière origine", self.schema_ref, self.table_geo, "geom_point", "1=2", "", self.interviews_check_node, 6, False, True)
            self.o_boundary_layer.loadNamedStyle(self.caller.styles_dir + '/front_orig.qml')
            self.d_boundary_layer = add_layer(self.uri, self.iface, u"Frontière destination", self.schema_ref, self.table_geo, "geom_point", "1=2", "", self.interviews_check_node, 7, False, True)
            self.d_boundary_layer.loadNamedStyle(self.caller.styles_dir + '/front_dest.qml')
            self.o_zone_layer = add_layer(self.uri, self.iface, u"Zone origine", self.schema_ref, self.table_geo, "geom_polygon", "1=2", "", self.interviews_check_node, 8, False, True)        
            self.o_zone_layer.loadNamedStyle(self.caller.styles_dir + '/polygon_orig.qml')
            self.d_zone_layer = add_layer(self.uri, self.iface, u"Zone destination", self.schema_ref, self.table_geo, "geom_polygon", "1=2", "", self.interviews_check_node, 9, False, True)
            self.d_zone_layer.loadNamedStyle(self.caller.styles_dir + '/polygon_dest.qml')
            self.interview_layer = add_layer(self.uri, self.iface, u"Interviews", self.ui.comboBox_schema_enq.currentText(), TEMP_TABLE_ITW + SUFFIXE_IMPORT, None, filter, None, self.interviews_check_node, 10, False, False)
            self.iface.mapCanvas().refreshAllLayers()
            
            # Population des comboBoxes
            s = "(SELECT code_pays || ' - ' || libelle, code_pays, libelle FROM "+self.schema_ref+"."+self.table_geo+" \
                  WHERE type_lieu = 1) \
                 UNION \
                 (SELECT  'N - Aucun', 'N', 'Aucun') \
                 UNION \
                 (SELECT  'X - Inconnu', 'X', 'Inconnu') \
                 ORDER BY 1"
            self.countryModel.setQuery(s, self.db) 
            
            s = "(SELECT code_zone || ' - ' || libelle, code_zone, code_pays, libelle FROM "+self.schema_ref+"."+self.table_geo+" \
                  WHERE type_lieu = 2) \
                 UNION \
                 (SELECT  'N - Aucune', 'N', 'Aucune', 'Aucun') \
                 UNION \
                 (SELECT  'X - Inconnue', 'X', 'Inconnue', 'Inconnu') \
                 ORDER BY 1"
            self.zoneModel.setQuery(s, self.db) 

            s = "(SELECT code_port || ' - ' || libelle, code_port, code_pays, libelle FROM "+self.schema_ref+"."+self.table_geo+" WHERE type_lieu = 7) \
                 UNION \
                 (SELECT  'N - Aucune', 'N', 'Aucune', 'Aucune') \
                 UNION \
                 (SELECT  'X - Inconnue', 'X', 'Inconnue', 'Inconnue') \
                 ORDER BY 1"
            self.portModel.setQuery(s, self.db)
        
            s = "(SELECT code_front || ' - ' || libelle, code_front, code_pays, libelle FROM "+self.schema_ref+"."+self.table_geo+" \
                  WHERE type_lieu = 8) \
                 UNION \
                 (SELECT  'N - Aucune', 'N', 'Aucune', 'Aucune') \
                 UNION \
                 (SELECT  'X - Inconnue', 'X', 'Inconnue', 'Inconnue') \
                 ORDER BY 1"
            self.boundaryModel.setQuery(s, self.db)
            
            s = "SELECT modalite || ' - ' || libelle, modalite, libelle \
                 FROM "+self.schema_ref+".n_codif_standard \
                 WHERE champ = 'motif_orig' AND modalite !=  'I' \
                 ORDER BY 1"
            self.purposeModel.setQuery(s, self.db)
            
            s = "SELECT modalite || ' - ' || libelle, modalite, libelle \
                 FROM "+self.schema_ref+".n_codif_standard \
                 WHERE champ = 'code_controle_od' \
                 ORDER BY 1"
            self.ODCodeModel.setQuery(s, self.db)
            
            self.ui.tabWidget.setCurrentIndex(1)      
            
        else: # Contrôle des OD impossible 
            self.od_control = False
            self.ui.tabWidget.setTabEnabled(1, False)
                
        if (self.od_control == False and self.goods_codif == False):
            box = QMessageBox()
            box.setModal(True)
            box.setStandardButtons(QMessageBox.Close)
            box.setText(msg)
            box.exec_()
                
        if (self.od_control or self.goods_codif):        
            s = "SELECT code_poste::character varying || ' - ' || num_point::character varying || ' - ' || id_itw::character varying as id, id_itw, code_poste, num_point \
                 FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" \
                 WHERE "+filter+" \
                 ORDER BY code_poste, num_point, id_itw"
            self.interviewModel.setQuery(s, self.db)
            
            s = "SELECT modalite || ' - ' || libelle, modalite, libelle \
                 FROM "+self.schema_ref+".n_codif_standard \
                 WHERE champ = 'type_veh' AND modalite != '99' AND modalite != 'I' \
                 ORDER BY 1"
            self.vehTypeModel.setQuery(s, self.db)
            
            self.ui.comboBox_interview.setCurrentIndex(0) 
            self._slot_comboBox_interview_current_index_changed(0)
            self._slot_pushButton_next_clicked()
            self._slot_pushButton_prev_clicked()
    
    def update_geo_code(self, code, type_lieu, orig_dest):
        self.sth_changed()
        final_code = ""
        final_lib = ""
        final_code_prec = ""
        
        if (code == None) or (code == 'X'):
            final_code = 'X'
            final_lib = 'X'
        elif (code == 'N'):
            final_code = 'N'
            final_lib = 'N'
        else:
            if (type_lieu == 3):
                col_prec = "code_zone"
                col = "code_com"
            elif (type_lieu == 4):
                col_prec = "code_com"
                col = "code_pole"
            elif (type_lieu == 5):
                col_prec = "code_com"
                col = "code_voie"
            elif (type_lieu == 6):
                col_prec = "code_com"
                col = "code_adr"
            
            s="SELECT "+col+", "+col+" || ' - ' || libelle, "+col_prec+" FROM "+self.schema_ref+"."+self.table_geo+" \
               WHERE type_lieu = "+str(type_lieu)+" AND "+col+" = '"+code+"'"
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
            if (q.size()>0):
                q.next()
                final_code = q.value(0)
                final_lib = q.value(1)
                final_code_prec = q.value(2)
            else:
                final_code = 'X'
                final_lib = 'X'
                final_code_prec = 'X'
        
        if (orig_dest == 1): # Origines
            if (type_lieu == 3):
                self.code_com_o = final_code
                if (final_code_prec != self.code_zone_o):
                    self.ui.label_o_municipality.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_o_municipality.setStyleSheet("")
                self.ui.label_o_municipality.setText(final_lib)
                self.code_voie_o = "N"
                self.ui.label_o_street.setText("N")
                self.code_adr_o = "N"
                self.ui.label_o_address.setText("N")
                self.code_pole_o = "N"
                self.ui.label_o_pole.setText("N")
            elif (type_lieu == 4):
                self.code_pole_o = final_code
                if (final_code_prec != self.code_com_o):
                    self.ui.label_o_pole_error.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_o_pole_error.setStyleSheet("")
                self.ui.label_o_pole.setText(final_lib)
                self.code_voie_o = "N"
                self.ui.label_o_street.setText("N")
                self.code_adr_o = "N"
                self.ui.label_o_address.setText("N")
            elif (type_lieu == 5):
                self.code_voie_o = final_code
                if (final_code_prec != self.code_com_o):
                    self.ui.label_o_street_error.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_o_street_error.setStyleSheet("")
                self.ui.label_o_street.setText(final_lib)
                self.code_adr_o = "N"
                self.ui.label_o_address.setText("N")
            elif (type_lieu == 6):
                self.code_adr_o = final_code
                if (final_code_prec != self.code_voie_o):
                    self.ui.label_o_address_error.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_o_address_error.setStyleSheet("")
                self.ui.label_o_address.setText(final_lib)
        elif (orig_dest == 2): # Destinations
            if (type_lieu == 3):
                self.code_com_d = final_code
                if (final_code_prec != self.code_zone_d):
                    self.ui.label_d_municipality.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_d_municipality.setStyleSheet("")
                self.ui.label_d_municipality.setText(final_lib)
                self.code_voie_d = "N"
                self.ui.label_d_street.setText("N")
                self.code_adr_d = "N"
                self.ui.label_d_address.setText("N")
                self.code_pole_d = "N"
                self.ui.label_d_pole.setText("N")
            elif (type_lieu == 4):
                self.code_pole_d = final_code
                if (final_code_prec != self.code_com_d):
                    self.ui.label_d_pole_error.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_d_pole_error.setStyleSheet("")
                self.ui.label_d_pole.setText(final_lib)
                self.code_voie_d = "N"
                self.ui.label_d_street.setText("N")
                self.code_adr_d = "N"
                self.ui.label_d_address.setText("N")
            elif (type_lieu == 5):
                self.code_voie_d = final_code
                if (final_code_prec != self.code_com_d):
                    self.ui.label_d_street_error.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_d_street_error.setStyleSheet("")
                self.ui.label_d_street.setText(final_lib)
                self.code_adr_d = "N"
                self.ui.label_d_address.setText("N")
            elif (type_lieu == 6):
                self.code_adr_d = final_code
                if (final_code_prec != self.code_voie_d):
                    self.ui.label_d_address_error.setStyleSheet("<font color='Red'>!!</font>")
                else:
                    self.ui.label_d_address_error.setStyleSheet("")
                self.ui.label_d_address.setText(final_lib)
        self.geo_changed()
    
    def _slot_comboBox_interview_current_index_changed(self, indexChosenLine):
        self.resGeoModel.clear()
        self.resGoodsModel.clear()
        self.ui.checkBox_tonnage.setChecked(False)
        
        if (indexChosenLine>=0):        
            # Champs généraux
            s = "SELECT enq.type_veh, point.date_enq, n_codif_standard.libelle as per_enq, enq.code_poste, enq.num_point \
                 FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" enq \
                 JOIN "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_POINT_ENQ + SUFFIXE_IMPORT+" point \
                 ON (point.code_poste::character varying = enq.code_poste::character varying AND point.num_point::character varying = enq.num_point::character varying) \
                 JOIN "+self.schema_ref+".n_codif_standard ON enq.per_enq = n_codif_standard.modalite \
                 WHERE n_codif_standard.champ = 'per_enq' AND id_itw::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("id_itw"))+"' \
                   AND enq.code_poste::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("code_poste"))+"' \
                   AND enq.num_point::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("num_point"))+"'"
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
            q.next()
            
            if len(self.vehTypeModel.match(self.vehTypeModel.index(0,1), 0, q.value(0), 1))>0:
                self.ui.comboBox_veh_type.setCurrentIndex(self.vehTypeModel.match(self.vehTypeModel.index(0,1), 0, q.value(0), 1)[0].row())
            d = QDate.fromString(str(q.value(1)), "yyyy-MM-dd")
            self.ui.dateEdit.setDate(d)
            self.ui.label_per_enq_value.setText(q.value(2))
            self.code_poste = str(q.value(3))
            self.num_point = int(q.value(4))
            
            if (self.od_control):
                self.point_enq_layer.setSubsetString("code_poste = '"+str(self.interviewModel.record(indexChosenLine).value("code_poste"))+"' AND num_point::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("num_point"))+"'")
                
                # Colonnes obligatoires
                s = "SELECT enq.pays_dest_ini, enq.pays_dest, enq.zone_dest_ini, enq.zone_dest_prec, enq.zone_dest, enq.commune_dest_ini, enq.commune_dest_prec, enq.commune_dest, \
                            enq.pays_orig_ini, enq.pays_orig, enq.zone_orig_ini, enq.zone_orig_prec, enq.zone_orig, enq.commune_orig_ini, enq.commune_orig_prec, enq.commune_orig, \
                            enq.code_controle_od, enq.immat_pays \
                     FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW+SUFFIXE_IMPORT+" enq \
                     WHERE id_itw::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("id_itw"))+"' \
                       AND enq.code_poste = '"+str(self.interviewModel.record(indexChosenLine).value("code_poste"))+"' \
                       AND enq.num_point::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("num_point"))+"'"  
                q = QtSql.QSqlQuery(self.db)
                q.exec_(unicode(s))
                q.next()
                
                # Destination
                self.ui.label_d_ini_country.setText(q.value(0))
                if len(self.countryModel.match(self.countryModel.index(0,1), 0, q.value(1), 1))>0:
                    self.ui.comboBox_d_country.setCurrentIndex(self.countryModel.match(self.countryModel.index(0,1), 0, q.value(1), 1)[0].row())
                else:
                    self.ui.comboBox_d_country.setCurrentIndex(self.countryModel.match(self.countryModel.index(0,1), 0, 'X', 1)[0].row())
                self.ui.label_d_ini_zone.setText(q.value(2))
                self.ui.lineEdit_d_zone_prec.setText(q.value(3))
                if len(self.zoneModel.match(self.zoneModel.index(0,1), 0, q.value(4), 1))>0:
                    self.ui.comboBox_d_zone.setCurrentIndex(self.zoneModel.match(self.zoneModel.index(0,1), 0, q.value(4), 1)[0].row())
                else:
                    self.ui.comboBox_d_zone.setCurrentIndex(self.zoneModel.match(self.zoneModel.index(0,1), 0, 'X', 1)[0].row())
                self.ui.label_d_ini_municipality.setText(q.value(5))
                self.ui.lineEdit_d_municipality_prec.setText(q.value(6))
                self.update_geo_code(q.value(7), 3, 2)
                
                # Origine
                self.ui.label_o_ini_country.setText(q.value(8))
                if len(self.countryModel.match(self.countryModel.index(0,1), 0, q.value(9), 1))>0:
                    self.ui.comboBox_o_country.setCurrentIndex(self.countryModel.match(self.countryModel.index(0,1), 0, q.value(9), 1)[0].row())
                else:
                    self.ui.comboBox_o_country.setCurrentIndex(self.countryModel.match(self.countryModel.index(0,1), 0, 'X', 1)[0].row())
                self.ui.label_o_ini_zone.setText(q.value(10))
                self.ui.lineEdit_o_zone_prec.setText(q.value(11))
                if len(self.zoneModel.match(self.zoneModel.index(0,1), 0, q.value(12), 1))>0:
                    self.ui.comboBox_o_zone.setCurrentIndex(self.zoneModel.match(self.zoneModel.index(0,1), 0, q.value(12), 1)[0].row())
                else:
                    self.ui.comboBox_o_zone.setCurrentIndex(self.zoneModel.match(self.zoneModel.index(0,1), 0, 'X', 1)[0].row())
                self.ui.label_o_ini_municipality.setText(q.value(13))
                self.ui.lineEdit_o_municipality_prec.setText(q.value(14))
                self.update_geo_code(q.value(15), 3, 1)
                
                if len(self.ODCodeModel.match(self.ODCodeModel.index(0,1), 0, q.value(16), 1))>0:
                    self.ui.comboBox_od_code.setCurrentIndex(self.ODCodeModel.match(self.ODCodeModel.index(0,1), 0, q.value(16), 1)[0].row())
                else:
                    self.ui.comboBox_od_code.setCurrentIndex(self.ODCodeModel.match(self.ODCodeModel.index(0,1), 0, 'X', 1)[0].row())
                
                if len(self.countryModel.match(self.countryModel.index(0,1), 0, q.value(17), 1))>0:
                    self.ui.comboBox_plate_country.setCurrentIndex(self.countryModel.match(self.countryModel.index(0,1), 0, q.value(17), 1)[0].row())
                
                # Colonnes facultatives
                s = "SELECT "+", ".join(self.real_optional_od_columns)+" \
                     FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" \
                     WHERE id_itw::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("id_itw"))+"' \
                       AND code_poste::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("code_poste"))+"' \
                       AND num_point::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("num_point"))+"'"
                q = QtSql.QSqlQuery(self.db)
                q.exec_(unicode(s))
                q.next()
                
                # Réinitialisation des champs facultatifs
                self.ui.label_d_ini_pole.setText("")
                self.ui.lineEdit_d_pole_prec.setText("")
                self.ui.label_d_pole.setText("N")
                
                self.ui.label_o_ini_pole.setText("")
                self.ui.lineEdit_o_pole_prec.setText("")
                self.ui.label_o_pole.setText("N")
                
                self.ui.label_d_ini_street.setText("")
                self.ui.lineEdit_d_street_prec.setText("")
                self.ui.label_d_street.setText("N")
                
                self.ui.label_o_ini_street.setText("")
                self.ui.lineEdit_o_street_prec.setText("")
                self.ui.label_o_street.setText("N")
                
                self.ui.lineEdit_d_street_num_prec.setText("")
                self.ui.label_d_address.setText("N")
                
                self.ui.lineEdit_o_street_num_prec.setText("")
                self.ui.label_o_address.setText("N")
                
                self.ui.label_o_ini_purpose.setText("")
                self.ui.lineEdit_o_purpose_prec.setText("")
                self.ui.comboBox_o_purpose.setCurrentIndex(-1)
                
                self.ui.label_d_ini_purpose.setText("")
                self.ui.lineEdit_d_purpose_prec.setText("")
                self.ui.comboBox_d_purpose.setCurrentIndex(-1)
                
                self.ui.label_o_ini_port.setText("")
                self.ui.lineEdit_o_port_prec.setText("")
                self.ui.comboBox_o_port.setCurrentIndex(-1)
                
                self.ui.label_d_ini_port.setText("")
                self.ui.lineEdit_d_port_prec.setText("")
                self.ui.comboBox_d_port.setCurrentIndex(-1)
                
                self.ui.lineEdit_o_boundary_prec.setText("")
                self.ui.comboBox_o_boundary.setCurrentIndex(-1)
                
                self.ui.lineEdit_d_boundary_prec.setText("")
                self.ui.comboBox_d_boundary.setCurrentIndex(-1)
                
                i=-1
                for col in self.real_optional_od_columns:
                    i=i+1
                    if (col == "pole_dest_ini"):
                        self.ui.label_d_ini_pole.setText(q.value(i))
                    elif (col == "pole_dest_prec"):
                        self.ui.lineEdit_d_pole_prec.setText(q.value(i))
                    elif (col == "pole_dest"):
                        self.update_geo_code(q.value(i), 4, 2)
                        
                    elif (col == "pole_orig_ini"):
                        self.ui.label_o_ini_pole.setText(q.value(i))
                    elif (col == "pole_orig_prec"):
                        self.ui.lineEdit_o_pole_prec.setText(q.value(i))
                    elif (col == "pole_orig"):
                        self.update_geo_code(q.value(i), 4, 1)
                        
                    elif (col == "voie_dest_ini"):
                        self.ui.label_d_ini_street.setText(q.value(i))
                    elif (col == "voie_dest_prec"):
                        self.ui.lineEdit_d_street_prec.setText(q.value(i))
                    elif (col == "voie_dest"):
                        self.update_geo_code(q.value(i), 5, 2)                    
                    
                    elif (col == "voie_orig_ini"):
                        self.ui.label_o_ini_street.setText(q.value(i))
                    elif (col == "voie_orig_prec"):
                        self.ui.lineEdit_o_street_prec.setText(q.value(i))
                    elif (col == "voie_orig"):
                        self.update_geo_code(q.value(i), 5, 1)
                        
                    elif (col == "num_voie_dest_prec"): 
                        self.ui.lineEdit_d_street_num_prec.setText(q.value(i))
                    elif (col == "adresse_dest"):
                        self.update_geo_code(q.value(i), 6, 2) 
                    
                    elif (col == "num_voie_orig_prec"):
                        self.ui.lineEdit_o_street_num_prec.setText(q.value(i))                    
                    elif (col == "adresse_orig"):
                        self.update_geo_code(q.value(i), 6, 1) 
                        
                    elif (col == "motif_orig_ini"):
                        self.ui.label_o_ini_purpose.setText(q.value(i))
                    elif (col == "motif_orig_prec"):
                        self.ui.lineEdit_o_purpose_prec.setText(q.value(i))
                    elif (col == "motif_orig"):
                        if (len(self.purposeModel.match(self.purposeModel.index(0,1), 0, q.value(i), 1))>0):
                            self.ui.comboBox_o_purpose.setCurrentIndex(self.purposeModel.match(self.purposeModel.index(0,1), 0, q.value(i), 1)[0].row())
                        else:
                            self.ui.comboBox_o_purpose.setCurrentIndex(self.purposeModel.match(self.purposeModel.index(0,1), 0, 'X', 1)[0].row())
                    elif (col == "motif_dest_ini"):
                        self.ui.label_d_ini_purpose.setText(q.value(i))
                    elif (col == "motif_dest_prec"):
                        self.ui.lineEdit_d_purpose_prec.setText(q.value(i))
                    elif (col == "motif_dest"):
                        if (len(self.purposeModel.match(self.purposeModel.index(0,1), 0, q.value(i), 1))>0):
                            self.ui.comboBox_d_purpose.setCurrentIndex(self.purposeModel.match(self.purposeModel.index(0,1), 0, q.value(i), 1)[0].row())
                        else:
                            self.ui.comboBox_d_purpose.setCurrentIndex(self.purposeModel.match(self.purposeModel.index(0,1), 0, 'X', 1)[0].row())
                    elif (col == "port_debarq_orig_ini"):
                        self.ui.label_o_ini_port.setText(q.value(i))
                    elif (col == "port_embarq_orig_prec"):
                        self.ui.lineEdit_o_port_prec.setText(q.value(i))
                    elif (col == "port_debarq_orig"):
                        if (len(self.portModel.match(self.portModel.index(0,1), 0, q.value(i), 1))>0):
                            self.ui.comboBox_o_port.setCurrentIndex(self.portModel.match(self.portModel.index(0,1), 0, q.value(i), 1)[0].row())
                        else:
                            self.ui.comboBox_o_port.setCurrentIndex(self.portModel.match(self.portModel.index(0,1), 0, 'X', 1)[0].row())
                    elif (col == "port_embarq_dest_ini"):
                        self.ui.label_d_ini_port.setText(q.value(i))
                    elif (col == "port_embarq_dest_prec"):
                        self.ui.lineEdit_d_port_prec.setText(q.value(i))
                    elif (col == "port_embarq_dest"):
                        if (len(self.portModel.match(self.portModel.index(0,1), 0, q.value(i), 1))>0):
                            self.ui.comboBox_d_port.setCurrentIndex(self.portModel.match(self.portModel.index(0,1), 0, q.value(i), 1)[0].row())
                        else:
                            self.ui.comboBox_d_port.setCurrentIndex(self.portModel.match(self.portModel.index(0,1), 0, 'X', 1)[0].row())
                    elif (col == "frontiere_orig_prec"):
                        self.ui.lineEdit_o_boundary_prec.setText(q.value(i))
                    elif (col == "frontiere_orig"):
                        if (len(self.boundaryModel.match(self.boundaryModel.index(0,1), 0, q.value(i), 1))>0):
                            self.ui.comboBox_o_boundary.setCurrentIndex(self.streetModel.match(self.streetModel.index(0,1), 0, q.value(i), 1)[0].row())
                        else:                        
                            self.ui.comboBox_o_boundary.setCurrentIndex(self.streetModel.match(self.streetModel.index(0,1), 0, 'X', 1)[0].row())
                    elif (col == "frontiere_dest_prec"):
                        self.ui.lineEdit_d_boundary_prec.setText(q.value(i))
                    elif (col == "frontiere_dest"):
                        if (len(self.boundaryModel.match(self.boundaryModel.index(0,1), 0, q.value(i), 1))>0):
                            self.ui.comboBox_d_boundary.setCurrentIndex(self.streetModel.match(self.streetModel.index(0,1), 0, q.value(i), 1)[0].row())
                        else:                        
                            self.ui.comboBox_d_boundary.setCurrentIndex(self.streetModel.match(self.streetModel.index(0,1), 0, 'X', 1)[0].row())
            
            # layers = [self.point_enq_layer, self.o_point_layer, self.d_point_layer]
            # self.caller.zoomToLayersList(layers, True)
            
        if (self.goods_codif):
            # Colonnes obligatoires
            s = "SELECT marchandises_prec, marchandises \
                 FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" \
                 WHERE id_itw::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("id_itw"))+"' \
                   AND code_poste::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("code_poste"))+"' \
                   AND num_point::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("num_point"))+"'"  
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
            q.next()
            
            if len(self.goodsModel.match(self.goodsModel.index(0,1), 0, q.value(1), 1))>0:
                self.ui.comboBox_goods.setCurrentIndex(self.goodsModel.match(self.goodsModel.index(0,1), 0, q.value(1), 1)[0].row())
            else:
                self.ui.comboBox_goods.setCurrentIndex(self.goodsModel.match(self.goodsModel.index(0,1), 0, 'X', 1)[0].row())
            self.ui.lineEdit_goods.setText(str(q.value(0)))
            
            # Colonnes facultatives
            s = "SELECT "+", ".join(self.real_optional_goods_columns)+" \
                 FROM "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" \
                 WHERE id_itw::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("id_itw"))+"' \
                   AND code_poste::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("code_poste"))+"' \
                   AND num_point::character varying = '"+str(self.interviewModel.record(indexChosenLine).value("num_point"))+"'"
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
            q.next()
            
            # Réinitialisation des champs facultatifs
            self.ui.comboBox_plaques_orange.setCurrentIndex(-1)
            self.ui.comboBox_matiere.setCurrentIndex(-1)
            self.ui.checkBox_tonnage.setChecked(False)
            
            i=-1
            for col in self.real_optional_goods_columns:
                i=i+1
                if (col == "plaques_orange"):
                    if len(self.plaquesOrangeModel.match(self.plaquesOrangeModel.index(0,1), 0, q.value(1), 1))>0:
                        self.ui.comboBox_plaques_orange.setCurrentIndex(self.plaquesOrangeModel.match(self.plaquesOrangeModel.index(0,1), 0, q.value(1), 1)[0].row())
                    else:
                        self.ui.comboBox_plaques_orange.setCurrentIndex(self.plaquesOrangeModel.match(self.plaquesOrangeModel.index(0,1), 0, 'X', 1)[0].row())
                if (col == "mat_dangereuse_code_bas"):
                    if len(self.dangerGoodModel.match(self.dangerGoodModel.index(0,1), 0, q.value(1), 1))>0:
                        self.ui.comboBox_matiere.setCurrentIndex(self.dangerGoodModel.match(self.dangerGoodModel.index(0,1), 0, q.value(1), 1)[0].row())
                    else:
                        self.ui.comboBox_matiere.setCurrentIndex(self.dangerGoodModel.match(self.dangerGoodModel.index(0,1), 0, 'I', 1)[0].row())
                elif (col == "tonnage"):
                    if ((str(q.value(i)) != 'N') and (str(q.value(i)) != 'X') and (q.value(i) != None)):
                        self.ui.checkBox_tonnage.setChecked(True)
                        self.ui.doubleSpinBox_tonnage.setValue(float(q.value(i)))
                        self.ui.checkBox_tonnage_non_reponse.setChecked(False)
                    elif (str(q.value(i)) == 'X'):
                        self.ui.checkBox_tonnage.setChecked(True)
                        self.ui.checkBox_tonnage_non_reponse.setChecked(True)
                        self.ui.doubleSpinBox_tonnage.setValue(0)   
                    else:
                        self.ui.checkBox_tonnage.setChecked(False)
                        self.ui.checkBox_tonnage_non_reponse.setChecked(False)
                        self.ui.doubleSpinBox_tonnage.setValue(0)           
            self._slot_pushButton_search_goods_clicked()
                
        # Désactivation du bouton enregistrer
        self.ui.pushButton_save.setEnabled(False)
        
    def _slot_checkBox_tonnage_toggled(self):
        self.sth_changed()  
        if (self.ui.checkBox_tonnage.isChecked()):
            self.ui.doubleSpinBox_tonnage.setEnabled(True)
        else:
            self.ui.doubleSpinBox_tonnage.setEnabled(False)
    
    def _slot_pushButton_prev_clicked(self):
        if (self.ui.comboBox_interview.currentIndex() > 0):
            self.ui.comboBox_interview.setCurrentIndex(self.ui.comboBox_interview.currentIndex()-1)
    
    def _slot_pushButton_next_clicked(self):
        if (self.ui.comboBox_interview.currentIndex() < self.interviewModel.rowCount()):
            self.ui.comboBox_interview.setCurrentIndex(self.ui.comboBox_interview.currentIndex()+1)
    
    def geo_changed(self):
        self.sth_changed()
        
        if (self.ui.comboBox_d_country.currentIndex()>=0):
            self.code_pays_d = self.countryModel.record(self.ui.comboBox_d_country.currentIndex()).value("code_pays")
        else:
            self.code_pays_d = 'N'
        
        if (self.ui.comboBox_d_zone.currentIndex()>=0):
            self.code_zone_d = self.zoneModel.record(self.ui.comboBox_d_zone.currentIndex()).value("code_zone")
        else:
            self.code_zone_d = 'N'
            
        if (self.ui.comboBox_o_country.currentIndex()>=0):
            self.code_pays_o = self.countryModel.record(self.ui.comboBox_o_country.currentIndex()).value("code_pays")
        else:
            self.code_pays_o = 'N'
        
        if (self.ui.comboBox_o_zone.currentIndex()>=0):
            self.code_zone_o = self.zoneModel.record(self.ui.comboBox_o_zone.currentIndex()).value("code_zone")
        else:
            self.code_zone_o = 'N'
            
        # Origines        
        id = None
        if (self.code_adr_o != "N" and self.code_adr_o != "X" and self.code_adr_o != ""):
            id = self.code_adr_o               
            self.o_zone_layer.setSubsetString("type_lieu = 6 and code_adr = '"+id+"'")
            self.o_point_layer.setSubsetString("type_lieu = 6 and code_adr = '"+id+"'")
        if (id == None) and (self.code_voie_o !='N' and self.code_voie_o != 'X' and self.code_voie_o != ""):
            id = self.code_voie_o
            self.o_zone_layer.setSubsetString("type_lieu = 5 and code_voie = '"+id+"'")
            self.o_point_layer.setSubsetString("type_lieu = 5 and code_voie = '"+id+"'")
        if (id == None) and (self.code_pole_o !='N' and self.code_pole_o != 'X' and self.code_pole_o != ""):
            id = self.code_pole_o
            self.o_zone_layer.setSubsetString("type_lieu = 4 and code_pole = '"+id+"'")
            self.o_point_layer.setSubsetString("type_lieu = 4 and code_pole = '"+id+"'")
        if (id == None) and (self.code_com_o !='N' and self.code_com_o != 'X' and self.code_com_o != ""):
            id = self.code_com_o
            self.o_zone_layer.setSubsetString("type_lieu = 3 and code_com = '"+id+"'")
            self.o_point_layer.setSubsetString("type_lieu = 3 and code_com = '"+id+"'")
        if ((id == None) or (id == 'N') or (id == 'X')) and (self.ui.comboBox_o_zone.currentIndex()>=0):
            id = self.zoneModel.record(self.ui.comboBox_o_zone.currentIndex()).value("code_zone")
            self.o_zone_layer.setSubsetString("type_lieu = 2 and code_zone = '"+id+"'")
            self.o_point_layer.setSubsetString("type_lieu = 2 and code_zone = '"+id+"'")
        if ((id == None) or (id == 'N') or (id == 'X')) and (self.ui.comboBox_o_country.currentIndex()>=0):
            id = self.countryModel.record(self.ui.comboBox_o_country.currentIndex()).value("code_pays")
            self.o_zone_layer.setSubsetString("type_lieu = 1 and code_pays = '"+id+"'")
            self.o_point_layer.setSubsetString("type_lieu = 1 and code_pays = '"+id+"'")
        if ((id == None) or (id == 'N') or (id == 'X')):
            self.o_zone_layer.setSubsetString("1=2")
            self.o_point_layer.setSubsetString("1=2")
        
        id=None
        if (self.ui.comboBox_o_port.currentIndex()>=0):
            id = self.portModel.record(self.ui.comboBox_o_port.currentIndex()).value("code_port")        
        if (id!=None) and (id != "N") and (id != "X") and (id != "I"):
            self.o_port_layer.setSubsetString("type_lieu = 7 and code_port = '"+id+"'")
        else:
            self.o_port_layer.setSubsetString("1=2")
        
        id=None
        if (self.ui.comboBox_o_boundary.currentIndex()>=0):
            id = self.boundaryModel.record(self.ui.comboBox_o_boundary.currentIndex()).value("code_front")        
        if (id!=None) and (id != "N") and (id != "X") and (id != "I"):
            self.o_boundary_layer.setSubsetString("type_lieu = 8 and code_front = '"+id+"'")
        else:
            self.o_boundary_layer.setSubsetString("1=2")
            
        # Destinations       
        if (self.code_adr_d != "N") and (self.code_adr_d != "X" and self.code_adr_d != ""):
            id = self.code_adr_d               
            self.d_zone_layer.setSubsetString("type_lieu = 6 and code_adr = '"+id+"'")
            self.d_point_layer.setSubsetString("type_lieu = 6 and code_adr = '"+id+"'")   
        if (id == None) and (self.code_voie_d !='N') and (self.code_voie_d != 'X') and (self.code_adr_d != ""):
            id = self.code_voie_d
            self.d_zone_layer.setSubsetString("type_lieu = 5 and code_voie = '"+id+"'")
            self.d_point_layer.setSubsetString("type_lieu = 5 and code_voie = '"+id+"'")
        if (id == None) and (self.code_pole_d !='N') and (self.code_pole_d != 'X') and (self.code_pole_d != ""):
            id = self.code_pole_d
            self.d_zone_layer.setSubsetString("type_lieu = 4 and code_pole = '"+id+"'")
            self.d_point_layer.setSubsetString("type_lieu = 4 and code_pole = '"+id+"'")
        if (id == None) and (self.code_com_d !='N') and (self.code_com_d != 'X') and (self.code_com_d != ""):
            id = self.code_com_d
            self.d_zone_layer.setSubsetString("type_lieu = 3 and code_com = '"+id+"'")
            self.d_point_layer.setSubsetString("type_lieu = 3 and code_com = '"+id+"'")
        if ((id == None) or (id == 'N') or (id == 'X')) and (self.ui.comboBox_d_zone.currentIndex()>=0):
            id = self.zoneModel.record(self.ui.comboBox_d_zone.currentIndex()).value("code_zone")
            self.d_zone_layer.setSubsetString("type_lieu = 2 and code_zone = '"+id+"'")
            self.d_point_layer.setSubsetString("type_lieu = 2 and code_zone = '"+id+"'")
        if ((id == None) or (id == 'N') or (id == 'X')) and (self.ui.comboBox_d_country.currentIndex()>=0):
            id = self.countryModel.record(self.ui.comboBox_d_country.currentIndex()).value("code_pays")
            self.d_zone_layer.setSubsetString("type_lieu = 1 and code_pays = '"+id+"'")
            self.d_point_layer.setSubsetString("type_lieu = 1 and code_pays = '"+id+"'")
        if ((id == None) or (id == 'N') or (id == 'X')):
            self.d_zone_layer.setSubsetString("1=2")
            self.d_point_layer.setSubsetString("1=2")
        
        id=None
        if (self.ui.comboBox_d_port.currentIndex()>=0):
            id = self.portModel.record(self.ui.comboBox_d_port.currentIndex()).value("code_port")        
        if (id!=None) and (id != "N") and (id != "X") and (id != "I"):
            self.d_port_layer.setSubsetString("type_lieu = 7 and code_port = '"+id+"'")
        else:
            self.d_port_layer.setSubsetString("1=2")
        
        id=None
        if (self.ui.comboBox_d_boundary.currentIndex()>=0):
            id = self.boundaryModel.record(self.ui.comboBox_d_boundary.currentIndex()).value("code_front")        
        if (id!=None) and (id != "N") and (id != "X") and (id != "I"):
            self.d_boundary_layer.setSubsetString("type_lieu = 8 and code_front = '"+id+"'")
        else:
            self.d_boundary_layer.setSubsetString("1=2")
    
    def search_orig(self, searched_text, nb_sol, type_lieu):
        self.ui.pushButton_choose_o.setEnabled(True)
        if (type_lieu == 2 or type_lieu == 9):
            self.search_zone(self.code_pays_o, searched_text, nb_sol)
        elif (type_lieu == 3):
            self.search_municipality(self.code_pays_o, self.code_zone_o, searched_text, nb_sol)
        elif (type_lieu == 4):
            self.search_pole(self.code_pays_o, self.code_zone_o, self.code_com_o, searched_text, nb_sol)
        elif (type_lieu == 5):
            self.search_street(self.code_pays_o, self.code_zone_o, self.code_com_o, searched_text, nb_sol)
        elif (type_lieu == 6):
            self.search_address(self.code_pays_o, self.code_zone_o, self.code_com_o, self.code_voie_o, searched_text, nb_sol)
        elif (type_lieu == 7):
            self.search_port(self.code_pays_o, searched_text, nb_sol)
        elif (type_lieu == 8):
            self.search_boundary(self.code_pays_o, searched_text, nb_sol)
    
    def search_dest(self, searched_text, nb_sol, type_lieu):
        self.ui.pushButton_choose_d.setEnabled(True)
        if (type_lieu == 2 or type_lieu == 9):
            self.search_zone(self.code_pays_d, searched_text, nb_sol)
        elif (type_lieu == 3):
            self.search_municipality(self.code_pays_d, self.code_zone_d, searched_text, nb_sol)
        elif (type_lieu == 4):
            self.search_pole(self.code_pays_d, self.code_zone_d, self.code_com_d, searched_text, nb_sol)
        elif (type_lieu == 5):
            self.search_street(self.code_pays_d, self.code_zone_d, self.code_com_d, searched_text, nb_sol)
        elif (type_lieu == 6):
            self.search_address(self.code_pays_d, self.code_zone_d, self.code_com_d, self.code_voie_d, searched_text, nb_sol)
        elif (type_lieu == 7):
            self.search_port(self.code_pays_d, searched_text, nb_sol)
        elif (type_lieu == 8):
            self.search_boundary(self.code_pays_d, searched_text, nb_sol)
    
    def search_zone(self, code_pays, searched_text, nb_sol):
        if (self.ui.checkBox_code.isChecked()):
            compar = "t.code_zone || ' ' || t.libelle"
        else:
            compar = "t.libelle"
        s = "SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, t.libelle \
             FROM "+self.schema_ref+"."+self.table_geo+" t LEFT JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                           LEFT JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
             WHERE pays.type_lieu = 1 AND pays.code_pays = '"+ code_pays +"' AND zone.type_lieu = 2 AND (t.type_lieu = 2 OR t.type_lieu = 9) AND t.libelle is not null \
             ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
             LIMIT "+str(nb_sol)
        self.resGeoModel.setQuery(s, self.db)        
        self.ui.tableView_geo.setModel(self.resGeoModel)
        header_names = ["Pays", "Code pays", "Zone", "Code zone", u"Libellé"]
        for i in range(len(header_names)):
            self.resGeoModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_geo.verticalHeader().setDefaultSectionSize(16)
        self.ui.tableView_geo.resizeColumnsToContents()
        self.ui.tableView_geo.setColumnHidden(1, True)
        self.ui.tableView_geo.setColumnHidden(3, True)
        self.ui.tableView_geo.selectRow(0)        
    
    def search_municipality(self, code_pays, code_zone, searched_text, nb_sol):
        if (self.ui.checkBox_code.isChecked()):
            compar = "t.code_com || ' ' || t.libelle"
        else:
            compar = "t.libelle"
        
        if (self.ui.radioButton_princ.isChecked()):
            s = "SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, t.libelle as com, t.code_com \
                 FROM "+self.schema_ref+"."+self.table_geo+" t LEFT JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                               LEFT JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
                 WHERE t.type_lieu = 3 AND pays.type_lieu = 1 AND zone.type_lieu = 2 AND zone.code_zone = '"+code_zone+"' AND t.libelle is not null\
                 ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
                 LIMIT "+str(nb_sol)
        
        elif (self.ui.radioButton_lim.isChecked()):
            s="SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, t.libelle as com, t.code_com \
               FROM "+self.schema_ref+"."+self.table_geo+" t LEFT JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                             LEFT JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
                                                             LEFT JOIN "+self.schema_ref+"."+self.table_geo+" zone_princ ON (st_touches(zone_princ.geom_polygon, zone.geom_polygon)) \
               WHERE t.type_lieu = 3 AND zone_princ.type_lieu = 2 AND zone_princ.code_zone = '"+code_zone+"' AND pays.type_lieu = 1 AND zone.type_lieu = 2 AND t.libelle is not null\
               ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
               LIMIT "+str(nb_sol)
        self.resGeoModel.setQuery(s, self.db)        
        self.ui.tableView_geo.setModel(self.resGeoModel)
        header_names = ["Pays", "Code pays", "Zone", "Code zone", "Commune", "Code commune"]
        for i in range(len(header_names)):
            self.resGeoModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_geo.verticalHeader().setDefaultSectionSize(16)
        self.ui.tableView_geo.resizeColumnsToContents()
        self.ui.tableView_geo.setColumnHidden(1, True)
        self.ui.tableView_geo.setColumnHidden(3, True)
        self.ui.tableView_geo.setColumnHidden(5, True)
        self.ui.tableView_geo.selectRow(0)
    
    def search_pole(self, code_pays, code_zone, code_com, searched_text, nb_sol):
        if (self.ui.checkBox_code.isChecked()):
            compar = "t.code_pole || ' ' || t.libelle"
        else:
            compar = "t.libelle"
        
        if (self.ui.radioButton_princ.isChecked()):
            s = "SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, com.libelle as com, com.code_com, t.libelle as pole, t.code_pole \
                 FROM "+self.schema_ref+"."+self.table_geo+" t \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" com ON (com.code_com = t.code_com) \
                 WHERE t.type_lieu = 4 AND com.code_com = '"+code_com+"' AND com.type_lieu = 3 AND zone.type_lieu = 2 AND pays.type_lieu = 1 AND t.libelle is not null \
                 ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
                 LIMIT "+str(nb_sol)
        elif (self.ui.radioButton_lim.isChecked()):
            s = "SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, com.libelle as com, com.code_com, t.libelle as pole, t.code_pole \
                   FROM "+self.schema_ref+"."+self.table_geo+" t \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" com ON (com.code_com = t.code_com) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" com_princ ON (st_touches(com_princ.geom_polygon, com.geom_polygon)) \
                   WHERE t.type_lieu = 4 AND com_princ.type_lieu = 3 AND com_princ.code_com = '"+code_com+"' AND com.type_lieu = 3 AND zone.type_lieu = 2 AND pays.type_lieu = 1 AND t.libelle is not null  \
                   ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
                   LIMIT "+str(nb_sol)
        self.resGeoModel.setQuery(s, self.db)        
        self.ui.tableView_geo.setModel(self.resGeoModel)
        header_names = ["Pays", "Code pays", "Zone", "Code zone", "Commune", "Code commune", "Pôle", "Code pôle"]
        for i in range(len(header_names)):
            self.resGeoModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_geo.verticalHeader().setDefaultSectionSize(16)
        self.ui.tableView_geo.resizeColumnsToContents()
        self.ui.tableView_geo.setColumnHidden(1, True)
        self.ui.tableView_geo.setColumnHidden(3, True)
        self.ui.tableView_geo.setColumnHidden(5, True)
        self.ui.tableView_geo.setColumnHidden(7, True)
        self.ui.tableView_geo.selectRow(0)
    
    def search_street(self, code_pays, code_zone, code_com, searched_text, nb_sol):
        if (self.ui.checkBox_code.isChecked()):
            compar = "t.code_voie || ' ' || t.libelle"
        else:
            compar = "t.libelle"
        
        if (self.ui.radioButton_lim.isChecked()):
            s = "SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, com.libelle as com, com.code_com, t.libelle as voie, t.code_voie \
                   FROM "+self.schema_ref+"."+self.table_geo+" t \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" com ON (com.code_com = t.code_com) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" com_princ ON (st_touches(com_princ.geom_polygon, com.geom_polygon)) \
                   WHERE t.type_lieu = 5 AND com_princ.type_lieu = 3 AND com_princ.code_com = '"+code_com+"' AND com.type_lieu = 3 AND zone.type_lieu = 2 AND pays.type_lieu = 1 AND t.libelle is not null  \
                   ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
                   LIMIT "+str(nb_sol)
        elif (self.ui.radioButton_princ.isChecked()):
            s = "SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, com.libelle as com, com.code_com, t.libelle as voie, t.code_voie \
                 FROM "+self.schema_ref+"."+self.table_geo+" t \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
                                                        JOIN "+self.schema_ref+"."+self.table_geo+" com ON (com.code_com = t.code_com) \
                 WHERE t.type_lieu = 5 AND com.code_com = '"+code_com+"' AND com.type_lieu = 3 AND zone.type_lieu = 2 AND pays.type_lieu = 1 AND t.libelle is not null \
                 ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
                 LIMIT "+str(nb_sol)
        self.resGeoModel.setQuery(s, self.db)        
        self.ui.tableView_geo.setModel(self.resGeoModel)
        header_names = ["Pays", "Code pays", "Zone", "Code zone", "Commune", "Code commune", "Voie", "Code voie"]
        for i in range(len(header_names)):
            self.resGeoModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_geo.verticalHeader().setDefaultSectionSize(16)
        self.ui.tableView_geo.resizeColumnsToContents()
        self.ui.tableView_geo.setColumnHidden(1, True)
        self.ui.tableView_geo.setColumnHidden(3, True)
        self.ui.tableView_geo.setColumnHidden(5, True)
        self.ui.tableView_geo.setColumnHidden(7, True)
        self.ui.tableView_geo.selectRow(0)
           
    def search_address(self, code_pays, code_zone, code_com, code_voie, searched_text, nb_sol):
        if (self.ui.checkBox_code.isChecked()):
            compar = "t.code_adr || ' ' || t.libelle"
        else:
            compar = "t.libelle"
        
        s="SELECT pays, code_pays, zone, code_zone, com, code_com, voie, code_voie, adr, code_adr FROM (\
               (\
               SELECT pays.libelle as pays, pays.code_pays, zone.libelle as zone, zone.code_zone, com.libelle as com, com.code_com, \
                            voie.libelle as voie, voie.code_voie, t.libelle as adr, t.code_adr, similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') as sim, 1 as ordre \
               FROM "+self.schema_ref+"."+self.table_geo+" t \
                                                            JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
                                                            JOIN "+self.schema_ref+"."+self.table_geo+" zone ON (zone.code_zone = t.code_zone) \
                                                            JOIN "+self.schema_ref+"."+self.table_geo+" com ON (com.code_com = t.code_com) \
                                                            JOIN "+self.schema_ref+"."+self.table_geo+" voie ON (voie.code_voie = t.code_voie)\
               WHERE t.libelle is not null AND t.code_com = '"+code_com+"' AND t.code_voie = '"+code_voie+"' AND t.type_lieu = 6 \
                 AND pays.type_lieu = 1 AND zone.type_lieu = 2 AND com.type_lieu = 3 AND voie.type_lieu = 5 \
               ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
               LIMIT "+str(nb_sol)+" \
               ) \
           ) q \
           ORDER BY ordre, sim DESC"
        self.resGeoModel.setQuery(s, self.db)        
        self.ui.tableView_geo.setModel(self.resGeoModel)
        header_names = ["Pays", "Code pays", "Zone", "Code zone", "Commune", "Code commune", "Voie", "Code voie", "Adresse", "Code adresse"]
        for i in range(len(header_names)):
            self.resGeoModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_geo.verticalHeader().setDefaultSectionSize(16)
        self.ui.tableView_geo.resizeColumnsToContents()
        self.ui.tableView_geo.setColumnHidden(1, True)
        self.ui.tableView_geo.setColumnHidden(3, True)
        self.ui.tableView_geo.setColumnHidden(5, True)
        self.ui.tableView_geo.setColumnHidden(7, True)
        self.ui.tableView_geo.setColumnHidden(9, True)
        self.ui.tableView_geo.selectRow(0)
          
    def search_port(self, code_pays, searched_text, nb_sol):
        if (self.ui.checkBox_code.isChecked()):
            compar = "t.code_port || ' ' || t.libelle"
        else:
            compar = "t.libelle"
        
        s="SELECT pays.libelle as pays, pays.code_pays, t.libelle as port, t.code_port \
           FROM "+self.schema_ref+"."+self.table_geo+" t LEFT JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
           WHERE t.libelle is not null AND t.code_pays ='"+code_pays+"' AND t.type_lieu = 7 AND pays.type_lieu = 1 \
           ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
           LIMIT "+str(nb_sol)
        self.resGeoModel.setQuery(s, self.db)        
        self.ui.tableView_geo.setModel(self.resGeoModel)
        header_names = ["Pays", "Code pays", "Port", "Code port"]
        for i in range(len(header_names)):
            self.resGeoModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_geo.verticalHeader().setDefaultSectionSize(16)
        self.ui.tableView_geo.resizeColumnsToContents()
        self.ui.tableView_geo.setColumnHidden(1, True)
        self.ui.tableView_geo.setColumnHidden(3, True)
        self.ui.tableView_geo.selectRow(0)
        
    def search_boundary(self, code_pays, searched_text, nb_sol):
        if (self.ui.checkBox_code.isChecked()):
            compar = "t.code_front || ' ' || t.libelle"
        else:
            compar = "t.libelle"
        
        s="SELECT id, pays.libelle as pays, pays.code_pays, t.libelle as front, t.code_front \
           FROM "+self.schema_ref+"."+self.table_geo+" t LEFT JOIN "+self.schema_ref+"."+self.table_geo+" pays ON (pays.code_pays = t.code_pays) \
           WHERE t.libelle is not null AND t.code_pays ='"+code_pays+"' AND t.type_lieu = 8 AND pays.type_lieu = 1 \
           ORDER BY similarity("+compar+", '"+ unicode(searched_text.replace("'", "''")) +"') DESC \
           LIMIT "+str(nb_sol) 
        self.resGeoModel.setQuery(s, self.db)        
        self.ui.tableView_geo.setModel(self.resGeoModel)
        header_names = ["Pays", "Code pays", "Frontière", "Code frontière"]
        for i in range(len(header_names)):
            self.resGeoModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_geo.verticalHeader().setDefaultSectionSize(16)
        self.ui.tableView_geo.resizeColumnsToContents()
        self.ui.tableView_geo.setColumnHidden(1, True)
        self.ui.tableView_geo.setColumnHidden(3, True)
        self.ui.tableView_geo.selectRow(0)
    
    def _slot_pushButton_search_o_zone_clicked(self):
        searched_text = self.ui.lineEdit_o_zone_prec.text()
        self.type_lieu = 2
        self.search_orig(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_d_zone_clicked(self):
        searched_text = self.ui.lineEdit_d_zone_prec.text()
        self.type_lieu = 2
        self.search_dest(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_o_municipality_clicked(self):
        searched_text = self.ui.lineEdit_o_municipality_prec.text()
        self.type_lieu = 3
        self.search_orig(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_d_municipality_clicked(self):
        searched_text = self.ui.lineEdit_d_municipality_prec.text()
        self.type_lieu = 3
        self.search_dest(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_o_pole_clicked(self):
        searched_text = self.ui.lineEdit_o_pole_prec.text()
        self.type_lieu = 4
        self.search_orig(searched_text, self.ui.spinBox.value(), self.type_lieu)
            
    def _slot_pushButton_search_d_pole_clicked(self):
        searched_text = self.ui.lineEdit_d_pole_prec.text()
        self.type_lieu = 4
        self.search_dest(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_o_street_clicked(self):
        searched_text = self.ui.lineEdit_o_street_prec.text()
        self.type_lieu = 5
        self.search_orig(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_d_street_clicked(self):
        searched_text = self.ui.lineEdit_d_street_prec.text()
        self.type_lieu = 5
        self.search_dest(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_o_address_clicked(self):
        searched_text = self.ui.lineEdit_o_street_num_prec.text()
        self.type_lieu = 6
        self.search_orig(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_d_address_clicked(self):
        searched_text = self.ui.lineEdit_d_street_num_prec.text()
        self.type_lieu = 6
        self.search_dest(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_o_port_clicked(self):
        searched_text = self.ui.lineEdit_o_port_prec.text()
        self.type_lieu = 7
        self.search_orig(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_d_port_clicked(self): 
        searched_text = self.ui.lineEdit_d_port_prec.text()
        self.type_lieu = 7
        self.search_dest(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_o_boundary_clicked(self):
        searched_text = self.ui.lineEdit_o_street_prec.text()
        self.type_lieu = 8
        self.search_orig(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_d_boundary_clicked(self):
        searched_text = self.ui.lineEdit_d_street_prec.text()
        self.type_lieu = 8
        self.search_dest(searched_text, self.ui.spinBox.value(), self.type_lieu)
    
    def _slot_pushButton_search_goods_clicked(self):
        searched_text = self.ui.lineEdit_goods.text()
        s="SELECT libelle_march, modalite, libelle_modalite \
           FROM (\
                SELECT DISTINCT ON (n_codif_standard.modalite) m.libelle as libelle_march, n_codif_standard.modalite, n_codif_standard.libelle as libelle_modalite, similarity(m.libelle, '"+ unicode(searched_text.replace("'", "''")) +"') \
                FROM "+self.schema_ref+".n_corresp_marchandises m \
                JOIN "+self.schema_ref+".n_codif_standard ON (n_codif_standard.modalite = m.modalite)\
                WHERE n_codif_standard.champ = 'marchandises'\
                ORDER BY n_codif_standard.modalite, similarity(m.libelle, '"+ unicode(searched_text.replace("'", "''")) +"') DESC\
           ) q\
           ORDER BY similarity DESC\
           LIMIT "+str(self.ui.spinBox.value())
        self.resGoodsModel.setQuery(s, self.db)
        self.ui.tableView_goods.setModel(self.resGoodsModel)
        self.ui.tableView_goods.verticalHeader().setDefaultSectionSize(16)
        header_names = ["Marchandises", "Code NST", u"Libellé NST"]
        for i in range(len(header_names)):
            self.resGoodsModel.setHeaderData(i, Qt.Horizontal, header_names[i])
        self.ui.tableView_goods.resizeColumnsToContents()
        self.ui.tableView_goods.selectRow(0)
    
    def _slot_pushButton_choose_o(self):
        if (self.ui.tableView_geo.selectionModel().hasSelection()):
            item = self.ui.tableView_geo.selectionModel().selectedRows()[0]
            
            code_pays = self.resGeoModel.record(item.row()).value("code_pays")            
            self.ui.comboBox_o_country.setCurrentIndex(self.countryModel.match(self.countryModel.index(0,1), 0, code_pays, 1)[0].row())
            
            if (self.type_lieu in [2,3,4,5,6,9]):
                code_zone = self.resGeoModel.record(item.row()).value("code_zone")
                self.ui.comboBox_o_zone.setCurrentIndex(self.zoneModel.match(self.zoneModel.index(0,1), 0, code_zone, 1)[0].row())
            if (self.type_lieu in [3,4,5,6]):
                code_com = self.resGeoModel.record(item.row()).value("code_com")
                self.update_geo_code(code_com, 3, 1)
            if (self.type_lieu == 4):
                code_pole = self.resGeoModel.record(item.row()).value("code_pole")
                self.update_geo_code(code_pole, 4, 1)
            if (self.type_lieu in [5,6]):
                code_voie = self.resGeoModel.record(item.row()).value("code_voie")
                self.update_geo_code(code_voie, 5, 1)
            if (self.type_lieu == 6):
                code_adr = self.resGeoModel.record(item.row()).value("code_adr")
                self.update_geo_code(code_adr, 6, 1)
            if (self.type_lieu == 7):
                code_port = self.resGeoModel.record(item.row()).value("code_port")
                self.ui.comboBox_o_port.setCurrentIndex(self.portModel.match(self.portModel.index(0,1), 0, code_port, 1)[0].row())            
            if (self.type_lieu == 8):
                code_front = self.resGeoModel.record(item.row()).value("code_front")
                self.ui.comboBox_o_boundary.setCurrentIndex(self.boundaryModel.match(self.boundaryModel.index(0,1), 0, code_front, 1)[0].row())
    
    def _slot_pushButton_choose_d(self):
        if (self.ui.tableView_geo.selectionModel().hasSelection()):
            item = self.ui.tableView_geo.selectionModel().selectedRows()[0]
            
            code_pays = self.resGeoModel.record(item.row()).value("code_pays")            
            self.ui.comboBox_d_country.setCurrentIndex(self.countryModel.match(self.countryModel.index(0,1), 0, code_pays, 1)[0].row())
            
            if (self.type_lieu in [2,3,4,5,6]):
                code_zone = self.resGeoModel.record(item.row()).value("code_zone")
                self.ui.comboBox_d_zone.setCurrentIndex(self.zoneModel.match(self.zoneModel.index(0,1), 0, code_zone, 1)[0].row())
            else:
                self.ui.comboBox_d_zone.setCurrentIndex(self.zoneModel.match(self.zoneModel.index(0,1), 0, 'N', 1)[0].row())
            if (self.type_lieu in [3,4,5,6]):
                code_com = self.resGeoModel.record(item.row()).value("code_com")
                self.update_geo_code(code_com, 3, 2)
            else:
                self.update_geo_code('N', 3, 1)
            if (self.type_lieu == 4):
                code_pole = self.resGeoModel.record(item.row()).value("code_pole")
                self.update_geo_code(code_pole, 4, 2)
            else:
                self.update_geo_code('N', 4, 1)
            if (self.type_lieu in [5,6]):
                code_voie = self.resGeoModel.record(item.row()).value("code_voie")
                self.update_geo_code(code_voie, 5, 2)
            else:
                self.update_geo_code('N', 5, 1)
            if (self.type_lieu == 6):
                code_adr = self.resGeoModel.record(item.row()).value("code_adr")
                self.update_geo_code(code_adr, 6, 2)
            else:
                self.update_geo_code('N', 6, 1)
            if (self.type_lieu == 7):
                code_port = self.resGeoModel.record(item.row()).value("code_port")
                self.ui.comboBox_d_port.setCurrentIndex(self.portModel.match(self.portModel.index(0,1), 0, code_port, 1)[0].row())
            
            if (self.type_lieu == 8):
                code_front = self.resGeoModel.record(item.row()).value("code_front")
                self.ui.comboBox_d_boundary.setCurrentIndex(self.boundaryModel.match(self.boundaryModel.index(0,1), 0, code_front, 1)[0].row())
    
    def _slot_pushButton_choose_goods_clicked(self):
        if (self.ui.tableView_goods.selectionModel().hasSelection()):
            item = self.ui.tableView_goods.selectionModel().selectedRows()[0]
            code_march = self.resGoodsModel.record(item.row()).value("modalite")
            self.ui.comboBox_goods.setCurrentIndex(self.goodsModel.match(self.goodsModel.index(0,1), 0, code_march, 1)[0].row())
    
    def _slot_pushButton_save_goods_asso_clicked(self):
        s = "INSERT INTO "+self.schema_ref+".n_corresp_marchandises(libelle, modalite) \
             VALUES('"+self.ui.lineEdit_goods.text()+"', '"+self.goodsModel.record(self.ui.comboBox_goods.currentIndex()).value("modalite")+"')"
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        self._slot_pushButton_search_goods_clicked()
    
    def _slot_pushButton_delete_goods_asso_clicked(self):
        if (self.ui.tableView_goods.selectionModel().hasSelection()):
            item = self.ui.tableView_goods.selectionModel().selectedRows()[0]
            code_march = self.resGoodsModel.record(item.row()).value("modalite")
            march = self.resGoodsModel.record(item.row()).value("libelle_march")
        s = "DELETE FROM "+self.ui.comboBox_schema_ref.currentText()+".n_corresp_marchandises \
             WHERE libelle = '"+str(march)+"' AND modalite = '"+str(code_march)+"'"
        q = QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        self._slot_pushButton_search_goods_clicked()
    
    def _slot_pushButton_invert_od_clicked(self):
        o_country_index = self.ui.comboBox_o_country.currentIndex()
        self.ui.comboBox_o_country.setCurrentIndex(self.ui.comboBox_d_country.currentIndex())
        self.ui.comboBox_d_country.setCurrentIndex(o_country_index)
        
        o_zone_index = self.ui.comboBox_o_zone.currentIndex()
        self.ui.comboBox_o_zone.setCurrentIndex(self.ui.comboBox_d_zone.currentIndex())
        self.ui.comboBox_d_zone.setCurrentIndex(o_zone_index)
        
        com = self.code_com_d
        self.update_geo_code(self.code_com_o, 3, 2)
        self.update_geo_code(com, 3, 1)
        
        pole = self.code_pole_d
        self.update_geo_code(self.code_pole_o, 4, 2)
        self.update_geo_code(pole, 4, 1)
        
        voie = self.code_voie_d
        self.update_geo_code(self.code_voie_o, 5, 2)
        self.update_geo_code(voie, 5, 1)
        
        adr = self.code_adr_d
        self.update_geo_code(self.code_adr_o, 6, 2)
        self.update_geo_code(adr, 6, 1)
        
    def _slot_pushButton_invert_od_purposes_clicked(self):
        o_purpose_index = self.ui.comboBox_o_purpose.currentIndex()
        self.ui.comboBox_o_purpose.setCurrentIndex(self.ui.comboBox_d_purpose.currentIndex())
        self.ui.comboBox_d_purpose.setCurrentIndex(o_purpose_index)
    
    def _slot_pushButton_invert_od_ports_clicked(self):
        o_port_index = self.ui.comboBox_o_port.currentIndex()
        self.ui.comboBox_o_port.setCurrentIndex(self.ui.comboBox_d_port.currentIndex())
        self.ui.comboBox_d_port.setCurrentIndex(o_port_index)
    
    def _slot_pushButton_invert_od_boundaries_clicked(self):
        o_boundary_index = self.ui.comboBox_o_boundary.currentIndex()
        self.ui.comboBox_o_boundary.setCurrentIndex(self.ui.comboBox_d_boundary.currentIndex())
        self.ui.comboBox_d_boundary.setCurrentIndex(o_boundary_index)
    
    def _slot_pushButton_save_clicked(self):
        self.ui.pushButton_save.setEnabled(False)
        s = ""
        if (self.od_control):        
            for col in self.real_optional_od_columns:
                if (col == "pole_dest"):
                    s = s + "pole_dest = '"+ self.code_pole_d +"', "
                elif (col == "pole_orig"):
                    s = s + "pole_orig = '"+ self.code_pole_o +"', "
                elif (col == "voie_dest"):
                    s = s + "voie_dest = '"+ self.code_voie_d +"', "
                elif (col == "voie_orig"):
                    s = s + "voie_orig = '"+ self.code_voie_o +"', "
                elif (col == "adresse_dest"):
                    s = s + "adresse_dest = '"+ self.code_adr_d +"', "
                elif (col == "adresse_orig"):
                    s = s + "adresse_orig = '"+ self.code_adr_o +"', "
                elif (col == "motif_dest"):
                    s = s + "motif_dest = '"+ self.purposeModel.record(self.ui.comboBox_d_purpose.currentIndex()).value("modalite") +"', "
                elif (col == "motif_orig"):
                   s = s + "motif_orig = '"+ self.purposeModel.record(self.ui.comboBox_o_purpose.currentIndex()).value("modalite") +"', "
                elif (col == "port_embarq_dest"):
                    s = s + "port_embarq_dest = '"+ self.portModel.record(self.ui.comboBox_d_port.currentIndex()).value("code_port") +"', "
                elif (col == "port_debarq_orig"):
                    s = s + "port_debarq_orig = '"+ self.portModel.record(self.ui.comboBox_o_port.currentIndex()).value("code_port") +"', "
                elif (col == "frontiere_dest"):
                    s = s + "frontiere_dest = '"+ self.boundaryModel.record(self.ui.comboBox_d_boundary.currentIndex()).value("code_front") +"', "
                elif (col == "frontiere_orig"):
                    s = s + "frontiere_orig = '"+ self.boundaryModel.record(self.ui.comboBox_o_boundary.currentIndex()).value("code_front") +"', "
                
                    
            s = "UPDATE "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" \
                 SET immat_pays = '"+ self.countryModel.record(self.ui.comboBox_plate_country.currentIndex()).value("code_pays") +"', \
                     type_veh = '"+ self.vehTypeModel.record(self.ui.comboBox_veh_type.currentIndex()).value("modalite") +"', \
                     pays_orig = '"+ self.countryModel.record(self.ui.comboBox_o_country.currentIndex()).value("code_pays") +"', \
                     pays_dest = '"+ self.countryModel.record(self.ui.comboBox_d_country.currentIndex()).value("code_pays") +"', \
                     zone_orig = '"+ self.zoneModel.record(self.ui.comboBox_o_zone.currentIndex()).value("code_zone") +"', \
                     zone_dest = '"+ self.zoneModel.record(self.ui.comboBox_d_zone.currentIndex()).value("code_zone") +"', \
                     commune_orig = '"+ self.code_com_o +"', \
                     commune_dest = '"+ self.code_com_d +"', "+s+"\
                     code_controle_od = '"+ self.ODCodeModel.record(self.ui.comboBox_od_code.currentIndex()).value("modalite") +"'\
                 WHERE id_itw::character varying = '"+str(self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("id_itw"))+ "' AND code_poste::character varying = '"+self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("code_poste") + "' AND num_point::character varying = '"+str(self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("num_point"))+"'"
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
            
            s = "UPDATE "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" \
                 SET code_controle_od = '"+ self.ODCodeModel.record(self.ui.comboBox_od_code.currentIndex()).value("modalite") +"'\
                 WHERE commune_orig = '"+ self.code_com_o +"' AND commune_dest = '"+ self.code_com_d +"' AND code_poste = '"+ self.code_poste +"' AND num_point = "+str(self.num_point)+" AND commune_orig != commune_dest"
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
        
        s=""
        if (self.goods_codif): 
            for col in self.real_optional_goods_columns:
                if (col == "tonnage"):
                        if (self.ui.checkBox_tonnage.isChecked()) and (self.ui.checkBox_tonnage_non_reponse.isChecked() == False):
                            s = s + "tonnage = '"+ str(self.ui.doubleSpinBox_tonnage.value()) +"', "
                        elif (self.ui.checkBox_tonnage.isChecked()):
                            s = s + "tonnage = 'X', "
                        else:
                            s = s + "tonnage = 'N', "
                if (col == "plaques_orange"):
                    s = s +"plaques_orange = '"+ self.plaquesOrangeModel.record(self.ui.comboBox_plaques_orange.currentIndex()).value("modalite") +"', "
            s = "UPDATE "+self.ui.comboBox_schema_enq.currentText()+"."+TEMP_TABLE_ITW + SUFFIXE_IMPORT+" \
                 SET "+s+" marchandises = '"+ self.goodsModel.record(self.ui.comboBox_goods.currentIndex()).value("modalite") +"' \
                 WHERE id_itw::character varying = '"+str(self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("id_itw"))+ "' \
                 AND code_poste::character varying = '"+self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("code_poste") + "' AND num_point::character varying = '"+str(self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("num_point"))+"'"
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
    
    def _slot_pushButton_open_form_clicked(self):
        for feature in self.interview_layer.getFeatures(): 
            if ((feature['code_poste'] == self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("code_poste")) and \
                (feature['num_point'] == self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("num_point")) and \
                (feature['id_itw'] == self.interviewModel.record(self.ui.comboBox_interview.currentIndex()).value("id_itw"))):
                self.iface.openFeatureForm(self.interview_layer, feature, False)
                break
    
    def _slot_pushButton_reset_clicked(self):
        currentIndex = self.ui.comboBox_interview.currentIndex()
        self._slot_comboBox_interview_current_index_changed(currentIndex)
    
    def _slot_close(self):
        self.hide()
        self.interviews_check_node = self.caller.node_group.findGroup("Contrôle manuel des interviews")
        self.caller.node_group.removeChildNode(self.interviews_check_node)
    