#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface
from processing.tools import postgis

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os
import processing

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from compare_matrices_dialog import Ui_Dialog

class compare_matrices_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface 
        self.plugin_dir = self.caller.plugin_dir
        self.styles_dir = self.caller.styles_dir
        
        self.modelZonage = QtSql.QSqlQueryModel()        
        self.ui.comboBox_zonage1.setModel(self.modelZonage)
        self.ui.comboBox_zonage2.setModel(self.modelZonage)
        
        self.modelMatrice1 = QtSql.QSqlQueryModel()
        self.ui.comboBox_matrice1.setModel(self.modelMatrice1)
        
        self.modelMatrice2 = QtSql.QSqlQueryModel()
        self.ui.comboBox_matrice2.setModel(self.modelMatrice2)        
        
        self.modelZonageCible = QtSql.QSqlQueryModel()        
        self.ui.comboBox_zonage_cible.setModel(self.modelZonageCible)
        
        # Connexion des signaux et des slots
        self._connectSlots()
        
    def _connectSlots(self):
        self.ui.comboBox_zonage1.currentIndexChanged.connect(self._slot_comboBox_zonage1_current_index_changed)
        self.ui.comboBox_zonage2.currentIndexChanged.connect(self._slot_comboBox_zonage2_current_index_changed)
        self.ui.comboBox_matrice1.currentIndexChanged.connect(self.update_apply_button)
        self.ui.comboBox_matrice2.currentIndexChanged.connect(self.update_apply_button)
        self.ui.comboBox_zonage_cible.currentIndexChanged.connect(self.update_apply_button)
        self.ui.buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self._slot_apply)
        self.ui.buttonBox.button(QDialogButtonBox.Cancel).clicked.connect(self._slot_cancel)
    
    def _slot_comboBox_zonage1_current_index_changed(self, indexChosenLine):
        # Update comboBox_matrice
        if (self.ui.comboBox_zonage.currentIndex()>0):
            s="SELECT 'Aucune matrice'::character varying as nom, null as id \
               UNION \
               ( \
                    SELECT DISTINCT nom, id \
                    FROM "+self.caller.schema_travail+".matrice \
                    WHERE id_zonage = "+str(self.modelZonage.record(self.ui.comboBox_zonage1.currentIndex()).value("id"))+" \
               ) \
               ORDER BY 2 NULLS FIRST"
        else:
            s="SELECT 'Aucune matrice'::character varying as nom, null as id"
            
        self.modelMatrice1.setQuery(s, self.caller.db)
        self.ui.comboBox_matrice1.setCurrentIndex(0)
        
        self.update_zonage_cible()
    
    def _slot_comboBox_zonage2_current_index_changed(self, indexChosenLine):
        # Update comboBox_matrice
        if (self.ui.comboBox_zonage.currentIndex()>0):
            s="SELECT 'Aucune matrice'::character varying as nom, null as id \
               UNION \
               ( \
                    SELECT DISTINCT matrice.nom, matrice.id \
                    FROM "+self.caller.schema_travail+".matrice \
                    WHERE id_zonage = "+str(self.modelZonage.record(self.ui.comboBox_zonage2.currentIndex()).value("id"))+" \
               ) \
               ORDER BY 2 NULLS FIRST"
        else:
            s="SELECT 'Aucune matrice'::character varying as nom, null as id"
            
        self.modelMatrice2.setQuery(s, self.caller.db)
        self.ui.comboBox_matrice2.setCurrentIndex(0)
        
        self.update_zonage_cible()
    
    def update_zonage_cible(self):
        # Update comboBox_zonage_cible
        if ((self.ui.comboBox_zonage1.currentIndex()>0) and (self.ui.comboBox_zonage2.currentIndex()>0)):
            s="SELECT 'Aucun zonage'::character varying as nom, null as id \
               UNION \
               ( \
                    SELECT DISTINCT macrozonage.nom, macrozonage.id \
                    FROM "+self.caller.schema_travail+".corresp_zones JOIN "+self.caller.schema_travail+".zone macrozone ON (corresp_zones.id_macrozone = macrozone.id) \
                                                                      JOIN "+self.caller.schema_travail+".zone zone ON (corresp_zones.id_zone = zone.id) \
                                                                      JOIN "+self.caller.schema_travail+".zonage macrozonage ON (macrozone.id_zonage = macrozonage.id) \
                    WHERE zone.id_zonage = "+str(self.modelZonage.record(self.ui.comboBox_zonage1.currentIndex()).value("id"))+" \
               ) \
               UNION \
               ( \
                    SELECT DISTINCT macrozonage.nom, macrozonage.id \
                    FROM "+self.caller.schema_travail+".corresp_zones JOIN "+self.caller.schema_travail+".zone macrozone ON (corresp_zones.id_macrozone = macrozone.id) \
                                                                      JOIN "+self.caller.schema_travail+".zone zone ON (corresp_zones.id_zone = zone.id) \
                                                                      JOIN "+self.caller.schema_travail+".zonage macrozonage ON (macrozone.id_zonage = macrozonage.id) \
                    WHERE zone.id_zonage = "+str(self.modelZonage.record(self.ui.comboBox_zonage2.currentIndex()).value("id"))+" \
               ) \
               UNION \
               ( \
                    SELECT zonage.nom, zonage.id \
                    FROM "+self.caller.schema_travail+".zonage \
                    WHERE zonage.id = "+str(self.modelZonage.record(self.ui.comboBox_zonage1.currentIndex()).value("id"))+" AND zonage.id = "+str(self.modelZonage.record(self.ui.comboBox_zonage2.currentIndex()).value("id"))+" \
               ) \
               ORDER BY 2 NULLS FIRST"
        else:
            s="SELECT 'Aucun zonage'::character varying as nom, null as id"
        
        self.modelMacrozonage.setQuery(s, self.caller.db)
        self.ui.comboBox_macrozonage1.setCurrentIndex(0)
    
    def update_apply_button(self):
        if (self.ui.comboBox_zonage_cible.currentIndex()>0) and (self.ui.comboBox_matrice1.currentIndex()>0) and (self.ui.comboBox_matrice2.currentIndex()>0):
            self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(True)
        else:
            self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(False)
    
    def _slot_apply(self):
        pass

    def _slot_cancel(self):
        self.hide()

