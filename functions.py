#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import sys
import os
import string
import subprocess

from PyQt5 import QtSql

import qgis
from qgis.core import *

def transform_external_cmd( cmd ):
    line_cmd = (cmd[0] + ' "' +'" "'.join(cmd[1:]) + '"').replace( '\\', '/' )#.encode('latin1')
    return line_cmd

def execute_external_cmd( cmd ):
    line_cmd = transform_external_cmd( cmd ) 
    r = subprocess.run( line_cmd , capture_output=True )
    return r.returncode

def pg_array2py_list( pg_array ):
    if (str(pg_array) == "NULL"):
        return []
    else:
        return list(map(int, pg_array[1:len(pg_array)-1].split(",")))

def pg_array2py_str_list( pg_array ):
    if (str(pg_array) == "NULL"):
        return []
    else:
        return list(map(str, pg_array[1:len(pg_array)-1].split(",")))

def add_layer(uri, iface, layer_name, schema_name, table_name, geom_column, subset_string, id_column, parent_node, position, expanded, visible):
    uri.setDataSource(schema_name, table_name, geom_column, subset_string, id_column)
    layer = QgsVectorLayer(uri.uri(), layer_name, "postgres")
    if layer.isValid():
        QgsProject.instance().addMapLayer(layer, False)
        node_layer = QgsLayerTreeLayer(layer)
        node_layer.setExpanded(expanded)
        parent_node.insertChildNode(position, node_layer)
        node_layer.setItemVisibilityChecked(visible)
    else:
        iface.messageBar().pushMessage("Attention","La couche '"+layer_name+"' n'a pas pu être chargée",level=1)
    return layer
    
def add_query_results(db_string, iface, layer_name, query, geom_column, subset_string, id_column, parent_node, position, expanded):
    if (geom_column == None):
        uri = "%s key='%s' table=\"(%s)\" sql=" % (db_string,id_column,query)
    else:
        uri = "%s key='%s' table=\"(%s)\" (%s) sql=" % (db_string,id_column,query,geom_column)
    layer = QgsVectorLayer(uri, layer_name, "postgres")
    if layer.isValid():
        QgsProject.instance().addMapLayer(layer, False)
        node_layer = QgsLayerTreeLayer(layer)
        node_layer.setExpanded(expanded)
        parent_node.insertChildNode(position, node_layer)
        layer.setSubsetString(subset_string)
    else:
        iface.messageBar().pushMessage("Attention","La couche '"+layer_name+"' n'a pas pu être chargée",level=1)
    return layer
    
def field_to_value_relation(source_layer, source_field_name, target_layer, target_id_field, target_field_name):
    for source_field in source_layer.fields():
        for target_field in target_layer.fields():
            if ((source_field.name() == source_field_name) and (target_field.name() == target_field_name)):
                try:
                    config = {'AllowMulti': False,
                              'AllowNull': False,
                              'FilterExpression': '',
                              'Key': target_id_field,
                              'Layer': target_layer.id(),
                              'LayerName': target_layer.name(), 
                              'NofColumns': 1,
                              'OrderByValue': False,
                              'UseCompleter': False,
                              'Value': target_field.name()}
                    widget_setup = QgsEditorWidgetSetup('ValueRelation', config)
                    source_layer.setEditorWidgetSetup(source_layer.fields().indexFromName( source_field_name ), widget_setup)
                                        
                except:
                    pass
    else:
        return False
    return True   

def field_to_value_map(layer, field_name, map):
    for field in layer.fields():
        if (field.name() == field_name):
            try:
                widget_setup = QgsEditorWidgetSetup('ValueMap', map)
                layer.setEditorWidgetSetup(layer.fields.indexFromName( field_name ), widget_setup)
                
            except:
                pass
            
def add_relation(referenced_layer, referencing_layer, referenced_field, referencing_field, relation_id, relation_name):
    rel = QgsRelation()
    rel.setReferencedLayer(str(referenced_layer.id()))
    rel.setReferencingLayer(str(referencing_layer.id()))
    rel.addFieldPair(referencing_field,referenced_field)
    rel.setId(relation_id)
    rel.setName(relation_name)
    QgsProject.instance().relationManager().addRelation(rel)
    
    
    
    