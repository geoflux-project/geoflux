#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import sys
import os

PGRESTORE_EXE = (os.path.dirname(__file__)+"/exe/pg_restore.exe" if sys.platform.startswith('win') else "pg_restore")
PGDUMP_EXE = (os.path.dirname(__file__)+"/exe/pg_dump.exe" if sys.platform.startswith('win') else "pg_dump")
PSQL_EXE = (os.path.dirname(__file__)+"/exe/psql.exe" if sys.platform.startswith('win') else "psql") 
SHP2PGSQL_EXE = (os.path.dirname(__file__)+"/exe/shp2pgsql.exe" if sys.platform.startswith('win') else "shp2pgsql") 
SRID = 2154
REF_FILE = os.path.dirname(__file__)+"/ref.csv"
    