#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface
from processing.tools import postgis

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os
import processing

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from validate_dialog import Ui_Dialog

class validate_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface 
        self.plugin_dir = self.caller.plugin_dir
        self.styles_dir = self.caller.styles_dir
        
        # Connexion des signaux et des slots
        self._connectSlots()
        
    def _connectSlots(self):
        pass
    
    def update_apply_button(self):
        if (self.ui.comboBox_zonage_cible.currentIndex()>0) and (self.ui.comboBox_matrice1.currentIndex()>0) and (self.ui.comboBox_matrice2.currentIndex()>0):
            self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(True)
        else:
            self.ui.buttonBox.button(QDialogButtonBox.Apply).setEnabled(False)
    
    def _slot_apply(self):
        pass

    def _slot_cancel(self):
        self.hide()

