        #!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from create_zone_dialog import Ui_Dialog

class create_zone_dialog(QDialog): 
    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self) 
        self.caller = caller
        self.iface = self.caller.iface
        self.plugin_dir = self.caller.plugin_dir
        
        self.ui.comboBox_zonage.setModel(self.caller.modelZonage)
        self.ui.comboBox_existing_zone.setModel(self.caller.modelZone)
        
        # Connexion des signaux et des slots
        self._connectSlots()
    
    def _connectSlots(self):
        self.ui.pushButton_add_zone.clicked.connect(self._slot_pushButton_add_zone_clicked)
        self.ui.radioButton_fusion.toggled.connect(self._slot_radioButton_fusion_toggled)
        
    def _slot_radioButton_fusion_toggled(self):
        if (self.ui.radioButton_fusion.isChecked()):
            self.ui.comboBox_existing_zone.setEnabled(True)
            self.ui.lineEdit_code_nouvelle_zone.setEnabled(False)
            self.ui.lineEdit_nom_nouvelle_zone.setEnabled(False)
            self.ui.label1.setEnabled(False)
            self.ui.label2.setEnabled(False)
        else:
            self.ui.comboBox_existing_zone.setEnabled(False)
            self.ui.lineEdit_code_nouvelle_zone.setEnabled(True)
            self.ui.lineEdit_nom_nouvelle_zone.setEnabled(True)
            self.ui.label1.setEnabled(True)
            self.ui.label2.setEnabled(True)
    
    def _slot_pushButton_add_zone_clicked(self):
        if (self.iface.activeLayer() is not None):
            self.layer = self.iface.activeLayer()
            source=self.layer.source()	
            kvp = source.split(" ")
            for kv in kvp:
                if kv.startswith("table"):
                    table = kv.split("=")[1][1:-1] # nom sous Postgres de la table associée à la couche active
            nom_table=table.replace('"','') # Mise en forme du nom
            for kv in kvp:
                if kv.startswith("key"):
                    identifiant = kv.split("=")[1][1:-1]  # nom de la colonne d'identifiant dans la table associée à la couche active
            selection = self.layer.selectedFeatures()      
            valeurs_selectionnees=()
            for feat in selection :
                valeurs_selectionnees+=(feat[identifiant],)
            valeurs_selectionnees=str(valeurs_selectionnees).replace(',)',')')		            
            if len(selection)==0:
                self.iface.messageBar().pushMessage("Attention", "Sélection vide", level=1)
            elif (self.caller.dockwidget.ui.comboBox_zonage.currentIndex() == 0):
                self.iface.messageBar().pushMessage("Attention", "Aucun zonage de destination sélectionné", level=1)
            else:
                s=""
                q=QtSql.QSqlQuery(self.caller.db)
                    
                if (self.ui.radioButton_fusion.isChecked()==False): # Fusion de polygones issus de la même couche
                    if (len(self.ui.lineEdit_code_nouvelle_zone.text().strip())==0):
                        self.iface.messageBar().pushMessage("Attention","Définir le code de la zone à créer",level=1)
                    else:
                        s="INSERT INTO "+self.caller.matrix_schema+".zone(id_zonage, code_zone, nom, prec_geo, geom) "+\
                          "VALUES("+str(self.caller.modelZonage.record(self.caller.dockwidget.ui.comboBox_zonage.currentIndex()).value("id"))+", '"+\
                          str(self.ui.lineEdit_code_nouvelle_zone.text())+"', '"+\
                          str(self.ui.lineEdit_nom_nouvelle_zone.text())+"', "+\
                          str(self.ui.comboBox_prec_geoloc.currentText()[0])+", "+\
                          "st_transform((SELECT st_multi(st_union(geom)) FROM "+nom_table+" WHERE "+str(identifiant)+" IN "+valeurs_selectionnees+" ), 2154))"
                else: # Fusion avec une zone existante du zonage
                    s="UPDATE "+self.caller.matrix_schema+".zone \
                       SET prec_geo = "+str(self.ui.comboBox_prec_geoloc.currentText()[0])+", \
                           geom = st_multi(st_union(zone.geom, st_transform((SELECT st_union(geom) FROM "+nom_table+" WHERE "+str(identifiant)+" IN "+valeurs_selectionnees+" ), 2154))) \
                       WHERE id = "+str(self.caller.modelZone.record(self.ui.comboBox_existing_zone.currentIndex()).value("id"))
                

                    
                if q.exec_(s)== True:
                    self.iface.messageBar().pushMessage("Information","La zone "+str(self.ui.lineEdit_code_nouvelle_zone.text())+" a été créée",level=3)
                    self.caller._slot_comboBox_zonage_current_index_changed(self.caller.dockwidget.ui.comboBox_zonage.currentIndex())
                    self.iface.mapCanvas().refreshAllLayers()
                    
                    # Unselect all (to avoid taking selection n the next created zone)
                    for a in self.iface.attributesToolBar().actions(): 
                        if a.objectName() == 'mActionDeselectAll':
                            a.trigger()
                            break                    
                else:
                    if (self.ui.radioButton_fusion.isChecked()==False):
                        self.iface.messageBar().pushMessage("Erreur","La création de zone a échoué, probablement en raison d'une absence d'unicité des codes de zones : "+s,level=2)           
                    else:
                        self.iface.messageBar().pushMessage("Erreur","La création de zone a échoué : "+s, level=2)
        else :
            self.iface.messageBar().pushMessage("Attention","Aucune couche de polygones n'est sélectionnée",level=1)
    
    
    