#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Définition des colonnes obligatoires dans les fichiers standard d'enquêtes OD

TEMP_TABLE_QUEST="n_questionnaire"
TEMP_TABLE_CODIF="n_codif"
TEMP_TABLE_CPT_AUTO="n_cpt_auto"
TEMP_TABLE_CPT_MANUEL="n_cpt_manuel"
TEMP_TABLE_POINT_CPT_AUTO="n_point_cpt_auto"
TEMP_TABLE_POINT_ENQ="n_point_enq"
TEMP_TABLE_ITW="n_interview"
SUFFIXE_IMPORT="_import"
SUFFIXE_EXPORT="_export"

MANDATORY_TABLES = [TEMP_TABLE_QUEST+SUFFIXE_IMPORT, TEMP_TABLE_CODIF+SUFFIXE_IMPORT, TEMP_TABLE_ITW+SUFFIXE_IMPORT, TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT, TEMP_TABLE_POINT_ENQ+SUFFIXE_IMPORT, TEMP_TABLE_POINT_CPT_AUTO+SUFFIXE_IMPORT, TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT]

CPT_MANUEL_MANDATORY_NUM_COL = ['num_point', 'vl_fr', 'vl_et', 'cc_fr', 'cc_et', '2rm_fr', '2rm_et', 'vul_fr', 'vul_et', 'vl_hors', 'bus_cars', 'tracteurs']
CPT_MANUEL_MANDATORY_CHAR_COL = ['code_poste', 'per_enq']
CPT_MANUEL_MANDATORY_COL = CPT_MANUEL_MANDATORY_NUM_COL + CPT_MANUEL_MANDATORY_CHAR_COL
CPT_MANUEL_MANDATORY_COL_OPT1 = ['pl_fr_sans_tmd', 'pl_et_sans_tmd', 'pl_fr_tmd', 'pl_et_tmd']
CPT_MANUEL_MANDATORY_COL_OPT2 = ['pl_fr_2_3_ess', 'pl_et_2_3_ess', 'pl_fr_4_ess_plus', 'pl_et_4_ess_plus']

CPT_AUTO_MANDATORY_COL = ['id_point', 'jour', 'per', 'vl', 'pl']
POINT_CPT_AUTO_MANDATORY_COL = ['geom', 'materiel', 'route', 'gest', 'angle', 'pr', 'abs', 'lib_sens', 'date_debut', 'date_fin', 'def_pl', 'comment', 'prest']
POINT_ENQ_MANDATORY_COL = ['geom', 'code_poste', 'num_point', 'angle', 'route', 'pr', 'abs', 'lib_sens', 'prec_loc', 'date_enq', 'nb_enq', 'nb_pers', 'enq_vl', 'enq_pl', 'support', 'protocole', 'comment', 'millesime', 'point_cpt', 'gest', 'command', 'amo', 'prest']

QUEST_MANDATORY_COL = ['code_poste', 'num_point', 'ordre', 'champ', 'quest_vl', 'quest_pl', 'libelle']
CODIF_MANDATORY_COL = ['champ', 'controle', 'modalite', 'libelle']
ITW_MANDATORY_COL = ['id_itw','code_poste','num_point','per_enq','type_veh','immat_pays','pays_orig','zone_orig','commune_orig','pays_dest','zone_dest','commune_dest','code_controle_od', 'coef_joe', 'coef_pe']
ITW_OD_CONTROL_OPT_COL = ["pole_dest_ini", "pole_dest_prec", "pole_dest", "voie_dest_ini", "voie_dest_prec", "voie_dest", "num_voie_dest_prec", "adresse_dest", "port_embarq_dest_ini", "port_embarq_dest", \
                               "pole_orig_ini", "pole_orig_prec", "pole_orig", "voie_orig_ini", "voie_orig_prec", "voie_orig", "num_voie_orig_prec", "adresse_orig", "port_debarq_orig_ini", "port_debarq_orig", \
                               "motif_orig_ini", "motif_orig_prec", "motif_orig", "motif_dest_ini", "motif_dest_prec", "motif_dest" ]
ITW_GOODS_CONTROL_OPT_COL = ["plaques_orange", "mat_dangereuse_code_bas", "tonnage"]
                               
GEO_MANDATORY_COL = ["code_pays", "code_zone", "code_com", "code_voie", "code_adr", "code_pole", "code_port", "code_front", "type_lieu", "libelle", "geom_point", "geom_polygon"]

QUESTIONS_TO_DELETE = list( set(ITW_MANDATORY_COL) - set(['type_veh', 'per_enq']) ) + ['comment', 'red_per_hor', 'red_per_enq', 'red_type_veh1', 'red_type_veh2', 'red_type_veh3', 'coef1', 'coef2', 'coef3', 'coef4', 'red_valide', \
     'pays_dest_ini', 'zone_dest_ini', 'zone_dest_prec', 'port_embarq_dest_ini', 'commune_dest_ini', 'commune_dest_prec', 'voie_dest_ini', 'voie_dest_prec', 'voie_dest', 'num_voie_dest_prec', 'adresse_dest', 'pole_dest_ini', 'pole_dest_prec', 'pole_dest', 'type_lieu_dest', 'lieu_dest', 
     'pays_orig_ini', 'zone_orig_ini', 'zone_orig_prec', 'port_debarq_orig_ini', 'commune_orig_ini', 'commune_orig_prec', 'voie_orig_ini', 'voie_orig_prec', 'voie_orig', 'num_voie_orig_prec', 'adresse_orig', 'pole_orig_ini', 'pole_orig_prec', 'pole_orig', 'type_lieu_orig', 'lieu_orig', 
     'motif_dest_ini', 'motif_dest_prec', 'motif_orig_ini', 'motif_orig_prec', 'type_carrosserie_ini', 'type_carrosserie_prec', 'marchandises_prec', 'entree_france_prec', 'sortie_france_prec', \
     'pays_trav_ini', 'pays_trav', 'zone_trav_ini', 'zone_trav', 'commune_trav_ini', 'commune_trav_prec', 'commune_trav', 'type_lieu_trav', 'lieu_trav', 'activite_prec', \
     'pays_dom_ini', 'pays_dom', 'zone_dom_ini', 'zone_dom', 'commune_dom_ini', 'commune_dom_prec', 'commune_dom', 'type_lieu_dom', 'lieu_dom']
    
# Périodes d'agrégation des comptages
PER_AG = [15, 30, 60]
    



